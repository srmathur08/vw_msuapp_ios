//
//  AppDelegate.swift
//  MSU
//
//  Created by vectorform on 09/06/21.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import SwiftBackgroundLocation
import BackgroundTasks
import SwiftyJSON
import FirebaseMessaging
import Firebase
import FirebaseCrashlytics
import FirebaseCore
import UserNotifications


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    
    var window: UIWindow?
    var orientationLock = UIInterfaceOrientationMask.all
    
    var locationManager: CLLocationManager? = CLLocationManager()
    var myLocationManager:CLLocationManager? = CLLocationManager()
    var backgroundLocationManager = BackgroundLocationManager(regionConfig: RegionConfig(regionRadius: 10.0))
    
    var locationManagerTrack = TrackingHeadingLocationManager()
    
    var lat = ""
    var lng = ""
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return self.orientationLock
    }
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
//        FirebaseApp.configure()
//        Messaging.messaging().delegate = self
        AppUtility.lockOrientation(.portrait)
        if(isJailbroken()){
            let alertController = UIAlertController.init(title: "Error", message: "This is a jailborken device", preferredStyle: .alert)
            alertController.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
                (action) in
                UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
            }))
            DispatchQueue.main.async {
                self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
            }
            return false;
        }
        else{
            //                   let login = LoginViewController.instantiate(fromAppStoryboard: .Login)
            //            login.present(login, animated: true, completion: nil)
        }
        GMSPlacesClient.provideAPIKey(StringConstants.GoogleAPIKEY)
        GMSServices.provideAPIKey(StringConstants.GoogleAPIKEY)
        startTracking()
        initializeLocationManager(launchOptions: launchOptions);
        if #available(iOS 13.0, *) {
            registerBackgroundTaks()
        } else {
            // Fallback on earlier versions
    //          application.setMinimumBackgroundFetchInterval(60)
        }
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
                  
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
                UNUserNotificationCenter.current().requestAuthorization(
                   options: authOptions,
                   completionHandler: {_, _ in })
//                registerForPushNotifications()
              } else {
                  let settings: UIUserNotificationSettings =
                      UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
                  application.registerUserNotificationSettings(settings)
              }
        return true
    }
    
//    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
//            InstanceID.instanceID().instanceID { (result, error) in
//                if let error = error {
//                    print("Error fetching remote instance ID: \(error)")
//                } else if let result = result {
//                    FacadeLayer.sharedInstance.fcmToken = result.token
////                    DefaultsWrapper.fcmToken = result.token
//                    print("Remote instance ID token: \(result.token)")
//                }
//            }
//        }
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "MSU")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    // MARK: -  Jail Broken
    private func isJailbroken() -> Bool {
        if TARGET_IPHONE_SIMULATOR != 1 {
            // Check 1 : existence of files that are common for jailbroken devices
            if FileManager.default.fileExists(atPath: "/Applications/Cydia.app")
                || FileManager.default.fileExists(atPath: "/Library/MobileSubstrate/MobileSubstrate.dylib")
                || FileManager.default.fileExists(atPath: "/bin/bash")
                || FileManager.default.fileExists(atPath: "/usr/sbin/sshd")
                || FileManager.default.fileExists(atPath: "/etc/apt")
                || FileManager.default.fileExists(atPath: "/var/cache/apt")
                || FileManager.default.fileExists(atPath: "/var/lib/cydia")
                || FileManager.default.fileExists(atPath: "/var/log/syslog")
                || FileManager.default.fileExists(atPath: "/var/tmp/cydia.log")
                || FileManager.default.fileExists(atPath: "/bin/sh")
                || FileManager.default.fileExists(atPath: "/usr/sbin/sshd")
                || FileManager.default.fileExists(atPath: "/usr/libexec/ssh-keysign")
                || FileManager.default.fileExists(atPath: "/etc/ssh/sshd_config")
                || FileManager.default.fileExists(atPath: "/usr/sbin/sshd")
                || FileManager.default.fileExists(atPath: "/private/var/lib/apt/")
                || UIApplication.shared.canOpenURL(URL(string:"cydia://package/com.example.package")!) {
                return true
            }
            
            // Check 2 : Reading and writing in system directories (sandbox violation)
            let stringToWrite = "Jailbreak Test"
            do {
                try stringToWrite.write(toFile:"/private/JailbreakTest.txt", atomically:true, encoding:String.Encoding.utf8)
                //Device is jailbroken
                return true
            } catch {
                return false
            }
        }
        else{
            return false
        }
    }
//    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
//        // fetch data from internet now
//        let userId = DefaultsWrapper.loginUserId
//        if(userId != 0){
//            self.saveUserLocation(lat:self.lat, lng: self.lng)
//        }
//        completionHandler(.noData)
//    }
//    
//    func apiCall(fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void){
//
//      //Make call here
//        let userId = DefaultsWrapper.loginUserId
//        if(userId != 0){
//            self.saveUserLocation(lat:self.lat, lng: self.lng)
//        }
//        completionHandler(UIBackgroundFetchResult.noData)
//    }

    //MARK: Regiater BackGround Tasks
    @available(iOS 13.0, *)
    private func registerBackgroundTaks() {
        BGTaskScheduler.shared.register(forTaskWithIdentifier: "com.VWassist.locationupdate", using: nil) { task in
            //This task is cast with processing request (BGProcessingTask)
            self.handleAppRefreshTask(task: task as! BGProcessingTask)
        }
    }
    @available(iOS 13.0, *)
    func handleAppRefreshTask(task: BGProcessingTask) {
        //Todo Work
        /*
         //AppRefresh Process
         */
        
        let userId = DefaultsWrapper.loginUserId
        
        if(userId != 0){
            self.saveUserLocation(lat:self.lat, lng: self.lng)
        }
        task.expirationHandler = {
            //This Block call by System
            //Canle your all tak's & queues
        }
        
        task.setTaskCompleted(success: true)
    }
    //MARK:- Methods for grabbing location
    
    func initializeLocationManager(launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        if launchOptions?[UIApplication.LaunchOptionsKey.location] != nil {
            if myLocationManager == nil {
                myLocationManager = CLLocationManager()
                myLocationManager?.delegate = self
                myLocationManager?.distanceFilter = 10
                myLocationManager?.desiredAccuracy = kCLLocationAccuracyBest
                myLocationManager?.allowsBackgroundLocationUpdates = true
                myLocationManager?.startUpdatingLocation()
                myLocationManager?.startMonitoringSignificantLocationChanges()
            } else {
                myLocationManager = nil
                myLocationManager = CLLocationManager()
                myLocationManager?.delegate = self
                myLocationManager?.distanceFilter = 10
                myLocationManager?.desiredAccuracy = kCLLocationAccuracyBest
                myLocationManager?.allowsBackgroundLocationUpdates = true
                myLocationManager?.startUpdatingLocation()
                myLocationManager?.startMonitoringSignificantLocationChanges()
            }
        } else {
            myLocationManager?.delegate = self
            myLocationManager?.distanceFilter = 10
            myLocationManager?.desiredAccuracy = kCLLocationAccuracyBest
            myLocationManager?.allowsBackgroundLocationUpdates = true
            
            if myLocationManager?.authorizationStatus == .notDetermined {
                myLocationManager?.requestAlwaysAuthorization()
            }
            else if myLocationManager?.authorizationStatus == .denied {
//                goToSettings()
            }
            else if myLocationManager?.authorizationStatus == .authorizedWhenInUse {
                myLocationManager?.requestAlwaysAuthorization()
            }
            else if myLocationManager?.authorizationStatus == .authorizedAlways {
                myLocationManager?.startUpdatingLocation()
                myLocationManager?.startMonitoringSignificantLocationChanges()
            }
        }
    }
    func goToSettings(){
        let alert = UIAlertController(title: "Allow Always Location Access", message: "Needs access to your location. Please  turn on Location Services in your device settings.", preferredStyle: UIAlertController.Style.alert)
        
        // Button to Open Settings
        alert.addAction(UIAlertAction(title: "Settings", style: UIAlertAction.Style.default, handler: { action in
            let settingsUrl = URL(string: UIApplication.openSettingsURLString)
            if UIApplication.shared.canOpenURL(settingsUrl!) {
                UIApplication.shared.open(settingsUrl!, completionHandler: { (success) in
                    print("Settings opened: \(success)")
                })
            }
        }))
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        var rootVc =  UIApplication.shared.windows.first?.rootViewController
        while (rootVc!.presentedViewController != nil) {
            rootVc = rootVc!.presentedViewController
        }
        rootVc!.present(alert, animated: true, completion: nil)
    }
}


extension AppDelegate{
    func startTracking() {
        self.backgroundLocationManager.start() { [unowned self] result in
            if case let .Success(location) = result {
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.updateBackgroundLocation(location: location)
                }
            }
        }
        
        self.locationManagerTrack.manager(for: .always, completion: { result in
            if case let .Success(manager) = result {
                manager.startUpdatingLocation(isHeadingEnabled: true) { [weak self] result in
                    if case let .Success(locationHeading) = result, let location = locationHeading.location {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                        self?.updateLocation(location: location)
                        }
                    }
                }
            }
        })
        
    }
    private func updateBackgroundLocation(location: CLLocation) {
        let tempCoor = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        
        let templat = String(describing: tempCoor.latitude).removeOptionalFromString()
        let templng = String(describing: tempCoor.longitude).removeOptionalFromString()
        self.lat = templat
        self.lng = templng
        saveUserLocation(lat: templat, lng: templng)
        print("##### background update location")
        print(templat, templng)
    }
    private func updateLocation(location: CLLocation) {
        
        
        let tempCoor = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        
        let templat = String(describing: tempCoor.latitude).removeOptionalFromString()
        let templng = String(describing: tempCoor.longitude).removeOptionalFromString()
        self.lat = templat
        self.lng = templng
        saveUserLocation(lat: templat, lng: templng)
        print("@@@@@@update location")
        print(templat, templng)
    }
    
    func saveUserLocation(lat: String, lng: String){
        let userId = DefaultsWrapper.loginUserId
        
        let postData :[String: AnyObject] = ["UserId": userId,
                                             "Lat": lat,
                                             "Lng": lng] as [String: AnyObject]
        print(JSON(postData))
        FacadeLayer.sharedInstance.webserviceManager.genericAPICalls(requestParams: postData, apiPath: VFApiCalls.updateLocation) {  (status, response) in
//            guard let strongSelf = self else{ return }
            
            if(status){
                print(response)
            }
        }
        
    }
}
