/* 
Copyright (c) 2022 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct DashboardCountModel : Mappable {
	var service_Missed_Month : Int?
	var service_Assigned_Today : Int?
	var service_Missed_Today : Int?
	var service_Completed_Today : Int?
	var service_Assigned_Month : Int?
	var estimate_Amount_Today : Int?
	var estimate_Amount_Month : Int?
	var service_Completed_Month : Int?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		service_Missed_Month <- map["Service_Missed_Month"]
		service_Assigned_Today <- map["Service_Assigned_Today"]
		service_Missed_Today <- map["Service_Missed_Today"]
		service_Completed_Today <- map["Service_Completed_Today"]
		service_Assigned_Month <- map["Service_Assigned_Month"]
		estimate_Amount_Today <- map["Estimate_Amount_Today"]
		estimate_Amount_Month <- map["Estimate_Amount_Month"]
		service_Completed_Month <- map["Service_Completed_Month"]
	}

}
