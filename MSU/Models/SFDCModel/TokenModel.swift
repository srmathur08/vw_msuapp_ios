//
//  TokenModel.swift
//  MSU
//
//  Created by vectorform on 21/08/21.
//

import Foundation
import ObjectMapper

struct TokenModel : Mappable {
    var issued_at : String?
    var id : String?
    var instance_url : String?
    var access_token : String?
    var signature : String?
    var token_type : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        issued_at <- map["issued_at"]
        id <- map["id"]
        instance_url <- map["instance_url"]
        access_token <- map["access_token"]
        signature <- map["signature"]
        token_type <- map["token_type"]
    }

}
