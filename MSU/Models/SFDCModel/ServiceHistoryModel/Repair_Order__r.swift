//
//  LoginModel.swift
//  MSU
//
//  Created by vectorform on 21/08/21.
//


import Foundation
import ObjectMapper

struct Repair_Order__r : Mappable {
	var attributes : Attributes?
	var safety_Remarks__c : String?
	var city__c : String?
	var id : String?
	var address__c : String?
	var total_GST__c : String?
	var pincode__c : String?
	var state__c : String?
	var discount_Amount__c : String?
	var discount__c : String?
	var address2__c : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		attributes <- map["attributes"]
		safety_Remarks__c <- map["Safety_Remarks__c"]
		city__c <- map["City__c"]
		id <- map["Id"]
		address__c <- map["address__c"]
		total_GST__c <- map["Total_GST__c"]
		pincode__c <- map["Pincode__c"]
		state__c <- map["State__c"]
		discount_Amount__c <- map["Discount_Amount__c"]
		discount__c <- map["Discount__c"]
		address2__c <- map["address2__c"]
	}

}
