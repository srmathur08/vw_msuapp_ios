//
//  LoginModel.swift
//  MSU
//
//  Created by vectorform on 21/08/21.
//


import Foundation
import ObjectMapper

struct VehicleMasterData : Mappable {
	var vW_ModelDesc__c : String?
	var product__c : String?
	var product__r : Product__r?
	var id : String?
	var vW_ModelYear__c : String?
	var variant_name__c : String?
	var attributes : Attributes?
	var first_Delivery_Date__c : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		vW_ModelDesc__c <- map["VW_ModelDesc__c"]
		product__c <- map["Product__c"]
		product__r <- map["Product__r"]
		id <- map["Id"]
		vW_ModelYear__c <- map["VW_ModelYear__c"]
		variant_name__c <- map["Variant_name__c"]
		attributes <- map["attributes"]
		first_Delivery_Date__c <- map["First_Delivery_Date__c"]
	}

}
