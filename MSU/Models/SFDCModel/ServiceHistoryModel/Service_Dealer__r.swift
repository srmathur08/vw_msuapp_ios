//
//  LoginModel.swift
//  MSU
//
//  Created by vectorform on 21/08/21.
//

import Foundation
import ObjectMapper

struct Service_Dealer__r : Mappable {
	var id : String?
	var attributes : Attributes?
	var name : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		id <- map["Id"]
		attributes <- map["attributes"]
		name <- map["Name"]
	}

}
