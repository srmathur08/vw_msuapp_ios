//
//  LoginModel.swift
//  MSU
//
//  Created by vectorform on 21/08/21.
//

import Foundation
import ObjectMapper

struct Product__r : Mappable {
	var grade_Description__c : String?
	var id : String?
	var attributes : Attributes?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		grade_Description__c <- map["Grade_Description__c"]
		id <- map["Id"]
		attributes <- map["attributes"]
	}

}
