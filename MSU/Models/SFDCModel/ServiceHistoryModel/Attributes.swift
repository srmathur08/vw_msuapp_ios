//
//  LoginModel.swift
//  MSU
//
//  Created by vectorform on 21/08/21.
//

import Foundation
import ObjectMapper

struct Attributes : Mappable {
	var type : String?
	var url : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		type <- map["type"]
		url <- map["url"]
	}

}
