//
//  LoginModel.swift
//  MSU
//
//  Created by vectorform on 21/08/21.
//

import Foundation
import ObjectMapper

struct CustomerMasterData : Mappable {
	var vW_Active__c : Bool?
	var attributes : Attributes?
	var id : String?
	var vW_CustomerName__c : String?
	var vW_Primary_Email_ID__c : String?
	var vW_First_name__c : String?
	var vW_Last_Name__c : String?
	var vW_End_User_Mobile__c : String?
	var vW_Pin_code__c : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		vW_Active__c <- map["VW_Active__c"]
		attributes <- map["attributes"]
		id <- map["Id"]
		vW_CustomerName__c <- map["VW_CustomerName__c"]
		vW_Primary_Email_ID__c <- map["VW_Primary_Email_ID__c"]
		vW_First_name__c <- map["VW_First_name__c"]
		vW_Last_Name__c <- map["VW_Last_Name__c"]
		vW_End_User_Mobile__c <- map["VW_End_User_Mobile__c"]
		vW_Pin_code__c <- map["VW_Pin_code__c"]
	}

}
