//
//  LoginModel.swift
//  MSU
//
//  Created by vectorform on 21/08/21.
//

import Foundation
import ObjectMapper

struct Service_History_Data : Mappable {
	var delivery_Date_Nadcon__c : String?
	var selling_Dealer__r : Selling_Dealer__r?
	var vW_VIN__c : String?
	var service_Dealer__r : Service_Dealer__r?
	var vW_Brand__c : String?
	var repair_Order__r : Repair_Order__r?
	var amount__c : Int?
	var customer_Voice__c : String?
	var service_Dealer__c : String?
	var vW_RO_Number__c : String?
	var mileage_Out__c : Int?
	var repair_Order__c : String?
	var item_Amount__c : Int?
	var attributes : Attributes?
	var recordTypeId : String?
	var vW_Registration_No__c : String?
	var post_Service_feedback_for_VW_HQ__c : Bool?
	var selling_Dealer__c : String?
	var labor_Amount__c : Int?
	var labour_Description__c : String?
	var rO_Closed_Date__c : String?
	var customer_Email__c : String?
	var customer_Name__c : String?
	var customer_Mobile__c : String?
	var part_Description__c : String?
	var rO_Type__c : String?
	var selling_Dealer_Code__c : String?
	var id : String?
	var createdDate : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		delivery_Date_Nadcon__c <- map["Delivery_Date_Nadcon__c"]
		selling_Dealer__r <- map["Selling_Dealer__r"]
		vW_VIN__c <- map["VW_VIN__c"]
		service_Dealer__r <- map["Service_Dealer__r"]
		vW_Brand__c <- map["VW_Brand__c"]
		repair_Order__r <- map["Repair_Order__r"]
		amount__c <- map["Amount__c"]
		customer_Voice__c <- map["Customer_Voice__c"]
		service_Dealer__c <- map["Service_Dealer__c"]
		vW_RO_Number__c <- map["VW_RO_Number__c"]
		mileage_Out__c <- map["Mileage_Out__c"]
		repair_Order__c <- map["Repair_Order__c"]
		item_Amount__c <- map["Item_Amount__c"]
		attributes <- map["attributes"]
		recordTypeId <- map["RecordTypeId"]
		vW_Registration_No__c <- map["VW_Registration_No__c"]
		post_Service_feedback_for_VW_HQ__c <- map["Post_Service_feedback_for_VW_HQ__c"]
		selling_Dealer__c <- map["Selling_Dealer__c"]
		labor_Amount__c <- map["Labor_Amount__c"]
		labour_Description__c <- map["Labour_Description__c"]
		rO_Closed_Date__c <- map["RO_Closed_Date__c"]
		customer_Email__c <- map["Customer_Email__c"]
		customer_Name__c <- map["Customer_Name__c"]
		customer_Mobile__c <- map["Customer_Mobile__c"]
		part_Description__c <- map["Part_Description__c"]
		rO_Type__c <- map["RO_Type__c"]
		selling_Dealer_Code__c <- map["Selling_Dealer_Code__c"]
		id <- map["Id"]
		createdDate <- map["CreatedDate"]
	}

}
