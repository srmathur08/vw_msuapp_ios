//
//  LoginModel.swift
//  MSU
//
//  Created by vectorform on 21/08/21.
//


import Foundation
import ObjectMapper

struct ServiceHistoryModel : Mappable {
	var vehicleMasterData : VehicleMasterData?
	var message : String?
	var customerMasterData : CustomerMasterData?
	var overall_customer_satisfaction_score : String?
	var service_History_Data : [Service_History_Data]?
	var dealer_PSF_Question_rated_less_than_4_star : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		vehicleMasterData <- map["vehicleMasterData"]
		message <- map["Message"]
		customerMasterData <- map["customerMasterData"]
		overall_customer_satisfaction_score <- map["Overall_customer_satisfaction_score"]
		service_History_Data <- map["Service_History_Data"]
		dealer_PSF_Question_rated_less_than_4_star <- map["Dealer_PSF_Question_rated_less_than_4_star"]
	}

}
