//
//  LoginModel.swift
//  MSU
//
//  Created by vectorform on 30/07/21.
//

import Foundation
import ObjectMapper

struct LoginModel : Mappable {
    var result : String?
    var role : String?
    var userId : Int?
    var dealership : String?
    var dealershipId : Int?
    var email : String?
    var roleId : Int?
    var fullname : String?
    var fcmToken: String?
    var profilePicUrl: String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        result <- map["Result"]
        role <- map["Role"]
        userId <- map["UserId"]
        dealership <- map["Dealership"]
        dealershipId <- map["DealershipId"]
        email <- map["Email"]
        roleId <- map["RoleId"]
        fullname <- map["Fullname"]
        fcmToken <- map["FcmToken"]
        profilePicUrl <- map["ProfilePicUrl"]
    }

}
