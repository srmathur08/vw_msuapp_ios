//
//  VehicleHealthCheckUpIdModel.swift
//  MSU
//
//  Created by vectorform on 24/08/21.
//

import Foundation
import ObjectMapper

struct VehicleHealthCheckUpIdModel : Mappable {
    var result : String?
    var vehicleCheckupId : Int?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        result <- map["Result"]
        vehicleCheckupId <- map["VehicleCheckupId"]
    }

}
