//
//  ImageMarkerModel.swift
//  MSU
//
//  Created by vectorform on 18/08/21.
//

import Foundation
import UIKit

struct ImageMarkerModel: Codable {
    var x: CGFloat = 0.0
    var y: CGFloat = 0.0
    var mark: String = ""
    
    init?(x: CGFloat, y: CGFloat, mark: String) {
        self.x = x
        self.y = y
        self.mark = mark
    }
}
