//
//  VehicleReportModel.swift
//  MSU
//
//  Created by vectorform on 12/08/21.
//

import Foundation
import ObjectMapper

struct VehicleReportModel  {
    
    var sectionName: String?
    var icon: String?
    var score: String?
    var scoreColor: String?
    
    
    init?(name: String, icon: String, score: String, scoreColor: String) {
        self.sectionName = name
        self.icon = icon
        self.score = score
        self.scoreColor = scoreColor
    }

}

