//
//  Images360Model.swift
//  MSU
//
//  Created by vectorform on 18/08/21.
//

import Foundation


struct Images360Model {
    var imagePath: String = ""
    var angle: String = ""
    var markers : [ImageMarkerModel]?
    
    init?(imagePath: String, angle: String, markers: [ImageMarkerModel]) {
        self.imagePath = imagePath
        self.angle = angle
        self.markers = markers
    }
}
