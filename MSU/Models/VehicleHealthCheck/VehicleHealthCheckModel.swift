

import Foundation
import ObjectMapper


struct VehicleHealthCheckModel : Mappable {
	var index : Int?
	var sectionName : String?
	var subSection : [SubSection]?
    var icon: String?
    var score: String?
    var scoreColor: String?
    var images360Array: [Images360Model] = [Images360Model]()
    var serviceAdvisorSign: String = ""
    var customerSign: String = ""
    var cleaningRemark: String = ""
    var damageRemark: String = ""
    var otherNotes: String = ""
    
	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		index <- map["Index"]
		sectionName <- map["SectionName"]
		subSection <- map["SubSection"]
        icon <- map["icon"]
        score <- map["score"]
        scoreColor <- map["scoreColor"]
        images360Array <- map["images360Array"]
        serviceAdvisorSign <- map["serviceAdvisorSign"]
        customerSign <- map["customerSign"]
	}

}
