/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct ServiceRequests : Mappable {
	
	var createdDate : String?
	var lng : String?
	var regNum : String?
	var dealerships : String?
	var variantName : String?
	var invoiceAmount : String?
	var groupDealerId : String?
	var vehicleModelId : Int?
	var vehicleModel : String?
	var serviceDate : String?
	var srnumber : String?
	var customerName : String?
	var vehicleCheckup : String?
	var srid : Int?
	var vehicleImage : String?
	var vehicleCheckupSecData : String?
	var errorMessage : String?
	var odometerReading : String?
	var isStarted : Bool?
	var serviceDateTime : String?
	var isDeleted : Bool?
	var serviceType : String?
	var vinnumber : String?
	var dealershipId : Int?
	var duration : String?
	var isCompleted : Bool?
	var lat : String?
	var vehicleModelData : String?
	var models : String?
	var groupDealers : String?
    var isVCCompleted: Bool = false
    var type: String?
    var serviceOther: String?
    
    var isOilChange : Bool?
    var isAirFilter : Bool?
    var isBrakes : Bool?
    var isBrakesDisk : Bool?
    var isSuspensionStrut : Bool?
    var isShockAbsorber : Bool?
    var isControlArm : Bool?
    var isBatteryCheck : Bool?
    var isBulbReplace : Bool?
    var isHornReplace : Bool?
    var isTyreCheck : Bool?
    var isTyreReplace : Bool?
    
    var revenue : String?
    var camps : String?
    var sFDCOdometerReading: Int?
    var mobileNumber: String?
	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		
		createdDate <- map["CreatedDate"]
		lng <- map["Lng"]
		regNum <- map["RegNum"]
		dealerships <- map["Dealerships"]
		variantName <- map["VariantName"]
		invoiceAmount <- map["InvoiceAmount"]
		groupDealerId <- map["GroupDealerId"]
		vehicleModelId <- map["VehicleModelId"]
		vehicleModel <- map["VehicleModel"]
		serviceDate <- map["ServiceDate"]
		srnumber <- map["Srnumber"]
		customerName <- map["CustomerName"]
		vehicleCheckup <- map["VehicleCheckup"]
		srid <- map["Srid"]
		vehicleImage <- map["VehicleImage"]
		vehicleCheckupSecData <- map["VehicleCheckupSecData"]
		errorMessage <- map["ErrorMessage"]
		odometerReading <- map["OdometerReading"]
		isStarted <- map["IsStarted"]
		serviceDateTime <- map["ServiceDateTime"]
		isDeleted <- map["IsDeleted"]
		serviceType <- map["ServiceType"]
		vinnumber <- map["Vinnumber"]
		dealershipId <- map["DealershipId"]
		duration <- map["Duration"]
		isCompleted <- map["IsCompleted"]
		lat <- map["Lat"]
		vehicleModelData <- map["VehicleModelData"]
		models <- map["Models"]
		groupDealers <- map["GroupDealers"]
        isVCCompleted <- map["IsVCCompleted"]
        type <- map["Type"]
        serviceOther <- map["ServiceOther"]
        revenue <- map["Revenue"]
        camps <- map["Camps"]
        
        isOilChange <- map["IsOilChange"]
        isAirFilter <- map["IsAirFilter"]
        isBrakes <- map["IsBrakes"]
        isBrakesDisk <- map["IsBrakesDisk"]
        isSuspensionStrut <- map["IsSuspensionStrut"]
        isShockAbsorber <- map["IsShockAbsorber"]
        isControlArm <- map["IsControlArm"]
        isBatteryCheck <- map["IsBatteryCheck"]
        isBulbReplace <- map["IsBulbReplace"]
        isHornReplace <- map["IsHornReplace"]
        isTyreCheck <- map["IsTyreCheck"]
        isTyreReplace <- map["IsTyreReplace"]

        sFDCOdometerReading <- map["SFDCOdometerReading"]
        mobileNumber <- map["MobileNumber"]
	}

}

