//
//  ComplaintModel.swift
//  MSU
//
//  Created by vectorform on 28/02/22.
//

import Foundation
import ObjectMapper



struct ComplaintModel {
    
    var complaint: String?
    var isChecked: Bool?
    var isMandatory: Bool?

    init?(complaint: String, isChecked: Bool, isMandatory: Bool) {
        self.complaint = complaint
        self.isChecked = isChecked
        self.isMandatory = isMandatory
    }
}
