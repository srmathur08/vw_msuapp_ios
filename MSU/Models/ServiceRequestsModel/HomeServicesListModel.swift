//
//  HomeServicesListModel.swift
//  MSU
//
//  Created by vectorform on 16/06/21.
//

import Foundation
import ObjectMapper

struct HomeServicesModel :Mappable{
    var serviceRequests : [ServiceRequests]?
    var result : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        serviceRequests <- map["ServiceRequests"]
        result <- map["Result"]
    }

}
