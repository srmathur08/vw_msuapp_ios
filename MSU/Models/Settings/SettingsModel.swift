//
//  SettingsModel.swift
//  MSU
//
//  Created by vectorform on 10/03/22.
//

import Foundation
import ObjectMapper

struct SettingsModel : Mappable {
    var data : [SettingsData]?
    var result : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        data <- map["data"]
        result <- map["Result"]
    }

}
