//
//  ServiceDetailsModel.swift
//  MSU
//
//  Created by vectorform on 26/08/21.
//

import Foundation
import UIKit

struct ServiceDetailsModel: Codable {
    var imageName: String = ""
    var titleName: String = ""
    
    init?(imageName: String, titleName: String) {
        self.imageName = imageName
        self.titleName = titleName
    }
}
