//
//  ProfilePicModel.swift
//  MSU
//
//  Created by vectorform on 10/03/22.
//

import Foundation
import ObjectMapper

struct ProfilePicModel : Mappable {
    var result : String?
    var profilePicUrl : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        result <- map["Result"]
        profilePicUrl <- map["ProfilePicUrl"]
    }

}
