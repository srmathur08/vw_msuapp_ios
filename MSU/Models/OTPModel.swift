//
//  OTPModel.swift
//  MSU
//
//  Created by vectorform on 16/03/22.
//

import Foundation
import ObjectMapper

struct OTPModel : Mappable {
    var oTP : String?
    var result : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        oTP <- map["OTP"]
        result <- map["Result"]
    }

}
