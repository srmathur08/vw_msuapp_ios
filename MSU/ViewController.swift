//
//  ViewController.swift
//  MSU
//
//  Created by vectorform on 09/06/21.
//

import UIKit

class ViewController: UIViewController {

 
    @IBOutlet weak var mobileNumberInputView: InputTextView!
    @IBOutlet weak var passwordInputView: InputTextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        mobileNumberInputView.inputTextfield.addTarget(self, action: #selector(textfieldValueChanged), for: .editingChanged)
        
    }
    override func viewDidLayoutSubviews() {

//        self.view =  GradientView(direction: [.down, .right], colors: ColorConstants.Gradient.appGradientColor)
    }

    @objc func textfieldValueChanged(_ textField: UITextField){
        if(textField.text!.count <= 0){
            mobileNumberInputView.showErrorView(message: " ", errorMessage: "Please enter valid number")
        }
        else{
            mobileNumberInputView.hideErrorView()
            if textField.text?.count == 10 {
                mobileNumberInputView.showTickImage()
            } else if (textField.text?.count)! > 10 {
                var str = textField.text
                str?.removeLast()
                textField.text = str
                mobileNumberInputView.showTickImage()
            }
            else{
                mobileNumberInputView.hideTickImage()
            }
        }
    }
}

