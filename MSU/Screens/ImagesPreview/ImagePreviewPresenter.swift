//
//  ImagePreviewPresenter.swift
//  MSU
//
//  Created by vectorform on 27/08/21.
//

import Foundation

class ImagePreviewPresenter: BasePresenter, IImagePreviewPresenter{
 
    private weak var myView: IImagePreviewController? {
        return view as? IImagePreviewController
    }
    
    func closeButtonClicked() {
        myView?.dismiss(animated: true, completion: nil)
    }

}
