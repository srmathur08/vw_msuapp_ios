//
//  ImagePreviewContract.swift
//  MSU
//
//  Created by vectorform on 27/08/21.
//

import Foundation


protocol IImagePreviewController: IBaseViewController {
    
}

protocol IImagePreviewPresenter: IBasePresenter {
    
    func closeButtonClicked()
}
