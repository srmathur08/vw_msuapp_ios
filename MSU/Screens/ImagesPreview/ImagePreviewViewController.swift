//
//  ImagePreviewViewController.swift
//  MSU
//
//  Created by vectorform on 27/08/21.
//

import UIKit
import ImageSlideshow
import SwiftyJSON
import AVFoundation

class ImagePreviewViewController: BaseViewController, IImagePreviewController {

    
    @IBOutlet weak var imageViewer: ImageSlideshow!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var topStackView: UIStackView!
    
    var imagesArray = [String]()
    var reportImagesModel: [VehicleCheckupAssets] = [VehicleCheckupAssets]()
    var selectedIndex:Int = 0
    var kingFisherSourceArray  = [KingfisherSource]()
    var currentPage: Int = 0
    var isFromModels: Bool = false
    private let presenter: IImagePreviewPresenter = ImagePreviewPresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.connectToView(view: self)
        // Do any additional setup after loading the view.
        self.perform(#selector(setupTopStackView), with: self, afterDelay: 0.2)
        
        mapUIWithData()
    }
    
    @objc  func setupTopStackView(){
        if(isFromModels){
            for i in 0..<reportImagesModel.count {
                let view = UIView()
                topStackView.addArrangedSubview(view)
            }
        }
        else{
            for i in 0..<imagesArray.count {
                let view = UIView()
                topStackView.addArrangedSubview(view)
            }
        }
        
        settopindicatorPage(index: selectedIndex)
        self.currentPage = selectedIndex
    }
    func settopindicatorPage(index: Int) {
        for i in 0..<topStackView.subviews.count {
            let topview = topStackView.subviews[i]
            if i == index {
                topview.backgroundColor = ColorConstants.appSecondaryBlueColor
            } else {
                topview.backgroundColor = ColorConstants.underlineColor
            }
        }
    }
    
    func mapUIWithData(){
        if(isFromModels){
            let baseUrl = BaseRequest()
            let baseUrlString = baseUrl.baseUrlString
            for i in 0..<reportImagesModel.count {
                let imagePath = reportImagesModel[i].asset ?? ""
                let finalUrl = baseUrlString + imagePath
                let inputUrl = KingfisherSource(urlString:finalUrl)
                self.kingFisherSourceArray.append(inputUrl!)
            }
        }
        else{
            for i in 0..<imagesArray.count {
                let imagePath = imagesArray[i]
                let url = URL.init(fileURLWithPath: imagePath)
                let stringVal = String(describing: url).removeOptionalFromString()
                let inputUrl = KingfisherSource(urlString:stringVal)
                self.kingFisherSourceArray.append(inputUrl!)
            }
        }
        
        imageViewer.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
        imageViewer.contentScaleMode = UIViewContentMode.scaleAspectFill
        
        let pageControl = UIPageControl()
        imageViewer.activityIndicator = DefaultActivityIndicator()
        imageViewer.delegate = self
        imageViewer.circular = false
        imageViewer.currentPageChanged = { page in
            pageControl.currentPage = self.selectedIndex
        }
        imageViewer.setImageInputs(self.kingFisherSourceArray)
        self.imageSlideshow(imageViewer, didChangeCurrentPageTo: selectedIndex)
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(ImagePreviewViewController.didTap))
        imageViewer.addGestureRecognizer(recognizer)
    }
    
    @objc func didTap() {
        let fullScreenController = imageViewer.presentFullScreenController(from: self)
        // set the activity indicator for full screen controller (skipping the line will show no activity indicator)
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: UIActivityIndicatorView.Style.medium, color: nil)
    }
    
    @IBAction func closebuttonAction(_ sender: UIButton) {
        presenter.closeButtonClicked()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension ImagePreviewViewController: ImageSlideshowDelegate {
    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
        imageViewer.setCurrentPage(page, animated: false)
        if(isFromModels){
            if reportImagesModel.count != 0 {
                settopindicatorPage(index: page)
                self.currentPage = page
            }
        }
        else{
            if imagesArray.count != 0 {
                settopindicatorPage(index: page)
                self.currentPage = page
            }
        }
       
    }
    
}
