//
//  OTPViewController.swift
//  MSU
//
//  Created by vectorform on 15/03/22.
//

import UIKit
import SwiftyJSON
import ObjectMapper


protocol otpSubmitClickedDelegate:AnyObject{
    func submitClicked()
    
}

class OTPViewController: BottomPopupViewController {
    
    
    @IBOutlet weak var otpEnterInputTextFields: InputTextView!
    @IBOutlet weak var submitButton: MSUButton!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var resendBottomView: UIView!
    
    var delegate: otpSubmitClickedDelegate?
    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismiss: Bool?
    
    var receivedOTP: String?
    var userMobileNumber: String?
    
    private let presenter: IOTPPresenter = OTPPresenter()
    
    var timer = Timer()
    var totalSecond = 30
    var isResendClick: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter.connectToView(view: self)
        print(receivedOTP)
        otpEnterInputTextFields.inputTextfield.addTarget(self, action: #selector(otpTextfieldValueChanged), for: .editingChanged)
        
        otpEnterInputTextFields.updateWidthConstraint(widthVal: self.view.frame.size.width - 25)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        applyStylings()
    }
    // MARK: - Apply Stylings to Objects
    /// This function is used for applying stylings on UI
    ///
    private func applyStylings(){
        otpEnterInputTextFields.textInputView(hintText: "Enter OTP", isSecureTextEntry: false, returnKeyType: UIReturnKeyType.done, keyboardtype: UIKeyboardType.phonePad,textColor: ColorConstants.solidWhite, tintColor: ColorConstants.solidWhite, lineColor: ColorConstants.underlineColor)
        
        otpEnterInputTextFields.inputTextfield.frame = CGRect(x: otpEnterInputTextFields.frame.origin.x, y: otpEnterInputTextFields.frame.origin.y, width: otpEnterInputTextFields.frame.size.width, height: otpEnterInputTextFields.frame.size.height)
        
        timerLabel.applyLabelStyling(textColor: ColorConstants.solidWhite, isBold: false, size: 14.0, alignment: .center, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
//        resendButton.appButtonStyling(titleColor: ColorConstants.appSecondaryBlueColor, backgroundColor: ColorConstants.clear)
       
        resendButton.titleLabel?.font = Font.vwheadfont.with(size: CGFloat(16.0), weight: .vwheadbold)
        resendButton.isUserInteractionEnabled = false
        resendButton.alpha = 0.6
        resendBottomView.backgroundColor = ColorConstants.appSecondaryBlueColor
        
        submitButton.appButtonStyling(titleColor: ColorConstants.appButtonDisableColor, backgroundColor: ColorConstants.appButtonDisableColor)
        
        submitButton.titleLabel?.font = Font.vwheadfont.with(size: CGFloat(16.0), weight: .vwheadregular)
        submitButton.isEnabled = false
        
        startTimer()
        
    }

    
    @objc func otpTextfieldValueChanged(_ textField: UITextField){
        otpEnterInputTextFields.hideErrorView()
        if((textField.text?.count)! <= 0){
            otpEnterInputTextFields.showErrorView(message: "Enter OTP", errorMessage: ErrorStringConstants.otpError)
            submitButton.isEnabled = false
        }
       else if (textField.text?.count)! > 6 {
              var str = otpEnterInputTextFields.inputTextfield.text
                str?.removeLast()
                otpEnterInputTextFields.inputTextfield.text = str
               
            }
        else if(textField.text?.count)! == 6 {
            submitButton.isEnabled = true
            submitButton.appButtonStyling(titleColor: ColorConstants.solidWhite, backgroundColor: ColorConstants.appSecondaryBlueColor)
        }
        else if(textField.text?.count)! < 6 {
            submitButton.isEnabled = false
            submitButton.appButtonStyling(titleColor: ColorConstants.appButtonDisableColor, backgroundColor: ColorConstants.appButtonDisableColor)
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
 
    //MARK: - timer methods
    func startTimer() {
      timer.invalidate()
          timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
      }
    @objc func updateTime() {
        timerLabel.isHidden = false
        if totalSecond != 0 {
            totalSecond -= 1
        } else {
            endTimer()
            timerLabel.isHidden = true
            resendButton.isUserInteractionEnabled = true
            resendButton.alpha = 1.0
        }
        timerLabel.text = timeFormatted(totalSecond)
    }
    func endTimer() {
        timer.invalidate()
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        return String(format: "00:%02d", seconds)
     }
    
    @IBAction func submitButtonAction(_ sender: MSUButton) {
        if((otpEnterInputTextFields.inputTextfield.text == "" || otpEnterInputTextFields.inputTextfield.text?.count ?? 0 < 6) || (otpEnterInputTextFields.inputTextfield.text != receivedOTP)){
            
            otpEnterInputTextFields.showErrorView(message: "Enter OTP", errorMessage: ErrorStringConstants.otpError)
        }
        else{
            DefaultsWrapper.isLoggedIn = true
            delegate?.submitClicked()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func resendButtonAction(_ sender: UIButton) {
        resendButton.titleLabel?.font = Font.vwheadfont.with(size: CGFloat(16.0), weight: .vwheadbold)
        resendButton.isUserInteractionEnabled = false
        resendButton.alpha = 0.6
        resendBottomView.backgroundColor = ColorConstants.appSecondaryBlueColor
        totalSecond = 30
        startTimer()

        self.resendOTP(mobNo: userMobileNumber!)
    }
    
    func resendOTP(mobNo: String)
    {
        let postString: [String : String]  = ["MobileNo":mobNo]
        FacadeLayer.sharedInstance.webserviceManager.genericAPICalls(requestParams: postString as [String:AnyObject], apiPath: VFApiCalls.getOTP) { [weak self] (status, response) in
            guard let strongSelf = self else{
                return
            }
            if(status){
                let responseVal = JSON(response as Any)
                if let model = Mapper<OTPModel>().map(JSONObject:responseVal.dictionaryObject){
                    let otp = model.oTP ?? ""
                    strongSelf.receivedOTP = otp
                }
            }
            else{
                let errorVal = JSON(response as Any)
                let errorString = "\(errorVal)"
                print(errorString)
                var errormsg: String = ""
                if(errorString != ErrorStringConstants.noInternet){
                    errormsg = errorVal["Reason"].stringValue
                    if(errormsg == ""){
                        errormsg = ErrorStringConstants.defaultError
                    }
                }
                else{
                    errormsg = ErrorStringConstants.noInternet
                }
            }
        }
    }
    override var popupHeight: CGFloat { return height ?? CGFloat(300) }
    
    override var popupTopCornerRadius: CGFloat { return topCornerRadius ?? CGFloat(10) }
    
    override var popupPresentDuration: Double { return presentDuration ?? 1.0 }
    
    override var popupDismissDuration: Double { return dismissDuration ?? 1.0 }
    
    override var popupShouldDismissInteractivelty: Bool { return false}
    
    override var popupDimmingViewAlpha: CGFloat { return BottomPopupConstants.kDimmingViewDefaultAlphaValue }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

