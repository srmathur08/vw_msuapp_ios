//
//  LoginRouter.swift
//  MSU
//
//  Created by vectorform on 14/06/21.
//

import Foundation
import UIKit

class LoginRouter: BaseNavigationController, ILoginRouter {

    

    init() {
        super.init(nibName: nil, bundle: nil)
        pushLoginController(animated: false)
    }
    // MARK: - Push Login View Controller
    func pushLoginController(animated: Bool) {
        let controller = LoginViewController.instantiate(fromAppStoryboard: .LoginStoryboard)
        controller.modalPresentationStyle = .fullScreen
        pushViewController(controller, animated: animated)
    }
    
    // MARK: - Present OTP View Controller
    func presentOTPViewController(delegate: otpSubmitClickedDelegate, animated: Bool, otp: String, mobNo: String)
    {
        let controller = OTPViewController.instantiate(fromAppStoryboard: .LoginStoryboard)
        controller.height = 550
        controller.receivedOTP = otp
        controller.userMobileNumber = mobNo
        controller.delegate = delegate
        present(controller, animated: true, completion: nil)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
