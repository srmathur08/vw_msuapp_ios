//
//  ILoginRouter.swift
//  MSU
//
//  Created by vectorform on 14/06/21.
//

import Foundation
import UIKit


protocol ILoginRouter {
  
    /// This function called to push login view controller
    ///
    /// - Parameter animated: true/false
    func pushLoginController(animated: Bool)
    
    // This function called to Present OTP View Controller
    ///
    /// - Parameter animated: true/false
    func presentOTPViewController(delegate: otpSubmitClickedDelegate, animated: Bool, otp: String, mobNo: String)
}
