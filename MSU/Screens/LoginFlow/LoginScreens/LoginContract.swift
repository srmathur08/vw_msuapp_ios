//
//  LoginContract.swift
//  MSU
//
//  Created by vectorform on 14/06/21.
//

import Foundation
import UIKit

protocol ILoginViewController: IBaseViewController {

    /// This function called to show OTP view
    ///
    func showOTPView(receivedOTP: String)
}


protocol ILoginPresenter: IBasePresenter {
    /// This function called on login button click
    ///
    ///      Warning: mobile number and password must not be empty
    /// - Parameter mobileNumber:must be a string
    /// - Parameter password: must be a string
    func loginButtonPressed(mobileNumber: String, password: String, buttonTitle: String)
    
    /// This function called for getting settings data
    ///
    func getSettingsData()
    
    /// This function called to navigate to home
    ///
    func navigateToHome()
    
    /// This fumction is used to get the OTP
    ///
    func getOTP(mobNum: String)
    
    // This fumction is called to Present OTP View
    ///
    func presentOTPView(receivedOTPVal: String, mobileNum: String, delegateVal: otpSubmitClickedDelegate)
}
