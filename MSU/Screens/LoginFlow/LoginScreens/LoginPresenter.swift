//
//  LoginPresenter.swift
//  MSU
//
//  Created by vectorform on 14/06/21.
//

import Foundation
import SwiftyJSON
import ObjectMapper

class LoginPresenter: BasePresenter, ILoginPresenter {



    var loginRouter: LoginRouter? {
        return navController as? LoginRouter
    }
    private weak var myView: ILoginViewController? {
        return self.view as? ILoginViewController
    }
    
    var savaLoginModel: LoginModel?
    var receivedNumber: String = ""
    // MARK: - Call Login API
    func loginButtonPressed(mobileNumber: String, password: String, buttonTitle: String) {
        self.myView?.showLoader()
            let postData: [String : String]  = ["PhoneNo":mobileNumber,
                                                "Password":password,
                                                "FcmToken":FacadeLayer.sharedInstance.fcmTokem
                                                ]
            FacadeLayer.sharedInstance.webserviceManager.genericAPICalls(requestParams: postData as [String : AnyObject], apiPath: VFApiCalls.login) { [weak self] (status, response) in
                guard let strongSelf = self else{ return }
                if(status){
                    print(response)
                     let responseVal = JSON(response as Any)
                    strongSelf.myView?.removeLoader()
                    if let userDetails = Mapper<LoginModel>().map(JSONObject:responseVal.dictionaryObject){
                        let role = userDetails.role ?? ""
                            if(role.contains(Roles.serviceTechnician) || role.contains(Roles.serviceAdvisor)){
                                DefaultsWrapper.loginUserId = userDetails.userId ?? 0
                                DefaultsWrapper.dealershipId = userDetails.dealershipId ?? 0
                                DefaultsWrapper.username = userDetails.fullname ?? ""
                                DefaultsWrapper.mobileNumber = mobileNumber
                                DefaultsWrapper.dealership = userDetails.dealership ?? ""
                                DefaultsWrapper.loggedInRole = userDetails.role ?? ""
                                DefaultsWrapper.loggedEmail = userDetails.email ?? ""
                                DefaultsWrapper.userProfilePic = userDetails.profilePicUrl ?? ""
                                FacadeLayer.sharedInstance.setLoginDetails(input: userDetails)
                            }
                            else{
                                strongSelf.myView?.displayDefaultAlertForError(errorTitle: ErrorStringConstants.errorTitle, errorDescription: ErrorStringConstants.authorizeError)
                            }
                        }
                    strongSelf.receivedNumber = mobileNumber
                    strongSelf.getOTP(mobNum: mobileNumber)
                }
                else{
                    strongSelf.myView?.removeLoader()
                    let errorVal = JSON(response as Any)
                    let errorString = "\(errorVal)"
                    print(errorString)
                    var errormsg: String = ""
                    if(errorString != ErrorStringConstants.noInternet){
                            errormsg = errorVal["Reason"].stringValue
                        if(errormsg == ""){
                            errormsg = ErrorStringConstants.defaultError
                        }
                    }
                    else{
                        errormsg = ErrorStringConstants.noInternet
                    }
                    strongSelf.myView?.displayDefaultAlertForError(errorTitle: ErrorStringConstants.errorTitle, errorDescription: errormsg)
                }
            }
    }
    
    // MARK: - Navigate to home
    func navigateToHome()
    {
        self.sectionRouter?.navigateToHomeFlow(animate: true)
    }
    
    func presentOTPView(receivedOTPVal: String, mobileNum: String, delegateVal: otpSubmitClickedDelegate) {
        loginRouter?.presentOTPViewController(delegate: delegateVal, animated: true, otp: receivedOTPVal, mobNo: mobileNum)
//        loginRouter?.presentOTPViewController(animated: true, otp: receivedOTPVal, mobNo: mobileNum)
    }
    func getOTP(mobNum: String){
        let postString: [String : String]  = ["MobileNo":mobNum]
        FacadeLayer.sharedInstance.webserviceManager.genericAPICalls(requestParams: postString as [String:AnyObject], apiPath: VFApiCalls.getOTP) { [weak self] (status, response) in
            guard let strongSelf = self else{
                return
            }
            if(status){
                strongSelf.myView?.removeLoader()
                let responseVal = JSON(response as Any)
                if let model = Mapper<OTPModel>().map(JSONObject:responseVal.dictionaryObject){
                    let otp = model.oTP ?? ""
                    strongSelf.presentOTPView(receivedOTPVal: otp, mobileNum: mobNum, delegateVal: self?.myView as! otpSubmitClickedDelegate)
                }
            }
            else{
                strongSelf.myView?.removeLoader()
                let errorVal = JSON(response as Any)
                let errorString = "\(errorVal)"
                print(errorString)
                var errormsg: String = ""
                if(errorString != ErrorStringConstants.noInternet){
                    errormsg = errorVal["Reason"].stringValue
                    if(errormsg == ""){
                        errormsg = ErrorStringConstants.defaultError
                    }
                }
                else{
                    errormsg = ErrorStringConstants.noInternet
                }
                strongSelf.myView?.displayDefaultAlertForError(errorTitle: ErrorStringConstants.errorTitle, errorDescription: errormsg)
            }
        }
    }
    // MARK: - Get Settings Data
    func getSettingsData(){
        let postString: [String : String]  = [:]
        FacadeLayer.sharedInstance.webserviceManager.genericAPICallsWithoutParam(requestParams: postString as [String : AnyObject], apiPath: VFApiCalls.getSettings) { [weak self] (status, response) in
            guard let strongSelf = self else{
                return
            }
            if(status){
                let responseVal = JSON(response as Any)
                if let settingsModel = Mapper<SettingsModel>().map(JSONObject:responseVal.dictionaryObject){
                    if let dataModel = settingsModel.data{
                        strongSelf.serializeSettingsData(dataObj: dataModel)
                    }
                }
            }
            else{
                strongSelf.myView?.removeLoader()
                let errorVal = JSON(response as Any)
                let errorString = "\(errorVal)"
                print(errorString)
                var errormsg: String = ""
                if(errorString != ErrorStringConstants.noInternet){
                    errormsg = errorVal["Reason"].stringValue
                    if(errormsg == ""){
                        errormsg = ErrorStringConstants.defaultError
                    }
                }
                else{
                    errormsg = ErrorStringConstants.noInternet
                }
                strongSelf.myView?.displayDefaultAlertForError(errorTitle: ErrorStringConstants.errorTitle, errorDescription: errormsg)
            }
        }
    }
    
    //MARK: - Serialise Settings
    func serializeSettingsData(dataObj: [SettingsData]){
        var versionVal = ""
        var storeLinkVal = ""
        
        for (_, elementObj) in dataObj.enumerated(){
           
            let name = elementObj.KeyVal
            switch name {
            case StringConstants.iOSVersion:
                versionVal = elementObj.valueVal ?? ""
            case StringConstants.iTunesLink:
                storeLinkVal = elementObj.valueVal ?? ""
            default:
                print("")
            }
        }
        self.checkForVersionUpdate(version: versionVal, storeUrl: storeLinkVal)
    }
    //MARK: - Check Version Update
    func checkForVersionUpdate(version: String, storeUrl: String){
        let appVersionDict = Bundle.main.infoDictionary
     
        if let dict = appVersionDict{
            let appVersion = dict["CFBundleShortVersionString"] as! String
            if appVersion.compare(version, options: .numeric) == .orderedAscending {

                let alertController = UIAlertController.init(title: ErrorStringConstants.defaultTitle, message: ErrorStringConstants.versionUpdate, preferredStyle: .alert)
                alertController.addAction(UIAlertAction.init(title: "Update", style: .default, handler: { (action) in
                    UIApplication.shared.open(URL.init(string: storeUrl)!, options:[:], completionHandler: nil)
                }))
                self.myView?.present(alertController, animated: true, completion: nil)
            }
        }
    }
}
