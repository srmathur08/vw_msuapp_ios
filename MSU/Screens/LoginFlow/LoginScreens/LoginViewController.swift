//
//  LoginViewController.swift
//  MSU
//
//  Created by vectorform on 10/06/21.
//

import UIKit

class LoginViewController: BaseViewController, ILoginViewController, otpSubmitClickedDelegate {
 
    
    // MARK: - View Contoller Outlets
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var mobileNumberInputView: InputTextView!
    @IBOutlet weak var passwordInputView: InputTextView!
    @IBOutlet weak var otpInputTextView: InputTextView!
    @IBOutlet var welcomeLabel: UILabel!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var loginButton: MSUButton!
    @IBOutlet weak var passwordButton: UIButton!
    
   
    private let presenter: ILoginPresenter = LoginPresenter()
    var receivedOTPVal : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.connectToView(view: self)
        // Do any additional setup after loading the view.
        mobileNumberInputView.inputTextfield.addTarget(self, action: #selector(mobileTextfieldValueChanged), for: .editingChanged)
        passwordInputView.inputTextfield.addTarget(self, action: #selector(passwordTextfieldValueChanged), for: .editingChanged)
        
        presenter.getSettingsData()
        #if DEBUG
//        mobileNumberInputView.inputTextfield.text = "9550527102"
//        passwordInputView.inputTextfield.text = "Ajay@123"
        #endif
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(DefaultsWrapper.isLoggedIn){
            presenter.navigateToHome()
        }
        applyStylings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    override func viewDidLayoutSubviews() {
        
       
        //        self.view =  GradientView(direction: [.down, .right], colors: ColorConstants.Gradient.appGradientColor)
    }
    // MARK: - Apply Stylings to Objects
    /// This function is used for applying stylings on UI
    ///
    private func applyStylings(){
        
        welcomeLabel.applyLabelStyling(textColor: ColorConstants.solidWhite, isBold: true, size: 24.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        
        loginLabel.applyLabelStyling(textColor: ColorConstants.solidWhite, isBold: false, size: 30.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        loginButton.isEnabled = false
        
        loginButton.appButtonStyling(titleColor: ColorConstants.appButtonDisableColor, backgroundColor: ColorConstants.appButtonDisableColor)
        
        loginButton.titleLabel?.font = Font.vwheadfont.with(size: CGFloat(16.0), weight: .vwheadregular)
        
        mobileNumberInputView.textInputView( hintText: "Mobile Number", isSecureTextEntry: false, returnKeyType: UIReturnKeyType.next, keyboardtype: UIKeyboardType.phonePad,textColor: ColorConstants.solidWhite, tintColor: ColorConstants.solidWhite, lineColor: ColorConstants.underlineColor)
        mobileNumberInputView.updateWidthConstraint(widthVal: self.view.frame.size.width - 25)
        
        passwordInputView.textInputView( hintText: "Password", isSecureTextEntry: true, returnKeyType: UIReturnKeyType.done, keyboardtype: UIKeyboardType.default ,textColor: ColorConstants.solidWhite, tintColor: ColorConstants.solidWhite, lineColor: ColorConstants.underlineColor)
        passwordInputView.updateWidthConstraint(widthVal: self.view.frame.size.width - 25)
        
        otpInputTextView.textInputView( hintText: "OTP", isSecureTextEntry: true, returnKeyType: UIReturnKeyType.done, keyboardtype: UIKeyboardType.default ,textColor: ColorConstants.solidWhite, tintColor: ColorConstants.solidWhite, lineColor: ColorConstants.underlineColor)
        otpInputTextView.updateWidthConstraint(widthVal: self.view.frame.size.width - 25)
    }
    
    // MARK: - Mobile number text change
    /// This function called on mobile text field text change
    ///
    /// - Parameter textField: mobile text field changes delegate method
    
    @objc func mobileTextfieldValueChanged(_ textField: UITextField){
        if(textField.text!.count <= 0){
            mobileNumberInputView.showErrorView(message: " ", errorMessage: ErrorStringConstants.mobileNumberError)
        }
        else{
            mobileNumberInputView.hideErrorView()
            if textField.text?.count == 10 {
                mobileNumberInputView.showTickImage()
            } else if (textField.text?.count)! > 10 {
                var str = textField.text
                str?.removeLast()
                textField.text = str
                mobileNumberInputView.showTickImage()
            }
            else{
                mobileNumberInputView.hideTickImage()
            }
        }
        enableLoginButton()
    }
    
    // MARK: - Password text change
    /// This function called on password text field text change
    ///
    /// - Parameter textField: password text field changes delegate method
    @objc func passwordTextfieldValueChanged(_ textField: UITextField){
        if(textField.text?.count == 0){
            passwordInputView.showErrorView(message: " ", errorMessage: ErrorStringConstants.passwordError)
        }
        else{
            passwordInputView.hideErrorView()
        }
        enableLoginButton()
    }
    // MARK: - Enable Login Button
    /// This function called  for enabling/disabling login button
    ///
    ///
    private  func enableLoginButton(){
        if((mobileNumberInputView.inputTextfield.text?.count != 0 && mobileNumberInputView.inputTextfield.text?.count == 10)  && passwordInputView.inputTextfield.text?.count != 0){
            loginButton.isEnabled = true
            loginButton.appButtonStyling(titleColor: .white, backgroundColor: ColorConstants.appSecondaryBlueColor)
        }
        else{
            loginButton.isEnabled = false
            loginButton.appButtonStyling(titleColor: ColorConstants.appButtonDisableColor, backgroundColor: ColorConstants.appButtonDisableColor)
        }
    }
    
    // MARK: - Show OTP View
    func showOTPView(receivedOTP: String)
    {
        self.otpInputTextView.isHidden = false
        self.loginButton.setTitle(StringConstants.login, for: .normal)

        receivedOTPVal = receivedOTP
        
    }
    
    func submitClicked() {
        presenter.navigateToHome()
    }
    
    // MARK: - User Interactions
    // MARK: - Login Button Clicked
    /// This function called on Login button action
    ///
    @IBAction func loginButtonAction(_ sender: UIButton) {
        let buttonTitleVal = sender.title(for: .normal) ?? ""
            presenter.loginButtonPressed(mobileNumber: mobileNumberInputView.inputTextfield.text ?? "", password: passwordInputView.inputTextfield.text ?? "", buttonTitle: buttonTitleVal)
    }
    
    /// This function called on password toggle button action
    ///
    @IBAction func passwordShowAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if(sender.isSelected){
            passwordInputView.inputTextfield.isSecureTextEntry = false
        }
        else{
            passwordInputView.inputTextfield.isSecureTextEntry = true
        }
    }
    
//    @IBAction func whatsappButton(_ sender: UIButton) {
//        if let urlString = "https://wa.me/\(+919666977753)/?text=Hi. ".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
//            let url = URL(string: urlString),
//            UIApplication.shared.canOpenURL(url) {
//                UIApplication.shared.open(url)
//        }
//    }
//
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
