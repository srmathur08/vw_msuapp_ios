//
//  ServiceHistoryDetailsViewController.swift
//  MSU
//
//  Created by vectorform on 22/07/21.
//

import UIKit

class ServiceHistoryDetailsViewController: BaseViewController, IServiceHistoryDetailsViewController {

    
    private let presenter: IServiceHistoryDetailsPresenter = ServiceHistoryDetailsPresenter()
    // MARK: - View Contoller Outlets
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var topCardView: CardView!
    @IBOutlet weak var ratingsLabel: UILabel!
    @IBOutlet weak var serviceStatusLabel: UILabel!
    @IBOutlet weak var serviceDateTimeLabel: UILabel!
    @IBOutlet weak var repairOrderTitleLabel: UILabel!
    @IBOutlet weak var repairOrderDataLabel: UILabel!
    @IBOutlet weak var repairOrderBottomView: UIView!
    @IBOutlet weak var roTypeTitleLabel: UILabel!
    @IBOutlet weak var roTypeDataLabel: UILabel!
    @IBOutlet weak var roServiceTypeTitleLabel: UILabel!
    @IBOutlet weak var roServiceTypeDataLabel: UILabel!
    @IBOutlet weak var roTypeBottomView: UIView!
    @IBOutlet weak var serviceOpenDateTitleLabel: UILabel!
    @IBOutlet weak var serviceOpenDateDataLabel: UILabel!
    @IBOutlet weak var serviceOpenTimeLabel: UILabel!
    @IBOutlet weak var serviceClosedDateTitleLabel: UILabel!
    @IBOutlet weak var serviceClosedDateDataLabel: UILabel!
    @IBOutlet weak var serviceClosedTimeLabel: UILabel!
    @IBOutlet weak var serviceBottomView: UIView!
    @IBOutlet weak var serviceEngineerTitleLabel: UILabel!
    @IBOutlet weak var serviceEngineerDataLabel: UILabel!
    @IBOutlet weak var serviceEnginnerBottomView: UIView!
    @IBOutlet weak var instrructionsContainerView: UIView!
    @IBOutlet weak var serviceInstructionsTitleLabel: UILabel!
    @IBOutlet weak var serviceInstructionsBottomView: UIView!
    @IBOutlet weak var advisorInstructionsContainerView: UIView!
    @IBOutlet weak var advisorInstructionsTitleLabel: UILabel!
    @IBOutlet weak var advisorInstructionsBottomView: UIView!
    @IBOutlet weak var invoiceAmountTitleLabel: UILabel!
    @IBOutlet weak var invoiceAmountDataLabel: UILabel!
    @IBOutlet weak var labourAmountTitleLabel: UILabel!
    @IBOutlet weak var labourAmountDataLabel: UILabel!
    @IBOutlet weak var partsAmountTItleLabel: UILabel!
    @IBOutlet weak var partsAmountDataLabel: UILabel!
    
    @IBOutlet weak var instructionsTextView: UITextView!
    @IBOutlet weak var instructionViewHeightConstraint: NSLayoutConstraint!
    var serviceHistory: Service_History_Data?
    var feedbackScore: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.connectToView(view: self)
        // Do any additional setup after loading the view.
        scrollViewHeightConstraint.constant = 900
        applyStylings()
    }
    
    // MARK: - View Contoller Outlets
    func applyStylings(){
        headerLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: true, size: 18.0, alignment: .center, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        topCardView.backgroundColor = ColorConstants.appGreyColor
     
        seperatorView.backgroundColor = ColorConstants.underlineColor
        
        serviceStatusLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: true, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        ratingsLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        serviceDateTimeLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        repairOrderTitleLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: true, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        repairOrderDataLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        repairOrderBottomView.backgroundColor = ColorConstants.underlineColor
        
        roTypeTitleLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: true, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        roTypeDataLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        roServiceTypeTitleLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: true, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        roServiceTypeDataLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        roServiceTypeTitleLabel.isHidden = true  // as updated by client removed service type fields
        roServiceTypeDataLabel.isHidden = true
        roTypeBottomView.backgroundColor = ColorConstants.underlineColor
        
        serviceOpenDateTitleLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: true, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        serviceOpenDateDataLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        serviceOpenTimeLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        serviceClosedDateTitleLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: true, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        serviceClosedDateDataLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        serviceClosedTimeLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        serviceBottomView.backgroundColor = ColorConstants.underlineColor
        
        serviceEngineerTitleLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: true, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        serviceEngineerDataLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        serviceEngineerDataLabel.isHidden = true
        serviceEnginnerBottomView.backgroundColor = ColorConstants.clear
        
        serviceInstructionsTitleLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: true, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        serviceInstructionsBottomView.backgroundColor = ColorConstants.underlineColor
        
        advisorInstructionsTitleLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: true, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        advisorInstructionsBottomView.backgroundColor = ColorConstants.underlineColor
        
        invoiceAmountTitleLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: true, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        invoiceAmountDataLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        labourAmountTitleLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: true, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        labourAmountDataLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        partsAmountTItleLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: true, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        partsAmountDataLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        instructionsTextView.textColor = ColorConstants.appLabelsTextColor
        instructionsTextView.font = Font.vwtextfont.with(size: 12.0, weight: .vwtextregular)
        if let modelVal = serviceHistory{
            self.updateUI(model: modelVal)
        }
        
    }
    // MARK: - Update UI
    func updateUI(model: Service_History_Data){
        
        self.repairOrderDataLabel.text = model.vW_RO_Number__c ?? ""
        self.roTypeDataLabel.text = model.rO_Type__c ?? ""
        self.roServiceTypeDataLabel.text = ""
        
        let openDate = model.createdDate ?? ""
        let openObj = openDate.split(separator: "T")
        if (openObj.count > 0){
            let finalOpenDate = String(describing: openObj[0]).removeOptionalFromString()
            self.serviceOpenDateDataLabel.text = finalOpenDate.convertDateOnGoingServiceDetails(input: finalOpenDate)
           
//            Commented time value  as we are not getting for complete time
//            let splitTime = openObj[1]
//            let splitTimeStr = splitTime.split(separator: ".")
//            let finalTime = String(describing: splitTimeStr[0]).removeOptionalFromString()
//            self.serviceOpenTimeLabel.text = finalTime.convert24hoursTimeToTwelveHours()
            
        }
        let compDate = model.rO_Closed_Date__c ?? ""
            let finalCompDate = String(describing: compDate).removeOptionalFromString()
            self.serviceClosedDateDataLabel.text = finalCompDate.convertDateOnGoingServiceDetails(input: finalCompDate)
            self.serviceDateTimeLabel.text = finalCompDate.convertDateOnGoingServiceDetails(input: finalCompDate)
            self.serviceClosedTimeLabel.text = ""
        
//        self.serviceEngineerDataLabel.text = model.customer_Name__c ?? ""
        
        self.invoiceAmountDataLabel.text = String(describing: model.amount__c).removeOptionalFromString()
        instructionsTextView.text = model.customer_Voice__c ?? ""
        ratingsLabel.text = feedbackScore
    }
    
    // MARK: - User interactions
    // MARK: - back button action
    @IBAction func backButtonAction(_ sender: UIButton) {
        presenter.backButtonClicked()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
