//
//  ServiceHistoryDetailsContract.swift
//  MSU
//
//  Created by vectorform on 22/07/21.
//

import Foundation
protocol IServiceHistoryDetailsViewController: IBaseViewController {

}


protocol IServiceHistoryDetailsPresenter: IBasePresenter {
    ///This function called pop back view controller
    ///
    func backButtonClicked()
}
