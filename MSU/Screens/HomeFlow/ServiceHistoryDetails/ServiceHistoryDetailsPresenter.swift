//
//  ServiceHistoryDetailsPresenter.swift
//  MSU
//
//  Created by vectorform on 22/07/21.
//

import Foundation

class ServiceHistoryDetailsPresenter: BasePresenter, IServiceHistoryDetailsPresenter {

    var homeRouter: HomeRouter? {
        return navController as? HomeRouter
    }
    private weak var myView: IServiceHistoryDetailsViewController? {
        return self.view as? IServiceHistoryDetailsViewController
    }
    
    // MARK: - back button action
    func backButtonClicked() {
        homeRouter?.didSelectBack()
    }
    

}
