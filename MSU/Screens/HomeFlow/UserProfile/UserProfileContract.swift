//
//  UserProfileContract.swift
//  MSU
//
//  Created by vectorform on 26/08/21.
//

import Foundation

protocol IUserProfileViewController: IBaseViewController {
    
    ///This function called to update UI
    ///
    func updateUI()
}

protocol IUserProfilePresenter: IBasePresenter {
    ///This function called to bind UI
    ///
    func bindUI()
    
    ///This function called to pop back view controller
    ///
    func closeButtonClicked()
    
    ///This function called to logout of the application
    ///
    func logoutClicked()
    
    
    ///This function called to save Profile Pic
    ///
    func saveProfilePicToAPI(name: String, base64String: String)
    
    ///This function called to push notifications view
    ///
    func pushNotificationsView()
}

