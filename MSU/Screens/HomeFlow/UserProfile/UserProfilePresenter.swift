//
//  UserProfilePresenter.swift
//  MSU
//
//  Created by vectorform on 26/08/21.
//

import Foundation
import UIKit
import SwiftyJSON
import ObjectMapper

class UserProfilePresenter: BasePresenter, IUserProfilePresenter{

    var homeRouter: HomeRouter? {
        return navController as? HomeRouter
    }

    
    private weak var myView: IUserProfileViewController? {
        return self.view as? IUserProfileViewController
    }
    
    // MARK: - Close button action
    func closeButtonClicked() {
        homeRouter?.didSelectBack()
    }
    
    // MARK: - Logout button action
    func logoutClicked(){
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        self.sectionRouter?.navigateToLoginFlow(animate: true)
    }
    
    // MARK: - Bind UI in View
    func bindUI() {
        self.myView?.updateUI()
    }
    // MARK: - Save Profile Pic
    func saveProfilePicToAPI(name: String, base64String: String)
    {
        self.myView?.showLoader()
        let userId = DefaultsWrapper.loginUserId
        let postData: [String : Any]  = ["ImageFileName":name as Any,
                                         "ImageBase64String":base64String as Any,
                                         "UserId": userId as Any
                                            ]
        
        FacadeLayer.sharedInstance.webserviceManager.genericAPICalls(requestParams: postData as [String: AnyObject], apiPath: VFApiCalls.updateUserProfilePic) { [weak self] (status, response) in
            guard let strongSelf = self else{ return }
            if(status){
                strongSelf.myView?.removeLoader()
                let responseVal = JSON(response as Any)
                if let model = Mapper<ProfilePicModel>().map(JSONObject:responseVal.dictionaryObject){
                    DefaultsWrapper.userProfilePic = model.profilePicUrl ?? ""
                    strongSelf.myView?.updateUI()
                }
            }
            else{
                strongSelf.myView?.removeLoader()
                   let errorVal = JSON(response as Any)
                   let errorString = "\(errorVal)"
                   print(errorString)
                   var errormsg: String = ""
                   if(errorString != ErrorStringConstants.noInternet){
                           errormsg = errorVal["Reason"].stringValue
                       if(errormsg == ""){
                           errormsg = ErrorStringConstants.defaultError
                       }
                   }
                   else{
                       errormsg = ErrorStringConstants.noInternet
                   }
                   strongSelf.myView?.displayDefaultAlertForError(errorTitle: ErrorStringConstants.errorTitle, errorDescription: errormsg)
                }
        }
    }
    
    // MARK: - Push notifications view controller
    func pushNotificationsView()
    {
        homeRouter?.pushUserProfileToNotification(animated: true)
    }
}
