//
//  UserProfileViewController.swift
//  MSU
//
//  Created by vectorform on 26/08/21.
//

import UIKit
import Photos
class UserProfileViewController: BaseViewController, IUserProfileViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var sectionLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var seperatorView: UIView!

    @IBOutlet weak var fullNameInputTextView: MSUInputTextFields!
    @IBOutlet weak var emailInputTextView: MSUInputTextFields!
    @IBOutlet weak var mobileInputTextView: MSUInputTextFields!
    @IBOutlet weak var roleInputTextView: MSUInputTextFields!
    @IBOutlet weak var dealershipInputTextView: MSUInputTextFields!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var versionLabel: UILabel!
   
    @IBOutlet weak var logoutButton: MSUButton!
    @IBOutlet weak var logoutUnderline: UIView!
    
    @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!
  
    
    @IBOutlet weak var notificationButton: UIButton!
    private let presenter: IUserProfilePresenter = UserProfilePresenter()
    
    let picker = UIImagePickerController()
    var pickedImageName: String = ""
    var imageBase64String: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      
        presenter.connectToView(view: self)
        self.showLoader()
    }
    
    // MARK: - ApplyStylings to objects
    func applyStylings(){
        sectionLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: true, size: 18.0, alignment: .center, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        
        seperatorView.backgroundColor = ColorConstants.underlineColor
        logoutButton.appButtonStyling(titleColor: ColorConstants.appSecondaryBlueColor, backgroundColor: ColorConstants.clear)
       
        logoutButton.titleLabel?.font = Font.vwheadfont.with(size: CGFloat(14.0), weight: .vwheadbold)
        
        logoutUnderline.backgroundColor = ColorConstants.appSecondaryBlueColor
        
        
        fullNameInputTextView.textInputView( hintText: "Full Name", isSecureTextEntry: false, returnKeyType: UIReturnKeyType.next, keyboardtype: UIKeyboardType.default, textColor: ColorConstants.appPrimaryColor, tintColor: ColorConstants.appPrimaryColor, lineColor: ColorConstants.underlineColor)
        fullNameInputTextView.inputTextfield.frame = CGRect(x: fullNameInputTextView.frame.origin.x, y: fullNameInputTextView.frame.origin.y, width: fullNameInputTextView.frame.size.width, height: fullNameInputTextView.frame.size.height)
        fullNameInputTextView.inputTextfield.isUserInteractionEnabled = false
        fullNameInputTextView.updateWidthConstraint(widthVal: self.view.frame.size.width - 25)
        
        emailInputTextView.textInputView( hintText: "Email", isSecureTextEntry: false, returnKeyType: UIReturnKeyType.next, keyboardtype: UIKeyboardType.default, textColor: ColorConstants.appPrimaryColor, tintColor: ColorConstants.appPrimaryColor, lineColor: ColorConstants.underlineColor)
        emailInputTextView.inputTextfield.frame = CGRect(x: emailInputTextView.frame.origin.x, y: emailInputTextView.frame.origin.y, width: emailInputTextView.frame.size.width, height: fullNameInputTextView.frame.size.height)
        emailInputTextView.inputTextfield.isUserInteractionEnabled = false
        emailInputTextView.updateWidthConstraint(widthVal: self.view.frame.size.width - 25)
        
        mobileInputTextView.textInputView( hintText: "Mobile", isSecureTextEntry: false, returnKeyType: UIReturnKeyType.next, keyboardtype: UIKeyboardType.default, textColor: ColorConstants.appPrimaryColor, tintColor: ColorConstants.appPrimaryColor, lineColor: ColorConstants.underlineColor)
        mobileInputTextView.inputTextfield.frame = CGRect(x: mobileInputTextView.frame.origin.x, y: mobileInputTextView.frame.origin.y, width: mobileInputTextView.frame.size.width, height: fullNameInputTextView.frame.size.height)
        mobileInputTextView.inputTextfield.isUserInteractionEnabled = false
        mobileInputTextView.updateWidthConstraint(widthVal: self.view.frame.size.width - 25)
        
        roleInputTextView.textInputView( hintText: "Role", isSecureTextEntry: false, returnKeyType: UIReturnKeyType.next, keyboardtype: UIKeyboardType.default, textColor: ColorConstants.appPrimaryColor, tintColor: ColorConstants.appPrimaryColor, lineColor: ColorConstants.underlineColor)
        roleInputTextView.inputTextfield.frame = CGRect(x: roleInputTextView.frame.origin.x, y: roleInputTextView.frame.origin.y, width: roleInputTextView.frame.size.width, height: roleInputTextView.frame.size.height)
        roleInputTextView.inputTextfield.isUserInteractionEnabled = false
        roleInputTextView.updateWidthConstraint(widthVal: self.view.frame.size.width - 25)
        
        dealershipInputTextView.textInputView( hintText: "Dealership", isSecureTextEntry: false, returnKeyType: UIReturnKeyType.next, keyboardtype: UIKeyboardType.default, textColor: ColorConstants.appPrimaryColor, tintColor: ColorConstants.appPrimaryColor, lineColor: ColorConstants.underlineColor)
        dealershipInputTextView.inputTextfield.frame = CGRect(x: dealershipInputTextView.frame.origin.x, y: dealershipInputTextView.frame.origin.y, width: dealershipInputTextView.frame.size.width, height: dealershipInputTextView.frame.size.height)
        dealershipInputTextView.inputTextfield.isUserInteractionEnabled = false
        dealershipInputTextView.updateWidthConstraint(widthVal: self.view.frame.size.width - 25)
        
        versionLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: true, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        
        
        profileImageView.layer.cornerRadius = profileImageView.frame.size.width / 2
        profileImageView.layer.masksToBounds = true
        profileImageView.layer.borderColor = ColorConstants.underlineColor.cgColor
        profileImageView.layer.borderWidth = 1.0
        

        picker.delegate = self
        picker.allowsEditing = true
        
        presenter.bindUI()
          let userImageTap =  UITapGestureRecognizer(target: self, action: #selector(userImageTapped(_:)))
          profileImageView.addGestureRecognizer(userImageTap)
          profileImageView.isUserInteractionEnabled = true
        notificationButton.setTitle("", for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scrollViewHeightConstraint.constant = 550.0
        notificationButton.setTitle("", for: .normal)
        applyStylings()
    }
    
    // MARK: - Update UI
    func updateUI()
    {
        fullNameInputTextView.inputTextfield.text = DefaultsWrapper.username
        emailInputTextView.inputTextfield.text = DefaultsWrapper.loggedEmail
        mobileInputTextView.inputTextfield.text = DefaultsWrapper.mobileNumber
        roleInputTextView.inputTextfield.text = DefaultsWrapper.loggedInRole
        dealershipInputTextView.inputTextfield.text = DefaultsWrapper.dealership
        
        let appVersionDict = Bundle.main.infoDictionary
        if let dict = appVersionDict{
            let appVersionVal = dict["CFBundleShortVersionString"] as! String
            versionLabel.text = "V " + appVersionVal
        }
        
        let baseUrl = BaseRequest()
        var baseUrlString = baseUrl.baseUrlString
        let imagePath = DefaultsWrapper.userProfilePic
        if(imagePath.count != 0){
            let imgPath = imagePath
                baseUrlString += imgPath
            self.profileImageView.kf.indicatorType = .activity
            let url = URL(string: baseUrlString)
                self.profileImageView.kf.setImage(with: url)
        }
        self.removeLoader()
    }
    // MARK: - User interactions
    // MARK: - back button action
    @IBAction func backButtonAction(_ sender: UIButton) {
        presenter.closeButtonClicked()
    }
    
    // MARK: - Logout button action
    @IBAction func logoutButtonAction(_ sender: MSUButton) {
        presenter.logoutClicked()
    }
    
    // MARK: - Notification Action
    @IBAction func notificationAction(_ sender: UIButton) {
        presenter.pushNotificationsView()
    }
    // MARK: - User Image taooed
    
    @objc func userImageTapped(_ sender:UITapGestureRecognizer){
        showActionSheet()
    }
    
    func showActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
            actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
                 self.camera()
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
                 self.photoLibrary()
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        actionSheet.popoverPresentationController?.sourceView = self.profileImageView
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func camera()
      {
         if UIImagePickerController.isSourceTypeAvailable(.camera){
                 picker.sourceType = .camera
                 picker.cameraDevice = .rear
             self.present(picker, animated: true, completion: nil)
         }
         else{
         }
     }
    
    func photoLibrary() {
        picker.sourceType = .photoLibrary
        self.present(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
            if let chosenImage = info[.originalImage] as? UIImage{
                let imageData:Data =  chosenImage.jpegData(compressionQuality: 1.0)!
                self.imageBase64String = imageData.base64EncodedString()
                self.profileImageView.image = chosenImage
            }
                if let url = info[UIImagePickerController.InfoKey.imageURL] as? URL {
                    self.pickedImageName = url.lastPathComponent
                }
                else{
                    let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let currentDateStr = formatter.string(from: Date())
                    let currentDate = formatter.date(from: currentDateStr)
                        formatter.dateFormat = "dd/MM/yyyy-HH:mm:ss"
                    let myStringafd = formatter.string(from: currentDate!)
                        self.pickedImageName = "Image \(myStringafd).png"
                }
              self.dismiss(animated: true, completion: nil)
             
        presenter.saveProfilePicToAPI(name: self.pickedImageName, base64String: self.imageBase64String)
        }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
