//
//  NotificationsContract.swift
//  MSU
//
//  Created by vectorform on 10/03/22.
//

import Foundation

protocol INotificationsViewController: IBaseViewController {
    
    ///This function called to get the notifications model
    ///
    ///    - Parameter model: list of notifications 
    func getNotificationsList(model:[NotificationData])
}

protocol INotificaionsPresenter: IBasePresenter {
    ///This function called to pop back view controller
    ///
    func closeButtonClicked()
    
    
    ///This function called to get Notifications list
    ///
    func getNotifications()
    
    ///This function called to update the notifications
    ///
    ///    - Parameter notId: integer
    func updateNotification(notId: Int)
    
}
