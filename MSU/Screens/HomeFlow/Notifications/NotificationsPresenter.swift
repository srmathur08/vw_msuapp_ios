//
//  NotificationsPresenter.swift
//  MSU
//
//  Created by vectorform on 10/03/22.
//

import Foundation
import UIKit
import SwiftyJSON
import ObjectMapper

class NotificationsPresenter: BasePresenter, INotificaionsPresenter{
    
    var homeRouter: HomeRouter? {
        return navController as? HomeRouter
    }

    private weak var myView: INotificationsViewController? {
        return self.view as? INotificationsViewController
    }
    
    // MARK: - Close button action
    func closeButtonClicked() {
        homeRouter?.didSelectBack()
    }
    
    // MARK: - Get Notifications
    func getNotifications()
    {
        self.myView?.showLoader()
        let postData: [String : Any]  = ["UserId":DefaultsWrapper.loginUserId as Any]
        
        FacadeLayer.sharedInstance.webserviceManager.genericAPICalls(requestParams: postData as [String: AnyObject], apiPath: VFApiCalls.getNotifications) { [weak self] (status, response) in
            guard let strongSelf = self else{ return }
            if(status){
                strongSelf.myView?.removeLoader()
                let responseVal = JSON(response as Any)
                if let model = Mapper<NotificationModel>().map(JSONObject:responseVal.dictionaryObject){
                    if let listModel =  model.data{
                        strongSelf.myView?.getNotificationsList(model: listModel)
                    }
                }
            }
            else{
                strongSelf.myView?.removeLoader()
                let errorVal = JSON(response as Any)
                let errorString = "\(errorVal)"
                print(errorString)
                var errormsg: String = ""
                if(errorString != ErrorStringConstants.noInternet){
                        errormsg = errorVal["Reason"].stringValue
                    if(errormsg == ""){
                        errormsg = ErrorStringConstants.defaultError
                    }
                }
                else{
                    errormsg = ErrorStringConstants.noInternet
                }
                strongSelf.myView?.displayDefaultAlertForError(errorTitle: ErrorStringConstants.errorTitle, errorDescription: errormsg)
            }
        }
    }
    
    // MARK: - Update Notifications
    func updateNotification(notId: Int)
    {
        self.myView?.showLoader()
        let postData: [String : Any]  = ["UserId":DefaultsWrapper.loginUserId as Any,
                                         "NotificationId": notId as Any]
        
        FacadeLayer.sharedInstance.webserviceManager.genericAPICalls(requestParams: postData as [String: AnyObject], apiPath: VFApiCalls.updateNotifications) { [weak self] (status, response) in
            guard let strongSelf = self else{ return }
            if(status){
                strongSelf.myView?.removeLoader()
                strongSelf.getNotifications()
            }
            else{
                strongSelf.myView?.removeLoader()
                let errorVal = JSON(response as Any)
                let errorString = "\(errorVal)"
                print(errorString)
                var errormsg: String = ""
                if(errorString != ErrorStringConstants.noInternet){
                        errormsg = errorVal["Reason"].stringValue
                    if(errormsg == ""){
                        errormsg = ErrorStringConstants.defaultError
                    }
                }
                else{
                    errormsg = ErrorStringConstants.noInternet
                }
                strongSelf.myView?.displayDefaultAlertForError(errorTitle: ErrorStringConstants.errorTitle, errorDescription: errormsg)
            }
        }
        
    }
}
