//
//  NotificationTableViewAdaptor.swift
//  MSU
//
//  Created by vectorform on 10/03/22.
//

import Foundation
import UIKit

protocol NotificationListDelegate: AnyObject {
    ///This function called pass value from adaptor to controller
    ///
    ///     Warning id should not be empty
    ///   - Parameter id: must be a integer value
    func getNotificationId(_ id: Int)
}

class NotificationTableViewAdaptor: NSObject, ITableViewAdapter, UITableViewDelegate, UITableViewDataSource {
    
    var tableView: UITableView
    
    typealias DataType = NotificationData
    var data: [DataType] = []
    
    var notificationDelegate: NotificationListDelegate?
    
    required init(tableView: UITableView) {
        self.tableView = tableView
        self.tableView.backgroundColor = ColorConstants.clear
        self.tableView.separatorColor = ColorConstants.clear
        super.init()
        let nibName = UINib(nibName: "NotificaitonsTableViewCell", bundle:nil)
        self.tableView.register(nibName, forCellReuseIdentifier: "NotificaitonsTableViewCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 100.0
        
    }
    
    // MARK: - Update table view with model
    ///This function called to map list of SR in table view
    ///
    ///     Warning data should not be empty
    ///   - Parameter data: list of Service_History_Data
    func updateData(data: [NotificationData]) {
        self.data = data
        tableView.reloadData()
    }
    
    
    // MARK: - Tableview delegate and methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: NotificaitonsTableViewCell = tableView.dequeueReusableCell(
            withIdentifier: "NotificaitonsTableViewCell",
            for: indexPath) as? NotificaitonsTableViewCell {
            
            let modelObj = data[indexPath.row]
            cell.notificationTitleLabel.text = modelObj.titleVal ?? ""
            cell.descriptionLabel.text = modelObj.descriptionVal ?? ""
            
            let isReadVal = modelObj.isRead ?? false
            if(isReadVal){
                cell.contentCardView.alpha = 0.6
            }
            else{
                cell.contentCardView.alpha = 1.0
            }
            
            var createdDate = modelObj.createdDate ?? ""
            let createDateArray = createdDate.split(separator: "T")
            if(createDateArray.count > 0){
                createdDate = String(describing: createDateArray[0]).removeOptionalFromString()
            }
            createdDate = createdDate.convertDateStringToFormatsNotifications(input: createdDate)
            
            cell.datelabel.text = createdDate
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedRow = data[indexPath.row]
        let notificationId = selectedRow.idVal ?? 0
        let isRead = selectedRow.isRead ?? false
        if(!isRead){
            notificationDelegate?.getNotificationId(notificationId)
        }
    }
    
}
