//
//  NotificaitonsTableViewCell.swift
//  MSU
//
//  Created by vectorform on 10/03/22.
//

import UIKit

class NotificaitonsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var notificationTitleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var datelabel: UILabel!
    
    @IBOutlet weak var contentCardView: CardView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        notificationTitleLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: false, size: 16.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        
        descriptionLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: false, size: 16.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        
        datelabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: false, size: 16.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
