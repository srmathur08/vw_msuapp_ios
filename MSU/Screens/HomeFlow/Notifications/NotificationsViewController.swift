//
//  NotificationsViewController.swift
//  MSU
//
//  Created by vectorform on 10/03/22.
//

import UIKit

class NotificationsViewController: BaseViewController, INotificationsViewController, NotificationListDelegate {
 
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var notificationTitle: UILabel!
    @IBOutlet weak var viewSeperator: UIView!
    @IBOutlet weak var notificationsTableView: UITableView!
    @IBOutlet weak var noDataLabel: UILabel!
    
    private let presenter: INotificaionsPresenter = NotificationsPresenter()
    
    private lazy var tableViewAdapter: NotificationTableViewAdaptor  = {
        NotificationTableViewAdaptor(tableView: self.notificationsTableView)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter.connectToView(view: self)
        presenter.getNotifications()
        applyStylings()
    }
    
    // MARK: - ApplyStylings to objects
    func applyStylings(){
        notificationTitle.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: true, size: 18.0, alignment: .center, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        
        noDataLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: true, size: 16.0, alignment: .center, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        noDataLabel.text = StringConstants.noDataText
        noDataLabel.isHidden = true
        
        viewSeperator.backgroundColor = ColorConstants.underlineColor
    }
    
    // MARK: - Get Notifications model
    func getNotificationsList(model:[NotificationData])
    {
        if(model.count == 0){
            noDataLabel.isHidden = false
            notificationsTableView.isHidden = true
        }
        else{
            noDataLabel.isHidden = true
            notificationsTableView.isHidden = false
            tableViewAdapter.notificationDelegate = self
            tableViewAdapter.updateData(data: model)
        }
    }
    
    
    // MARK: - Get Notifications id from list
    func getNotificationId(_ id: Int) {
        presenter.updateNotification(notId: id)
    }
    
    // MARK: - User interactions
    // MARK: - back button action
    @IBAction func backButtonAction(_ sender: UIButton) {
        presenter.closeButtonClicked()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
