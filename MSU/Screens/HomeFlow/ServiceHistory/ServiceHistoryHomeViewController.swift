//
//  ServiceHistoryHomeViewController.swift
//  MSU
//
//  Created by vectorform on 21/07/21.
//

import UIKit
import SwiftyJSON
import ObjectMapper
import Alamofire


class ServiceHistoryHomeViewController: BaseViewController, IServiceHistoryHomeViewController, ServiceHistoryHomeButtonDelegate {


    private let presenter: IServiceHistoryHomePresenter = ServiceHistoryHomePresenter()
    // MARK: - View Contoller Outlets
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var serviceHistoryTableView: UITableView!
    @IBOutlet weak var sendServiceHistoryButton: MSUButton!
    
    @IBOutlet weak var viewSeperator: UIView!
    private lazy var tableViewAdapter: ServiceHistoryTableViewAdaptor  = {
        ServiceHistoryTableViewAdaptor(tableView: self.serviceHistoryTableView)
    }()
    
    var serviceHistoryRecords: [Service_History_Data]?
    var vinNumber: String = ""
    var overallScore: String = ""
    var serviceRequestId: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.connectToView(view: self)
        presenter.getVinNumber(vin: vinNumber)
        // Do any additional setup after loading the view.
        applyStylings()
    }
    // MARK: - Apply Stylings to Objects
    private func applyStylings(){
        headerLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: true, size: 18.0, alignment: .center, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        
        viewSeperator.backgroundColor = ColorConstants.underlineColor
        
        sendServiceHistoryButton.appButtonStyling(titleColor: ColorConstants.solidWhite, backgroundColor: ColorConstants.appSecondaryBlueColor)
       
        sendServiceHistoryButton.titleLabel?.font = Font.vwheadfont.with(size: CGFloat(14.0), weight: .vwheadbold)
        
        presenter.createSFDCToken()
    }
    
    // MARK: - bind service history list
    func bindServicesHistoryList(model: [Service_History_Data]) {
        let firstThreeRecords:[Service_History_Data] = model.limit(3)
        self.serviceHistoryRecords = firstThreeRecords
        tableViewAdapter.serviceHomeDelegate = self
        tableViewAdapter.updateData(data: firstThreeRecords)
    }
    
    // MARK: - Back button action
    @IBAction func backButtonAction(_ sender: UIButton) {
        presenter.backButtonClicked()
    }
    
    func getOverallScore(score: String) {
        overallScore = score
    }
    
    // MARK: - Send Service History clicked
    @IBAction func sendServiceHistoryButtonAction(_ sender: MSUButton) {
        presenter.sendServiceHistory(srNum: serviceRequestId)
    }
    
    // MARK: - Service History button clicked
    func getServiceDetails(_ tag: Int) {
        if let selectedModel = self.serviceHistoryRecords?[tag]{
            presenter.viewServiceDetails(serviceModel: selectedModel, feedbackScore: overallScore)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension Sequence {
    func limit(_ max: Int) -> [Element] {
        return self.enumerated()
            .filter { $0.offset < max }
            .map { $0.element }
    }
}
