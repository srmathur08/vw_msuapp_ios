//
//  ServiceHistoryHomeTableViewCell.swift
//  MSU
//
//  Created by vectorform on 21/07/21.
//

import UIKit


class ServiceHistoryHomeTableViewCell: UITableViewCell {

    @IBOutlet weak var cardView: CardView!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var starImage: UIImageView!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var serviceView: UIView!
    @IBOutlet weak var serviceIcon: UIImageView!
    @IBOutlet weak var serviceNameLabel: UILabel!
    @IBOutlet weak var serviceUserName: UIView!
    @IBOutlet weak var serviceUserIcon: UIImageView!
    @IBOutlet weak var serviceUserNameLabel: UILabel!
    @IBOutlet weak var serviceSeperatorView: UIView!
    @IBOutlet weak var viewServiceDetailsButton: MSUButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        addViews()
    }
    // MARK: - Apply stylings to outlets
    ///This function called to apply stylings
    ///
    func addViews() {
        dateTimeLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: true, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType:  StringConstants.fontText)
        
        ratingLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType:  StringConstants.fontText)
        
        serviceNameLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        serviceUserNameLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        serviceSeperatorView.backgroundColor = ColorConstants.underlineColor
        
        viewServiceDetailsButton.appButtonStyling(titleColor: ColorConstants.appSecondaryBlueColor, backgroundColor: ColorConstants.clear)
        
        viewServiceDetailsButton.titleLabel?.font = Font.vwheadfont.with(size: CGFloat(14.0), weight: .vwheadregular)
        self.layoutSubviews()
    }
    

       
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
