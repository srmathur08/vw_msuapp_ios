//
//  ServiceHistoryHomeContract.swift
//  MSU
//
//  Created by vectorform on 21/07/21.
//

import Foundation

protocol IServiceHistoryHomeViewController: IBaseViewController {
    
    ///This function called to push service history view controller
    ///
    ///     Warning model must not be empty
    ///   - Parameter model: list of Service_History_Data
    func bindServicesHistoryList(model: [Service_History_Data])
    
    ///This function called to get the get service score
    ///
    ///     Warning score must not be empty
    ///   - Parameter score: string
    func getOverallScore(score: String)
}


protocol IServiceHistoryHomePresenter: IBasePresenter {
    ///This function called to push service history details view controller
    ///
    ///     Warning serviceModel must not be empty
    ///   - Parameter serviceModel: Service_History_Data
    func viewServiceDetails(serviceModel: Service_History_Data, feedbackScore: String)
    
    ///This function called to pop back
    ///
    func backButtonClicked()
    
    ///This function called to crearte SFDC token
    ///
    func createSFDCToken()
    
    ///This function called to get the Vin number
    ///
    ///     Warning vin must not be empty
    ///   - Parameter vin: string
    func getVinNumber(vin: String)
    
    ///This function called to send Service history
    ///
    func sendServiceHistory(srNum: Int)
    
}
