//
//  ServiceHistoryTableViewAdaptor.swift
//  MSU
//
//  Created by vectorform on 21/07/21.
//

import Foundation
import UIKit

protocol ServiceHistoryHomeButtonDelegate: AnyObject {
    ///This function called pass value from adaptor to controller
    ///
    ///     Warning tag should not be empty
    ///   - Parameter tag: must be a integer value
    func getServiceDetails(_ tag: Int)
}
class ServiceHistoryTableViewAdaptor: NSObject, ITableViewAdapter, UITableViewDelegate, UITableViewDataSource {
   
    var tableView: UITableView
    
    typealias DataType = Service_History_Data
    var data: [DataType] = []
    var serviceHomeDelegate: ServiceHistoryHomeButtonDelegate?
    
    required init(tableView: UITableView) {
        self.tableView = tableView
        self.tableView.backgroundColor = ColorConstants.clear
        self.tableView.separatorColor = ColorConstants.clear
        self.tableView.separatorInset = .zero
        super.init()
        let nibName = UINib(nibName: "ServiceHistoryHomeTableViewCell", bundle:nil)
        self.tableView.register(nibName, forCellReuseIdentifier: "ServiceHistoryHomeTableViewCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
    }
    
    // MARK: - Update table view with model
    ///This function called to map list of SR in table view
    ///
    ///     Warning data should not be empty
    ///   - Parameter data: list of Service_History_Data
    func updateData(data: [Service_History_Data]) {
        self.data = data
        tableView.reloadData()
        
    }
    
    func updateData() {
//        self.data = [data]
        tableView.reloadData()
    }
    
    // MARK: - Tableview delegate and methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: ServiceHistoryHomeTableViewCell = tableView.dequeueReusableCell(
            withIdentifier: "ServiceHistoryHomeTableViewCell",
            for: indexPath) as? ServiceHistoryHomeTableViewCell {

            let serviceObj = self.data[indexPath.row]
            let createDate = serviceObj.createdDate ?? ""
            let createDateObj = createDate.split(separator: ".")
            if(createDateObj.count > 0) {
                let finalOpenDate = String(describing: createDateObj[0]).removeOptionalFromString()
                cell.dateTimeLabel.text = finalOpenDate.convertDateStringToFormats(input: finalOpenDate)
            }
            cell.serviceUserNameLabel.text = serviceObj.customer_Name__c ?? ""
            cell.serviceNameLabel.text = serviceObj.rO_Type__c ?? ""
            cell.viewServiceDetailsButton.tag = indexPath.row
            cell.viewServiceDetailsButton.addTarget(self, action: #selector(viewServiceDetails), for: .touchUpInside)
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170.0
    }
    
    // MARK: - Call back button action to delegate
   @objc func viewServiceDetails(sender: UIButton) {
    serviceHomeDelegate?.getServiceDetails(sender.tag)
    }
}
