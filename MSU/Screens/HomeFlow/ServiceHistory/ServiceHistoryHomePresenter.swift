//
//  ServiceHistoryHomePresenter.swift
//  MSU
//
//  Created by vectorform on 21/07/21.
//

import Foundation
import SwiftyJSON
import ObjectMapper
class ServiceHistoryHomePresenter: BasePresenter, IServiceHistoryHomePresenter {
 

    var homeRouter: HomeRouter? {
        return navController as? HomeRouter
    }
    private weak var myView: IServiceHistoryHomeViewController? {
        return self.view as? IServiceHistoryHomeViewController
    }
    
    var receivedVin: String = ""
    var receivedSRid: Int = 0
    func viewServiceDetails(serviceModel: Service_History_Data, feedbackScore: String) {
        print("view service")
        homeRouter?.pushServiceHistoryDetailsController(animated: true,model: serviceModel, score: feedbackScore)
    }
    // MARK: - back Button Clicked
    func backButtonClicked() {
        homeRouter?.didSelectBack()
    }
    
    // MARK: - Get Vin number
    func getVinNumber(vin: String)
    {
        receivedVin = vin
    }
    
    // MARK: - create SFDC token
    func createSFDCToken(){
        self.myView?.showLoader()
        let urlPath = SFDCCalls.baseRequest + SFDCCalls.token
        FacadeLayer.sharedInstance.sfdcServiceManeger.createSalesForceToken(url: urlPath) { [weak self] (status , response) in
            guard let strongSelf = self else{ return }
            if(status){
                let responseVal = JSON(response as Any)
                if let responseModel = Mapper<TokenModel>().map(JSONObject:responseVal.dictionaryObject){
                    FacadeLayer.sharedInstance.sfdcAccessToken = responseModel.access_token ?? ""
                    FacadeLayer.sharedInstance.sfdcInstanceUrl = responseModel.instance_url ?? ""
                    strongSelf.getServiceHistoryData()
                }
                else{
                    strongSelf.myView?.removeLoader()
                    strongSelf.myView?.displayDefaultAlertForError(errorTitle: ErrorStringConstants.errorTitle, errorDescription: "Error generating token")
                }
            }
        }
    }
    
    ///This function called to get the list of service history
    ///
    ///     Warning vin must not be empty
    ///   - Parameter receivedVin: string
    func getServiceHistoryData(){
        
        let urlPath =  FacadeLayer.sharedInstance.sfdcInstanceUrl + SFDCCalls.getServiceHistory.replacingOccurrences(of: "VINNUMBER", with: receivedVin)
        
        FacadeLayer.sharedInstance.sfdcServiceManeger.getSFDCGenericAPICalls(url: urlPath, accesstoken: FacadeLayer.sharedInstance.sfdcAccessToken) { [weak self] (status , response) in
            guard let strongSelf = self else{ return }
            if(status){
                strongSelf.myView?.removeLoader()
                let responseVal = JSON(response as Any)
                
                if let serviceModel = Mapper<ServiceHistoryModel>().map(JSONObject: responseVal.dictionaryObject){
                    print(serviceModel)
                    strongSelf.myView?.bindServicesHistoryList(model: serviceModel.service_History_Data!)
                    strongSelf.myView?.getOverallScore(score: serviceModel.overall_customer_satisfaction_score ?? "")
                }
            }
            else{
                strongSelf.myView?.removeLoader()
            }
        }
    }
    
    
    // MARK: - Send SHR 
    func sendServiceHistory(srNum: Int)
    {
        self.myView?.showLoader()
        let postData: [String : AnyObject]  = ["Srid":srNum as Any] as [String: AnyObject]
        
        FacadeLayer.sharedInstance.webserviceManager.genericAPICalls(requestParams: postData, apiPath: VFApiCalls.sendServiceHistory) { [weak self] (status , response) in
            guard let strongSelf = self else{ return }
            if(status){
                strongSelf.myView?.removeLoader()
                strongSelf.myView?.displayDefaultAlertForError(errorTitle: ErrorStringConstants.defaultTitle, errorDescription: ErrorStringConstants.sendSHR )
            }
            else{
                strongSelf.myView?.removeLoader()
                strongSelf.myView?.displayDefaultAlertForError(errorTitle: ErrorStringConstants.errorTitle, errorDescription: ErrorStringConstants.defaultError)
            }
        }
    }
}
