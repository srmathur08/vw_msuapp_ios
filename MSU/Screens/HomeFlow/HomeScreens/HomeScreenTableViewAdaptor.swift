//
//  HomeScreenTableViewAdaptor.swift
//  MSU
//
//  Created by vectorform on 16/06/21.
//

import Foundation
import UIKit
import Kingfisher

protocol ServiceHomeGetDirectionsButtonDelegate: AnyObject {
    ///This function called pass model from adaptor to controller
    ///
    ///     Warning model should not be empty
    ///   - Parameter vin: ServiceRequests
    func getServiceDirections(model: ServiceRequests)
}

class HomeScreenTableViewAdaptor: NSObject, ITableViewAdapter, UITableViewDelegate, UITableViewDataSource {
   

    var tableView: UITableView
    
    typealias DataType = ServiceRequests
    var data: [DataType] = []
    
    weak var serviceHomeDelegate: ServiceHomeGetDirectionsButtonDelegate?
    required init(tableView: UITableView) {
        self.tableView = tableView
        self.tableView.backgroundColor = ColorConstants.clear
        self.tableView.separatorColor = ColorConstants.clear
        self.tableView.separatorInset = .zero
        super.init()
        let nibName = UINib(nibName: "HomeScreenTableViewCell", bundle:nil)
        self.tableView.register(nibName, forCellReuseIdentifier: "HomeScreenTableViewCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
    }
    // MARK: - Update table Data
    ///This function called to map SR data from contr0ller
    ///
    ///     Warning data should not be empty
    ///   - Parameter data: list of ServiceRequests
    func updateData(data: [ServiceRequests]) {
        self.data = data
        tableView.reloadData()
    }
    
    // MARK: - Tableview delegate and datasource methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: HomeScreenTableViewCell = tableView.dequeueReusableCell(
            withIdentifier: "HomeScreenTableViewCell",
            for: indexPath) as? HomeScreenTableViewCell {
            let baseUrl = BaseRequest()
            var baseUrlString = baseUrl.baseUrlString
            
            let serviceObj = data[indexPath.row]
            cell.regNoLabel.text = serviceObj.regNum ?? ""
            cell.usernameLabel.text = serviceObj.customerName ?? ""
            cell.typeofServiceLabel.text = serviceObj.serviceType ?? ""
            cell.kmsLabel.text = serviceObj.odometerReading ?? "" + " KM"
            cell.updateStatusButton.tag = indexPath.row
            let imagePath = serviceObj.vehicleImage ?? ""
            if(imagePath.count != 0){
                let imgPath = imagePath
//                     imgPath.remove(at: imagePath.startIndex)
                    baseUrlString += imgPath
                    cell.vehicleImageView.kf.indicatorType = .activity
                let url = URL(string: baseUrlString)
                     cell.vehicleImageView.kf.setImage(with: url)
                }
            cell.updateStatusButton.addTarget(self, action: #selector(getDirectionsClicked), for: .touchUpInside)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180.0
    }
    
    // MARK: - Call back button action to delegate
  @objc func getDirectionsClicked(sender: UIButton) {
    
    self.serviceHomeDelegate?.getServiceDirections(model: self.data[sender.tag])
    }
    
}
