//
//  HomeScreenTableViewCell.swift
//  MSU
//
//  Created by vectorform on 16/06/21.
//

import UIKit
import Foundation

class HomeScreenTableViewCell: UITableViewCell {

    @IBOutlet weak var cardView: CardView!
    @IBOutlet weak var vehicleImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var regNoLabel: UILabel!
    @IBOutlet weak var kmsLabel: UILabel!
    @IBOutlet weak var typeofServiceLabel: UILabel!
    @IBOutlet weak var seperatorLine: UIView!
    @IBOutlet weak var updateStatusButton: MSUButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.addViews()
    }

      
     func addViews() {
        usernameLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: false, size: 18.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
       
        regNoLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        kmsLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: false, size: 12.0, alignment: .center, bgColor: ColorConstants.appYellowColor, fontType: StringConstants.fontText)
        kmsLabel.layer.cornerRadius = 5
        kmsLabel.layer.masksToBounds = true
        
        typeofServiceLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        seperatorLine.backgroundColor = ColorConstants.underlineColor
        
        updateStatusButton.appButtonStyling(titleColor: ColorConstants.appSecondaryBlueColor, backgroundColor: ColorConstants.clear)
        
        updateStatusButton.titleLabel?.font = Font.vwheadfont.with(size: CGFloat(14.0), weight: .vwheadregular)
        self.layoutSubviews()
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
