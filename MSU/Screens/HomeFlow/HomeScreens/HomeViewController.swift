//
//  HomeViewController.swift
//  MSU
//
//  Created by vectorform on 14/06/21.
//

import UIKit
import SwiftyJSON
import ObjectMapper
import Alamofire
import Kingfisher

class HomeViewController: BaseViewController, IHomeViewController, ServiceHomeGetDirectionsButtonDelegate {
 
   

    private let presenter: IHomePresenter = HomePresenter()
    
    // MARK: - View Contoller Outlets
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var serviceCardView: UIView!
    @IBOutlet weak var upcomingLabel: UILabel!
    @IBOutlet weak var servicesTableView: UITableView!
    @IBOutlet weak var notificaitonCountLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollContentView: UIView!
    @IBOutlet weak var scrollContentViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var usernameTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var wipCardHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var carImageWidth: NSLayoutConstraint!
    @IBOutlet weak var wipCarImage: UIImageView!
    @IBOutlet weak var wipUsername: UILabel!
    @IBOutlet weak var wipRegNo: UILabel!
    @IBOutlet weak var wipKmsreading: UILabel!
    @IBOutlet weak var wipServiceType: UILabel!
    @IBOutlet weak var wipSeperatorView: UIView!
    @IBOutlet weak var wipUpdateButton: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var addButton: Floaty!

    @IBOutlet weak var noDataLabel: UILabel!
    // Count Outlets
    @IBOutlet weak var countView: UIView!
    @IBOutlet weak var countStackView: UIStackView!
    @IBOutlet weak var assignedView: CardView!
    @IBOutlet weak var assignedCount: UILabel!
    @IBOutlet weak var assignedLabel: UILabel!
    @IBOutlet weak var completedView: CardView!
    @IBOutlet weak var completedCount: UILabel!
    @IBOutlet weak var completedLabel: UILabel!
    @IBOutlet weak var missedView: CardView!
    @IBOutlet weak var missedCount: UILabel!
    @IBOutlet weak var missedLabel: UILabel!
    @IBOutlet weak var amountView: CardView!
    @IBOutlet weak var amountCount: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var todayMtdStackView: UIStackView!
    @IBOutlet weak var todayLabel: UILabel!
    @IBOutlet weak var mtdLabel: UILabel!
    
    
    private lazy var tableViewAdapter: HomeScreenTableViewAdaptor  = {
        HomeScreenTableViewAdaptor(tableView: self.servicesTableView)
    }()
    let scrollTopEdgeInsets:CGFloat = (SF.screenHeight/3) ///scrollView Top insets size
    var upcomingRequests: [ServiceRequests]?

    var isInProgressSR: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        noDataLabel.isHidden = true
        presenter.connectToView(view: self)
        layoutAddButton()
        applyStylings()
        presenter.getSettingsData()
        // Do any additional setup after loading the view.
     
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.fetchUserData()
       
        presenter.getServiceDataByDate()
        presenter.getDasboardCount()
        if(FacadeLayer.sharedInstance.isFromHealthCheck){
            FacadeLayer.sharedInstance.isFromHealthCheck = false
            presenter.presentInspectionReport()
        }
        todayLabel.applyLabelStyling(textColor: ColorConstants.solidWhite, isBold: true, size: 14.0, alignment: .center, bgColor: ColorConstants.appSecondaryBlueColor, fontType: StringConstants.fontHead)
        
        mtdLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: true, size: 14.0, alignment: .center, bgColor: ColorConstants.solidWhite, fontType: StringConstants.fontHead)
    }
    
    // MARK: - Apply Stylings to Objects
    /// This function is used for applying stylings on UI
    ///
    private func applyStylings(){
        wipUsername.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: false, size: 18.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        wipRegNo.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        wipKmsreading.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: false, size: 12.0, alignment: .center, bgColor: ColorConstants.appYellowColor, fontType: StringConstants.fontText)
        wipKmsreading.layer.cornerRadius = 5
        wipKmsreading.layer.masksToBounds = true
        wipServiceType.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        wipSeperatorView.backgroundColor = ColorConstants.underlineColor
        dateLabel.applyLabelStyling(textColor: ColorConstants.solidWhite, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
       
        usernameLabel.applyLabelStyling(textColor: ColorConstants.solidWhite, isBold: true, size: 30.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
       
        progressLabel.applyLabelStyling(textColor: ColorConstants.solidWhite, isBold: true, size: 16.0, alignment: .left, bgColor: ColorConstants.clear, fontType:  StringConstants.fontHead)
        
        upcomingLabel.applyLabelStyling(textColor: ColorConstants.appGreyColor, isBold: true, size: 16.0, alignment: .left, bgColor: ColorConstants.clear, fontType:  StringConstants.fontHead)
        
        notificaitonCountLabel.applyLabelStyling(textColor: ColorConstants.solidWhite, isBold: false, size: 12.0, alignment: .center, bgColor: ColorConstants.appSecondaryBlueColor, fontType:  StringConstants.fontText)
        
        notificaitonCountLabel.layer.cornerRadius = notificaitonCountLabel.frame.size.width / 2
        notificaitonCountLabel.layer.masksToBounds = true
        
//        bindServicesList(responseVal: JSON) // need to pass response data
        wipUpdateButton.addTarget(self, action: #selector(wipUpdateButtonAction), for: .touchUpInside)
        wipUpdateButton.titleLabel?.textColor = ColorConstants.appSecondaryBlueColor
        
        scrollView.delegate = self
        scrollView.contentInset = UIEdgeInsets(top: scrollTopEdgeInsets, left: 0, bottom: 0, right: 0)
        self.serviceCardView.isHidden = false
        self.progressLabel.isHidden = false
        self.profileImageView.layer.cornerRadius = profileImageView.frame.size.width / 2
        self.profileImageView.layer.masksToBounds = true
        
        let userImageTap =  UITapGestureRecognizer(target: self, action: #selector(userImageTapped(_:)))
        profileImageView.addGestureRecognizer(userImageTap)
        profileImageView.isUserInteractionEnabled = true
        
        dateLabel.text = Date().getFormattedDayDate(input: Date())
        noDataLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: true, size: 16.0, alignment: .center, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        noDataLabel.text = StringConstants.noDataText
        
        assignedCount.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: true, size: 14.0, alignment: .center, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        assignedLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: true, size: 14.0, alignment: .center, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        
        completedCount.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: true, size: 14.0, alignment: .center, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        completedLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: true, size: 14.0, alignment: .center, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        
        missedCount.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: true, size: 14.0, alignment: .center, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        missedLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: true, size: 14.0, alignment: .center, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        
        amountCount.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: true, size: 14.0, alignment: .center, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        amountLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: true, size: 14.0, alignment: .center, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        
        
        
        assignedLabel.text = StringConstants.assigned
        completedLabel.text = StringConstants.completed
        missedLabel.text = StringConstants.missed
        amountLabel.text = StringConstants.amount
        todayLabel.text = StringConstants.today
        mtdLabel.text = StringConstants.mtd
        
        let todayLabelTap =  UITapGestureRecognizer(target: self, action: #selector(todayLabelTapped(_:)))
        todayLabel.addGestureRecognizer(todayLabelTap)
        todayLabel.isUserInteractionEnabled = true
        
        let mtdLabelTap =  UITapGestureRecognizer(target: self, action: #selector(mtdLabelTapped(_:)))
        mtdLabel.addGestureRecognizer(mtdLabelTap)
        mtdLabel.isUserInteractionEnabled = true

    }
    
    // MARK: - User Interactions
    ///This function called to  add layout to plus button at bottom of screen
    ///
    
    func layoutAddButton() {
        addButton.buttonImage = UIImage(named: "addicon")
        addButton.hasShadow = false
        addButton.backgroundColor = .clear
        addButton.openAnimationType = .pop
        addButton.addItem("Vehicle HealthCheck", icon: UIImage(named: "healthCheckicon")) { tapped in
            self.vechicleHealthCheckClicked()
        }
        addButton.isHidden = true
//        addButton.addItem("Service Request", icon: UIImage(named: "serviceRequesticon")){tapped in
//            self.serviceRequestClicked()
//        }
    }
    
    // MARK: - Bind services list
    func bindServicesList(model: [ServiceRequests]) {
        self.upcomingRequests = model
        
        if(self.upcomingRequests?.count == 0){
            noDataLabel.isHidden = false
            scrollView.isHidden = true
        }
        else{
            noDataLabel.isHidden = true
            scrollView.isHidden = false
            self.scrollContentViewHeightConstraint.constant = CGFloat(model.count * 250)
            tableViewAdapter.serviceHomeDelegate = self
            tableViewAdapter.updateData(data: model)
            if(isInProgressSR)
            {
                self.scrollViewTopConstraint.constant = 250
            }
            else{
                self.scrollViewTopConstraint.constant = 100
            }
        }
    }
    
    // MARK: - Bind In Progress services list
    func bindInProgressData(model: ServiceRequests) {
        addButton.isHidden = true
        let baseUrl = BaseRequest()
        var baseUrlString = baseUrl.baseUrlString
        let imagePath = model.vehicleImage ?? ""
        
        if(imagePath.count != 0){
            let imgPath = imagePath
                baseUrlString += imgPath
            self.wipCarImage.kf.indicatorType = .activity
            let url = URL(string: baseUrlString)
                self.wipCarImage.kf.setImage(with: url)
            }
        
        self.scrollViewTopConstraint.constant = 160
        headerViewHeightConstraint.constant = 400
        
        self.wipRegNo.text = model.regNum ?? ""
        self.wipUsername.text = model.customerName ?? ""
        self.wipServiceType.text = model.serviceType ?? ""
        self.wipKmsreading.text = model.odometerReading ?? ""
        
        isInProgressSR = true
    }
    
    // MARK: - Hide inprogress view
 
    func hideInProgress(){
        addButton.isHidden = true
        self.serviceCardView.isHidden = true
        self.progressLabel.isHidden = true
        headerViewHeightConstraint.constant = 250
        self.scrollViewTopConstraint.constant = 100
        isInProgressSR = false
    }
    
    // MARK: - Set User data
    func userDetails(title: String, profilePic: String) {
        let baseUrl = BaseRequest()
        var baseUrlString = baseUrl.baseUrlString
        let imagePath = profilePic
        if(imagePath.count != 0){
            let imgPath = imagePath
                baseUrlString += imgPath
            self.profileImageView.kf.indicatorType = .activity
            let url = URL(string: baseUrlString)
                self.profileImageView.kf.setImage(with: url)
        }
        self.usernameLabel.text = title
    }
    
    // MARK: - Bind Todays Count
    func bindTodaysCount(model: DashboardCountModel) {
        assignedCount.text = String(describing: model.service_Assigned_Today ?? 0).removeOptionalFromString()
        completedCount.text = String(describing:model.service_Completed_Today ?? 0).removeOptionalFromString()
        missedCount.text =  String(describing:model.service_Missed_Today ?? 0).removeOptionalFromString()
        let amt = model.estimate_Amount_Today ?? 0
        let amtStr = amt.formatPoints(num: Double(amt))
        amountCount.text = String(describing:amtStr).removeOptionalFromString()
    }
    
    // MARK: - Bind MTD Count
    func bindMTDCount(model: DashboardCountModel){
        assignedCount.text = String(describing:model.service_Assigned_Month ?? 0).removeOptionalFromString()
        completedCount.text = String(describing:model.service_Completed_Month ?? 0).removeOptionalFromString()
        missedCount.text = String(describing:model.service_Missed_Month ?? 0).removeOptionalFromString()
        let amt = model.estimate_Amount_Month ?? 0
        let amtStr = amt.formatPoints(num: Double(amt))
        amountCount.text = String(describing:amtStr).removeOptionalFromString()
    }
    
  
  // MARK: - User Interactions
  // MARK: - Update Status WIP
    @objc func wipUpdateButtonAction(){
            presenter.updateStatusClicked()
    }
    
    // MARK: - Get Directions button clicked
    func getServiceDirections(model: ServiceRequests) {
        presenter.upComingServiceDetailsClicked(model: model)
    }
    
    // MARK: - Service Request Clicked
    func serviceRequestClicked(){
        presenter.serviceRequestClicked()
    }
    // MARK: - Vehicle Health Clicked
    func vechicleHealthCheckClicked(){
        presenter.vehicleHealthCheckClicked()
    }
    
    // MARK: - User Image tapped
    @objc func userImageTapped(_ sender:UITapGestureRecognizer){
        presenter.profileImageTapped()
    }
    
    // MARK: - Today Label tapped
    @objc func todayLabelTapped(_ sender:UITapGestureRecognizer){
        todayLabel.applyLabelStyling(textColor: ColorConstants.solidWhite, isBold: true, size: 14.0, alignment: .center, bgColor: ColorConstants.appSecondaryBlueColor, fontType: StringConstants.fontHead)
        
        mtdLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: true, size: 14.0, alignment: .center, bgColor: ColorConstants.solidWhite, fontType: StringConstants.fontHead)
        presenter.returnTodaysCount()
    }
    
    // MARK: - MTD Label tapped
    @objc func mtdLabelTapped(_ sender:UITapGestureRecognizer){
        todayLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: true, size: 14.0, alignment: .center, bgColor: ColorConstants.solidWhite, fontType: StringConstants.fontHead)
        
        mtdLabel.applyLabelStyling(textColor: ColorConstants.solidWhite, isBold: true, size: 14.0, alignment: .center, bgColor: ColorConstants.appSecondaryBlueColor, fontType: StringConstants.fontHead)
        
        presenter.returnMTDCount()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK: - UIScrollViewDelegate methods
extension HomeViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(!isInProgressSR){
            self.scrollViewTopConstraint.constant = 200
            return
        }
        let minHeight:CGFloat = self.topbarHeight
        let maxHeight:CGFloat = (SF.screenHeight/3)
        let yPos = self.scrollView.contentOffset.y
        let headerViewHeight = (maxHeight - yPos)-(maxHeight-minHeight)
     ///manage Header Height
       
        if (headerViewHeight > maxHeight) {
           
            UIView.animate(withDuration: 1, delay: 0.5, options: .curveEaseIn) {
             

                UIView.animate(withDuration: 0.5, animations: {() -> Void in
                    if(self.isInProgressSR){
                        print("called if")
                        self.carImageWidth.constant = 100.0
                        self.wipCarImage.isHidden = false
                        self.headerViewHeightConstraint.constant = 400
                        
                        self.wipSeperatorView.isHidden = false
                        self.scrollViewTopConstraint.constant = self.topbarHeight + 80
                    }
                    else{
                        print("called Ajay")
                        self.carImageWidth.constant = 0.0
                        self.headerViewHeightConstraint.constant = (max(headerViewHeight, minHeight)) + 70
                        self.wipSeperatorView.isHidden = true
                        self.scrollViewTopConstraint.constant = 100
                    }
                    self.view.layoutIfNeeded()
                    self.wipCarImage.frame = CGRect(x: 15.0, y: self.wipCarImage.frame.origin.y, width: self.wipCarImage.frame.size.width, height: self.wipCarImage.frame.size.height)
                }, completion: {(_ finished: Bool) -> Void in
                    
                })
                } completion: { _ in
            }

         } else {
             
            UIView.animate(withDuration: 1, delay: 0.5, options: .curveEaseIn) {
                UIView.animate(withDuration: 0.5, animations: {() -> Void in
                if(self.isInProgressSR){
                    print("called else")
                self.headerViewHeightConstraint.constant = (max(headerViewHeight, minHeight)) + 185
                self.wipSeperatorView.isHidden = true
                self.scrollViewTopConstraint.constant = self.topbarHeight + 120
                    self.carImageWidth.constant = 0.0
                    
                }
                else{
                    self.headerViewHeightConstraint.constant = 250
                    self.wipSeperatorView.isHidden = true
                    self.scrollViewTopConstraint.constant = 0
                    self.carImageWidth.constant = 0.0
                }
                
                    self.wipCarImage.frame = CGRect(x: 15.0, y: self.wipCarImage.frame.origin.y, width: self.wipCarImage.frame.size.width, height: self.wipCarImage.frame.size.height)
                    self.view.layoutIfNeeded()
                }, completion: {(_ finished: Bool) -> Void in
//
                })

//                self.wipUpdateButton.frame = CGRect(x: self.view.frame.size.width - 50, y: 50.0, width: self.wipUpdateButton.frame.size.width, height: self.wipUpdateButton.frame.size.height)
                } completion: { _ in
                    
            }

        }
        self.viewDidLayoutSubviews()
       
    }
}
