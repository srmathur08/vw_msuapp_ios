//
//  HomeContract.swift
//  MSU
//
//  Created by vectorform on 14/06/21.
//

import Foundation


protocol IHomeViewController: IBaseViewController {

    ///This function called to map user name
    ///
    ///
    ///   - Parameter title: string
    func userDetails(title: String, profilePic: String)
    
    ///This function called to map data in table view adaptor for upcoming SR's
    ///
    ///     Warning model should not be empty
    ///   - Parameter model: list of ServiceRequests
  
    func bindServicesList(model: [ServiceRequests])
    
    ///This function called to map data for inprogress SR
    ///
    ///     Warning model should not be empty
    ///   - Parameter model: must be ServiceRequests
    func bindInProgressData(model: ServiceRequests)
    
    ///This function called to hide the inprogress card if no in-progress SR is present
    ///
    ///
    func hideInProgress()
    
    /// This function called to bind todays count
    ///
    func bindTodaysCount(model: DashboardCountModel)
    
    /// This function called to bind MTD Count
    ///
    func bindMTDCount(model: DashboardCountModel)
}


protocol IHomePresenter: IBasePresenter {
    
    ///This function called on update status click from WIP card
    ///
    func updateStatusClicked()
    
    ///This function called to get directions from upcoming sr list
    ///
    ///     Warning model should not be empty
    ///  - Parameter model: must be ServiceRequests
    func upComingServiceDetailsClicked(model:ServiceRequests)
    
    ///This function called to push create service histrory view controller
    ///
    func serviceRequestClicked()
    
    ///This function called to push vehicle health check
    ///
    func vehicleHealthCheckClicked()
    
    ///This function called to get user details
    ///
    func fetchUserData()
    
    ///This function called to present Inspection report view controller
    ///
    func presentInspectionReport()
    
    ///This function called to get the sr for the current date.
    ///
    ///    Warning Date format should be dd/mm/yyyy
    ///
    func getServiceDataByDate()
    
    ///This function called to push user profile view controller
    ///
    func profileImageTapped()
    
    /// This function called for getting settings data
    ///
    func getSettingsData()
    
    /// This function called to get Dashboard Count
    ///
    func getDasboardCount()
    
    /// This function called to get TDY Dashboard Count
    ///
    func returnTodaysCount()
    
    /// This function called to get MTD Dashboard Count
    ///
    func returnMTDCount()
}

