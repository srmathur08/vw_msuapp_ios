//
//  HomePresenter.swift
//  MSU
//
//  Created by vectorform on 14/06/21.
//

import Foundation
import SwiftyJSON
import ObjectMapper

class HomePresenter: BasePresenter, IHomePresenter {

    var homeRouter: HomeRouter? {
        return navController as? HomeRouter
    }

    
    private weak var myView: IHomeViewController? {
        return self.view as? IHomeViewController
    }
    var inProgress: ServiceRequests?
    var dashboardModel: DashboardCountModel?
    // MARK: - WIP Update status clicked
    func updateStatusClicked() {
        if let modelVal = inProgress{
            homeRouter?.pushHomeToWIPController(animated: true, model: modelVal)
        }
    }
    // MARK: - Upcoming Service History Get Directions
    func upComingServiceDetailsClicked(model: ServiceRequests) {
        print("service History clicked")
        
        startVehicelHealthCheck(modelVal: model)
    }
    
    // MARK: - Start vehicle health check
    func startVehicelHealthCheck(modelVal: ServiceRequests){
//        if let model = modelVal{
            let postData: [String: AnyObject] = ["Srid": modelVal.srid as Any,
                                                 "StatusId": UpdateStatus.isServiceEngOutforRepair as Any,
                                                 "ServiceOther": "" as Any,
                                                 "Revenue" : "" as Any] as [String: AnyObject]
            FacadeLayer.sharedInstance.webserviceManager.genericAPICalls(requestParams: postData, apiPath: VFApiCalls.updateSRStatus) {   [weak self] (status, response) in
                guard let strongSelf = self else{ return }
                if(status){
                    strongSelf.myView?.removeLoader()
                    strongSelf.homeRouter?.pushHomeToWIPController(animated: true, model: modelVal)
                }
                else{
                    strongSelf.myView?.removeLoader()
                       let errorVal = JSON(response as Any)
                       let errorString = "\(errorVal)"
                       print(errorString)
                       var errormsg: String = ""
                       if(errorString != ErrorStringConstants.noInternet){
                               errormsg = errorVal["Reason"].stringValue
                           if(errormsg == ""){
                               errormsg = ErrorStringConstants.defaultError
                           }
                       }
                       else{
                           errormsg = ErrorStringConstants.noInternet
                       }
                    strongSelf.myView?.displayDefaultAlertForError(errorTitle: ErrorStringConstants.errorTitle, errorDescription: errormsg)
                    }
            }
//        }
    }
    // MARK: - Create Service Request
    func serviceRequestClicked() {
        print("service request clicked")
        homeRouter?.pushHometoCreateServiceController(animated: true)
    }
    // MARK: - Create Vehicle Health Check
    func vehicleHealthCheckClicked() {
        print("vehicle health clicked")
        if let inProgressVal = inProgress{
            if inProgressVal.isVCCompleted{
                homeRouter?.pushHomeToWIPController(animated: true, model: inProgressVal)
            }
            else{
                homeRouter?.pushHomeToVehicleHealthCheck(animated: true, model: inProgressVal)
            }
        }
        else{
            myView?.displayDefaultAlertForError(errorTitle: ErrorStringConstants.errorTitle, errorDescription: ErrorStringConstants.inProgressError)
        }
    }
    // MARK: -Profile image tapped
    func profileImageTapped(){
        homeRouter?.pushHomeToUserProfileController(animated: true)
    }
    
    
    // MARK: - Present inspection report
    func presentInspectionReport() {
        homeRouter?.pushVehicleInspectionController(animated: true)
    }
    // MARK: - Fetch User Data
    func fetchUserData() {
        let userArray = DefaultsWrapper.username.split(separator: " ")
        var username: String = ""
        if(userArray.count > 0){
            username = String(describing: userArray[0]).removeOptionalFromString()
        }
        
        myView?.userDetails(title: username, profilePic: DefaultsWrapper.userProfilePic)
    }
    
    // MARK: - Get Services data by date
    func getServiceDataByDate(){
        self.myView?.showLoader()
        let dealId = DefaultsWrapper.dealershipId
        let  currentDate = Date()
        let currentDateStr = currentDate.getFormattedDate(input: currentDate)
        let postData: [String : AnyObject]  = ["DealershipId":dealId as Any,
                                               "Assignee": DefaultsWrapper.loginUserId as Any
                                                ] as [String: AnyObject]
        
        FacadeLayer.sharedInstance.webserviceManager.genericAPICalls(requestParams: postData, apiPath: VFApiCalls.getServiceRequests) { [weak self] (status, response) in
            guard let strongSelf = self else{ return }
            if(status){
                let responseVal = JSON(response as Any)
                if let model = Mapper<HomeServicesModel>().map(JSONObject:responseVal.dictionaryObject){
                    if let serviceModel = model.serviceRequests{
                        self?.filterResponse(model: serviceModel)
                    }
                }
                strongSelf.myView?.removeLoader()
            }
            else{
                strongSelf.myView?.removeLoader()
                let errorVal = JSON(response as Any)
                let errorString = "\(errorVal)"
                print(errorString)
                var errormsg: String = ""
                if(errorString != ErrorStringConstants.noInternet){
                        errormsg = errorVal["Reason"].stringValue
                    if(errormsg == ""){
                        errormsg = ErrorStringConstants.defaultError
                    }
                }
                else{
                    errormsg = ErrorStringConstants.noInternet
                }
                strongSelf.myView?.hideInProgress()
                strongSelf.myView?.displayDefaultAlertForError(errorTitle: ErrorStringConstants.errorTitle, errorDescription: errormsg)
            }
        }
    }
    // MARK: - Get DashboardCount
    func getDasboardCount() {
        self.myView?.showLoader()
        let postData: [String : AnyObject]  = ["Assignee":DefaultsWrapper.loginUserId as Any] as [String: AnyObject]
        
        FacadeLayer.sharedInstance.webserviceManager.genericAPICalls(requestParams: postData, apiPath: VFApiCalls.getDashboardCount) { [weak self] (status, response) in
            guard let strongSelf = self else{ return }
            if(status){
                strongSelf.myView?.removeLoader()
                let responseVal = JSON(response as Any)
                if let model = Mapper<DashbordModel>().map(JSONObject:responseVal.dictionaryObject){
                    strongSelf.dashboardModel = model.serviceRequests!
                    strongSelf.returnTodaysCount()
                }
            }
            else{
                strongSelf.myView?.removeLoader()
            }
        }
    }
    
    // MARK: - Return Todays count
    func returnTodaysCount() {
        if let viewModel = self.dashboardModel{
            self.myView?.bindTodaysCount(model: viewModel)
        }
     
    }
    // MARK: - FReturn MTD count
    func returnMTDCount() {
        if let viewModel = self.dashboardModel{
            self.myView?.bindMTDCount(model: viewModel)
        }
    }
    // MARK: - Filter out the sr response
    ///This function called to filter the upcoming and in progress SR's
    ///  completed SR's should be flitered out
    ///
    ///         Warning model should not be empty
    /// - Parameter model: list of ServiceRequests
    
    func filterResponse(model: [ServiceRequests]){

        var compFilterArray = model.filter { val in
            val.isCompleted != true
        }

        let index = compFilterArray.firstIndex(where: { val in
            val.isStarted == true
        })
        if let indexVal = index{
            inProgress = compFilterArray[0]
            if let inProgressVal = inProgress{
                myView?.bindInProgressData(model: inProgressVal)
            }
            else{
                myView?.hideInProgress()
            }
            compFilterArray.remove(at: indexVal)
        }
        else{
            myView?.hideInProgress()
        }
        myView?.bindServicesList(model: compFilterArray)
    }
    
    // MARK: - Get Settings Data
    func getSettingsData(){
        let postString: [String : String]  = [:]
        FacadeLayer.sharedInstance.webserviceManager.genericAPICallsWithoutParam(requestParams: postString as [String : AnyObject], apiPath: VFApiCalls.getSettings) { [weak self] (status, response) in
            guard let strongSelf = self else{
                return
            }
            if(status){
                let responseVal = JSON(response as Any)
                if let settingsModel = Mapper<SettingsModel>().map(JSONObject:responseVal.dictionaryObject){
                    if let dataModel = settingsModel.data{
                        strongSelf.serializeSettingsData(dataObj: dataModel)
                    }
                }
            }
            else{
                strongSelf.myView?.removeLoader()
                let errorVal = JSON(response as Any)
                let errorString = "\(errorVal)"
                print(errorString)
                var errormsg: String = ""
                if(errorString != ErrorStringConstants.noInternet){
                    errormsg = errorVal["Reason"].stringValue
                }
                else{
                    errormsg = ErrorStringConstants.noInternet
                }
                strongSelf.myView?.displayDefaultAlertForError(errorTitle: ErrorStringConstants.errorTitle, errorDescription: errormsg)
            }
        }
    }
    
    //MARK: - Serialise Settings
    func serializeSettingsData(dataObj: [SettingsData]){
        var versionVal = ""
        var storeLinkVal = ""
        
        for (_, elementObj) in dataObj.enumerated(){
           
            let name = elementObj.KeyVal
            switch name {
            case StringConstants.iOSVersion:
                versionVal = elementObj.valueVal ?? ""
            case StringConstants.iTunesLink:
                storeLinkVal = elementObj.valueVal ?? ""
            default:
                print("")
            }
        }
        self.checkForVersionUpdate(version: versionVal, storeUrl: storeLinkVal)
    }
    //MARK: - Check Version Update
    func checkForVersionUpdate(version: String, storeUrl: String){
        let appVersionDict = Bundle.main.infoDictionary
     
        if let dict = appVersionDict{
            let appVersion = dict["CFBundleShortVersionString"] as! String
            if appVersion.compare(version, options: .numeric) == .orderedAscending {

                let alertController = UIAlertController.init(title: ErrorStringConstants.defaultTitle, message: ErrorStringConstants.versionUpdate, preferredStyle: .alert)
                alertController.addAction(UIAlertAction.init(title: "Update", style: .default, handler: { (action) in
                    UIApplication.shared.open(URL.init(string: storeUrl)!, options:[:], completionHandler: nil)
                }))
                self.myView?.present(alertController, animated: true, completion: nil)
            }
        }
    }

    
}
