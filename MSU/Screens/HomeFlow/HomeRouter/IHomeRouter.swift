//
//  IHomeRouter.swift
//  MSU
//
//  Created by vectorform on 14/06/21.
//

import Foundation

protocol IHomeRouter: BaseNavigationController {
    ///This function called to push home view controller
    ///
    ///   - Parameter animated: true/false
    func pushHomeScreenController(animated: Bool)
    
    ///This function called from home to wip controller
    ///
    ///     Warning model should not be empty
    ///  - Parameter animated: true/false
    ///  - Parameter model: ServiceRequests
    func pushHomeToWIPController(animated: Bool, model: ServiceRequests)
    
    ///This function called from home to vehicle health check  controller
    ///
    ///     Warning model should not be empty
    ///   - Parameter animated: true/false
    ///   - Parameter model: ServiceRequests
    func pushHomeToVehicleHealthCheck(animated: Bool, model: ServiceRequests)
    
    ///This function called from wip to vehicle health check  controller
    ///
    ///   - Parameter animated: true/false
    func pushWipToVehicleHealthCheck(animated: Bool)
    
    ///This function called from push home to service history view controller
    ///
    ///       Warning vinNumber should not be empty
    ///   - Parameter animated: true/false
    ///   - Parameter vinNumber: String
    ///     - Parameter srid: Integer
    func pushServiceHistoryController(animated: Bool, vinNumber: String, srID: Int)
    
    ///This function called from push service history to  service details view controller
    ///
    ///        Warning model should not be empty
    ///   - Parameter animated: true/false
    ///   - Parameter model: Service_History_Data
    func pushServiceHistoryDetailsController(animated: Bool, model: Service_History_Data, score: String)
    
    ///This function called from push Vehicle health check
    ///
    ///        Warning model should not be empty
    ///   - Parameter animated: true/false
    ///   - Parameter model: ServiceRequests
    func pushVehicleHealthCheckController(animated: Bool, model: ServiceRequests)
    
    ///This function called from push health preview view controller
    ///
    ///   - Parameter animated: true/false
    func pushVehicleInspectionController(animated: Bool)
    
    ///This function called from push  health preview details
    ///
    ///       Warning model should not be empty
    ///   - Parameter animated: true/false
    ///   - Parameter model: VehicleHealthCheckModel
    func pushVehicleInspectionDetailsController(animated: Bool, model: VehicleHealthCheckModel)
    
    ///This function called from pushhealth check 360 view controller
    ///
    ///       Warning markersModel,signModel should not be empty
    ///   - Parameter animated: true/false
    ///   - Parameter markersModel: VehicleHealthCheckModel
    ///   - Parameter signModel: VehicleHealthCheckModel
    func pushVehickeInspection360DetailsController(animated: Bool, markersModel: VehicleHealthCheckModel, signModel: VehicleHealthCheckModel)
  
    ///This function called from pushhealth  check images view controller
    ///
    ///     Warning vehicleCheckUpId should not be zero
    ///   - Parameter animated: true/false
    ///   - Parameter vehicleCheckUpId: Int value
    func pushVehicleImagesViewController(animated: Bool, vehicleCheckUpId: Int)
    
    ///This function called to get Inprogress SR
    ///
    ///  - Returns  ServiceRequests
    func getInProgressSR() -> ServiceRequests?
    
    ///This function called from home to create service  controller
    ///
    ///   - Parameter animated: true/false
    func pushHometoCreateServiceController(animated: Bool)
    
    ///This function called from home to User profile  controller
    ///
    ///  - Parameter animated: true/false
    func pushHomeToUserProfileController(animated: Bool)
    
    ///This function called to present Image view contoller to view images in full view
    ///
    ///     Warning images should not be empty
    ///   - Parameter animated: true/false
    ///   - Parameter images: List of images as  string
    func presentImageViewController(index: Int, images: [String], reportImages:[VehicleCheckupAssets], isModel: Bool)
    func getVehicleCheckUpId() -> Int
    
    ///This function called from  User profile to Notification controller
    ///
    ///  - Parameter animated: true/false
    func pushUserProfileToNotification(animated: Bool)
}

