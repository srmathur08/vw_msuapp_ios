//
//  HomeRouter.swift
//  MSU
//
//  Created by vectorform on 14/06/21.
//

import Foundation

class HomeRouter: BaseNavigationController, IHomeRouter {

    init() {
        super.init(nibName: nil, bundle: nil)
        pushHomeScreenController(animated: false)
    }
    var InProgressRequests: ServiceRequests?
    var vehicleChekUpIdVal: Int = 0
    // MARK: - Push Home Screen Controller
    func pushHomeScreenController(animated: Bool) {
        let controller = HomeViewController.instantiate(fromAppStoryboard: .HomeStoryboard)
        controller.modalPresentationStyle = .fullScreen
        pushViewController(controller, animated: animated)
    }

    // MARK: - Push Service History Controller
    func pushServiceHistoryController(animated: Bool, vinNumber: String, srID: Int){
        let controller = ServiceHistoryHomeViewController.instantiate(fromAppStoryboard: .HomeStoryboard)
        controller.vinNumber = vinNumber
        controller.serviceRequestId = srID
        controller.modalPresentationStyle = .fullScreen
        pushViewController(controller, animated: animated)
    }
    
    // MARK: - Push Service History Details Controller
    func pushServiceHistoryDetailsController(animated: Bool, model: Service_History_Data, score: String){
        let controller = ServiceHistoryDetailsViewController.instantiate(fromAppStoryboard: .HomeStoryboard)
        controller.serviceHistory = model
        controller.feedbackScore = score
        controller.modalPresentationStyle = .fullScreen
        pushViewController(controller, animated: animated)
    }
    
    // MARK: - Push Vehicle Health Check Controller
    func pushVehicleHealthCheckController(animated: Bool, model: ServiceRequests) {
        InProgressRequests = model
        let controller = VehicleHealthCheckViewController.instantiate(fromAppStoryboard: .VehicleHealthCheckStoryboard)
        controller.modalPresentationStyle = .fullScreen
        pushViewController(controller, animated: animated)
    }
    
    // MARK: - Push Vehicle Inspection Controller
    func pushVehicleInspectionController(animated: Bool) {
        let controller = HealthPreviewViewController.instantiate(fromAppStoryboard: .VehicleHealthCheckStoryboard)
        controller.modalPresentationStyle = .fullScreen
        if let healthCheckModel = FacadeLayer.sharedInstance.getHealthCheckModel() {
            controller.vehicleModel = healthCheckModel
        }
        pushViewController(controller, animated: animated)
    }
    
   
    // MARK: - Push Vehicle Inspection Details Controller
    
    func pushVehicleInspectionDetailsController(animated: Bool, model: VehicleHealthCheckModel) {
        let controller = ReportDetailsViewController.instantiate(fromAppStoryboard: .VehicleHealthCheckStoryboard)
        controller.vehicleModel = model
        pushViewController(controller, animated: animated)
    }
    
    // MARK: - Push Vehicle Inspection 360 Details Controller
    func pushVehickeInspection360DetailsController(animated: Bool, markersModel: VehicleHealthCheckModel, signModel: VehicleHealthCheckModel){
        let controller = ReportsDetails360ViewController.instantiate(fromAppStoryboard: .VehicleHealthCheckStoryboard)
        controller.signatureModel = signModel
        controller.image360Model = markersModel
        pushViewController(controller, animated: animated)
    }
    
    // MARK: - Push Vehicle Images Controller
    func pushVehicleImagesViewController(animated: Bool, vehicleCheckUpId: Int) {
        self.vehicleChekUpIdVal = vehicleCheckUpId
        let controller = VehicleImagesViewController.instantiate(fromAppStoryboard: .VehicleHealthCheckStoryboard)
        controller.modalPresentationStyle = .fullScreen
        controller.vehicleCheckUpIdVal = vehicleCheckUpId
        pushViewController(controller, animated: animated)
    }
    
    // MARK: - Get In Progress SR Details
    func getInProgressSR() -> ServiceRequests? {
        return self.InProgressRequests
    }
    
    ///This function called to get vehicleChekUpId
    ///
    /// - Returns  vehicleChekUpIdVal: Int value
    
    func getVehicleCheckUpId() -> Int{
       return vehicleChekUpIdVal
    }
    
    // MARK: - Push Home to WIP controller
    func pushHomeToWIPController(animated: Bool, model: ServiceRequests) {
        InProgressRequests = model
        let controller = WorkInProgressViewController.instantiate(fromAppStoryboard: .HomeStoryboard)
        controller.modalPresentationStyle = .fullScreen
        pushViewController(controller, animated: animated)
    }
    
    func pushHomeToWIPControllerTest(animated: Bool) {
        let controller = WorkInProgressViewController.instantiate(fromAppStoryboard: .HomeStoryboard)
        controller.modalPresentationStyle = .fullScreen
        pushViewController(controller, animated: animated)
    }
    // MARK: - Push Home to vehicle health check
    func pushHomeToVehicleHealthCheck(animated: Bool, model: ServiceRequests) {
        InProgressRequests = model
        let controller = VehicleHealthCheckViewController.instantiate(fromAppStoryboard: .VehicleHealthCheckStoryboard)
        controller.modalPresentationStyle = .fullScreen
        pushViewController(controller, animated: animated)
    }
    
    // MARK: - Push WIP to vehicle health check
    func pushWipToVehicleHealthCheck(animated: Bool) {
        let controller = VehicleHealthCheckViewController.instantiate(fromAppStoryboard: .VehicleHealthCheckStoryboard)
        controller.modalPresentationStyle = .fullScreen
        pushViewController(controller, animated: animated)
    }
    
    // MARK: - Push HOME to Create Service
    func pushHometoCreateServiceController(animated: Bool) {
        let controller = CreateServiceViewController.instantiate(fromAppStoryboard: .HomeStoryboard)
        controller.modalPresentationStyle = .fullScreen
        pushViewController(controller, animated: animated)
    }
    
    // MARK: - Push HOME to User Profile
    func pushHomeToUserProfileController(animated: Bool)
    {
        let controller = UserProfileViewController.instantiate(fromAppStoryboard: .HomeStoryboard)
        controller.modalPresentationStyle = .fullScreen
        pushViewController(controller, animated: animated)
    }

    // MARK: - Push user profile to notification
    func pushUserProfileToNotification(animated: Bool)
    {
        let controller = NotificationsViewController.instantiate(fromAppStoryboard: .HomeStoryboard)
        controller.modalPresentationStyle = .fullScreen
        pushViewController(controller, animated: animated)
    }
    // MARK: - Present image view controller
    func presentImageViewController(index: Int, images: [String], reportImages:[VehicleCheckupAssets], isModel: Bool)
    {
        let controller = ImagePreviewViewController.instantiate(fromAppStoryboard: .HomeStoryboard)
        controller.modalPresentationStyle = .fullScreen
        controller.imagesArray = images
        controller.selectedIndex = index
        controller.reportImagesModel = reportImages
        controller.isFromModels = isModel
       present(controller, animated: true, completion: nil)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
