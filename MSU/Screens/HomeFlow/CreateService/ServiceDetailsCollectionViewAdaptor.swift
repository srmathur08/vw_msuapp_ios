//
//  ServiceDetailsCollectionViewAdaptor.swift
//  MSU
//
//  Created by vectorform on 26/08/21.
//

import Foundation
import UIKit

class ServiceDetailsCollectionViewAdaptor: NSObject, ICollectionViewAdapter, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    
    
    var collectionView: UICollectionView
    typealias DataType = ServiceDetailsModel
    var data: [DataType] = []
    
    private let spacing:CGFloat = 5.0
    required init(collectionView: UICollectionView) {
        self.collectionView = collectionView
        self.collectionView.backgroundColor = ColorConstants.clear
        super.init()
        self.collectionView.register(UINib(nibName: "ServiceDetailsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ServiceDetailsCollectionViewCell")
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.allowsMultipleSelection = true
        
        let layout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing)
            layout.minimumLineSpacing = spacing
            layout.minimumInteritemSpacing = spacing
            self.collectionView.collectionViewLayout = layout
        
    }
    
    // MARK: - Update table view with model
    ///This function called to map list of SR in table view
    ///
    ///     Warning data should not be empty
    ///   - Parameter data: list of ServiceDetailsModel
    func updateData(data: [ServiceDetailsModel]) {
        self.data = data
        collectionView.reloadData()
    }
    
    // MARK: - Collection view delegate methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell: ServiceDetailsCollectionViewCell = collectionView.dequeueReusableCell(
            withReuseIdentifier: "ServiceDetailsCollectionViewCell",
            for: indexPath) as? ServiceDetailsCollectionViewCell {
            
            let anObj = self.data[indexPath.item]
            cell.serviceName.text = anObj.titleName
            cell.serviceImageView.image = UIImage(named: anObj.imageName)
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfItemsPerRow:CGFloat = 2
        let spacingBetweenCells:CGFloat = 20
        let totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells)
        let width = (collectionView.bounds.width - totalSpacing)/numberOfItemsPerRow
        return CGSize(width: width, height: width - 25)
      }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? ServiceDetailsCollectionViewCell{
            cell.toggleSelected()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? ServiceDetailsCollectionViewCell{
            cell.toggleSelected()
        }
    }
}
