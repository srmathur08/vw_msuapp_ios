//
//  CreateServicePresenter.swift
//  MSU
//
//  Created by vectorform on 26/08/21.
//

import Foundation
import UIKit
import SwiftyJSON

class CreateServicePresenter: BasePresenter, ICreateServicePresenter{
 
    var homeRouter: HomeRouter? {
        return navController as? HomeRouter
    }

    private weak var myView: ICreateServiceViewController? {
        return self.view as? ICreateServiceViewController
    }
    
    // MARK: - Create service details model
    func createServiceDetailsModel(){
        var serviceModel: [ServiceDetailsModel] = [ServiceDetailsModel]()
        serviceModel.append(ServiceDetailsModel(imageName: "oilChange", titleName: "Oil Change")!)
        serviceModel.append(ServiceDetailsModel(imageName: "brakes", titleName: "Brakes")!)
        serviceModel.append(ServiceDetailsModel(imageName: "tyreCheck", titleName: "Tyre Check")!)
        serviceModel.append(ServiceDetailsModel(imageName: "battery", titleName: "Battery Check")!)
        
        self.updateCollectionViewData(modelVal: serviceModel)
    }
    
    // MARK: - Update collection view data
    func updateCollectionViewData(modelVal: [ServiceDetailsModel]){
        myView?.bindCollectionData(model: modelVal)
    
    }
    
    // MARK: - Close button action
    func closeButtonClicked()
    {
        homeRouter?.didSelectBack()
    }
}
