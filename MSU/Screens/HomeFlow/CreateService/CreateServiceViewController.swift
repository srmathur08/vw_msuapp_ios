//
//  CreateServiceViewController.swift
//  MSU
//
//  Created by vectorform on 26/08/21.
//

import UIKit

class CreateServiceViewController: BaseViewController, ICreateServiceViewController {

    // MARK: - View Contoller Outlets
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var sectionLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewContentView: UIView!
    @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var customerNameInputView: MSUInputTextFields!
    @IBOutlet weak var vehicelModelInputView: MSUInputTextFields!
    @IBOutlet weak var registrationNumberInputView: MSUInputTextFields!
    @IBOutlet weak var invoiceAmountInputView: MSUInputTextFields!
    @IBOutlet weak var durationInputView: MSUInputTextFields!
    @IBOutlet weak var serviceDetailsCollectionView: UICollectionView!

 
    @IBOutlet weak var createServiceButton: MSUButton!
    private let presenter: ICreateServicePresenter = CreateServicePresenter()
    
    private lazy var collectionViewAdapter: ServiceDetailsCollectionViewAdaptor = {
        ServiceDetailsCollectionViewAdaptor(collectionView: self.serviceDetailsCollectionView)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.connectToView(view: self)
        // Do any additional setup after loading the view.
        applyStylings()
        presenter.createServiceDetailsModel()
    }
    
    
    // MARK: - ApplyStylings to objects
    func applyStylings(){
        sectionLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: true, size: 18.0, alignment: .center, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        
        customerNameInputView.textInputView( hintText: "Customer Name", isSecureTextEntry: false, returnKeyType: UIReturnKeyType.next, keyboardtype: UIKeyboardType.default, textColor: ColorConstants.appPrimaryColor, tintColor: ColorConstants.appPrimaryColor, lineColor: ColorConstants.underlineColor)
        customerNameInputView.inputTextfield.frame = CGRect(x: customerNameInputView.frame.origin.x, y: customerNameInputView.frame.origin.y, width: customerNameInputView.frame.size.width, height: customerNameInputView.frame.size.height)
        
        vehicelModelInputView.textInputView( hintText: "Vehicle Model", isSecureTextEntry: false, returnKeyType: UIReturnKeyType.next, keyboardtype: UIKeyboardType.default, textColor: ColorConstants.appPrimaryColor, tintColor: ColorConstants.appPrimaryColor, lineColor: ColorConstants.underlineColor)
        vehicelModelInputView.inputTextfield.frame = CGRect(x: customerNameInputView.frame.origin.x, y: customerNameInputView.frame.origin.y, width: customerNameInputView.frame.size.width, height: customerNameInputView.frame.size.height)
        
        registrationNumberInputView.textInputView( hintText: "Vehicle Registration Number", isSecureTextEntry: false, returnKeyType: UIReturnKeyType.next, keyboardtype: UIKeyboardType.default, textColor: ColorConstants.appPrimaryColor, tintColor: ColorConstants.appPrimaryColor, lineColor: ColorConstants.underlineColor)
        registrationNumberInputView.inputTextfield.frame = CGRect(x: registrationNumberInputView.frame.origin.x, y: registrationNumberInputView.frame.origin.y, width: registrationNumberInputView.frame.size.width, height: registrationNumberInputView.frame.size.height)
        
        invoiceAmountInputView.textInputView( hintText: "Invoice Amount", isSecureTextEntry: false, returnKeyType: UIReturnKeyType.next, keyboardtype: UIKeyboardType.default, textColor: ColorConstants.appPrimaryColor, tintColor: ColorConstants.appPrimaryColor, lineColor: ColorConstants.underlineColor)
        invoiceAmountInputView.inputTextfield.frame = CGRect(x: invoiceAmountInputView.frame.origin.x, y: invoiceAmountInputView.frame.origin.y, width: 160.0, height: invoiceAmountInputView.frame.size.height)
        
        durationInputView.textInputView( hintText: "Duration", isSecureTextEntry: false, returnKeyType: UIReturnKeyType.next, keyboardtype: UIKeyboardType.default, textColor: ColorConstants.appPrimaryColor, tintColor: ColorConstants.appPrimaryColor, lineColor: ColorConstants.underlineColor)
        durationInputView.inputTextfield.frame = CGRect(x: durationInputView.frame.origin.x, y: durationInputView.frame.origin.y, width: 160.0, height: durationInputView.frame.size.height)
        
        createServiceButton.appButtonStyling(titleColor: ColorConstants.solidWhite, backgroundColor: ColorConstants.appSecondaryBlueColor)
        
        scrollViewHeightConstraint.constant = 700.0
    }
    
    // MARK: - Bind collection view data
    func bindCollectionData(model: [ServiceDetailsModel]){
        collectionViewAdapter.updateData(data: model)
    }
    
    // MARK: - User Interactions
    // MARK: - Close button action
    @IBAction func closeButtonAction(_ sender: UIButton) {
        presenter.closeButtonClicked()
    }
    
    // MARK: - create service action
    @IBAction func createServiceAction(_ sender: MSUButton) {
        presenter.closeButtonClicked()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
