//
//  ServiceDetailsCollectionViewCell.swift
//  MSU
//
//  Created by vectorform on 26/08/21.
//

import UIKit

class ServiceDetailsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var serviceImageView: UIImageView!
    @IBOutlet weak var serviceName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        serviceName.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: false, size: 14.0, alignment: .center, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
    }
    
    func toggleSelected ()
       {
           if (isSelected){
               checkImage.image = UIImage(named: "bluechecked")
           }else {
               checkImage.image = UIImage(named: "blueunchecked")
           }
       }

}
