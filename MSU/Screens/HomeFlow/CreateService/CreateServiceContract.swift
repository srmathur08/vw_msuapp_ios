//
//  CreateServiceContract.swift
//  MSU
//
//  Created by vectorform on 26/08/21.
//

import Foundation

protocol ICreateServiceViewController: IBaseViewController {
    ///This function called to bind service details collection view
    ///
    ///     Warning model must not be empty
    ///   - Parameter model: list of ServiceDetailsModel
    func bindCollectionData(model: [ServiceDetailsModel])
}

protocol ICreateServicePresenter: IBasePresenter {
    
    ///This function called to pop back view controller
    ///
    func closeButtonClicked()
    
    ///This function called to create service details model
    ///
    func createServiceDetailsModel()
}
