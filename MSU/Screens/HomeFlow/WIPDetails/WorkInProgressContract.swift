//
//  workInProgressContract.swift
//  MSU
//
//  Created by vectorform on 19/07/21.
//

import Foundation
import UIKit

protocol IWorkInProgressViewController: IBaseViewController {
    ///This function called to bind SR model
    ///
    ///     Warning model should not be empty
    ///   - Parameter model: ServiceRequests
    func bindSRModel(model: ServiceRequests)
    
    ///This function called to update UI after health check
    ///
    ///     Warning input should not be empty
    ///   - Parameter model: true/false
    func updateUIAfterHealthCheck(input: Bool)
    
    ///This function called to bind SR model
    ///
    ///     Warning input should not be empty
    ///   - Parameter input: String
    func showError(input: String)
    
    ///This function called to bind SR complaints list
    ///
    ///     Warning model should not be empty
    ///   - Parameter model: ServiceRequests
    func bindServicesComplaintsList(model: [ComplaintModel])
    
    
    ///This function called to update UI after health check
    ///
    ///     Warning input should not be empty
    ///   - Parameter model: true/false
    func vehicleHealthCheckCompleted()
    
}


protocol IWorkInProgressPresenter: IBasePresenter {
    ///This function called to map in progress data to UI
    ///
    ///
    func viewCreated()
    
    ///This function called to pop the view controller
    ///
    ///
    func closeButtonClicked()
    
    ///This function called to push service history view controller
    ///
    ///     Warning vin must be a string and should not be empty
    ///   - Parameter vin: String
    func serviceHistoryClicked(vin: String)
    
    ///This function called to push svehicle health check view controller
    ///
    ///
    func showVehicleHealthCheck()
    
    ///This function called to bind in progress data to UI
    ///
    ///
    func getSRModel()
    
    ///This function called to mark the service as completed
    ///
    ///
    func markServiceCompleted(others: String, revenue: String)
    

    ///This function called to get the Checked complaints list
    ///
    ///
    func getCheckedComplaintsList(model: [ComplaintModel])
    
    ///This function called to when service details clicked
    ///
    ///
    func serviceDetailsClicked()
    
    ///This function called to update service details
    ///
    ///
    func updateServiceDetailsToAPI(others: String, revenue: String)
}
