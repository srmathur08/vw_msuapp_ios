//
//  WorkInProgressViewController.swift
//  MSU
//
//  Created by vectorform on 19/07/21.
//

import UIKit
import MapKit
import Alamofire
import GoogleMaps
import GooglePlaces
import CoreLocation


class WorkInProgressViewController: BaseViewController, IWorkInProgressViewController, ServiceComplaintsCheckboxDelegate, UITextViewDelegate {
 


    // MARK: - View Contoller Outlets
//    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var bottomCardView: CardView!
    
    @IBOutlet weak var bottomCardViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var otherViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var complaintsViewHeightConstraint: NSLayoutConstraint!
   
    @IBOutlet weak var vehicleHealthcheckButtonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var slidingViewHeightConstraint: NSLayoutConstraint!
   
    
    @IBOutlet weak var buttonsBottmViewheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonBottomView: UIView!
    @IBOutlet weak var UserDetailsView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var regNoLabel: UILabel!
    @IBOutlet weak var serviceTypeLabel: UILabel!
    @IBOutlet weak var userDetailsSeperator: UIView!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var serviceComplaintsView: UIView!
    @IBOutlet weak var serviceComplaintsHeaderLabel: UILabel!
    @IBOutlet weak var viewHistoryButton: MSUButton!
    @IBOutlet weak var viewHistoryBottomLine: UIView!
    @IBOutlet weak var serviceComplaintsTableView: UITableView!
//    @IBOutlet weak var firstComplaintView: UIView!
//    @IBOutlet weak var firstComplaintCheckbox: UIButton!
//    @IBOutlet weak var firstComplaintNameLabel: UILabel!
    @IBOutlet weak var secondComplaintView: UIView!
    @IBOutlet weak var secondComplaintCheckbox: UIButton!
    @IBOutlet weak var secondComplaintNameLabel: UILabel!
    @IBOutlet weak var thirdComplaintView: UIView!
    @IBOutlet weak var thirdComplaintCheckbox: UIButton!
    @IBOutlet weak var thirdComplaintNameLabel: UILabel!
    @IBOutlet weak var otherView: UIView!
    @IBOutlet weak var otherServicesBottomView: UIView!
    @IBOutlet weak var otherServicesHeaderLabel: UILabel!
    @IBOutlet weak var vehicleHealthCheckButton: MSUButton!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var othersInputField: MSUInputTextFields!
    @IBOutlet weak var slideToOpenView: MTSlideToOpenView!
    @IBOutlet weak var typeLabel: UILabel!
    
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var othersLabel: UILabel!
    @IBOutlet weak var othersTextView: UITextView!
    @IBOutlet weak var serviceDetailsButton: MSUButton!
    @IBOutlet weak var serviceDetailsBottomView: UIView!
    @IBOutlet weak var serviceDetailsView: UIView!
    @IBOutlet weak var serviceDetailsDoneButton: MSUButton!
    
    @IBOutlet weak var serviceComplaintsErrorLabel: UILabel!
    
    @IBOutlet weak var serviceDetailsHeaderLabel: UILabel!
    @IBOutlet weak var repairEstimateInputTextField: MSUInputTextFields!
    @IBOutlet weak var closeButtonServiceComplaints: UIButton!
    
    @IBOutlet weak var openMapsLabel: UILabel!
    let locationManager = CLLocationManager()
    var currentLat = ""
    var currentLong = ""
    var destinationLat = ""
    var destinationLong = ""
    
    private let presenter: IWorkInProgressPresenter = WorkInProgressPresenter()
    
    private lazy var tableViewAdapter: ServiceComplaintsTableviewAdaptor  = {
        ServiceComplaintsTableviewAdaptor(tableView: self.serviceComplaintsTableView)
    }()
    
  
    var isReqServicesChecked: Bool = false
    var vinNumber: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.connectToView(view: self)
        closeButtonServiceComplaints.titleLabel?.text = ""
        // Ask for Authorisation from the User.
        locationManager.requestAlwaysAuthorization()

        // For use in foreground
        locationManager.requestWhenInUseAuthorization()
        locationManager.allowsBackgroundLocationUpdates = true
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        othersTextView.delegate = self
//        othersTextView.text = "Add text here..."
        othersTextView.textColor = ColorConstants.appPrimaryColor
        othersTextView.delegate = self

        // Do any additional setup after loading the view.
        presenter.getSRModel()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if(FacadeLayer.sharedInstance.isFromHealthCheck){
            presenter.closeButtonClicked()
        }
        applyStylings()
    }
    
    // MARK: - ApplyStylings to objects
    func applyStylings(){
        closeButton.layer.cornerRadius = closeButton.frame.size.width / 2
        closeButton.layer.masksToBounds = true
        nameLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: true, size: 18.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        
        regNoLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        serviceTypeLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        typeLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        dateTimeLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        serviceComplaintsHeaderLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: true, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        userDetailsSeperator.backgroundColor = ColorConstants.underlineColor
        
        viewHistoryButton.appButtonStyling(titleColor: ColorConstants.appSecondaryBlueColor, backgroundColor: ColorConstants.clear)
       
        viewHistoryButton.titleLabel?.font = Font.vwheadfont.with(size: CGFloat(12.0), weight: .vwheadbold)
        
        viewHistoryBottomLine.backgroundColor = ColorConstants.appSecondaryBlueColor
        
        serviceDetailsButton.appButtonStyling(titleColor: ColorConstants.appSecondaryBlueColor, backgroundColor: ColorConstants.clear)
       
        serviceDetailsButton.titleLabel?.font = Font.vwheadfont.with(size: CGFloat(12.0), weight: .vwheadbold)
        
        serviceDetailsBottomView.backgroundColor = ColorConstants.appSecondaryBlueColor
        
        serviceComplaintsErrorLabel.applyLabelStyling(textColor: ColorConstants.redMarkColor, isBold: true, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
//        firstComplaintNameLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        secondComplaintNameLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        thirdComplaintNameLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        otherServicesHeaderLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: false, size: 18.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
       
        otherServicesBottomView.backgroundColor = ColorConstants.underlineColor
        
        vehicleHealthCheckButton.appButtonStyling(titleColor: ColorConstants.solidWhite, backgroundColor: ColorConstants.appSecondaryBlueColor)
        

        serviceDetailsDoneButton.appButtonStyling(titleColor: ColorConstants.solidWhite, backgroundColor: ColorConstants.appSecondaryBlueColor)
        
        serviceDetailsHeaderLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: true, size: 20.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        slideToOpenView.isHidden = true
        callButton.layer.cornerRadius = callButton.frame.size.width / 2
        callButton.layer.masksToBounds = true
        callButton.backgroundColor = ColorConstants.appSecondaryBlueColor
        layoutSlidingButton()
        
        othersInputField.textInputView( hintText: "Others", isSecureTextEntry: false, returnKeyType: UIReturnKeyType.next, keyboardtype: UIKeyboardType.default, textColor: ColorConstants.appPrimaryColor, tintColor: ColorConstants.appPrimaryColor, lineColor: ColorConstants.underlineColor)
        othersInputField.inputTextfield.frame = CGRect(x: othersInputField.frame.origin.x, y: othersInputField.frame.origin.y, width: othersInputField.frame.size.width, height: othersInputField.frame.size.height)
        
        othersLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: true, size: 16.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        othersTextView.font = Font.vwheadfont.with(size: 14.0, weight: .vwheadregular)
        
        repairEstimateInputTextField.textInputView( hintText: "Repair Estimate", isSecureTextEntry: false, returnKeyType: UIReturnKeyType.done, keyboardtype: UIKeyboardType.default, textColor: ColorConstants.appPrimaryColor, tintColor: ColorConstants.appPrimaryColor, lineColor: ColorConstants.underlineColor)
        repairEstimateInputTextField.inputTextfield.frame = CGRect(x: repairEstimateInputTextField.frame.origin.x, y: repairEstimateInputTextField.frame.origin.y, width: repairEstimateInputTextField.frame.size.width, height: repairEstimateInputTextField.frame.size.height)
        
        openMapsLabel.applyLabelStyling(textColor: ColorConstants.appSecondaryBlueColor, isBold: true, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        
        presenter.viewCreated()
   
        setupSwipeInteractions()
    }
    // MARK: - Layout sliding button

    func layoutSlidingButton(){
        slideToOpenView.sliderViewTopDistance = 0
        slideToOpenView.sliderCornerRadius = 25
        slideToOpenView.showSliderText = true
        slideToOpenView.delegate = self
        slideToOpenView.thumbnailColor = ColorConstants.appSecondaryBlueColor
        slideToOpenView.slidingColor = ColorConstants.appPrimaryColor
        slideToOpenView.textColor = ColorConstants.appPrimaryColor
        slideToOpenView.labelText = "Drag to mark service complete"
        slideToOpenView.sliderTextLabel.text = "Service Complete"
        slideToOpenView.sliderBackgroundColor = ColorConstants.appPrimaryColorWithAlpha
        slideToOpenView.thumnailImageView.image = UIImage(named: "slidearrowIcon")
        slideToOpenView.isEnabled = false
    }
    
    // MARK: - Bind SR model
    func bindSRModel(model: ServiceRequests){
            self.nameLabel.text = model.customerName ?? ""
            self.regNoLabel.text = model.regNum ?? ""
            self.serviceTypeLabel.text = model.serviceType ?? ""
            let serviceDate = model.serviceDateTime ?? ""
            self.dateTimeLabel.text = serviceDate.convertDateStringToFormats(input: serviceDate)
            self.vinNumber = model.vinnumber ?? ""
            self.destinationLat = model.lat ?? ""
            self.destinationLong = model.lng ?? ""
            self.typeLabel.text = model.type ?? ""
            self.othersTextView.text = model.serviceOther ?? ""
            self.repairEstimateInputTextField.inputTextfield.text = model.revenue ?? ""
        self.perform(#selector(drawMap), with: self, afterDelay: 2.0)
    }
    

        
    // MARK: - Bind services complaint list
    func bindServicesComplaintsList(model: [ComplaintModel]) {
       
        serviceDetailsView.isHidden = false
        closeButtonServiceComplaints.titleLabel?.text = ""
        tableViewAdapter.serviceComplaintsButtonDelegate = self
        tableViewAdapter.updateData(complaintsArray: model)
    }
    
    // MARK: - Update UI after vehicle health check
    func updateUIAfterHealthCheck(input: Bool){
        if(input){
            self.vehicleHealthCheckButton.isHidden = true
            self.slideToOpenView.isHidden = false
//            self.bottomCardViewHeightConstraint.constant = 520.0
//            self.userDetailsSeperator.backgroundColor = ColorConstants.underlineColor
//            self.complaintsViewHeightConstraint.constant = 140.0
//            self.otherViewHeightConstraint.constant = 100.0
//            self.otherView.isHidden = false
//            self.serviceComplaintsView.isHidden = false
        }
        else{
//            self.bottomCardViewHeightConstraint.constant = 250.0
//            self.userDetailsSeperator.backgroundColor = ColorConstants.clear
//            self.complaintsViewHeightConstraint.constant = 0.0
//            self.otherViewHeightConstraint.constant = 0.0
//            self.otherView.isHidden = true
//            self.serviceComplaintsView.isHidden = true
            self.vehicleHealthCheckButton.isHidden = false
            self.slideToOpenView.isHidden = true
        }
    }
    

    // MARK: - User Interactions
    // MARK: - CloseButton Action
    @IBAction func closeButton(_ sender: UIButton) {
        presenter.closeButtonClicked()
      }
    // MARK: - Swipe Interactions
    func setupSwipeInteractions(){
        let upSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let downSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        
        upSwipe.direction = .up
        downSwipe.direction = .down
        
        bottomCardView.addGestureRecognizer(upSwipe)
        bottomCardView.addGestureRecognizer(downSwipe)
        
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView == othersTextView){
            if othersTextView.textColor == UIColor.lightGray{
                othersTextView.textColor = ColorConstants.appPrimaryColor
            }
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
         if(textView == othersTextView){
            if othersTextView.text.isEmpty {
                othersTextView.text = "Add text here..."
                othersTextView.textColor = UIColor.lightGray
            }
        }
    }
    // MARK: - Handle swipe gestures
    @objc func handleSwipes(_ sender: UISwipeGestureRecognizer) {
        let direction = sender.direction
        switch direction {
        case .up:
            UIView.animate(withDuration: 0.5) {
                self.bottomCardViewHeightConstraint.constant = 530.0
                self.userDetailsSeperator.backgroundColor = ColorConstants.underlineColor
                self.complaintsViewHeightConstraint.constant = 90.0
                self.otherViewHeightConstraint.constant = 130.0
                self.otherView.isHidden = false
                self.serviceComplaintsView.isHidden = false
                self.repairEstimateInputTextField.isHidden = false
                UIView.animate(withDuration: 0.5) {
                    self.vehicleHealthcheckButtonHeightConstraint.constant = 50.0
                    self.slidingViewHeightConstraint.constant = 50.0
                    self.buttonBottomView.isHidden = false
                    self.buttonsBottmViewheightConstraint.constant = 60.0
                }
                self.view.layoutSubviews()
                self.view.layoutIfNeeded()
            } completion: { (_) in
            }
        case .down:
            UIView.animate(withDuration: 0.5) {
                self.bottomCardViewHeightConstraint.constant = 130.0
                self.userDetailsSeperator.backgroundColor = ColorConstants.clear
                self.complaintsViewHeightConstraint.constant = 0.0
                self.otherViewHeightConstraint.constant = 0.0
                self.otherView.isHidden = true
                self.serviceComplaintsView.isHidden = true
                self.repairEstimateInputTextField.isHidden = true
                UIView.animate(withDuration: 0.5) {
                    self.vehicleHealthcheckButtonHeightConstraint.constant = 0.0
                    self.slidingViewHeightConstraint.constant = 0.0
                    self.buttonBottomView.isHidden = true
                    self.buttonsBottmViewheightConstraint.constant = 0.0
                }
                self.view.layoutSubviews()
                self.view.layoutIfNeeded()
            } completion: { (_) in
            }
        default:
            break
        }
    }
    // MARK: - Check box action service complaints
    func checkboxButtonClicked(_ tag: Int, selectedValues: [Int]) {
        if(selectedValues.count == tag){
            slideToOpenView.isEnabled = true
            isReqServicesChecked = true
        }
        else{
            slideToOpenView.isEnabled = false
            isReqServicesChecked = false
        }
    }
    
    
    @IBAction func closeServiceComplaintsAction(_ sender: UIButton) {
        closeButtonServiceComplaints.titleLabel?.text = ""
        serviceDetailsView.isHidden = true
    }
    // MARK: - Show errir
    func showError(input: String)
    {
        self.displayDefaultAlertForError(errorTitle: ErrorStringConstants.errorTitle, errorDescription: input)
    }
    // MARK: - View Service History Action
    @IBAction func viewHistoryButtonAction(_ sender: UIButton) {
        presenter.serviceHistoryClicked(vin: self.vinNumber)
    }
    
    // MARK: - Service Details Action
    @IBAction func serviceDetailsAction(_ sender: MSUButton) {
        presenter.serviceDetailsClicked()
    }
    
    @IBAction func serviceDetailsDoneAction(_ sender: MSUButton) {
        let sdata = tableViewAdapter.getDataList()
        var allFieldsMarked: Bool = true
        
        for i in 0..<sdata.count{
            if(sdata[i].isMandatory != sdata[i].isChecked){
                allFieldsMarked = false
            }
            else{
                allFieldsMarked = true
            }
        }
        closeButtonServiceComplaints.titleLabel?.text = ""
        if(allFieldsMarked){
            serviceDetailsView.isHidden = true
            serviceComplaintsErrorLabel.isHidden = true
            presenter.getCheckedComplaintsList(model: sdata)
        }
        else{
            serviceDetailsView.isHidden = false
            serviceComplaintsErrorLabel.isHidden = false
        }
   
    }
    
    func vehicleHealthCheckCompleted() {
        slideToOpenView.isEnabled = true
    }
    
    // MARK: - First Complaint Check box
    @IBAction func firstComplaintCheckboxAction(_ sender: UIButton) {
        
    }
    
    // MARK: - Second Complaint Check box
    @IBAction func secondComplaintCheckboxAction(_ sender: UIButton) {
        
    }
    
    // MARK: - Third Complaint Check box
    @IBAction func thirdComplaintCheckboxAction(_ sender: UIButton) {
        
    }
    
    // MARK: - Vehicle Health Check button
    @IBAction func vehicleHealthCheckButtonAction(_ sender: MSUButton) {
        presenter.showVehicleHealthCheck()
        
    }
    
    // MARK: - Call Button
    @IBAction func callButtonAction(_ sender: UIButton) {
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {  //if phone has an app
            if let url = URL(string: "comgooglemaps://?saddr=&daddr=\(self.destinationLat),\(self.destinationLong)&directionsmode=driving") {
               UIApplication.shared.open(url, options: [:])
             }}
        else {
               //Open in browser
            if let urlDestination = URL.init(string: "https://www.google.co.in/maps/dir/?saddr=\(currentLat),\(currentLong)&daddr=\(self.destinationLat),\(self.destinationLong)&directionsmode=driving") {
                       UIApplication.shared.open(urlDestination)
                   }
           }
    }
    
    ///This function called to draw map
    ///
    ///     Warning Source and destinate coordiante must not be empty
    ///   - Parameter sourceCordinate: CLLocationCoordinate2D
    ///   - Parameter destinationcordinate: CLLocationCoordinate2D
//    func drawMap(sourceCordinate : CLLocationCoordinate2D, destinationcordinate :CLLocationCoordinate2D)
    @objc func drawMap()
     {
         self.mapView.clear()
         var str = String(format:"https://maps.googleapis.com/maps/api/directions/json?origin=\(currentLat),\(currentLong)&destination=\(self.destinationLat),\(self.destinationLong)&key=\(StringConstants.GoogleAPIKEY)")
           str = str.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
         Alamofire.request(str).responseJSON { (responseObject) -> Void in
//            let resJson = JSON(responseObject.result.value as? [String : AnyObject])
            guard let resJson = responseObject.result.value as? [String : AnyObject]
                else {
                    return
                   }
            let finalPath  : GMSMutablePath = GMSMutablePath()
                if let routes = resJson["routes"] as? [[String : AnyObject]] {
                    if routes.count > 0 {
                       let first = routes.first
//                       if let legs = first!["legs"] as? [[String : AnyObject]] {
//                           for leg in legs {
//                               if let steps = leg["steps"] as? [[String : AnyObject]] {
//                                   for step in steps {
                                if let polyline = first?["overview_polyline"] as? [String : AnyObject] {
                                           if let points = polyline["points"] as? String {
                                            finalPath.appendPath(path: GMSMutablePath(fromEncodedPath: points))
                                               
                                           }
                                       }
//                                   }
//                               }
//                           }
//                       }
                }
                }
                let polyLine = GMSPolyline(path: finalPath)
                    polyLine.strokeWidth = 5
                    polyLine.strokeColor =  ColorConstants.appPrimaryColor
             if(self.currentLat.count > 0 && self.currentLong.count > 0){
                 let position = CLLocationCoordinate2D(latitude: CLLocationDegrees(self.currentLat)! , longitude: CLLocationDegrees(self.currentLong)!)
                     let marker = GMSMarker(position: position)
    //                 marker.icon = #imageLiteral(resourceName: "ic_usermaplocation")//UIImage(named: "ic_MapPinGreyIcon")
                     marker.title = "" // Addres
                     marker.map = self.mapView
                     let position2 = CLLocationCoordinate2D(latitude: CLLocationDegrees(self.destinationLat)! , longitude: CLLocationDegrees(self.destinationLong)!)
                     let marker1 = GMSMarker(position: position2)
    //                 marker1.icon = #imageLiteral(resourceName: "ic_usermaplocation")//UIImage(named: "ic_LocationPinRedIcon")
                     marker1.title = "" // Destination Address
                     marker1.map = self.mapView


                     let ThemeOrange = GMSStrokeStyle.solidColor(ColorConstants.appSecondaryBlueColor)
                     polyLine.spans = [GMSStyleSpan(style: ThemeOrange)]
                     polyLine.map = self.mapView
                    self.mapView.camera = GMSCameraPosition(
                      target: position,
                      zoom: 12,
                      bearing: 0,
                      viewingAngle: 0)
                 
                 self.focusMapToShowMarkers(markers: [marker,marker1])
             }
             
         }
         
     }
    
    func focusMapToShowMarkers(markers: [GMSMarker]) {
        
        let firstLocation = (markers.first!).position
        var bounds = GMSCoordinateBounds(coordinate: firstLocation, coordinate: firstLocation)
        
        for marker in markers {
            bounds = bounds.includingCoordinate(marker.position)
        }
        
        let update = GMSCameraUpdate.fit(bounds, withPadding: CGFloat(100))
        self.mapView.animate(with: update)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - Slider button delegate
extension WorkInProgressViewController:MTSlideToOpenDelegate{
    func mtSlideToOpenDelegateDidFinish(_ sender: MTSlideToOpenView) {
        slideToOpenView.thumbnailColor = ColorConstants.appPrimaryColor
        // Hit the Update serivce complaints before mark service complete
        presenter.updateServiceDetailsToAPI(others: othersTextView.text ?? "", revenue: self.repairEstimateInputTextField.inputTextfield.text ?? "")
    }
}
    
// MARK: - Location delegate
extension WorkInProgressViewController: CLLocationManagerDelegate{
    func locationManager( _ manager: CLLocationManager,didUpdateLocations locations: [CLLocation]) {
        guard let location = manager.location?.coordinate else {
            return
        }
        currentLat = "\(location.latitude)".removeOptionalFromString()
        currentLong = "\(location.longitude)".removeOptionalFromString()
        
    }
    
    func locationManager(_ manager: CLLocationManager,didFailWithError error: Error) {
        print(error)
    }
}


extension GMSMutablePath {
    func appendPath(path : GMSPath?) {
        if let path = path {
            for i in 0..<path.count() {
                self.add(path.coordinate(at: i))
            }
        }
    }
}
