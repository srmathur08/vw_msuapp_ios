//
//  ServiceComplaintsTableviewAdaptor.swift
//  MSU
//
//  Created by vectorform on 27/07/21.
//

import Foundation
import UIKit

protocol ServiceComplaintsCheckboxDelegate: AnyObject {
    ///This function called pass values from adaptor to controller
    ///
    ///     Warning selectedValues and tag should not be empty
    ///   - Parameter tag: must be a integer value
    ///   - Parameter selectedValues: list of integers
    func checkboxButtonClicked(_ tag: Int, selectedValues: [Int])
}

class ServiceComplaintsTableviewAdaptor: NSObject, ITableViewAdapter, UITableViewDelegate, UITableViewDataSource {
   
    var selectedComplaintsArray = [Int]()
    var tableView: UITableView
    
    typealias DataType = ComplaintModel
    var data: [DataType] = []
    var serviceComplaintsButtonDelegate: ServiceComplaintsCheckboxDelegate?
    var compArray: [ComplaintModel] = [ComplaintModel]()
    required init(tableView: UITableView) {
        self.tableView = tableView
        self.tableView.backgroundColor = ColorConstants.solidWhite
        self.tableView.separatorColor = ColorConstants.clear
        self.tableView.separatorInset = .zero
        super.init()
        let nibName = UINib(nibName: "ServiceComplaintsTableViewCell", bundle:nil)
        self.tableView.register(nibName, forCellReuseIdentifier: "ServiceComplaintsTableViewCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    // MARK: - Update table view with model
    ///This function called to map SR data from contr0ller
    ///
    ///     Warning data should not be empty
    ///   - Parameter data: list of HomeServicesModel
    func updateData(data: [ComplaintModel]) {
        self.data = data
        
    }
    
    func updateData(complaintsArray: [ComplaintModel]) {
//        self.data = [data]
        compArray = complaintsArray
        tableView.reloadData()
    }
    
    // MARK: - Tableview delegate and datasource methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return compArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: ServiceComplaintsTableViewCell = tableView.dequeueReusableCell(
            withIdentifier: "ServiceComplaintsTableViewCell",
            for: indexPath) as? ServiceComplaintsTableViewCell {
//            let serviceObj = data[indexPath.row]
//            cell.updateData(username: serviceObj.fullName ?? "", regNo: serviceObj.registrationNumber ?? "", kms: serviceObj.kms ?? "", type: serviceObj.serviceType ?? "")
            let compObj = compArray[indexPath.row]
            if(compObj.isMandatory ?? false){
                cell.descriptionLabel.text = "* " + compObj.complaint!
            }
            else{
                cell.descriptionLabel.text = compObj.complaint
            }
            
            cell.checkBoxButton.tag = indexPath.row
            if(compObj.isChecked ?? false){
                cell.checkBoxButton.isSelected = true
            }
            else{
                cell.checkBoxButton.isSelected = false
            }
            cell.checkBoxButton.addTarget(self, action: #selector(checkBoxButtonAction), for: .touchUpInside)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    // MARK: - Call back button action to delegate
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//            if let index = selectedComplaintsArray.firstIndex(of: indexPath.row) {
//                self.selectedComplaintsArray = self.selectedComplaintsArray.filter({$0 != index})
//            } else {
//                selectedComplaintsArray.append(indexPath.row)
//            }
//
//        serviceComplaintsButtonDelegate?.checkboxButtonClicked(self.compArray.count, selectedValues: self.selectedComplaintsArray)
//        tableView.reloadData()
//    }
    @objc func checkBoxButtonAction(sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if(sender.isSelected){
            self.compArray[sender.tag].isChecked = true
        }
        else{
            self.compArray[sender.tag].isChecked = false
        }
        tableView.reloadData()
    }
    
    func getDataList() -> [ComplaintModel]{
        
        return compArray
    }
}
