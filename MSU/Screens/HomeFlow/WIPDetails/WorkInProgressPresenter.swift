//
//  WorkInProgressPresenter.swift
//  MSU
//
//  Created by vectorform on 19/07/21.
//

import Foundation
import UIKit
import SwiftyJSON

class WorkInProgressPresenter: BasePresenter, IWorkInProgressPresenter {
   

 
    var homeRouter: HomeRouter? {
        return navController as? HomeRouter
    }
    private var isVehicleHealthCheckComp = false
    private weak var myView: IWorkInProgressViewController? {
        return self.view as? IWorkInProgressViewController
    }
    
    var complaints = [ComplaintModel]()
    func viewCreated(){
        if let inProgressModel = homeRouter?.InProgressRequests{
            isVehicleHealthCheckComp = inProgressModel.isVCCompleted
            myView?.updateUIAfterHealthCheck(input: isVehicleHealthCheckComp)
        }
    }
    
    // MARK: - Close BUtton Clicked
    func closeButtonClicked() {
        if let model = homeRouter?.getInProgressSR(){
            let vehicleCheck = model.isVCCompleted
            if(vehicleCheck){
                homeRouter?.pushHomeScreenController(animated: true)
            }
            else{
                homeRouter?.didSelectBack()
            }
        }
    }

    // MARK: - Service History Clicked
    func serviceHistoryClicked(vin: String) {
        if let model = homeRouter?.getInProgressSR(){
            homeRouter?.pushServiceHistoryController(animated: true, vinNumber: model.vinnumber ?? "", srID: model.srid ?? 0)
        }
    }
    
    // MARK: - Show Vehicle Health Check
    func showVehicleHealthCheck() {
        homeRouter?.pushWipToVehicleHealthCheck(animated: true)
    }
    


    
    // MARK: - Get SR Model
    func getSRModel(){
        if let model = homeRouter?.getInProgressSR(){
            myView?.bindSRModel(model: model)
            
            let isOilChange: Bool = model.isOilChange ?? false
            let isAirFilter: Bool = model.isAirFilter ?? false
            let isBrakesCheck: Bool = model.isBrakes ?? false
            let isBrakeDisc: Bool = model.isBrakesDisk ?? false
            let isSuspensionStrut = model.isSuspensionStrut ?? false
            let isShockAbsorber: Bool = model.isShockAbsorber ?? false
            let isControlArm: Bool = model.isControlArm ?? false
            let isBatteryCheck: Bool = model.isBatteryCheck ?? false
            let isBulbReplace: Bool = model.isBulbReplace ?? false
            let isHornReplace: Bool = model.isHornReplace ?? false
            let isTyreCheck: Bool = model.isTyreCheck ?? false
            let isTyreReplace: Bool = model.isTyreReplace ?? false
            
            if(isOilChange){
                self.complaints.append(ComplaintModel(complaint: ServiceComplaints.oilChange, isChecked: false, isMandatory: true)!)
                
            }
            if(isAirFilter){
                self.complaints.append(ComplaintModel(complaint: ServiceComplaints.airFilter, isChecked: false, isMandatory: true)!)
            }
            if(isBrakesCheck){
                self.complaints.append(ComplaintModel(complaint: ServiceComplaints.brakes, isChecked: false, isMandatory: true)!)
            }
            if(isBrakeDisc){
                self.complaints.append(ComplaintModel(complaint: ServiceComplaints.brakeDisc, isChecked: false, isMandatory: true)!)
            }
            if(isSuspensionStrut){
                self.complaints.append(ComplaintModel(complaint: ServiceComplaints.suspensionStrut, isChecked: false, isMandatory: true)!)
            }
            if(isShockAbsorber){
                self.complaints.append(ComplaintModel(complaint: ServiceComplaints.shockAbsorber, isChecked: false, isMandatory: true)!)
            }
            if(isControlArm){
                self.complaints.append(ComplaintModel(complaint: ServiceComplaints.controlAlarm, isChecked: false, isMandatory: true)!)
            }
            if(isBatteryCheck){
                self.complaints.append(ComplaintModel(complaint: ServiceComplaints.batteryReplace, isChecked: false, isMandatory: true)!)
            }
            if(isBulbReplace){
                self.complaints.append(ComplaintModel(complaint: ServiceComplaints.blubReplace, isChecked: false, isMandatory: true)!)
            }
            if(isHornReplace){
                self.complaints.append(ComplaintModel(complaint: ServiceComplaints.hornReplace, isChecked: false, isMandatory: true)!)
            }
            if(isTyreCheck){
                self.complaints.append(ComplaintModel(complaint: ServiceComplaints.tireCheck, isChecked: false, isMandatory: true)!)
            }
            if(isTyreReplace){
                self.complaints.append(ComplaintModel(complaint: ServiceComplaints.tireReplace, isChecked: false, isMandatory: true)!)
            }
            
        }
       
    }
  
    // MARK: - Service Details clicked
    func serviceDetailsClicked()
    {
        var compModel: [ComplaintModel] = [ComplaintModel]()
        compModel.append(ComplaintModel(complaint: ServiceComplaints.oilChange, isChecked: false, isMandatory: false)!)

        compModel.append(ComplaintModel(complaint: ServiceComplaints.airFilter, isChecked: false, isMandatory: false)!)

        compModel.append(ComplaintModel(complaint: ServiceComplaints.brakes, isChecked: false, isMandatory: false)!)
       
        compModel.append(ComplaintModel(complaint: ServiceComplaints.brakeDisc, isChecked: false, isMandatory: false)!)
   
        compModel.append(ComplaintModel(complaint: ServiceComplaints.suspensionStrut, isChecked: false, isMandatory: false)!)

        compModel.append(ComplaintModel(complaint: ServiceComplaints.shockAbsorber, isChecked: false, isMandatory: false)!)
 
        compModel.append(ComplaintModel(complaint: ServiceComplaints.controlAlarm, isChecked: false, isMandatory: false)!)

        compModel.append(ComplaintModel(complaint: ServiceComplaints.batteryReplace, isChecked: false, isMandatory: false)!)

        compModel.append(ComplaintModel(complaint: ServiceComplaints.blubReplace, isChecked: false, isMandatory: false)!)

        compModel.append(ComplaintModel(complaint: ServiceComplaints.hornReplace, isChecked: false, isMandatory: false)!)

        compModel.append(ComplaintModel(complaint: ServiceComplaints.tireCheck, isChecked: false, isMandatory: false)!)

        compModel.append(ComplaintModel(complaint: ServiceComplaints.tireReplace, isChecked: false, isMandatory: false)!)
        
        for i in 0..<complaints.count{
            for j in 0..<compModel.count{
                if(complaints[i].complaint == compModel[j].complaint){
                    compModel[j].isMandatory = complaints[i].isMandatory
                    compModel[j].isChecked = complaints[i].isChecked
                }
            }
        }
        
        compModel.sort{ $0.isMandatory! && !$1.isMandatory! }
        self.myView?.bindServicesComplaintsList(model: compModel)
    }
    
    // MARK: - Get Checked Complaints List
    func getCheckedComplaintsList(model: [ComplaintModel]) {
        complaints.removeAll()
        complaints.append(contentsOf: model)
        
        if let inProgressModel = homeRouter?.InProgressRequests{
            isVehicleHealthCheckComp = inProgressModel.isVCCompleted
            if(isVehicleHealthCheckComp){
                self.myView?.vehicleHealthCheckCompleted()
            }
        }
    }
    
    // MARK: - Mark Service Completed
    func markServiceCompleted(others: String, revenue: String){
        myView?.showLoader()
        if let model = homeRouter?.getInProgressSR(){
            let postData: [String: AnyObject] = ["Srid": model.srid as Any,
                                                 "StatusId": UpdateStatus.isCompleted as Any,
                                                 "ServiceOther": others as Any,
                                                 "Revenue": revenue as Any] as [String: AnyObject]
            FacadeLayer.sharedInstance.webserviceManager.genericAPICalls(requestParams: postData, apiPath: VFApiCalls.updateSRStatus) {   [weak self] (status, response) in
                guard let strongSelf = self else{ return }
                if(status){

                    strongSelf.myView?.removeLoader()
                    let vehicleCheck = model.isVCCompleted
                    if(vehicleCheck){
                        strongSelf.homeRouter?.pushHomeScreenController(animated: true)
                    }
                    else{
                        strongSelf.homeRouter?.didSelectBack()
                    }
                }
                else{
                    strongSelf.myView?.removeLoader()
                       let errorVal = JSON(response as Any)
                       let errorString = "\(errorVal)"
                       print(errorString)
                       var errormsg: String = ""
                       if(errorString != ErrorStringConstants.noInternet){
                               errormsg = errorVal["Reason"].stringValue
                           if(errormsg == ""){
                               errormsg = ErrorStringConstants.defaultError
                           }
                       }
                       else{
                           errormsg = ErrorStringConstants.noInternet
                       }
                        strongSelf.myView?.displayDefaultAlertForError(errorTitle: ErrorStringConstants.errorTitle, errorDescription: errormsg)
                }
            }
        }
    }
    
    // MARK: - Update Service Details
    func updateServiceDetailsToAPI(others: String, revenue: String){
        var oilChange: Bool = false
        var airfilter: Bool = false
        var brakes: Bool = false
        var brakesDisc: Bool = false
        var suspension: Bool = false
        var shock: Bool = false
        var controlArm: Bool = false
        var battery: Bool = false
        var bulb: Bool = false
        var horn: Bool = false
        var tyreCheck: Bool = false
        var tyreReplace: Bool = false
        
        for i in 0..<complaints.count{
            if(complaints[i].complaint == ServiceComplaints.oilChange){
                oilChange = complaints[i].isChecked ?? false
            }
            if(complaints[i].complaint == ServiceComplaints.airFilter){
                airfilter = complaints[i].isChecked ?? false
            }
            if(complaints[i].complaint == ServiceComplaints.brakes){
                brakes = complaints[i].isChecked ?? false
            }
            if(complaints[i].complaint == ServiceComplaints.brakeDisc){
                brakesDisc = complaints[i].isChecked ?? false
            }
            if(complaints[i].complaint == ServiceComplaints.suspensionStrut){
                suspension = complaints[i].isChecked ?? false
            }
            if(complaints[i].complaint == ServiceComplaints.shockAbsorber){
                shock = complaints[i].isChecked ?? false
            }
            if(complaints[i].complaint == ServiceComplaints.controlAlarm){
                controlArm = complaints[i].isChecked ?? false
            }
            if(complaints[i].complaint == ServiceComplaints.blubReplace){
                bulb = complaints[i].isChecked ?? false
            }
            if(complaints[i].complaint == ServiceComplaints.batteryReplace){
                battery = complaints[i].isChecked ?? false
            }
            if(complaints[i].complaint == ServiceComplaints.hornReplace){
                horn = complaints[i].isChecked ?? false
            }
            if(complaints[i].complaint == ServiceComplaints.tireCheck){
                tyreCheck = complaints[i].isChecked ?? false
            }
            if(complaints[i].complaint == ServiceComplaints.tireReplace){
                tyreReplace = complaints[i].isChecked ?? false
            }
        }
        if let model = homeRouter?.getInProgressSR(){
            let postData: [String: AnyObject] = ["Srid": model.srid as Any,
                                                 "IsOilChange":  oilChange as Any,
                                                 "IsAirFilter":  airfilter as Any,
                                                 "IsBrakes":  brakes as Any,
                                                 "IsBrakesDisk":  brakesDisc as Any,
                                                 "IsSuspensionStrut":  suspension as Any,
                                                 "IsShockAbsorber":  shock as Any,
                                                 "IsControlArm": controlArm as Any,
                                                 "IsBatteryCheck":  battery as Any,
                                                 "IsBulbReplace":  bulb as Any,
                                                 "IsHornReplace":  horn as Any,
                                                 "IsTyreCheck":  tyreCheck as Any,
                                                 "IsTyreReplace":  tyreReplace as Any] as [String: AnyObject]
            
            FacadeLayer.sharedInstance.webserviceManager.genericAPICalls(requestParams: postData, apiPath: VFApiCalls.updateServiceDetailSR) {[weak self] (status, response) in
                guard let strongSelf = self else{ return }
                if(status){
                    print(status)
                    strongSelf.markServiceCompleted(others: others, revenue: revenue)
                }
            }
        }
    }
}
