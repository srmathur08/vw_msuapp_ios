//
//  ServiceComplaintsTableViewCell.swift
//  MSU
//
//  Created by vectorform on 27/07/21.
//

import UIKit

class ServiceComplaintsTableViewCell: UITableViewCell {

    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        addViews()
    }

    func addViews() {
        descriptionLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 16.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        checkBoxButton.isSelected = false
    }
}
