//
//  ReportDetailsTableViewAdaptor.swift
//  MSU
//
//  Created by vectorform on 16/08/21.
//

import Foundation
import UIKit

class ReportDetailsTableViewAdaptor: NSObject, ITableViewAdapter, UITableViewDelegate, UITableViewDataSource
{
    var tableView: UITableView
    
    typealias DataType = SubSection
    var data: [DataType] = []
    
    required init(tableView: UITableView) {
        self.tableView = tableView
        self.tableView.backgroundColor = ColorConstants.clear
        self.tableView.separatorColor = ColorConstants.clear
        self.tableView.separatorInset = .zero
        super.init()
        let nibName = UINib(nibName: "ReportSubSectionTableViewCell", bundle:nil)
        self.tableView.register(nibName, forCellReuseIdentifier: "ReportSubSectionTableViewCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 60.0
        self.tableView.rowHeight = UITableView.automaticDimension
    }
    
    // MARK: - Update table Data
    ///This function called to map SR data from contr0ller
    ///
    ///     Warning data should not be empty
    ///   - Parameter data: list of SubSection
    func updateData(data: [SubSection]) {
        self.data = data
        tableView.reloadData()
    }
    
    // MARK: - Table view delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: ReportSubSectionTableViewCell = tableView.dequeueReusableCell(
            withIdentifier: "ReportSubSectionTableViewCell",
            for: indexPath) as? ReportSubSectionTableViewCell {
            
            let anObj = self.data[indexPath.row]
            let selectVal = anObj.status
            switch selectVal {
            case StatusEnum.Red.rawValue:
                cell.statusImageView.image = UIImage(named: "redcheckedicon")
            case StatusEnum.Yellow.rawValue:
                cell.statusImageView.image = UIImage(named: "yellowcheckedicon")
            case StatusEnum.Green.rawValue:
                cell.statusImageView.image = UIImage(named: "greencheckedicon")
            default:
                cell.statusImageView.image = UIImage(named: "unchecked")
            }
            if (indexPath.row % 2 == 0 ){
                cell.subSectionNameView.backgroundColor = ColorConstants.solidWhite
                cell.statusView.backgroundColor = ColorConstants.solidWhite
                cell.commentsView.backgroundColor = ColorConstants.solidWhite
            }
            else{
                cell.subSectionNameView.backgroundColor = ColorConstants.underlineColor
                cell.statusView.backgroundColor = ColorConstants.underlineColor
                cell.commentsView.backgroundColor = ColorConstants.underlineColor
            }
            
            cell.subSectionNameLabel.text = anObj.name ?? ""
            let valText = anObj.value ?? ""
            if(valText.contains("|")){
                let finalVal = anObj.value?.replacingOccurrences(of: "|", with: ", ")
                cell.commentsLabel.text = finalVal
            }
            else{
                cell.commentsLabel.text = anObj.value ?? ""
            }
            
            return cell
        }
        return UITableViewCell()
        }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
