//
//  ReportDetailsViewController.swift
//  MSU
//
//  Created by vectorform on 16/08/21.
//

import UIKit

class ReportDetailsViewController: BaseViewController, IReportDetailsViewController, ReportDetailsImagesSelectionDelegate {

    

    // MARK: - View Contoller Outlets
    
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollContainerView: UIView!
    @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var seperatorView: UIView!
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var vehicleTextLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var percentageLabel: UILabel!
    @IBOutlet weak var listView: UIView!
    @IBOutlet weak var listViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableHeaderView: UIView!
    @IBOutlet weak var headerPartsView: UIView!
    @IBOutlet weak var headerPartLabel: UILabel!
    @IBOutlet weak var headerStatusView: UIView!
    @IBOutlet weak var headerStatusLabel: UILabel!
    @IBOutlet weak var headerCommentsView: UIView!
    @IBOutlet weak var headerCommentsLabel: UILabel!
    @IBOutlet weak var subSectionTableView: UITableView!
    @IBOutlet weak var bottomImagesView: UIView!
    @IBOutlet weak var bottomImagesHeaderView: UIView!
    @IBOutlet weak var bottomImagesheaderLabel: UILabel!
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    
    
    private let presenter: IReportDetailsPresenter = ReportDetailsPresenter()
    
    private lazy var tableViewAdapter: ReportDetailsTableViewAdaptor  = {
        ReportDetailsTableViewAdaptor(tableView: self.subSectionTableView)
    }()
    
    private lazy var collectionViewAdapter: ReportImagesCollectionViewAdaptor  = {
        ReportImagesCollectionViewAdaptor(collectionView: self.imagesCollectionView)
    }()
    
    var vehicleModel: VehicleHealthCheckModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.connectToView(view: self)
        // Do any additional setup after loading the view.
        applyStylings()
    }
    
    // MARK: - Apply Stylings to Objects
   private func applyStylings()
   {
    headerTitleLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: true, size: 18.0, alignment: .center, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    
    seperatorView.backgroundColor = ColorConstants.underlineColor
    
    topView.layer.borderWidth = 1.0
    topView.layer.cornerRadius = 5.0
    topView.layer.masksToBounds = true

    vehicleTextLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: true, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
    
    scoreLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: true, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
    
    percentageLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: true, size: 24.0, alignment: .right, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    
    headerPartsView.backgroundColor = ColorConstants.appLabelsTextColor
    headerPartLabel.applyLabelStyling(textColor: ColorConstants.solidWhite, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    
    headerStatusView.backgroundColor = ColorConstants.appLabelsTextColor
    headerStatusLabel.applyLabelStyling(textColor: ColorConstants.solidWhite, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    
    headerCommentsView.backgroundColor = ColorConstants.appLabelsTextColor
    headerCommentsLabel.applyLabelStyling(textColor: ColorConstants.solidWhite, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    
    bottomImagesHeaderView.backgroundColor = ColorConstants.appLabelsTextColor
    bottomImagesheaderLabel.applyLabelStyling(textColor: ColorConstants.solidWhite, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    
    if let vehicleModel = vehicleModel{
        presenter.bindUI(model: vehicleModel)
    }
    
   }

    // MARK: - Bind SubSection table view
    func bindSubSectionAdaptor(inputModel: [SubSection])
    {
        tableViewAdapter.updateData(data: inputModel)
    }
    
    func updateTopViewColor(bgColor: UIColor, borderColor: UIColor, icon: String){
        topView.layer.borderColor = borderColor.cgColor
        topView.backgroundColor = bgColor
        imageIcon.image = UIImage(named: icon)
        imageIcon.setImageColor(color: borderColor)
    }
    // MARK: - Back Button
    @IBAction func backButtonAction(_ sender: UIButton) {
        presenter.closeButtonClicked()
    }
    
    // MARK: - Update UI Constraints
    func updateUIConstraints(sebSectionsCount: Int, assetsCount: Int){
        if(assetsCount == 0){
            bottomImagesView.isHidden = true
        }
        else{
            bottomImagesView.isHidden = false
        }
        listViewHeightConstraint.constant = CGFloat(sebSectionsCount * 50) + 10
        scrollViewHeightConstraint.constant =  listViewHeightConstraint.constant + CGFloat(assetsCount * 50) + 200
        self.viewDidLayoutSubviews()
    }
    
    // MARK: - Update top header view
    func updateTopViewText(header: String, score: String, percentage: String)
    {
        headerTitleLabel.text = header
        scoreLabel.text = score
        percentageLabel.text = percentage
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    // MARK: - Bind Assets to collection view
    func bindImagesCollectionView(array: [String]){
        collectionViewAdapter.delegate = self
        collectionViewAdapter.updateGalleyImages(imagesModel: array)
    }

    // MARK: - Image view tapped
    func imageTapped(index: Int, imagesModel: [String]) {
        presenter.showReportDetailsImagePreview(indexVal: index, imagesArray: imagesModel)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
