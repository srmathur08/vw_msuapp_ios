//
//  ReportDetailsContract.swift
//  MSU
//
//  Created by vectorform on 16/08/21.
//

import Foundation
import UIKit

protocol IReportDetailsViewController: IBaseViewController {
    ///This function called to update the top view with colors and text
    ///
    ///      Warning bgColor, borderColor and icon should not be empty
    ///  - Parameter model: must be a VehicleHealthCheckModel model
    ///  - Parameter bgColor: must be a UIColour value
    ///  - Parameter borderColor: must be a UIColour value
    ///  - Parameter icon: must be a String value
    func updateTopViewColor(bgColor: UIColor, borderColor: UIColor, icon: String)
    
    ///This function called to update the subsection table view
    ///
    ///      Warning model should not be empty
    ///  - Parameter model: must be a list of SubSection model
    func bindSubSectionAdaptor(inputModel: [SubSection])
    
    ///This function called to update the UI constraints in View
    ///
    ///      Warning sebSectionsCount and assetsCount should not be empty
    ///  - Parameter sebSectionsCount: must be a integer value
    ///  - Parameter assetsCount: must be a integer value
    func updateUIConstraints(sebSectionsCount: Int, assetsCount: Int)
    
    ///This function called to update the UI images in collection view
    ///
    ///      Warning array should not be empty
    ///  - Parameter array: must be a list of string values
    func bindImagesCollectionView(array: [String])
    
    ///This function called to update the top view with  text
    ///
    ///  - Parameter header:  must be a String value
    ///  - Parameter score:  must be a String value
    ///  - Parameter percentage: must be a String value
    func updateTopViewText(header: String, score: String, percentage: String)
    
}


protocol IReportDetailsPresenter: IBasePresenter {
    ///This function called to update the get the VehicleHealthCheckModel
    ///
    ///     Warning model should not be empty
    ///  - Parameter model:  must be a VehicleHealthCheckModel model
    func bindUI(model: VehicleHealthCheckModel)
    
    ///This function called to pop view controller
    ///
    func closeButtonClicked()
    
    ///This function called to present the images in Full view
    ///
    ///     Warning indexVal and imagesArray should not be empty
    ///  - Parameter indexVal: must be a integet value
    ///  - Parameter imagesArray: must be a list of imagesArray model
    func showReportDetailsImagePreview(indexVal: Int, imagesArray: [String])
}

