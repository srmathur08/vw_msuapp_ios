//
//  ReportSubSectionTableViewCell.swift
//  MSU
//
//  Created by vectorform on 16/08/21.
//

import UIKit

class ReportSubSectionTableViewCell: UITableViewCell {

 
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var subSectionNameView: UIView!
    @IBOutlet weak var subSectionNameLabel: UILabel!
    @IBOutlet weak var commentsView: UIView!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        subSectionNameLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        commentsLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
