//
//  ReportDetailsPresenter.swift
//  MSU
//
//  Created by vectorform on 16/08/21.
//

import Foundation

class ReportDetailsPresenter: BasePresenter, IReportDetailsPresenter {
    
    private weak var myView: IReportDetailsViewController? {
        return view as? IReportDetailsViewController
    }

    var homeRouter: HomeRouter? {
        return navController as? HomeRouter
    }
    
    // MARK: - Close button action
    func closeButtonClicked(){
        homeRouter?.didSelectBack()
    }
    
    // MARK: - Bind UI
    func bindUI(model: VehicleHealthCheckModel){
        let headerText = model.sectionName ?? ""
        let scoreText = model.sectionName! + " score"
        var percentagetext: String = ""
        let scoreVal = model.score
        if(scoreVal == "" || scoreVal == nil){
            percentagetext = "0 %"
        }
        else{
            percentagetext = (scoreVal?.appending(" %"))!
        }
        
        if(model.scoreColor == StringConstants.vehicleHealthGreenStatus){
            myView?.updateTopViewColor(bgColor: ColorConstants.greenMarkColorWithOpacity, borderColor: ColorConstants.greenMarkColor, icon: model.icon ?? "")
        }
        else if(model.scoreColor == StringConstants.vehicleHealthYellowStatus){
            myView?.updateTopViewColor(bgColor: ColorConstants.yellowMarkColorWithOpacity, borderColor: ColorConstants.yellowMarkColor, icon: model.icon ?? "")
        }
        else{
            myView?.updateTopViewColor(bgColor: ColorConstants.redMarkColorWithOpacity, borderColor: ColorConstants.redMarkColor, icon: model.icon ?? "")
        }
        if let sectionModel = model.subSection{
            myView?.bindSubSectionAdaptor(inputModel: sectionModel)
            getHeightConstraint(inputModel: model)
        }
        myView?.updateTopViewText(header: headerText, score: scoreText, percentage: percentagetext)
    }
    
    // MARK: - Get Height constraint
    func getHeightConstraint(inputModel: VehicleHealthCheckModel){
        let subSectionCount = inputModel.subSection?.count
        var assetsCount: Int = 0
        var assetsArray = [String]()
        for i in 0..<inputModel.subSection!.count{
            if let assets = inputModel.subSection?[i].assetPaths{
                assetsCount += assets.count
                assetsArray.append(contentsOf: assets)
            }
        }
        if(assetsCount != 0){
            myView?.bindImagesCollectionView(array: assetsArray)
        }
        myView?.updateUIConstraints(sebSectionsCount: subSectionCount!, assetsCount: assetsCount)
    }
    
    // MARK: - Show report details
    func showReportDetailsImagePreview(indexVal: Int, imagesArray: [String])
    {
        homeRouter?.presentImageViewController(index: indexVal, images: imagesArray, reportImages: [VehicleCheckupAssets](), isModel: false)
    }
}
