//
//  ReportImagesCollectionViewAdaptor.swift
//  MSU
//
//  Created by vectorform on 17/08/21.
//

import Foundation
import UIKit
protocol ReportDetailsImagesSelectionDelegate: AnyObject {
    ///This function called pass model from adaptor to controller
    ///
    ///     Warning index and imagesModel should not be empty
    ///   - Parameter index: must be a integer
    ///   - Parameter imagesModel: must be a list of string values
    func imageTapped(index: Int, imagesModel:[String])
}

class ReportImagesCollectionViewAdaptor: NSObject, ICollectionViewAdapter, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{

    var collectionView: UICollectionView
   
    typealias DataType = SubSection
    
    var data: [DataType] = []
    var galleryArray = [String]()
    
    private let spacing:CGFloat = 5.0
    var delegate: ReportDetailsImagesSelectionDelegate?
    required init(collectionView: UICollectionView) {
        self.collectionView = collectionView
        self.collectionView.backgroundColor = ColorConstants.clear
        super.init()
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing)
        layout.minimumLineSpacing = spacing
        layout.minimumInteritemSpacing = spacing
        self.collectionView.collectionViewLayout = layout
        
    }
    
    func updateData(data: [SubSection]) {
        self.data = data
    }
    
    // MARK: - Update collection view with model
    ///This function called to map list of SR in table view
    ///
    ///     Warning imagesModel should not be empty
    ///   - Parameter imagesModel: list of Strings
    func updateGalleyImages(imagesModel: [String]){
        self.galleryArray = imagesModel
        self.collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.galleryArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell: ReportImagesCollectionViewCell = collectionView.dequeueReusableCell(
            withReuseIdentifier: "ReportImagesCollectionViewCell",
            for: indexPath) as? ReportImagesCollectionViewCell {
          
            let anObj = galleryArray[indexPath.item]

            let imagesDir = ImagesDirectory()
            let assetIamge = imagesDir.getFileAt(filepath: anObj)
            cell.assetImageView.image = assetIamge
            cell.assetImageView.layer.cornerRadius = 5.0
            cell.assetImageView.layer.masksToBounds = true
    
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 70)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.imageTapped(index: indexPath.item, imagesModel: self.galleryArray)
    }
}
