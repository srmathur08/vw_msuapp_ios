//
//  ReportsDetails360Contract.swift
//  MSU
//
//  Created by vectorform on 18/08/21.
//

import Foundation

import UIKit

protocol IReportsDetails360ViewController: IBaseViewController {
    ///This function called to update the dot marks on the car image
    ///
    ///     Warning model should not be empty
    ///  - Parameter model: must be a Images360Model model
    func updateDotMarkers(model: Images360Model)
    
    ///This function called to update the counts of dot markers
    ///
    ///     Warning repair and  damageCount should not be empty
    ///  - Parameter repair: must be a Integer value
    ///  - Parameter damageCount: must be a Integer value
    func updateCounts(repair: Int, damageCount: Int)
    
    ///This function called to update the top view with colors and text
    ///
    ///  - Parameter borderColor: must be a UIColour value
    ///  - Parameter bgColor: must be a UIColour value
    ///  - Parameter sectionText: must be a String value
    ///  - Parameter headerText: must be a String value
    ///  - Parameter imageName: must be a String value
    func updateTopView(borderColor: UIColor, bgColor: UIColor, sectionText: String, headerText: String, imageName: String)
}



protocol IReportsDetails360Presenter: IBasePresenter {
    ///This function called to pop view controller
    ///
    func closeButtonClicked()
    
    ///This function called to get the vehicle health check model
    ///
    ///      Warning model should not be empty
    ///  - Parameter model: must be a VehicleHealthCheckModel model
    func getVehicleHealthCheckModel(model: VehicleHealthCheckModel)
    
    ///This function called to next button action for 360 image views
    ///
    func onNextClick()
    
    ///This function called to previous button action for 360 image views
    ///
    func onPreviousClick()
    
    ///This function called to update the markes on car images
    ///
    ///      Warning marker should not be empty
    ///  - Parameter marker: must be a ImageMarkerModel model
    func updateMarkers(marker: ImageMarkerModel)
}
