//
//  ReportsDetails360ViewController.swift
//  MSU
//
//  Created by vectorform on 18/08/21.
//

import UIKit

class ReportsDetails360ViewController: BaseViewController, IReportsDetails360ViewController {
 
    
    
    // MARK: - View Contoller Outlets
    @IBOutlet weak var sectionLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var vehicleTextLabel: UILabel!
    @IBOutlet weak var percentageLabel: UILabel!
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var carBaseImage: UIImageView!
    @IBOutlet weak var rotateLabel: UILabel!
    @IBOutlet weak var repairView: UIView!
    @IBOutlet weak var repairDotView: UIView!
    @IBOutlet weak var repairLabel: UILabel!
    @IBOutlet weak var damageView: UIView!
    @IBOutlet weak var damageDotView: UIView!
    @IBOutlet weak var damegeLabel: UILabel!
    @IBOutlet weak var signView: UIView!
    @IBOutlet weak var customerView: UIView!
    @IBOutlet weak var customerSignView: CardView!
    @IBOutlet weak var customerlabel: UILabel!
    @IBOutlet weak var customerSignImageView: UIImageView!
    @IBOutlet weak var advisorView: UIView!
    @IBOutlet weak var advisorLabel: UILabel!
    @IBOutlet weak var advisorSignView: CardView!
    @IBOutlet weak var advisorSignImageView: UIImageView!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var imagePreviousButton: UIButton!
    @IBOutlet weak var imageNextButton: UIButton!
    
    private let presenter: IReportsDetails360Presenter = ReportsDetails360Presenter()
    
    var image360Model: VehicleHealthCheckModel?
    var signatureModel: VehicleHealthCheckModel?
    var repairCount: Int = 0
    var damageCount: Int = 0
    var currentImageName: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter.connectToView(view: self)
        // Do any additional setup after loading the view.
        applyStylings()
    }
    
    // MARK: - Apply Stylings to Objects
   private func applyStylings(){
    sectionLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: true, size: 18.0, alignment: .center, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    
   
    seperatorView.backgroundColor = ColorConstants.underlineColor
    rotateLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 12.0, alignment: .center, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
    
    repairDotView.backgroundColor = ColorConstants.yellowMarkColor
    repairDotView.layer.cornerRadius = repairDotView.frame.size.width / 2
    repairDotView.layer.masksToBounds = true
    
    repairLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
    
    damageDotView.backgroundColor = ColorConstants.redMarkColor
    damageDotView.layer.cornerRadius = damageDotView.frame.size.width / 2
    damageDotView.layer.masksToBounds = true
    
    damegeLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
    
    customerlabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: true, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    
    advisorLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: true, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    
    vehicleTextLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: true, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
  
    if let vehicleModel = image360Model{
        presenter.getVehicleHealthCheckModel(model: vehicleModel)
    }
    updateSignatures()
   }
    
    // MARK: - Updating the markers in UI
    func updateDotMarkers(model: Images360Model){
        self.carImageView.subviews.forEach{$0.removeFromSuperview()}
        self.carImageView.image = UIImage(named: model.imagePath)
        self.currentImageName = model.imagePath
        for j in 0..<model.markers!.count{
            let xPoint = ((model.markers?[j].x ?? 0.0) * self.carImageView.frame.size.width) / 100.0
            let yPoint = ((model.markers?[j].y ?? 0.0)  * self.carImageView.frame.size.height)  / 100.0
            let marker = model.markers?[j].mark ?? ""
            let imageCenterOffset: CGFloat = 7.0
            let dotImage = UIImageView(frame: CGRect(x: xPoint-imageCenterOffset, y: yPoint-imageCenterOffset, width: 15, height: 15))
            if(marker == StringConstants.repairMarker){
                dotImage.image = UIImage(named: "yellowdot")
            }
            else if(marker == StringConstants.damageMarker){
                dotImage.image = UIImage(named: "reddot")
            }
            carImageView.addSubview(dotImage)
        }
    }
    
    // MARK: - Previous Image button Action
    @IBAction func imagePreviousAction(_ sender: UIButton) {
        presenter.onPreviousClick()
    }
    
    // MARK: - Next Image Button action
    @IBAction func imageNextAction(_ sender: UIButton) {
        presenter.onNextClick()
    }
    
    func updateCounts(repair: Int, damageCount: Int) {
        self.repairLabel.text = StringConstants.repairMarker + "s (" + "\(repair)" + ")"
        self.damegeLabel.text = StringConstants.damageMarker + "s (" + "\(damageCount)" + ")"
        let totalCount = String(describing: repair + damageCount).removeOptionalFromString()
        self.percentageLabel.text = totalCount
    }
    // MARK: - Updating the markers in UI
    func updateSignatures(){
        if let advisorImage = signatureModel?.serviceAdvisorSign{
            let imagesDir = ImagesDirectory()
            let assetIamge = imagesDir.getFileAt(filepath: advisorImage)
            self.advisorSignImageView.image = assetIamge
        }
        if let customerImage = signatureModel?.customerSign{
            let imagesDir = ImagesDirectory()
            let assetIamge = imagesDir.getFileAt(filepath: customerImage)
            self.customerSignImageView.image = assetIamge
        }
    }
    
    // MARK: - Update top view
    func updateTopView(borderColor: UIColor, bgColor: UIColor, sectionText: String, headerText: String, imageName: String){
        topView.layer.borderWidth = 1.0
        topView.layer.cornerRadius = 5.0
        topView.layer.masksToBounds = true
        topView.layer.borderColor = borderColor.cgColor
        topView.backgroundColor = bgColor
        
        sectionLabel.text = sectionText
        vehicleTextLabel.text = headerText
        
        icon.image = UIImage(named: imageName)
        icon.setImageColor(color: borderColor)
    }
    // MARK: - Back Button Action
    @IBAction func backbuttonAction(_ sender: UIButton) {
        presenter.closeButtonClicked()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
