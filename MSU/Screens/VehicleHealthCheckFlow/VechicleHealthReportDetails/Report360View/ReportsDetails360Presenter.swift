//
//  ReportsDetails360Presenter.swift
//  MSU
//
//  Created by vectorform on 18/08/21.
//

import Foundation
import UIKit

class ReportsDetails360Presenter: BasePresenter, IReportsDetails360Presenter {

    

    private weak var myView: IReportsDetails360ViewController? {
        return self.view as? IReportsDetails360ViewController
    }
    var currentPosition: Int = 0
    var vehicleModel: VehicleHealthCheckModel?
    
    var homeRouter: HomeRouter? {
        return navController as? HomeRouter
    }
    
    // MARK: - Get Vehicle check model
    func getVehicleHealthCheckModel(model: VehicleHealthCheckModel) {
        self.vehicleModel = model
         let markersModel = model.images360Array
        myView?.updateDotMarkers(model: markersModel[0])
        
        var repairCount: Int = 0
        var damageCount: Int = 0
            for i in 0..<model.images360Array.count{
            
                for j in 0..<model.images360Array[i].markers!.count{
                    if(model.images360Array[i].markers![j].mark == StringConstants.repairMarker){
                    repairCount += 1
                }
                    else if(model.images360Array[i].markers![j].mark == StringConstants.damageMarker){
                    damageCount += 1
                }
            }
            
        }
        myView?.updateCounts(repair: repairCount, damageCount: damageCount)
        
        myView?.updateTopView(borderColor: ColorConstants.greenMarkColor, bgColor: ColorConstants.greenMarkColorWithOpacity, sectionText:StringConstants.vehicleImagesText, headerText: StringConstants.scratch, imageName: model.icon ?? "")
    }

    // MARK: - Image Next  action
    func onNextClick() {
        if(currentPosition < (vehicleModel?.images360Array.count)! - 1){
            currentPosition += 1
            myView?.updateDotMarkers(model: (vehicleModel?.images360Array[currentPosition])!)
        }
        else{
            currentPosition = 0
            myView?.updateDotMarkers(model: (vehicleModel?.images360Array[currentPosition])!)
        }
    }
    
    // MARK: - Image Previous action
    func onPreviousClick() {
        if(currentPosition  == 0){
            currentPosition = (vehicleModel?.images360Array.count)! - 1
            myView?.updateDotMarkers(model: (vehicleModel?.images360Array[currentPosition])!)
        }
        else{
            currentPosition -= 1
            myView?.updateDotMarkers(model: (vehicleModel?.images360Array[currentPosition])!)
        }
    }
    
    // MARK: - Update markers in UI
    func updateMarkers(marker: ImageMarkerModel) {
        vehicleModel?.images360Array[currentPosition].markers?.append(marker)
        myView?.updateDotMarkers(model: (vehicleModel?.images360Array[currentPosition])!)
    }
    
    // MARK: - back button action
    func closeButtonClicked() {
        homeRouter?.didSelectBack()
    }
    
}
