//
//  VechicleHealthCheckHeaderview.swift
//  MSU
//
//  Created by vectorform on 03/08/21.
//

import UIKit
import SnapKit

class VehicleHealthCheckHeaderview: UIView {
    
    private let sectionIndexLabel: MSULabel = MSULabel()
    private let progressView: UIView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init (withIndex: Int) {
        self.init(frame: .zero)
        commonInit(withIndex: withIndex)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        fatalError("init(coder:) has not been implemented")
    }
    private func commonInit(withIndex: Int){
        
        sectionIndexLabel.text = "\(withIndex)"
        sectionIndexLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: false, size: 14.0, alignment: .center, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        
        progressView.backgroundColor = ColorConstants.appGreyColor
        self.addSubviews(views: [sectionIndexLabel,progressView])
        
       
        let labelWidthHeight: CGFloat = 20.0
        sectionIndexLabel.snp.makeConstraints {(make: ConstraintMaker) in
            make.top.equalToSuperview()
            make.leading.equalToSuperview()
            make.width.height.equalTo(labelWidthHeight)
        }
        let progressViewTop: CGFloat = 2.0
        let progressViewHeight: CGFloat = 2.0
        progressView.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(sectionIndexLabel.snp.bottom).offset(progressViewTop)
            make.left.right.equalToSuperview()
            make.height.equalTo(progressViewHeight)
        }
        
    }
    
    public func setViewColor(color: UIColor) {
        sectionIndexLabel.textColor = color
        progressView.backgroundColor = color
    }
    
    public func updateIndex(index: String) {
        sectionIndexLabel.text = index
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
