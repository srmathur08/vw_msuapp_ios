//
//  IVehicleHealthCheckRouter.swift
//  MSU
//
//  Created by vectorform on 29/07/21.
//

import Foundation
import UIKit

protocol IVehicleHealthCheckRouter: BaseNavigationController {
    ///This function called to push vehicle health check tableview controller
    ///
    ///     Warning inputModel should not be empty
    ///  - Parameter animated: true/false
    ///  - Parameter inputModel: VehicleHealthCheckModel
    func pushVehicleHealthCheckTableViewController(animated: Bool, inputModel: VehicleHealthCheckModel)
    
    ///This function called to push vehicle health 360 view controller
    ///
    ///     Warning inputModel should not be empty
    ///  - Parameter animated: true/false
    ///  - Parameter inputModel: VehicleHealthCheckModel
    func pushVehicleHealthCheck360ViewController(animated: Bool, inputModel: VehicleHealthCheckModel)
    
    ///This function called to push vehicle health Acknowledge view controller
    ///
    ///     Warning inputModel should not be empty
    ///  - Parameter animated: true/false
    ///  - Parameter inputModel: VehicleHealthCheckModel
    func pushVehicleHealthCheckAcknowledgeViewController(animated: Bool, inputModel: VehicleHealthCheckModel)
    
    ///This function called to push vehicle health signature view controller
    ///
    ///  - Parameter animated: true/false
    func presentSignatureViewController(animated: Bool,delegate: ISignatureView, imageView: UIImage)
    
    ///This function called to push vehicle health preview view controller
    ///
    ///     Warning inputModel should not be empty
    ///  - Parameter animated: true/false
    ///  - Parameter inputModel: list of VehicleHealthCheckModel
    func pushPreviewViewController(animated: Bool, inputModel: [VehicleHealthCheckModel])
    
    ///This function called to present vehicle health 360 view controller
    ///
    ///     Warning inputModel should not be empty
    ///  - Parameter animated: true/false
    ///  - Parameter inputModel: VehicleHealthCheckModel
    ///  - Parameter heightVal: must be a integer value
    ///  - Parameter widthVal: must be a integer value
    func presentLandScape360ViewController(animated: Bool, index: Int, inputModel: VehicleHealthCheckModel, delegate: Landscape360ViewDelagate, heightVal: CGFloat, widthVal: CGFloat)
}
