//
//  VehicleImagesContract.swift
//  MSU
//
//  Created by vectorform on 20/08/21.
//

import Foundation

protocol IVehicleImagesViewController: IBaseViewController {

}


protocol IVehicleImagesPresenter: IBasePresenter {
    ///This function called to pop view controller
    ///
    func backButtonClicked()
    
    ///This function called to Call API and push vehicle health check view controller
    ///
    ///     Warning galleryArray should not be empty
    ///  - Parameter galleryArray: List of string values
    func forwardButtonClicked(galleryArray: [String])
    
    ///This function called to set the VehicleCheckUpId 
    ///
    ///     Warning input should not be empty
    ///  - Parameter input: must be a integer value
    func setVehicleCheckUpId(input: Int)
 
    ///This function called to get the VehicleCheckUpId
    ///
    ///     Returns integer value
    func getVehicleCheckUpId() -> Int

}
