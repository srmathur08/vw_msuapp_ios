//
//  VehicleImagesCollectionViewAdaptor.swift
//  MSU
//
//  Created by vectorform on 20/08/21.
//

import Foundation
import UIKit

protocol VehicleImagesCancelDelagate: AnyObject {
    ///This function called pass values from adaptor to controller
    ///
    ///     Warning tag should not be empty
    ///   - Parameter tag: must be a integer value
    func cancelButtonClicked(input: Int)
}
class VehicleImagesCollectionViewAdaptor:  NSObject, ICollectionViewAdapter, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    var collectionView: UICollectionView
   
    typealias DataType = SubSection
    
    var data: [DataType] = []
    var galleryArray = [String]()
    
    private let spacing:CGFloat = 5.0
    var delegate: VehicleImagesCancelDelagate?
    required init(collectionView: UICollectionView) {
        self.collectionView = collectionView
        self.collectionView.backgroundColor = ColorConstants.clear
        super.init()
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(UINib(nibName: "VehicleHealthImagesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "VehicleHealthImagesCollectionViewCell")
        
//        let layout = UICollectionViewFlowLayout()
//        layout.sectionInset = UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing)
//        layout.minimumLineSpacing = spacing
//        layout.minimumInteritemSpacing = spacing
//        self.collectionView.collectionViewLayout = layout
        
    }
    
    func updateData(data: [SubSection]) {
        self.data = data
    }
    
    // MARK: - Update collection view with list of images URLs
    ///This function called to map SR data from contr0ller
    ///
    ///     Warning imagesModel should not be empty
    ///   - Parameter imagesModel: list of String values
    func updateGalleyImages(imagesModel: [String]){
        self.galleryArray = imagesModel
        self.collectionView.reloadData()
    }
    
    // MARK: - Collection view delegate and datasource methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.galleryArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell: VehicleHealthImagesCollectionViewCell = collectionView.dequeueReusableCell(
            withReuseIdentifier: "VehicleHealthImagesCollectionViewCell",
            for: indexPath) as? VehicleHealthImagesCollectionViewCell {
          
            let anObj = galleryArray[indexPath.item]

            let imagesDir = ImagesDirectory()
            let assetIamge = imagesDir.getFileAt(filepath: anObj)
            cell.assetImageView.image = assetIamge
            cell.assetImageView.layer.cornerRadius = 5.0
            cell.assetImageView.layer.masksToBounds = true
            cell.cancelButton.tag = indexPath.row
            cell.cancelButton.addTarget(self, action: #selector(cancelButtonClicked(sender:)), for: .touchUpInside)
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    
    // MARK: - Call back button action to delegate
    @objc func cancelButtonClicked(sender: UIButton){
        
        self.galleryArray.remove(at: sender.tag)
        delegate?.cancelButtonClicked(input: sender.tag)
        self.collectionView.reloadData()
    }
}
