//
//  VehicleImagesPresenter.swift
//  MSU
//
//  Created by vectorform on 20/08/21.
//

import Foundation
import SwiftyJSON
class VehicleImagesPresenter: BasePresenter, IVehicleImagesPresenter {
    
    
    private weak var myView: VehicleImagesViewController? {
        return self.view as? VehicleImagesViewController
    }
    
    var homeRouter: HomeRouter? {
        return navController as? HomeRouter
    }
    
    var vehicleCheckUpId: Int = 0
    
    // MARK: - Set vehicle check up id
    func setVehicleCheckUpId(input: Int) {
        vehicleCheckUpId = input
    }
    
    // MARK: - Get vehicle check up id
    func getVehicleCheckUpId() -> Int {
        return vehicleCheckUpId
    }
    func forwardButtonClicked(galleryArray: [String]){
        for i in 0..<galleryArray.count{
            let galleryImg = galleryArray[i]
            let assetName = galleryImg.lastPathComponent
            let imagesDir = ImagesDirectory()
            if let assetImage = imagesDir.getFileAt(filepath: galleryImg){
                let assetBase64String = self.myView?.convertImageToBase64String(img: assetImage)
                self.uploadAssets(assetName: assetName, assetBase64: assetBase64String ?? "")
            }
        }
        self.sectionRouter?.navigateToVehicleHealthCheckFlow(animate: false, checkUpId: vehicleCheckUpId)
    }
    
    // MARK: - Upload assets
    ///This function called to upload the assets to API
    ///
    ///     Warning assetName and assetBase64 should not be empty
    ///  - Parameter assetName: must be a string value
    ///  - Parameter assetName: must be a string value
    func uploadAssets(assetName: String, assetBase64: String){
        let postData: [String: AnyObject] = ["VehicleCheckupId": vehicleCheckUpId  as Any,
                                             "Asset": assetName as Any,
                                             "AssetBase64String": assetBase64 as Any,
                                             "IsLastChunk": false  as Any] as [String: AnyObject]
        print(JSON(postData as Any))
        FacadeLayer.sharedInstance.webserviceManager.genericAPICalls(requestParams: postData, apiPath: VFApiCalls.createVehicleCheckupAsset) { [weak self] (status, response) in
            guard let strongSelf = self else{ return }
            if(status){
                
            }
        }
    }
    
    // MARK: - back button action
    func backButtonClicked(){
        homeRouter?.didSelectBack()
    }
}
