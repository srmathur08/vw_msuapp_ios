//
//  VehicleImagesViewController.swift
//  MSU
//
//  Created by vectorform on 20/08/21.
//

import UIKit
import Photos

class VehicleImagesViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, VehicleImagesCancelDelagate {

    

    // MARK: - View Contoller Outlets
    @IBOutlet weak var sectionLabel: UILabel!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var uploadPhotoView: UIView!
    @IBOutlet weak var uploadPhotoLabel: UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var forwardButton: UIButton!
    
    @IBOutlet weak var errroLabel: UILabel!
    let picker = UIImagePickerController()
    
    private lazy var collectionViewAdapter: VehicleImagesCollectionViewAdaptor  = {
        VehicleImagesCollectionViewAdaptor(collectionView: self.imagesCollectionView)
    }()
    
    private let presenter: IVehicleImagesPresenter = VehicleImagesPresenter()
    
    var galleryImages = [String]()
    var vehicleCheckUpIdVal: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter.connectToView(view: self)
        presenter.setVehicleCheckUpId(input: self.vehicleCheckUpIdVal)
        // Do any additional setup after loading the view.
        picker.delegate = self
        picker.isEditing = true
        applyStylings()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if(FacadeLayer.sharedInstance.isFromHealthCheck){
            presenter.backButtonClicked()
        }
    }
    // MARK: - Apply Stylings to Objects
    private func applyStylings(){
        
        sectionLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: true, size: 18.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        
        uploadPhotoLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)

        errroLabel.applyLabelStyling(textColor: ColorConstants.redMarkColor, isBold: true, size: 16.0, alignment: .center, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        errroLabel.isHidden = true
        
        let uploadPhotoTap = UITapGestureRecognizer(target: self, action: #selector(self.uploadPhotosTapped(sender:)))
        uploadPhotoView.addGestureRecognizer(uploadPhotoTap)
        uploadPhotoView.isUserInteractionEnabled = true

        collectionViewAdapter.delegate = self
    }

    
    // MARK: - Upload photos view tapped
    ///This function called to when tapped on Upload photos view
    ///
    @objc func uploadPhotosTapped(sender: UITapGestureRecognizer){
        camera()
    }
    
    // MARK: - Open Camera
    ///This function called to open the camera
    ///
    func camera()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
                picker.sourceType = .camera
                picker.cameraDevice = .rear
            
            self.present(picker, animated: true, completion: nil)
        }
        else{
//            self.displayDefaultAlertForError(errorDescription: "No Camera")
        }
    }
    // MARK: - Image Picker Delegate Methods
    ///This function a imagePickerController delegate method
    ///
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
            if let chosenImage = info[.originalImage] as? UIImage{
                self.errroLabel.isHidden = true
                let imagesSection = ImagesDirectory()
                var imageName: String = ""
                let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let currentDateStr = formatter.string(from: Date())
                let currentDate = formatter.date(from: currentDateStr)
                    formatter.dateFormat = "dd-MM-yyyy-HH:mm:ss"
                let myStringafd = formatter.string(from: currentDate!)
                imageName = "Image\(myStringafd).png"
                let srID = String(describing: FacadeLayer.sharedInstance.vehicelCheckSrId).removeOptionalFromString()
                let imagepath = imagesSection.writeImageVehicleImages(imageName, image: chosenImage, srId: srID)
                self.galleryImages.append(imagepath)
                
                self.collectionViewAdapter.updateGalleyImages(imagesModel: self.galleryImages)
            }
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - User Interactions
    // MARK: - Cancel button action
    func cancelButtonClicked(input: Int) {
        self.galleryImages.remove(at: input)
    }
    
    // MARK: - Forward button action
    @IBAction func forwardbuttonAction(_ sender: UIButton) {
        presenter.forwardButtonClicked(galleryArray: self.galleryImages)
//        if(self.galleryImages.count > 0){
//            errroLabel.isHidden = true
//            presenter.forwardButtonClicked(galleryArray: self.galleryImages)
//        }
//        else{
//            errroLabel.isHidden = false
//        }
       
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
