//
//  VehicleHealthCheckViewController.swift
//  MSU
//
//  Created by vectorform on 28/07/21.
//

import UIKit

class VehicleHealthCheckViewController: BaseViewController, IVehicleHealthCheckViewController {

 
    
    // MARK: - View Contoller Outlets
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var middleView: UIView!
    @IBOutlet weak var dateNumberView: UIView!
    @IBOutlet weak var dateofServiceInputview: MSUInputTextFields!
    @IBOutlet weak var serviceNumberInputview: MSUInputTextFields!
    @IBOutlet weak var customerNameInputView: MSUInputTextFields!
    @IBOutlet weak var regVinView: UIView!
    @IBOutlet weak var registrationNumberInputview: MSUInputTextFields!
    @IBOutlet weak var vinNumberInputview: MSUInputTextFields!
    @IBOutlet weak var fuelView: UIView!
    @IBOutlet weak var fuelHeaderlabel: UILabel!
    @IBOutlet weak var vehicleMileageInputView: MSUInputTextFields!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var forwardButton: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var fuelSlider: FuelSlider!
    
    @IBOutlet weak var fuelPointerImage: UIImageView!
    private let presenter: IVehicleHealthCheckViewPresenter = VehicleHealthCheckViewPresenter()

    @IBOutlet weak var alertContentView: UIView!
    
    @IBOutlet weak var alertTextLabel: UILabel!
    
    @IBOutlet weak var goToVHCButton: MSUButton!
    
    @IBOutlet weak var skipButton: MSUButton!
    
    var fuelValue: Double = 0.0
    var serviceType: String = ""
    var sfdcReading: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.connectToView(view: self)
        presenter.getSRModel()
        // Do any additional setup after loading the view.
     
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if(FacadeLayer.sharedInstance.isFromHealthCheck){
            presenter.backButtonClicked()
        }
    }
    override func viewDidLayoutSubviews() {
        applyStylings()
    }
    
    // MARK: - Apply Stylings to Objects
    private func applyStylings(){
        headerLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: true, size: 18.0, alignment: .center, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        
        dateofServiceInputview.textInputView( hintText: "Date of Service", isSecureTextEntry: false, returnKeyType: UIReturnKeyType.next, keyboardtype: UIKeyboardType.default, textColor: ColorConstants.underlineColor, tintColor: ColorConstants.underlineColor, lineColor: ColorConstants.underlineColor)
        dateofServiceInputview.inputTextfield.frame = CGRect(x: dateofServiceInputview.frame.origin.x, y: dateofServiceInputview.frame.origin.y, width: 160.0, height: dateofServiceInputview.frame.size.height)
        dateofServiceInputview.inputTextfield.isEnabled = false
        dateofServiceInputview.updateWidthConstraint(widthVal: 150.0)
        
        serviceNumberInputview.textInputView( hintText: "Repair Service #", isSecureTextEntry: false, returnKeyType: UIReturnKeyType.next, keyboardtype: UIKeyboardType.default, textColor: ColorConstants.underlineColor, tintColor: ColorConstants.underlineColor, lineColor: ColorConstants.underlineColor)
        serviceNumberInputview.inputTextfield.frame = CGRect(x: serviceNumberInputview.frame.origin.x, y: serviceNumberInputview.frame.origin.y, width: 160.0, height: serviceNumberInputview.frame.size.height)
        serviceNumberInputview.inputTextfield.isEnabled = false
        serviceNumberInputview.updateWidthConstraint(widthVal: 150.0)
        
        customerNameInputView.textInputView( hintText: "Customer Name", isSecureTextEntry: false, returnKeyType: UIReturnKeyType.next, keyboardtype: UIKeyboardType.default, textColor: ColorConstants.appPrimaryColor, tintColor: ColorConstants.appPrimaryColor, lineColor: ColorConstants.underlineColor)
        customerNameInputView.inputTextfield.frame = CGRect(x: customerNameInputView.frame.origin.x, y: customerNameInputView.frame.origin.y, width: customerNameInputView.frame.size.width, height: customerNameInputView.frame.size.height)
        customerNameInputView.inputTextfield.isEnabled = false
        customerNameInputView.updateWidthConstraint(widthVal: self.view.frame.size.width - 50)
        
        registrationNumberInputview.textInputView( hintText: "Registration #", isSecureTextEntry: false, returnKeyType: UIReturnKeyType.next, keyboardtype: UIKeyboardType.default, textColor: ColorConstants.appPrimaryColor, tintColor: ColorConstants.appPrimaryColor, lineColor: ColorConstants.underlineColor)
        registrationNumberInputview.inputTextfield.frame = CGRect(x: registrationNumberInputview.frame.origin.x, y: registrationNumberInputview.frame.origin.y, width: 170.0, height: registrationNumberInputview.frame.size.height)
        registrationNumberInputview.inputTextfield.isEnabled = false
        registrationNumberInputview.updateWidthConstraint(widthVal: 150.0)
        
        vinNumberInputview.textInputView( hintText: "VIN", isSecureTextEntry: false, returnKeyType: UIReturnKeyType.next, keyboardtype: UIKeyboardType.default, textColor: ColorConstants.appPrimaryColor, tintColor: ColorConstants.appPrimaryColor, lineColor: ColorConstants.underlineColor)
        vinNumberInputview.inputTextfield.frame = CGRect(x: vinNumberInputview.frame.origin.x, y: vinNumberInputview.frame.origin.y, width: 160.0, height: vinNumberInputview.frame.size.height)
        vinNumberInputview.inputTextfield.isEnabled = false
        
        vinNumberInputview.updateWidthConstraint(widthVal: 150.0)
        
        vehicleMileageInputView.textInputView( hintText: "Vehicle Mileage", isSecureTextEntry: false, returnKeyType: UIReturnKeyType.done, keyboardtype: UIKeyboardType.phonePad, textColor: ColorConstants.appPrimaryColor, tintColor: ColorConstants.appPrimaryColor, lineColor: ColorConstants.underlineColor)
        vehicleMileageInputView.inputTextfield.frame = CGRect(x: vehicleMileageInputView.frame.origin.x, y: vehicleMileageInputView.frame.origin.y, width: vehicleMileageInputView.frame.size.width, height: vehicleMileageInputView.frame.size.height)
        vehicleMileageInputView.updateWidthConstraint(widthVal: self.view.frame.size.width - 50)
        
        skipButton.appButtonStyling(titleColor: ColorConstants.solidWhite, backgroundColor: ColorConstants.appSecondaryBlueColor)
        
        goToVHCButton.appButtonStyling(titleColor: ColorConstants.solidWhite, backgroundColor: ColorConstants.appSecondaryBlueColor)
        
        scrollViewHeightConstraint.constant = 500.0
        fuelSlider.minimumValue = 0
        fuelSlider.maximumValue = 100
        fuelSlider.value = 0
        for state: UIControl.State in [.normal, .selected, .highlighted] {
            fuelSlider.setThumbImage(UIImage.init(named: "sliderThumb"), for: state)
            }
        
        fuelSlider.addTarget(self, action: #selector(sliderValueChanged),for: .valueChanged)
    }

    @objc private func sliderValueChanged(_ sender: UISlider) {
        let sliderVal = sender.value
        let sliderValString  = String(format: "%.2f", sliderVal)
        fuelValue = Double(sliderValString) ?? 0.0
        print(fuelValue)
        self.fuelPointerImage.center.x = sender.setThumbValueWithLabel().x
        
    }
    // MARK: - Bind UI with model
     func bindUI(vehicleModel: ServiceRequests){
        let serviceDate = vehicleModel.serviceDateTime ?? ""
        let serviceDateStr = serviceDate.convertDateStringToFormats(input: serviceDate)
        let splitDate = serviceDateStr.split(separator: "|")
        if(splitDate.count > 0){
            let finalDate = String(describing: splitDate[0]).removeOptionalFromString()
            dateofServiceInputview.inputTextfield.text = finalDate
        }
        
        serviceNumberInputview.inputTextfield.text = vehicleModel.srnumber ?? ""
        customerNameInputView.inputTextfield.text = vehicleModel.customerName ?? ""
        registrationNumberInputview.inputTextfield.text = vehicleModel.regNum ?? ""
        vinNumberInputview.inputTextfield.text = vehicleModel.vinnumber ?? ""
        vehicleMileageInputView.inputTextfield.text = vehicleModel.odometerReading ?? ""
        sfdcReading = vehicleModel.sFDCOdometerReading ?? 0
    }
    
    // MARK: - Close button action
    @IBAction func closeButtonAction(_ sender: UIButton) {
        presenter.backButtonClicked()
    }
    
    // MARK: - Forword button aciton
    @IBAction func forwardButtonAction(_ sender: UIButton) {
        let vehicleInt = Int(vehicleMileageInputView.inputTextfield.text ?? "") ?? 0
        
        let error = ErrorStringConstants.mileageError + " " + String(describing: sfdcReading).removeOptionalFromString()
        if(vehicleInt < sfdcReading){
            vehicleMileageInputView.showErrorView(message: " ", errorMessage: error)
        }
        else{
            presenter.startVehicelHealthCheck(fuel: self.fuelValue, mileage: vehicleMileageInputView.inputTextfield.text ?? "")
        }
    }
    
    
    @IBAction func skipButtonAction(_ sender: MSUButton) {
        presenter.skipVHCclicked()
    }
    
    
    @IBAction func goVHCButtonAction(_ sender: MSUButton) {
        presenter.proceedVHCclicked(fuel: self.fuelValue, mileage: vehicleMileageInputView.inputTextfield.text ?? "")
    }
    // MARK: - Show Skip Alert for VHC
    func showSkipVHCAlert() {
        self.alertContentView.isHidden = false
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
