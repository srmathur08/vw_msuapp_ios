//
//  VehicleHealthCheckPresenter.swift
//  MSU
//
//  Created by vectorform on 28/07/21.
//

import Foundation
import SwiftyJSON
import ObjectMapper

class VehicleHealthCheckViewPresenter: BasePresenter, IVehicleHealthCheckViewPresenter {



    private weak var myView: IVehicleHealthCheckViewController? {
        return self.view as? IVehicleHealthCheckViewController
    }
    
    var homeRouter: HomeRouter? {
        return navController as? HomeRouter
    }
    
    // MARK: - back button action
    func backButtonClicked() {
        homeRouter?.didSelectBack()
    }

    // MARK: - Forward button action
    func forwardButtonClicked(fuel: Double, mileage: String) {
//        homeRouter?.pushVehicleImagesViewController(animated: true, vehicleCheckUpId: 32)
//        FacadeLayer.sharedInstance.vehicleCheckUpId = 14
      
        if let model = homeRouter?.getInProgressSR(){
            let postData: [String: AnyObject] = ["Srid": model.srid as Any,
                                                 "VehicleMileage": mileage as Any,
                                                 "FuelLevel": fuel as Any] as [String: AnyObject]
        FacadeLayer.sharedInstance.webserviceManager.genericAPICalls(requestParams: postData, apiPath: VFApiCalls.createVehicleCheckup) {  [weak self] (status, response) in
            guard let strongSelf = self else{ return }
            if(status){
                let responseVal = JSON(response as Any)
                if let responseModel = Mapper<VehicleHealthCheckUpIdModel>().map(JSONObject:responseVal.dictionaryObject){
                    FacadeLayer.sharedInstance.vehicleCheckUpId = responseModel.vehicleCheckupId ?? 0
                    strongSelf.homeRouter?.pushVehicleImagesViewController(animated: true, vehicleCheckUpId: responseModel.vehicleCheckupId ?? 0)
             }
                strongSelf.myView?.removeLoader()
           }
           else{
            strongSelf.myView?.removeLoader()
               let responseVal = JSON(response as Any)
               if let responseModel = Mapper<VCErrorModel>().map(JSONObject:responseVal.dictionaryObject){
                   FacadeLayer.sharedInstance.vehicleCheckUpId = responseModel.vehicleCheckupId ?? 0
                   strongSelf.homeRouter?.pushVehicleImagesViewController(animated: true, vehicleCheckUpId: responseModel.vehicleCheckupId ?? 0)
            }
            }
         }
       }
   }
    
    // MARK: - Start vehicle health check
    func startVehicelHealthCheck(fuel: Double, mileage: String){
        self.myView?.showLoader()
        if let model = homeRouter?.getInProgressSR(){
            let postData: [String: AnyObject] = ["Srid": model.srid as Any,
                                                 "StatusId": UpdateStatus.isStarted as Any,
                                                 "ServiceOther": "" as Any,
                                                 "Revenue": "" as Any] as [String: AnyObject]
            FacadeLayer.sharedInstance.webserviceManager.genericAPICalls(requestParams: postData, apiPath: VFApiCalls.updateSRStatus) {   [weak self] (status, response) in
                guard let strongSelf = self else{ return }
                if(status){
                    if(model.serviceType?.caseInsensitiveCompare(ServiceTypes.maintenance) == .orderedSame) || (model.serviceType?.caseInsensitiveCompare(ServiceTypes.inspectionService) == .orderedSame){
                        strongSelf.forwardButtonClicked(fuel: fuel, mileage: mileage)
                    }
                    else{
                        strongSelf.myView?.removeLoader()
                        strongSelf.myView?.showSkipVHCAlert()
                    }
                }
                else{
                    strongSelf.myView?.removeLoader()
                       let errorVal = JSON(response as Any)
                       let errorString = "\(errorVal)"
                       print(errorString)
                       var errormsg: String = ""
                       if(errorString != ErrorStringConstants.noInternet){
                               errormsg = errorVal["Reason"].stringValue
                           if(errormsg == ""){
                               errormsg = ErrorStringConstants.defaultError
                           }
                       }
                       else{
                           errormsg = ErrorStringConstants.noInternet
                       }
                       strongSelf.myView?.displayDefaultAlertForError(errorTitle: ErrorStringConstants.errorTitle, errorDescription: errormsg)
                    }
            }
        }
    }
    
    // MARK: - Get SR model
    func getSRModel()
    {
        if let model = homeRouter?.getInProgressSR(){
            myView?.bindUI(vehicleModel: model)
        }
    }
    
    // MARK: - Proceed VHC Clicked
    func proceedVHCclicked(fuel: Double, mileage: String) {
        self.forwardButtonClicked(fuel: fuel, mileage: mileage)
    }
    
    // MARK: - Skip VHC Clicked
    func skipVHCclicked() {
        self.homeRouter?.InProgressRequests?.isVCCompleted = true
        homeRouter?.didSelectBack()
    }
    
    
}
