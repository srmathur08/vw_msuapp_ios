//
//  VehicleHealthCheckContract.swift
//  MSU
//
//  Created by vectorform on 28/07/21.
//

import Foundation

protocol IVehicleHealthCheckViewController: IBaseViewController {
    ///This function called to set the values from SR mdel
    ///
    ///    Warning vehicleModel should not be empty
    ///  - Parameter vehicleModel: must be a ServiceRequests model
    func bindUI(vehicleModel: ServiceRequests)
    
    ///This function called to Skip for VHC
    ///

    func showSkipVHCAlert()
    
}


protocol IVehicleHealthCheckViewPresenter: IBasePresenter {
    ///This function called to pop the view controller
    ///
    func backButtonClicked()
    
    ///This function called on forward button action
    ///
    ///    Warning fuel and mileage should not be empty
    ///  - Parameter fuel: must be a Integer value
    ///  - Parameter mileage: must be a string value
    func forwardButtonClicked(fuel: Double, mileage: String)
    
    ///This function called to get SR model from home router
    ///
    func getSRModel()
    
    ///This function called to start the vehicle health check
    ///
    ///  - Parameter fuel: must be a Integer value
    ///  - Parameter mileage: must be a string value
    func startVehicelHealthCheck(fuel: Double, mileage: String)
    
    
    ///This function called when click on Proceed on alert
    ///
    ///  - Parameter fuel: must be a Integer value
    ///  - Parameter mileage: must be a string value
    func proceedVHCclicked(fuel: Double, mileage: String)
    
    
    ///This function called to start the vehicle health check
    ///
    func skipVHCclicked()
    
}
