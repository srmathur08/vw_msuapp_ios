//
//  HealthPreviewPresenter.swift
//  MSU
//
//  Created by vectorform on 12/08/21.
//

import Foundation
import SwiftyJSON
import ObjectMapper

class HealthPreviewPresenter: BasePresenter, IHealthPreviewPresenter {
 

    var vehicleReportModel: [VehicleReportModel]?
    
    private weak var myView: IHealthPreviewViewController? {
        return view as? IHealthPreviewViewController
    }
    
    var homeRouter: HomeRouter? {
        return navController as? HomeRouter
    }
    
    // MARK: - Close button action
    func closeButtonClicked(){
        if let model = homeRouter?.getInProgressSR(){
            self.myView?.showLoader()
            let postData: [String : AnyObject]  = ["Srid":model.srid as Any] as [String: AnyObject]
            
            FacadeLayer.sharedInstance.webserviceManager.genericAPICalls(requestParams: postData, apiPath: VFApiCalls.sendVehicleHistory) { [weak self] (status , response) in
                guard let strongSelf = self else{ return }
                if(status){
                    strongSelf.myView?.removeLoader()
                    let alertController = UIAlertController.init(title: ErrorStringConstants.defaultTitle, message: ErrorStringConstants.sendVHC, preferredStyle: .alert)
                    alertController.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: { (action) in
                        strongSelf.homeRouter?.pushHomeToWIPController(animated: false, model: model)
                    }))
                    strongSelf.myView?.present(alertController, animated: true, completion: nil)
                }
                else{
                    strongSelf.myView?.removeLoader()
                    strongSelf.myView?.displayDefaultAlertForError(errorTitle: ErrorStringConstants.errorTitle, errorDescription: ErrorStringConstants.defaultError)
                }
            }
        }
    }
    
    // MARK: - Mark vehicle health check completed
    func markVehicleHealthCheckCompleted(){
        let vehicleCheckUpId = homeRouter?.getVehicleCheckUpId()
        let postData :[String: AnyObject] = ["Id": vehicleCheckUpId as Any]  as [String: AnyObject]
        
        FacadeLayer.sharedInstance.webserviceManager.genericAPICalls(requestParams: postData, apiPath: VFApiCalls.markVehicleHealthCheckComplete) { [weak self] (status, response) in
            guard let strongSelf = self else{ return }
            if(status){
                strongSelf.homeRouter?.InProgressRequests?.isVCCompleted = true
            }
        }
        
    }
    
    // MARK: - Create report model
    func createReportModel(vehicleModel: [VehicleHealthCheckModel]) {
        vehicleReportModel = [VehicleReportModel]()
        vehicleReportModel?.append(VehicleReportModel(name: vehicleModel[0].sectionName ?? "", icon: vehicleModel[0].icon ?? "", score: vehicleModel[0].score ?? "", scoreColor: vehicleModel[0].scoreColor ?? "")!)
       
        vehicleReportModel?.append(VehicleReportModel(name: vehicleModel[1].sectionName ?? "", icon: vehicleModel[1].icon ?? "", score: vehicleModel[1].score ?? "", scoreColor: vehicleModel[1].scoreColor ?? "")!)
        
        vehicleReportModel?.append(VehicleReportModel(name: vehicleModel[2].sectionName ?? "", icon: vehicleModel[2].icon ?? "", score: vehicleModel[2].score ?? "", scoreColor: vehicleModel[2].scoreColor ?? "")!)
        
        vehicleReportModel?.append(VehicleReportModel(name: vehicleModel[3].sectionName ?? "", icon: vehicleModel[3].icon ?? "", score: vehicleModel[3].score ?? "", scoreColor: vehicleModel[3].scoreColor ?? "")!)
        
        vehicleReportModel?.append(VehicleReportModel(name: vehicleModel[4].sectionName ?? "", icon: vehicleModel[4].icon ?? "", score: vehicleModel[4].score ?? "", scoreColor: vehicleModel[4].scoreColor ?? "")!)
        
        vehicleReportModel?.append(VehicleReportModel(name: vehicleModel[5].sectionName ?? "", icon: vehicleModel[5].icon ?? "", score: vehicleModel[5].score ?? "", scoreColor: vehicleModel[5].scoreColor ?? "")!)
        
        
        var repairCount: Int = 0
        var damageCount: Int = 0
        var totalCount: String = ""
        let model = vehicleModel[6]
            for i in 0..<model.images360Array.count{
                for j in 0..<model.images360Array[i].markers!.count{
                    if(model.images360Array[i].markers![j].mark == StringConstants.repairMarker){
                    repairCount += 1
                }
                    else if(model.images360Array[i].markers![j].mark == StringConstants.damageMarker){
                    damageCount += 1
                }
            }
        }
        totalCount = String(describing: repairCount + damageCount).removeOptionalFromString()
        vehicleReportModel?.append(VehicleReportModel(name: vehicleModel[6].sectionName ?? "", icon: vehicleModel[6].icon ?? "", score: totalCount, scoreColor: vehicleModel[6].scoreColor ?? "")!)
        
        myView?.bindSectionsTableData(model: vehicleReportModel!)
    }
    
    // MARK: - Update service completed
    func updateServiceCompleted(){
        if let srId = homeRouter?.InProgressRequests?.srid{
            DefaultsWrapper.setSRCompleted(key: "\(srId)", value: true)
            self.getVehicleHealthCheckData(srIdVal: srId)
            myView?.updateReportDetails(model: (homeRouter?.InProgressRequests)!)
        }
    }
    
    // MARK: - Get vehicle health check data
    ///This function called to get the VehicleHealthCheckData
    ///
    ///     Warning srIdVal should not be empty
    ///  - Parameter srIdVal: must be a integer value
    func getVehicleHealthCheckData(srIdVal: Int){
        
        let postData: [String: AnyObject] = ["Srid": srIdVal as Any] as [String: AnyObject]
        
        FacadeLayer.sharedInstance.webserviceManager.genericAPICalls(requestParams: postData, apiPath: VFApiCalls.getVehicleCheckupData) { [weak self] (status, response) in
            guard let strongSelf = self else{ return }
            if(status){
                
                let responseVal = JSON(response as Any)
                if let responseModel = Mapper<VCReportModel>().map(JSONObject:responseVal.dictionaryObject){
                    if let modelVal = responseModel.vehicleCheckup?[0].vehicleCheckupAssets{
                        strongSelf.myView?.updateCollectionView(model: modelVal)
                    }
                    if let fuelVal = responseModel.vehicleCheckup?[0].fuelLevel{
                        strongSelf.myView?.updateFuelValue(input: fuelVal)
                    }
                   
                }
            }
        }
    }
    
    // MARK: - Show report details
    func showReportDetails(vehicleModel: VehicleHealthCheckModel)
    {
        homeRouter?.pushVehicleInspectionDetailsController(animated: true, model: vehicleModel)
    }
    
    // MARK: - show 360 report details
    func showReport360Details(signModel: VehicleHealthCheckModel, markersModel: VehicleHealthCheckModel) {
        homeRouter?.pushVehickeInspection360DetailsController(animated: true, markersModel: markersModel, signModel: signModel)
    }
    
    // MARK: - Show images in full screen
    func showReportImagePreview(indexVal: Int, imagesArray: [VehicleCheckupAssets])
    {
        homeRouter?.presentImageViewController(index: indexVal, images: [], reportImages: imagesArray, isModel: true)
    }
}
