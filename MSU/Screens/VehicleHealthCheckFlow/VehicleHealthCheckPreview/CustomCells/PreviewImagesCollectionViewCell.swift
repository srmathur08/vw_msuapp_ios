//
//  PreviewImagesCollectionViewCell.swift
//  MSU
//
//  Created by vectorform on 16/08/21.
//

import UIKit

class PreviewImagesCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var carImagesView: UIImageView!
  
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        carImagesView.layer.cornerRadius = 10
        carImagesView.layer.masksToBounds = true

    }

}
