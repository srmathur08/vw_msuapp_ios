//
//  HealthPreviewTableViewCell.swift
//  MSU
//
//  Created by vectorform on 12/08/21.
//

import UIKit

class HealthPreviewTableViewCell: UITableViewCell {

    
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var sectionName: UILabel!
    @IBOutlet weak var percentageLabel: UILabel!
    @IBOutlet weak var detailArrow: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        addViews()
    }
    
    func addViews() {
        self.sectionName.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: false, size: 16.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
