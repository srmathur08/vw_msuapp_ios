//
//  HealthPreviewTableViewAdaptor.swift
//  MSU
//
//  Created by vectorform on 12/08/21.
//

import Foundation
import UIKit

protocol ReportSubsectionDelegate: AnyObject {
    ///This function called pass model from adaptor to controller
    ///
    ///     Warning name should not be empty
    ///   - Parameter name: must be a string value
    func getSectionName(name: String)
}

class HealthPreviewTableViewAdaptor:  NSObject, ITableViewAdapter, UITableViewDelegate, UITableViewDataSource
{
    var tableView: UITableView
    typealias DataType = VehicleReportModel
    
    var data: [DataType] = []
    
    var delegate: ReportSubsectionDelegate?
    required init(tableView: UITableView) {
        self.tableView = tableView
        self.tableView.backgroundColor = ColorConstants.clear
        super.init()
        let nibName = UINib(nibName: "HealthPreviewTableViewCell", bundle:nil)
        self.tableView.register(nibName, forCellReuseIdentifier: "HealthPreviewTableViewCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 60.0
        self.tableView.rowHeight = UITableView.automaticDimension
    }
    // MARK: - Update table Data
    ///This function called to map data data from contr0ller
    ///
    ///     Warning data should not be empty
    ///   - Parameter data: list of VehicleReportModel
    func updateData(data: [VehicleReportModel]) {
        self.data = data
        tableView.reloadData()
    }
    
    // MARK: - Table view delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: HealthPreviewTableViewCell = tableView.dequeueReusableCell(
            withIdentifier: "HealthPreviewTableViewCell",
            for: indexPath) as? HealthPreviewTableViewCell {
         
            let anObj = self.data[indexPath.row]
            let sectionName = anObj.sectionName
           
           
            cell.imageIcon.image = UIImage(named: anObj.icon ?? "") 
            let scoreVal = anObj.score
            if(scoreVal == "" || scoreVal == nil){
                cell.percentageLabel.text = "0 %"
            }
            else{
                cell.percentageLabel.text = scoreVal?.appending(" %")
            }
            
            let scoreColor = anObj.scoreColor
            if(scoreColor == StringConstants.vehicleHealthGreenStatus){
                cell.percentageLabel.textColor = ColorConstants.greenMarkColor
            }
            else if(scoreColor == StringConstants.vehicleHealthYellowStatus){
                cell.percentageLabel.textColor = ColorConstants.yellowMarkColor
            }
            else{
                cell.percentageLabel.textColor = ColorConstants.redMarkColor
            }
            if(sectionName?.caseInsensitiveCompare(StringConstants.scratch) == .orderedSame){
                cell.sectionName.text = "Vehicle Images"
                cell.percentageLabel.text = anObj.score
                cell.percentageLabel.textColor = ColorConstants.greenMarkColor
            }
            else{
                cell.sectionName.text = sectionName
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let sectionName = self.data[indexPath.row].sectionName{
            delegate?.getSectionName(name: sectionName)
        }
    }
    
}
