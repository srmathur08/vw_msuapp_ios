//
//  HealthPreviewContract.swift
//  MSU
//
//  Created by vectorform on 12/08/21.
//

import Foundation

protocol IHealthPreviewViewController: IBaseViewController {
    ///This function called to set the section table view UI
    ///
    ///     Warning model should not be empty
    ///  - Parameter marker: must be a list of VehicleReportModel model
    func bindSectionsTableData(model: [VehicleReportModel])
    
    ///This function called to update the collection view
    ///
    ///     Warning model should not be empty
    ///  - Parameter marker: must be a list of VehicleCheckupAssets model
    func updateCollectionView(model: [VehicleCheckupAssets])
    
    ///This function called to  update report details
    ///
    ///     Warning model should not be empty
    ///  - Parameter marker: must be a ServiceRequests model
    func updateReportDetails(model: ServiceRequests)
    
    
    ///This function called to update the collection view
    ///
    ///     Warning input should not be empty
    ///  - Parameter input: must be a Integer
    func updateFuelValue(input: Int)
}


protocol IHealthPreviewPresenter: IBasePresenter {
 
    ///This function called to pop back
    ///
    func closeButtonClicked()
    
    ///This function called to create Report model with images
    ///
    ///     Warning vehicleModel should not be empty
    ///  - Parameter vehicleModel: must be a list of VehicleHealthCheckModel model
    func createReportModel(vehicleModel: [VehicleHealthCheckModel])
    
    ///This function called to push the report details
    ///
    ///     Warning vehicleModel should not be empty
    ///  - Parameter vehicleModel: must be a VehicleHealthCheckModel model
    func showReportDetails(vehicleModel: VehicleHealthCheckModel)
    
    ///This function called to push the 360 report details
    ///
    ///     Warning signModel should not be empty
    ///  - Parameter signModel: must be a VehicleHealthCheckModel model
    ///  - Parameter markersModel: must be a VehicleHealthCheckModel model
    func showReport360Details(signModel: VehicleHealthCheckModel, markersModel: VehicleHealthCheckModel)
    
    ///This function called to update the SR completed
    ///
    func updateServiceCompleted()
    
    ///This function called to present the images in Full view
    ///
    ///     Warning indexVal and imagesArray should not be empty
    ///  - Parameter indexVal: must be a integet value
    ///  - Parameter imagesArray: must be a list of imagesArray model
    func showReportImagePreview(indexVal: Int, imagesArray: [VehicleCheckupAssets])
    
    ///This function called to mark vehicle health check completed
    ///
    func markVehicleHealthCheckCompleted()
}

