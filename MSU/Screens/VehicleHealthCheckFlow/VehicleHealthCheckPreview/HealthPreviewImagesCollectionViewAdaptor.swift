//
//  HealthPreviewImagesCollectionViewAdaptor.swift
//  MSU
//
//  Created by vectorform on 16/08/21.
//

import Foundation
import UIKit
import Kingfisher

protocol ReportImagesSelectionDelegate: AnyObject {
    ///This function called pass model from adaptor to controller
    ///
    ///     Warning name should not be empty
    ///   - Parameter index: must be a string value
    ///   - Parameter imagesModel: must be a list of VehicleCheckupAssets value
    func imageTapped(index: Int, imagesModel:[VehicleCheckupAssets])
}

class HealthPreviewImagesCollectionViewAdaptor: NSObject, ICollectionViewAdapter, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{

    var collectionView: UICollectionView
    
    typealias DataType = VehicleCheckupAssets
    
    var data: [DataType] = []
    private let spacing:CGFloat = 2.0
    
    var staticImages: [UIImage] = [UIImage]()
    var delegate: ReportImagesSelectionDelegate?
    required init(collectionView: UICollectionView) {
        self.collectionView = collectionView
        self.collectionView.backgroundColor = ColorConstants.clear
        super.init()
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
        staticImages = [UIImage(named: "carFrontImage")!,UIImage(named: "carSideImage")!,UIImage(named: "carFrontImage")!,UIImage(named: "carSideImage")!]
        
    }
    
    ///This function called to map data data from contr0ller
    ///
    ///     Warning data should not be empty
    ///   - Parameter data: list of VehicleReportModel
    func updateData(data: [VehicleCheckupAssets]) {
        self.data = data
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.data.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell: PreviewImagesCollectionViewCell = collectionView.dequeueReusableCell(
            withReuseIdentifier: "PreviewImagesCollectionViewCell",
            for: indexPath) as? PreviewImagesCollectionViewCell {
        
            let anObj = self.data[indexPath.row]
//            cell.carImagesView.image = anObj
            let baseRequest = BaseRequest()
            var baseUrlString = baseRequest.baseUrlString
            let imagePath = anObj.asset ?? ""

            if(imagePath.count != 0){
                let imgPath = imagePath
                    baseUrlString += imgPath
                    cell.carImagesView.kf.indicatorType = .activity
                let url = URL(string: baseUrlString)
                     cell.carImagesView.kf.setImage(with: url)
                }
            return cell
        }
        return UICollectionViewCell()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.imageTapped(index: indexPath.item, imagesModel: self.data)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 280.0, height: 250.0)
    }
}
