//
//  HealthPreviewViewController.swift
//  MSU
//
//  Created by vectorform on 12/08/21.
//

import UIKit

class HealthPreviewViewController: BaseViewController, IHealthPreviewViewController, ReportSubsectionDelegate, ReportImagesSelectionDelegate {

    




    // MARK: - View Contoller Outlets
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var vehicleModelName: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewContentView: UIView!
    @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var topContainerView: UIView!
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var detailsView: CardView!
    @IBOutlet weak var dateTitleLabel: UILabel!
    @IBOutlet weak var dateDataLabel: UILabel!
    @IBOutlet weak var roTitleLabel: UILabel!
    @IBOutlet weak var roDataLabel: UILabel!
    @IBOutlet weak var customerNameTitleLabel: UILabel!
    @IBOutlet weak var customerNameDataLabel: UILabel!
    @IBOutlet weak var regNoTitleLabel: UILabel!
    @IBOutlet weak var regNoDataLabel: UILabel!
    @IBOutlet weak var vinTitleLabel: UILabel!
    @IBOutlet weak var vinDatalabel: UILabel!
    @IBOutlet weak var fuelTitleLabel: UILabel!
    @IBOutlet weak var fuelDataLabel: UILabel!
    @IBOutlet weak var serviceAdvisorTitleLabel: UILabel!
    @IBOutlet weak var serviceAdvisorDataLabel: UILabel!
    @IBOutlet weak var tableCardView: CardView!
    @IBOutlet weak var subSectionsTableView: UITableView!
    
    private let presenter: IHealthPreviewPresenter = HealthPreviewPresenter()
    
    private lazy var tableViewAdapter: HealthPreviewTableViewAdaptor  = {
        HealthPreviewTableViewAdaptor(tableView: self.subSectionsTableView)
    }()
    
    private lazy var collectionViewAdapter: HealthPreviewImagesCollectionViewAdaptor = {
        HealthPreviewImagesCollectionViewAdaptor(collectionView: self.imagesCollectionView)
    }()
    
    var vehicleModel: [VehicleHealthCheckModel]?
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter.connectToView(view: self)
        presenter.updateServiceCompleted()
        presenter.markVehicleHealthCheckCompleted()
        // Do any additional setup after loading the view.
        applyStylings()
    }
    
    // MARK: - Apply Stylings to Objects
   private func applyStylings(){
    
    topView.backgroundColor = ColorConstants.appPrimaryColor
    
    vehicleModelName.applyLabelStyling(textColor: ColorConstants.solidWhite, isBold: true, size: 18.0, alignment: .center, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    
    topContainerView.backgroundColor = ColorConstants.appPrimaryColor
    
    dateTitleLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    
    dateDataLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    
    roTitleLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    
    roDataLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    
    customerNameTitleLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    
    customerNameDataLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    
    regNoTitleLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    
    regNoDataLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    
    vinTitleLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    
    vinDatalabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    
    fuelTitleLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    
    fuelDataLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    
    serviceAdvisorTitleLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    
    serviceAdvisorDataLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    
        scrollViewHeightConstraint.constant = 950
    
        presenter.createReportModel(vehicleModel: vehicleModel!)
        
   }


    // MARK: - Bind sections data
    func bindSectionsTableData(model: [VehicleReportModel]){
        tableViewAdapter.delegate = self
        tableViewAdapter.updateData(data: model)
    }
    
    // MARK: - Update Collection View Data
    func updateCollectionView(model: [VehicleCheckupAssets]){
        collectionViewAdapter.delegate = self
        collectionViewAdapter.updateData(data: model)
    }
    
    // MARK: - Image tapped
    func imageTapped(index: Int, imagesModel: [VehicleCheckupAssets]) {
        presenter.showReportImagePreview(indexVal: index, imagesArray: imagesModel)
    }
    
    

    // MARK: - Bind sections data
    func updateReportDetails(model: ServiceRequests){
        let serviceDate = model.serviceDateTime ?? ""
        let serviceDateStr = serviceDate.convertDateStringToFormats(input: serviceDate)
        let splitDate = serviceDateStr.split(separator: "|")
        if(splitDate.count > 0){
            let finalDate = String(describing: splitDate[0]).removeOptionalFromString()
            dateDataLabel.text = finalDate
        }
        roDataLabel.text = model.srnumber ?? ""
        customerNameDataLabel.text = model.customerName ?? ""
        regNoDataLabel.text = model.regNum ?? ""
        vinDatalabel.text = model.vinnumber ?? ""
        
        serviceAdvisorDataLabel.text = DefaultsWrapper.username
        vehicleModelName.text = model.vehicleModel ?? ""
    }
    
    func updateFuelValue(input: Int) {
        fuelDataLabel.text = String(describing: input).removeOptionalFromString() + " %"
    }
    
    // MARK: - Get section name
    func getSectionName(name: String) {
        let sectionArray = self.vehicleModel?.filter({
            $0.sectionName == name
        })
        if (sectionArray?.count == 0){
            return
        }
        
        let markersArray = self.vehicleModel?.filter({
            $0.sectionName == StringConstants.scratch
        })
        
        let signArray = self.vehicleModel?.filter({
            $0.sectionName ==  StringConstants.acknowledge
        })
        
        if let sectionModel = sectionArray?[0]{
            let name = sectionModel.sectionName
            if(name?.caseInsensitiveCompare("Scratch/Dent/Damage") == .orderedSame){
                if let signModel = signArray?[0], let markersModel = markersArray?[0]{
                    presenter.showReport360Details(signModel: signModel, markersModel: markersModel)
                }
            }
            else{
                presenter.showReportDetails(vehicleModel: sectionModel)
            }
        }
        
    }
    // MARK: - Close button action
    @IBAction func closeButtonAction(_ sender: UIButton) {
        presenter.closeButtonClicked()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
