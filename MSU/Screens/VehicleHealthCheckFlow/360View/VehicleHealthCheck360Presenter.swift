//
//  VehicleHealthCheck360Presenter.swift
//  MSU
//
//  Created by vectorform on 11/08/21.
//

import Foundation
import UIKit

class VehicleHealthCheck360Presenter: BasePresenter, IVehicleHealthCheck360ViewPresenter {

    private weak var myView: IVehicleHealthCheck360ViewController?
    {
        return self.view as? IVehicleHealthCheck360ViewController
    }
    var currentPosition: Int = 0
    private var vehicleHealthCheckRouter: VehicleHealthCheckRouter? {
        return navController as? VehicleHealthCheckRouter
    }
    
    var images360Model: [Images360Model] = [Images360Model]()
    var markersModel: [ImageMarkerModel] = [ImageMarkerModel]()
    
    var vehicleModel: VehicleHealthCheckModel?
    
    // MARK: - get model from Controller
    func getVehicleHealthCheckModel(input: VehicleHealthCheckModel)
    {
        self.vehicleModel = input
    }
    // MARK: - Car Image tapped
    func carImageTapped(model: VehicleHealthCheckModel, index: Int)
    {
         let vehicleModel = model.images360Array
        myView?.updateDotMarkers(model: vehicleModel[index])
        self.vehicleModel = model
        if let model = self.vehicleModel{
            self.updateModelInRouter(input: model)
        }
        
    }
    // MARK: - Update markers in Controller and Router
    func updateMarkers(marker: ImageMarkerModel) {
        vehicleModel?.images360Array[currentPosition].markers?.append(marker)
        myView?.updateDotMarkers(model: (vehicleModel?.images360Array[currentPosition])!)
        updateModelInRouter(input: vehicleModel!)
    }
    
    // MARK: - Undo Marker
    func undoMarkers() {
        if((vehicleModel?.images360Array[currentPosition].markers!.count)! > 0){
            let count = (vehicleModel?.images360Array[currentPosition].markers!.count)!
            vehicleModel?.images360Array[currentPosition].markers?.remove(at: count  - 1)
            myView?.updateDotMarkers(model: (vehicleModel?.images360Array[currentPosition])!)
        }
        updateModelInRouter(input: vehicleModel!)
    }
    
    // MARK: - Next Image button Action
    func onNextClick() {
        if(currentPosition < (vehicleModel?.images360Array.count)! - 1){
            currentPosition += 1
            myView?.updateDotMarkers(model: (vehicleModel?.images360Array[currentPosition])!)
        }
        else{
            currentPosition = 0
            myView?.updateDotMarkers(model: (vehicleModel?.images360Array[currentPosition])!)
        }
    }
    
    // MARK: - Previous Image Button action
    func onPreviousClick() {
        if(currentPosition  == 0){
            currentPosition = (vehicleModel?.images360Array.count)! - 1
            myView?.updateDotMarkers(model: (vehicleModel?.images360Array[currentPosition])!)
        }
        else{
            currentPosition -= 1
            myView?.updateDotMarkers(model: (vehicleModel?.images360Array[currentPosition])!)
        }
    }

    // MARK: - Update remarks in model
    func updateRemarks(remarkName: String, remarkValue: String)
    {
        switch remarkName {
        case StringConstants.cleaningRemark:
            self.vehicleModel?.cleaningRemark = remarkValue
        case StringConstants.damageRemark:
            self.vehicleModel?.damageRemark = remarkValue
        case StringConstants.otherNotes:
            self.vehicleModel?.otherNotes = remarkValue
        default:
            print("Test")
        }
        if let model = self.vehicleModel{
            self.updateModelInRouter(input: model)
        }
    }
    
    // MARK: - Present landscape view
    func presentLandscapeView(delegate: Landscape360ViewDelagate, height:CGFloat, width: CGFloat)
    {
        self.vehicleHealthCheckRouter?.presentLandScape360ViewController(animated: true, index: currentPosition, inputModel: self.vehicleModel!, delegate: delegate, heightVal: height, widthVal: width)
    }
    // MARK: - Update values in router model
    func updateModelInRouter(input: VehicleHealthCheckModel) {
        self.vehicleHealthCheckRouter?.updateVehicleHealthModel(inputModel: input)
    }
    
}
