//
//  VehicleHealth360ViewController.swift
//  MSU
//
//  Created by vectorform on 11/08/21.
//

import UIKit

class VehicleHealth360ViewController: BaseViewController, IVehicleHealthCheck360ViewController, Landscape360ViewDelagate {

  
    // MARK: - View Contoller Outlets
    @IBOutlet weak var sectionTitleLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollContentView: UIView!
    @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var carImageBaseView: UIImageView!
    @IBOutlet weak var rotateLabel: UILabel!
    @IBOutlet weak var markView: UIView!
    @IBOutlet weak var markRepairView: UIView!
    @IBOutlet weak var markRepairDotView: UIView!
    @IBOutlet weak var markRepairLabel: UILabel!
    @IBOutlet weak var markDamageView: UIView!
    @IBOutlet weak var markDamageDotView: UIView!
    @IBOutlet weak var markDamageLabel: UILabel!
    @IBOutlet weak var cleaningRemarkInputView: MSUInputTextFields!
    @IBOutlet weak var damageRemarkInputView: MSUInputTextFields!
    @IBOutlet weak var othersRemarkInputView: MSUInputTextFields!
    
    @IBOutlet weak var undoView: UIView!
    @IBOutlet weak var undoLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var forwardButton: UIButton!
    private let presenter: IVehicleHealthCheck360ViewPresenter = VehicleHealthCheck360Presenter()
    
    var vehicleModel: VehicleHealthCheckModel?
    

    var markerTapped: String = ""

    var currentIndex: Int = 0
    var currentImageName: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.connectToView(view: self)
        
        // Do any additional setup after loading the view.
        applyStylings()
    }
    
    // MARK: - Apply Stylings to Objects
   private func applyStylings(){
        
    sectionTitleLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: false, size: 28.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
    
    rotateLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 12.0, alignment: .center, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
    
    markRepairDotView.backgroundColor = ColorConstants.yellowMarkColor
    markRepairDotView.layer.cornerRadius = markRepairDotView.frame.size.width / 2
    markRepairDotView.layer.masksToBounds = true
    
    markRepairLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
    
    markDamageDotView.backgroundColor = ColorConstants.redMarkColor
    markDamageDotView.layer.cornerRadius = markDamageDotView.frame.size.width / 2
    markDamageDotView.layer.masksToBounds = true
    
    markDamageLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
    
    cleaningRemarkInputView.textInputView( hintText: StringConstants.cleaningRemark, isSecureTextEntry: false, returnKeyType: UIReturnKeyType.next, keyboardtype: UIKeyboardType.default, textColor: ColorConstants.appPrimaryColor, tintColor: ColorConstants.appPrimaryColor, lineColor: ColorConstants.underlineColor)
    cleaningRemarkInputView.inputTextfield.frame = CGRect(x: cleaningRemarkInputView.frame.origin.x, y: cleaningRemarkInputView.frame.origin.y, width: cleaningRemarkInputView.frame.size.width, height: cleaningRemarkInputView.frame.size.height)
    
    cleaningRemarkInputView.inputTextfield.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
    cleaningRemarkInputView.inputTextfield.tag = 101
    
    damageRemarkInputView.textInputView( hintText: StringConstants.damageRemark, isSecureTextEntry: false, returnKeyType: UIReturnKeyType.next, keyboardtype: UIKeyboardType.default, textColor: ColorConstants.appPrimaryColor, tintColor: ColorConstants.appPrimaryColor, lineColor: ColorConstants.underlineColor)
    damageRemarkInputView.inputTextfield.frame = CGRect(x: damageRemarkInputView.frame.origin.x, y: damageRemarkInputView.frame.origin.y, width: damageRemarkInputView.frame.size.width, height: damageRemarkInputView.frame.size.height)
    
    damageRemarkInputView.inputTextfield.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
    damageRemarkInputView.inputTextfield.tag = 102
    
    othersRemarkInputView.textInputView( hintText: StringConstants.otherNotes, isSecureTextEntry: false, returnKeyType: UIReturnKeyType.next, keyboardtype: UIKeyboardType.default, textColor: ColorConstants.appPrimaryColor, tintColor: ColorConstants.appPrimaryColor, lineColor: ColorConstants.underlineColor)
    othersRemarkInputView.inputTextfield.frame = CGRect(x: othersRemarkInputView.frame.origin.x, y: othersRemarkInputView.frame.origin.y, width: othersRemarkInputView.frame.size.width, height: othersRemarkInputView.frame.size.height)
    
    othersRemarkInputView.inputTextfield.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
    othersRemarkInputView.inputTextfield.tag = 103
    
    
    undoLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: false, size: 16.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
    
        scrollViewHeightConstraint.constant = 530.0
        bindVehicleCheckList()
    
        let carImageTap = UITapGestureRecognizer(target: self, action: #selector(self.carImageViewTapped(_:)))
        carImageView.addGestureRecognizer(carImageTap)
        carImageView.isUserInteractionEnabled = true
    
        let undoTap = UITapGestureRecognizer(target: self, action: #selector(self.undoTapped(sender:)))
        undoView.addGestureRecognizer(undoTap)
        undoView.isUserInteractionEnabled = true
    
        let repairViewTap = UITapGestureRecognizer(target: self, action: #selector(self.repairViewTapped(_:)))
        markRepairView.addGestureRecognizer(repairViewTap)
        markRepairView.isUserInteractionEnabled = true
        
        let damageViewTap = UITapGestureRecognizer(target: self, action: #selector(self.damageViewTapped(_:)))
        markDamageView.addGestureRecognizer(damageViewTap)
        markDamageView.isUserInteractionEnabled = true
    
        presenter.getVehicleHealthCheckModel(input: vehicleModel!)
        if let model = vehicleModel?.images360Array{
            self.updateDotMarkers(model: model[0])
        }

    }
    
    // MARK: - For updating the text field values in router
    
    @objc func textFieldDidChange(textField: UITextField){
        let tag = textField.tag
        var name: String = ""
        if(tag == 101){
            name = StringConstants.cleaningRemark
        }
        else if(tag == 102){
            name = StringConstants.damageRemark
        }
        else if(tag == 103){
            name = StringConstants.otherNotes
        }
        presenter.updateRemarks(remarkName: name, remarkValue: textField.text ?? "")
    }
    
    // MARK: - Bind Vehicle Model
    
    private func bindVehicleCheckList(){
        sectionTitleLabel.text = vehicleModel?.sectionName ?? ""
    }
    
    // MARK: - Car image tap gesture
    @objc func carImageViewTapped(_ sender:UITapGestureRecognizer){
        presenter.presentLandscapeView(delegate: self, height: self.carImageView.frame.size.height, width: self.carImageView.frame.size.width)
//        let touchPoint : CGPoint = sender.location(in: carImageView)
//        if(markerTapped != ""){
//            let markers: ImageMarkerModel = ImageMarkerModel(x: CGFloat(touchPoint.x), y: CGFloat(touchPoint.y), mark: markerTapped)!
//            presenter.updateMarkers(marker: markers)
//        }
    }
    
    // MARK: - Update vehicle model
    func updateVehicleModel(index: Int, model: VehicleHealthCheckModel) {
            presenter.carImageTapped(model: model, index: index)
            presenter.getVehicleHealthCheckModel(input: model)
    }
    
    // MARK: - Undo View tapped
    @objc func undoTapped(sender: UITapGestureRecognizer){
        presenter.undoMarkers()
    }
    
    // MARK: - Repair view tapped
    @objc func repairViewTapped(_ sender:UITapGestureRecognizer){
        markerTapped = StringConstants.repairMarker
    }
    
    // MARK: - Damage View tapped
    @objc func damageViewTapped(_ sender:UITapGestureRecognizer){
        markerTapped = StringConstants.damageMarker
    }
    // MARK: - Updating the markers in UI
    func updateDotMarkers(model: Images360Model){
        self.carImageView.subviews.forEach{$0.removeFromSuperview()}
        self.carImageView.image = UIImage(named: model.imagePath)
        self.currentImageName = model.imagePath
        for j in 0..<model.markers!.count{
            let xPoint = ((model.markers?[j].x ?? 0.0) * self.carImageView.frame.size.width) / 100.0
            let yPoint = ((model.markers?[j].y ?? 0.0)  * self.carImageView.frame.size.height)  / 100.0
            let marker = model.markers?[j].mark ?? ""
//            let imageCenterOffset: CGFloat = 7.0
            let dotImage = UIImageView(frame: CGRect(x: xPoint, y: yPoint, width: 15, height: 15))
            if(marker == StringConstants.repairMarker){
                dotImage.image = UIImage(named: "yellowdot")
            }
            else if(marker == StringConstants.damageMarker){
                dotImage.image = UIImage(named: "reddot")
            }
            carImageView.addSubview(dotImage)
        }
    }

    // MARK: - Image Previous action
    @IBAction func backButtonAction(_ sender: UIButton) {
        presenter.onPreviousClick()
    }
    
    // MARK: - Image Next action
    @IBAction func forwardButtonAction(_ sender: UIButton) {
        presenter.onNextClick()
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

