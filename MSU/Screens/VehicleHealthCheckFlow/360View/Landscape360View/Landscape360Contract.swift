//
//  Landscape360Contract.swift
//  MSU
//
//  Created by vectorform on 27/08/21.
//

import Foundation
import UIKit

protocol ILandscape360ViewController: IBaseViewController {
    ///This function called to update the dot marks on the car image
    ///
    ///     Warning model should not be empty
    ///  - Parameter model: must be a Images360Model model
    func updateDotMarkers(model: Images360Model)
    
    ///This function called to update the VehicleHealthCheckModel and selected index
    ///
    ///     Warning model should not be empty
    ///  - Parameter model: must be a Images360Model model
    func updateVehicleModelIndex(index: Int, model: VehicleHealthCheckModel)
}

protocol ILandscape360ViewPresenter: IBasePresenter {
    ///This function called to get the VehicleHealthCheckModel from router
    ///
    ///     Warning input should not be empty
    ///  - Parameter input: must be a VehicleHealthCheckModel model
    func getVehicleHealthCheckModel(input: VehicleHealthCheckModel)
    
    ///This function called to set the initial markers on UI
    ///
    ///     Warning model should not be empty
    ///  - Parameter model: must be a list of Images360Model model
    func updateUIWithMarkers(model: [Images360Model])
    
    ///This function called on next button click
    ///
    func onNextClick()
    
    ///This function called on previous button click
    ///
    func onPreviousClick()
    
    ///This function called to set the markers on UI and update in model
    ///
    ///     Warning marker should not be empty
    ///  - Parameter marker: must be a Images360Model model
    func updateMarkers(marker: ImageMarkerModel)
    
    ///This function called to remove markers
    ///
    func undoMarkers()
    
    ///This function called to dismiss the view
    ///
    func closeButtonClicked()
    
}
