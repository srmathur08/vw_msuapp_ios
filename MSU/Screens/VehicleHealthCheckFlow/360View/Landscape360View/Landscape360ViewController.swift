//
//  Landscape360ViewController.swift
//  MSU
//
//  Created by vectorform on 27/08/21.
//

import UIKit
protocol Landscape360ViewDelagate: AnyObject {
    func updateVehicleModel(index: Int, model: VehicleHealthCheckModel)
}
class Landscape360ViewController: BaseViewController, ILandscape360ViewController {

    // MARK: - View controller outlets
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var buttonsView: UIView!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var markScratchView: UIView!
    @IBOutlet weak var scratchDotView: UIView!
    @IBOutlet weak var scratchLabel: UILabel!
    @IBOutlet weak var markDentView: UIView!
    @IBOutlet weak var dentDotView: UIView!
    @IBOutlet weak var dentDotLabel: UILabel!
    @IBOutlet weak var undoView: UIView!
    @IBOutlet weak var undoLabel: UILabel!
    
    var delegate: Landscape360ViewDelagate?
    var vehicleModel: VehicleHealthCheckModel?
    var selectedIndex: Int = 0
    private let presenter: ILandscape360ViewPresenter = Landscape360Presenter()
    var markerTapped: String = ""
    var currentImageName: String = ""
    var imageWidth: CGFloat = 0.0
    var imageHeight: CGFloat = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.connectToView(view: self)
        AppUtility.lockOrientation(.landscapeRight)
        // Do any additional setup after loading the view.
        applyStylings()
    }
    
    // MARK: - Apply Stylings to Objects
    private func applyStylings(){
        
        scratchDotView.backgroundColor = ColorConstants.yellowMarkColor
        scratchDotView.layer.cornerRadius = scratchDotView.frame.size.width / 2
        scratchDotView.layer.masksToBounds = true
        
        scratchLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        dentDotView.backgroundColor = ColorConstants.redMarkColor
        dentDotView.layer.cornerRadius = dentDotView.frame.size.width / 2
        dentDotView.layer.masksToBounds = true
        
        dentDotLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        undoLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: false, size: 16.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        let carImageTap = UITapGestureRecognizer(target: self, action: #selector(self.carImageViewTapped(_:)))
        carImageView.addGestureRecognizer(carImageTap)
        carImageView.isUserInteractionEnabled = true
        
        let undoTap = UITapGestureRecognizer(target: self, action: #selector(self.undoTapped(sender:)))
        undoView.addGestureRecognizer(undoTap)
        undoView.isUserInteractionEnabled = true
        
        let repairViewTap = UITapGestureRecognizer(target: self, action: #selector(self.repairViewTapped(_:)))
        markScratchView.addGestureRecognizer(repairViewTap)
        markScratchView.isUserInteractionEnabled = true
        
        let damageViewTap = UITapGestureRecognizer(target: self, action: #selector(self.damageViewTapped(_:)))
        markDentView.addGestureRecognizer(damageViewTap)
        markDentView.isUserInteractionEnabled = true
        
        presenter.getVehicleHealthCheckModel(input: vehicleModel!)
        self.perform(#selector(loadDetailsWithDelay), with: self, afterDelay: 1.0)
        
    }
    
    // MARK: - Load UI with delay
    @objc func loadDetailsWithDelay(){
        if let model = vehicleModel?.images360Array{
            self.updateDotMarkers(model: model[self.selectedIndex])
        }
    }
    
    // MARK: - Car image tap gesture
    @objc func carImageViewTapped(_ sender:UITapGestureRecognizer){
        let touchPoint : CGPoint = sender.location(in: carImageView)
        if(markerTapped != ""){
            let xVal = (touchPoint.x  * 100) / self.carImageView.frame.size.width
            let yVal = (touchPoint.y * 100) / self.carImageView.frame.size.height
            let markers: ImageMarkerModel = ImageMarkerModel(x: CGFloat(xVal), y: CGFloat(yVal), mark: markerTapped)!
            presenter.updateMarkers(marker: markers)
        }
    }
    // MARK: - Undo View tapped
    @objc func undoTapped(sender: UITapGestureRecognizer){
        presenter.undoMarkers()
    }
    
    // MARK: - Repair view tapped
    @objc func repairViewTapped(_ sender:UITapGestureRecognizer){
        markerTapped = StringConstants.repairMarker
    }
    
    // MARK: - Damage View tapped
    @objc func damageViewTapped(_ sender:UITapGestureRecognizer){
        markerTapped = StringConstants.damageMarker
    }
    
    
    // MARK: - Update Dot markers
    func updateDotMarkers(model: Images360Model) {
        self.carImageView.subviews.forEach{$0.removeFromSuperview()}
        self.carImageView.image = UIImage(named: model.imagePath)
        self.currentImageName = model.imagePath
        for j in 0..<model.markers!.count{
            let xPoint = ((model.markers?[j].x ?? 0.0) * self.carImageView.frame.size.width) / 100.0
            let yPoint = ((model.markers?[j].y ?? 0.0)  * self.carImageView.frame.size.height)  / 100.0
            let marker = model.markers?[j].mark ?? ""
//            let imageCenterOffset: CGFloat = 7.0
            let dotImage = UIImageView(frame: CGRect(x: xPoint, y: yPoint, width: 15, height: 15))
            if(marker == StringConstants.repairMarker){
                dotImage.image = UIImage(named: "yellowdot")
            }
            else if(marker == StringConstants.damageMarker){
                dotImage.image = UIImage(named: "reddot")
            }
            carImageView.addSubview(dotImage)
        }
    }
    
    // MARK: - Update vehicle model index
    func updateVehicleModelIndex(index: Int, model: VehicleHealthCheckModel)
    {
        self.selectedIndex = index
        self.vehicleModel = model
    }
    
    // MARK: - User Interactions
    // MARK: - Previous button action
    @IBAction func previousButtonAction(_ sender: UIButton) {
        presenter.onPreviousClick()
    }
    
    // MARK: - Next button action
    @IBAction func nextButtonAction(_ sender: UIButton) {
        presenter.onNextClick()
    }
    
    // MARK: - Close button action
    @IBAction func closeButtonAction(_ sender: UIButton) {
        self.delegate?.updateVehicleModel(index: self.selectedIndex, model: self.vehicleModel!)
        AppUtility.lockOrientation(.portrait)
        presenter.closeButtonClicked()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
