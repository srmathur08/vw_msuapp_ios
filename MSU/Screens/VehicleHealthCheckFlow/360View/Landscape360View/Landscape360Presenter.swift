//
//  Landscape360Presenter.swift
//  MSU
//
//  Created by vectorform on 27/08/21.
//

import Foundation
import UIKit


class Landscape360Presenter: BasePresenter, ILandscape360ViewPresenter {
   
    
    
    private weak var myView: ILandscape360ViewController?
    {
        return self.view as? ILandscape360ViewController
    }
    var currentPosition: Int = 0
    var images360Model: [Images360Model] = [Images360Model]()
    var markersModel: [ImageMarkerModel] = [ImageMarkerModel]()
    
    var vehicleModel: VehicleHealthCheckModel?
    
    // MARK: - get model from Controller
    func getVehicleHealthCheckModel(input: VehicleHealthCheckModel)
    {
        self.vehicleModel = input
    }
    // MARK: - Car Image tapped
    func updateUIWithMarkers(model: [Images360Model])
    {
            myView?.updateDotMarkers(model: model[0])

    }

    // MARK: - Next button action
    func onNextClick() {
        if(currentPosition < (vehicleModel?.images360Array.count)! - 1){
            currentPosition += 1
            myView?.updateDotMarkers(model: (vehicleModel?.images360Array[currentPosition])!)
        }
        else{
            currentPosition = 0
            myView?.updateDotMarkers(model: (vehicleModel?.images360Array[currentPosition])!)
        }
        updateModelInView(index: currentPosition, modelVal: vehicleModel!)
    }
    
    // MARK: - Previous button action
    func onPreviousClick() {
        if(currentPosition  == 0){
            currentPosition = (vehicleModel?.images360Array.count)! - 1
            myView?.updateDotMarkers(model: (vehicleModel?.images360Array[currentPosition])!)
        }
        else{
            currentPosition -= 1
            myView?.updateDotMarkers(model: (vehicleModel?.images360Array[currentPosition])!)
        }
        updateModelInView(index: currentPosition, modelVal: vehicleModel!)
    }
    
    // MARK: - Update markers
    func updateMarkers(marker: ImageMarkerModel) {
        vehicleModel?.images360Array[currentPosition].markers?.append(marker)
        myView?.updateDotMarkers(model: (vehicleModel?.images360Array[currentPosition])!)
        updateModelInView(index: currentPosition, modelVal: vehicleModel!)
    }
    
    // MARK: - Undo markers
    func undoMarkers() {
        if((vehicleModel?.images360Array[currentPosition].markers!.count)! > 0){
            let count = (vehicleModel?.images360Array[currentPosition].markers!.count)!
            vehicleModel?.images360Array[currentPosition].markers?.remove(at: count  - 1)
            myView?.updateDotMarkers(model: (vehicleModel?.images360Array[currentPosition])!)
        }
        updateModelInView(index: currentPosition, modelVal: vehicleModel!)
    }
    
    // MARK: - Close button action
    func closeButtonClicked()
    {
        self.myView?.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Update model in view
    func updateModelInView(index: Int, modelVal: VehicleHealthCheckModel) {
        self.myView?.updateVehicleModelIndex(index: index, model: modelVal)
    }
    
}
