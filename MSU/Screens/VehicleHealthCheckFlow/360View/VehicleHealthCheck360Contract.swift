//
//  VehicleHealthCheck360Contract.swift
//  MSU
//
//  Created by vectorform on 11/08/21.
//

import Foundation
import UIKit

protocol IVehicleHealthCheck360ViewController: IBaseViewController {
    ///This function called to update the markers on UI
    ///
    ///     Warning model should not be empty
    ///  - Parameter model: must be a Images360Model model
    func updateDotMarkers(model: Images360Model)
}


protocol IVehicleHealthCheck360ViewPresenter: IBasePresenter {
    func updateModelInRouter(input: VehicleHealthCheckModel)
   
    ///This function called to get the VehicleHealthCheckModel from router
    ///
    ///     Warning input should not be empty
    ///  - Parameter input: must be a VehicleHealthCheckModel model
    func getVehicleHealthCheckModel(input: VehicleHealthCheckModel)
    
    ///This function called to get the Car image tapped
    ///
    ///     Warning model and index should not be empty
    ///  - Parameter model: must be a VehicleHealthCheckModel model
    ///  - Parameter index: must be a Integer value
    func carImageTapped(model: VehicleHealthCheckModel, index: Int)
    
    ///This function called on next button click
    ///
    func onNextClick()
    
    ///This function called on previous button click
    ///
    func onPreviousClick()
    
    ///This function called to set the markers on UI and update in model
    ///
    ///     Warning marker should not be empty
    ///  - Parameter marker: must be a Images360Model model
    func updateMarkers(marker: ImageMarkerModel)
    
    ///This function called to remove markers
    ///
    func undoMarkers()
    
    ///This function called to update the remark values
    ///
    ///     Warning remarkName and remarkValue should not be empty
    ///  - Parameter remarkName: must be a String value
    ///  - Parameter remarkValue: must be a String value
    func updateRemarks(remarkName: String, remarkValue: String)
    
    ///This function called to update the remark values
    ///
    ///     Warning height and width should not be empty
    ///  - Parameter delegate: must be a Landscape360ViewDelagate(self)
    ///  - Parameter height: must be a CGFloat value
    ///  - Parameter width: must be a CGFloat value
    func presentLandscapeView(delegate: Landscape360ViewDelagate, height:CGFloat, width: CGFloat)
}
