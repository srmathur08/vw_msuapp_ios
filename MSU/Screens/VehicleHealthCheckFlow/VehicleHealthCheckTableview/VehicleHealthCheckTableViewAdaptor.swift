//
//  VehicleHealthCheckTableViewAdaptor.swift
//  MSU
//
//  Created by vectorform on 05/08/21.
//

import Foundation
import UIKit

protocol UpdateVehicleModelDelegate: AnyObject {
    ///This function called pass value from adaptor to controller
    ///
    ///     Warning inputModel should not be empty
    ///   - Parameter inputModel: must be a list of SubSection model
    func updateVehicleModel(inputModel: [SubSection])
    
    ///This function called pass value from adaptor to controller
    ///
    ///     Warning input should not be empty
    ///   - Parameter input: must be a String
    func updatePhotos(input: Int)
}
class VehicleHealthCheckTableViewAdaptor: NSObject, ITableViewAdapter, UITableViewDelegate, UITableViewDataSource, SubSectionRadioButtonDelegate {
  

    var tableView: UITableView
    
    typealias DataType = SubSection
    var data: [DataType] = []
    var selectedIndex: Int = 0
    
    var vehicleDelegate: UpdateVehicleModelDelegate?
    required init(tableView: UITableView) {
        self.tableView = tableView
        self.tableView.backgroundColor = ColorConstants.clear
        self.tableView.separatorColor = ColorConstants.clear
        self.tableView.separatorInset = .zero
        super.init()
        let nibName = UINib(nibName: "VehicleSubsectionsTableViewCell", bundle:nil)
        self.tableView.register(nibName, forCellReuseIdentifier: "VehicleSubsectionsTableViewCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 60.0
        self.tableView.rowHeight = UITableView.automaticDimension
    }
    
    // MARK: - Update table Data
    ///This function called bind table view with model
    ///
    ///     Warning data should not be empty
    ///   - Parameter data: must be a list of SubSection model
    func updateData(data: [SubSection]) {
        self.data = data
        tableView.reloadData()
    }
    
    // MARK: - Getting selected value to save in model
    func getSeletedValue(inputText: String) {
        if let getPrevValue = self.data[selectedIndex].value{
            if(getPrevValue == ""){
                self.data[selectedIndex].value = inputText
            }
            else{
                self.data[selectedIndex].value = getPrevValue + "|" + inputText
            }
        }
        else{
            self.data[selectedIndex].value = inputText
        }
        print(self.data[selectedIndex].value)
        self.data[selectedIndex].isValidated = true
        self.updateValuesinModel(inputVal: data)
    }
    
    // MARK: - Delete asset from list
    func cancelAssetClicked(input: Int) {
        let assetPaths = self.data[selectedIndex].assetPaths
        if(assetPaths.count > 0){
            do{
                try FileManager.default.removeItem(atPath: assetPaths[input])
            }
            catch{
                print("")
            }
            self.data[selectedIndex].assetPaths.remove(at: input)
        }
        self.updateValuesinModel(inputVal: self.data)
        tableView.reloadData()
    }
    
    // MARK: - Table view delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: VehicleSubsectionsTableViewCell = tableView.dequeueReusableCell(
            withIdentifier: "VehicleSubsectionsTableViewCell",
            for: indexPath) as? VehicleSubsectionsTableViewCell {
            
            let vehicleObj: SubSection = data[indexPath.row]
            cell.subSectionlabel.text = vehicleObj.name
            cell.redButton.tag = indexPath.row
            cell.yellowButton.tag = indexPath.row
            cell.greenButton.tag = indexPath.row
            cell.radioButtonDelegate = self
            cell.redButton.setImage(UIImage(named: "redunchecked"), for: .normal)
            cell.yellowButton.setImage(UIImage(named: "yellowunchecked"), for: .normal)
            cell.greenButton.setImage(UIImage(named: "greenunchecked"), for: .normal)
            cell.redButton.addTarget(self, action: #selector(redButtonClicked(sender:)), for: .touchUpInside)
            cell.yellowButton.addTarget(self, action: #selector(yellowButtonClicked(sender:)), for: .touchUpInside)
            cell.greenButton.addTarget(self, action: #selector(greenButtonClicked(sender:)), for: .touchUpInside)
            cell.checkBoxButton.addTarget(self, action: #selector(self.updateCheckBoxValues(sender:)), for: .touchUpInside)
            cell.checkBoxButton.tag = indexPath.row
            cell.collectionViewContainer.isHidden = true
            cell.bottomCollectionViewHeightConstraint.constant = 0.0
            
            let uploadPhotoTap = UITapGestureRecognizer(target: self, action: #selector(self.uploadPhotosTapped))
            cell.photosView.addGestureRecognizer(uploadPhotoTap)
            cell.photosView.isUserInteractionEnabled = true
            cell.photosView.tag = indexPath.row
            let galleryArray = vehicleObj.assetPaths
            if(galleryArray.count > 0){
                cell.updatePhotosCollectionView(input: galleryArray)
            }

            if(vehicleObj.status == StatusEnum.Red.rawValue)
            {
                cell.redButton.setImage(UIImage(named: "redcheckedicon"), for: .normal)
                cell.yellowButton.setImage(UIImage(named: "yellowunchecked"), for: .normal)
                cell.greenButton.setImage(UIImage(named: "greenunchecked"), for: .normal)
                
                let typeval = vehicleObj.rType
                if(typeval == ""){
                    cell.bottomCollectionViewHeightConstraint.constant = 0.0
                    cell.collectionViewContainer.isHidden = true
                }
                else{
                    switch typeval {
                    case StringConstants.vehicleHealthCheckType1:
                       
                        if let radiosCount = vehicleObj.rFields?.radios?.count,
                           let rFields =  vehicleObj.rFields,
                           let yFields = vehicleObj.yFields{
                            var count:  Int = 1
                            if(radiosCount == 1){
                                count += 1
                            }
                            else{
                                count = radiosCount
                            }
                            cell.updateCollectionView(rFields: rFields, yFields: yFields, status: StringConstants.vehicleHealthRedStatus, value: vehicleObj.value ?? "", isMulti: false)
                            cell.bottomCollectionViewHeightConstraint.constant = CGFloat(count * 40)
                        }
                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = false
                        cell.inputTextField.isHidden = true
                        cell.checkBoxView.isHidden = false
                        cell.uploadPhotoView.isHidden =  true
                        
                    case StringConstants.vehicleHealthCheckType2:
                        
                        if let radiosCount = vehicleObj.rFields?.radios?.count,
                           let rFields =  vehicleObj.rFields,
                           let yFields = vehicleObj.yFields{
                            var count:  Int = 1
                            if(radiosCount == 1){
                                count += 1
                            }
                            else{
                                count = radiosCount
                            }
                            cell.updateCollectionView(rFields: rFields, yFields: yFields, status: StringConstants.vehicleHealthRedStatus, value: vehicleObj.value ?? "", isMulti: false)
                            cell.bottomCollectionViewHeightConstraint.constant = CGFloat(count * 40)
                        }
                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = false
                        cell.inputTextField.isHidden = true
                        cell.checkBoxView.isHidden = true
                        cell.uploadPhotoView.isHidden =  false
                    
                    case StringConstants.vehicleHealthCheckType3:
                        
                        cell.bottomCollectionViewHeightConstraint.constant = 150.0
                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = true
                        cell.inputTextField.isHidden = false
                        cell.checkBoxView.isHidden = true
                        cell.uploadPhotoView.isHidden =  false
                        cell.updateHint(hint: vehicleObj.rFields?.hint ?? "")
                        if(vehicleObj.remarks == nil || vehicleObj.remarks == ""){
                            cell.inputTextField.inputTextfield.placeholder = vehicleObj.rFields?.hint
                        }
                        else{
                            cell.inputTextField.inputTextfield.text = vehicleObj.remarks
                        }
                    
                        cell.inputTextField.inputTextfield.text = vehicleObj.remarks
                    case StringConstants.vehicleHealthCheckType4:
                        
                        if let radiosCount = vehicleObj.rFields?.radios?.count,
                           let rFields =  vehicleObj.rFields,
                           let yFields = vehicleObj.yFields{
                            var count:  Int = 1
                            if(radiosCount == 1){
                                count += 1
                            }
                            else{
                                count = radiosCount
                            }
                            cell.updateCollectionView(rFields: rFields, yFields: yFields, status: StringConstants.vehicleHealthRedStatus, value: vehicleObj.value ?? "", isMulti: false)
                            cell.bottomCollectionViewHeightConstraint.constant = CGFloat(count * 40)
                        }
                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = false
                        cell.inputTextField.isHidden = true
                        cell.checkBoxView.isHidden = true
                        cell.uploadPhotoView.isHidden =  true
                        cell.bottomView.isHidden = true
                        cell.bottomViewHeightConstraint.constant = 2.0
                        
                    case StringConstants.vehicleHealthCheckType5:
                        
                        if let radiosCount = vehicleObj.rFields?.radios?.count,
                           let rFields =  vehicleObj.rFields,
                           let yFields = vehicleObj.yFields{
                            var count:  Int = 1
                            if(radiosCount == 1){
                                count += 1
                            }
                            else{
                                count = radiosCount
                            }
                            cell.updateCollectionView(rFields: rFields, yFields: yFields, status: StringConstants.vehicleHealthRedStatus, value: vehicleObj.value ?? "", isMulti: true)
                            cell.bottomCollectionViewHeightConstraint.constant = CGFloat(count * 40)
                        }
                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = false
                        cell.inputTextField.isHidden = true
                        cell.checkBoxView.isHidden = true
                        cell.uploadPhotoView.isHidden =  true
                        cell.bottomView.isHidden = true
                        cell.bottomViewHeightConstraint.constant = 2.0
                    case StringConstants.vehicleHealthCheckType6:
                        
                        if let radiosCount = vehicleObj.rFields?.radios?.count,
                           let rFields =  vehicleObj.rFields,
                           let yFields = vehicleObj.yFields{
                            var count:  Int = 1
                            if(radiosCount == 1){
                                count += 1
                            }
                            else{
                                count = radiosCount
                            }
                            cell.updateCollectionView(rFields: rFields, yFields: yFields, status: StringConstants.vehicleHealthRedStatus, value: vehicleObj.value ?? "", isMulti: false)
                            cell.bottomCollectionViewHeightConstraint.constant = CGFloat(count * 40)
                        }
                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = false
                        cell.inputTextField.isHidden = true
                        cell.checkBoxView.isHidden = true
                        cell.uploadPhotoView.isHidden =  false
                        cell.bottomView.isHidden = true
                        cell.bottomViewHeightConstraint.constant = 2.0
                    case StringConstants.vehicleHealthCheckType7:
                        
                        cell.bottomCollectionViewHeightConstraint.constant = 150.0
                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = true
                        cell.inputTextField.isHidden = false
                        cell.checkBoxView.isHidden = true
                        cell.uploadPhotoView.isHidden =  true
                        cell.updateHint(hint: vehicleObj.rFields?.hint ?? "")
                        if(vehicleObj.remarks == nil || vehicleObj.remarks == ""){
                            cell.inputTextField.inputTextfield.placeholder = vehicleObj.rFields?.hint
                        }
                        else{
                            cell.inputTextField.inputTextfield.text = vehicleObj.remarks
                        }
                    
                        
                    default:
                        cell.bottomCollectionViewHeightConstraint.constant = 0.0
                        cell.collectionViewContainer.isHidden = true
                    }
                }
            }
            else if(vehicleObj.status == StatusEnum.Yellow.rawValue)
            {
                cell.redButton.setImage(UIImage(named: "redunchecked"), for: .normal)
                cell.yellowButton.setImage(UIImage(named: "yellowcheckedicon"), for: .normal)
                cell.greenButton.setImage(UIImage(named: "greenunchecked"), for: .normal)
                
                let typeval = vehicleObj.yType
                if(typeval == ""){
                    cell.bottomCollectionViewHeightConstraint.constant = 0.0
                    cell.collectionViewContainer.isHidden = true
                }
                else{
                    switch typeval {
                    case StringConstants.vehicleHealthCheckType1:
                        
                        if let radiosCount = vehicleObj.yFields?.radios?.count,
                           let rFields =  vehicleObj.rFields,
                           let yFields = vehicleObj.yFields{
                            var count:  Int = 1
                            if(radiosCount == 1){
                                count += 1
                            }
                            else{
                                count = radiosCount
                            }
                            cell.updateCollectionView(rFields: rFields, yFields: yFields, status: StringConstants.vehicleHealthYellowStatus, value: vehicleObj.value ?? "", isMulti: false)
                            cell.bottomCollectionViewHeightConstraint.constant = CGFloat(count * 40)
                        }
                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = false
                        cell.inputTextField.isHidden = true
                        cell.checkBoxView.isHidden = false
                        cell.uploadPhotoView.isHidden =  true
                        
                    case StringConstants.vehicleHealthCheckType2:
                        
                        if let radiosCount = vehicleObj.yFields?.radios?.count,
                           let rFields =  vehicleObj.rFields,
                           let yFields = vehicleObj.yFields{
                            var count:  Int = 1
                            if(radiosCount == 1){
                                count += 1
                            }
                            else{
                                count = radiosCount
                            }
                            cell.updateCollectionView(rFields: rFields, yFields: yFields, status: StringConstants.vehicleHealthYellowStatus, value: vehicleObj.value ?? "", isMulti: false)
                            cell.bottomCollectionViewHeightConstraint.constant = CGFloat(count * 40)
                        }
                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = false
                        cell.inputTextField.isHidden = true
                        cell.checkBoxView.isHidden = true
                        cell.uploadPhotoView.isHidden =  false
                    
                    case StringConstants.vehicleHealthCheckType3:
                        
                        cell.bottomCollectionViewHeightConstraint.constant = 150.0
                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = true
                        cell.inputTextField.isHidden = false
                        cell.checkBoxView.isHidden = true
                        cell.uploadPhotoView.isHidden =  false
                        cell.updateHint(hint: vehicleObj.yFields?.hint ?? "")
                        if(vehicleObj.remarks == nil || vehicleObj.remarks == ""){
                            cell.inputTextField.inputTextfield.placeholder = vehicleObj.yFields?.hint
                        }
                        else{
                            cell.inputTextField.inputTextfield.text = vehicleObj.remarks
                        }
                    
                    
                    case StringConstants.vehicleHealthCheckType4:
                        if let radiosCount = vehicleObj.yFields?.radios?.count,
                           let rFields =  vehicleObj.rFields,
                           let yFields = vehicleObj.yFields{
                            var count:  Int = 1
                            if(radiosCount == 1){
                                count += 1
                            }
                            else{
                                count = radiosCount
                            }
                            cell.updateCollectionView(rFields: rFields, yFields: yFields, status: StringConstants.vehicleHealthYellowStatus, value: vehicleObj.value ?? "", isMulti: false)
                            cell.bottomCollectionViewHeightConstraint.constant = CGFloat(count * 40)
                        }
                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = false
                        cell.inputTextField.isHidden = true
                        cell.checkBoxView.isHidden = true
                        cell.uploadPhotoView.isHidden =  true
                        cell.bottomView.isHidden = true
                        cell.bottomViewHeightConstraint.constant = 2.0
                        
                    case StringConstants.vehicleHealthCheckType5:
                        
                        if let radiosCount = vehicleObj.yFields?.radios?.count,
                           let rFields =  vehicleObj.rFields,
                           let yFields = vehicleObj.yFields{
                            var count:  Int = 1
                            if(radiosCount == 1){
                                count += 1
                            }
                            else{
                                count = radiosCount
                            }
                            cell.updateCollectionView(rFields: rFields, yFields: yFields, status: StringConstants.vehicleHealthYellowStatus, value: vehicleObj.value ?? "", isMulti: true)
                            cell.bottomCollectionViewHeightConstraint.constant = CGFloat(count * 40)
                        }
                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = false
                        cell.inputTextField.isHidden = true
                        cell.checkBoxView.isHidden = true
                        cell.uploadPhotoView.isHidden =  true
                        cell.bottomView.isHidden = true
                        cell.bottomViewHeightConstraint.constant = 2.0
                    case StringConstants.vehicleHealthCheckType6:
                        
                        if let radiosCount = vehicleObj.yFields?.radios?.count,
                           let rFields =  vehicleObj.rFields,
                           let yFields = vehicleObj.yFields{
                            var count:  Int = 1
                            if(radiosCount == 1){
                                count += 1
                            }
                            else{
                                count = radiosCount
                            }
                            cell.updateCollectionView(rFields: rFields, yFields: yFields, status: StringConstants.vehicleHealthYellowStatus, value: vehicleObj.value ?? "", isMulti: false)
                            cell.bottomCollectionViewHeightConstraint.constant = CGFloat(count * 40)
                        }
                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = false
                        cell.inputTextField.isHidden = true
                        cell.checkBoxView.isHidden = true
                        cell.uploadPhotoView.isHidden =  false
                        cell.bottomView.isHidden = true
                        cell.bottomViewHeightConstraint.constant = 2.0
                    case StringConstants.vehicleHealthCheckType7:
                        
                        cell.bottomCollectionViewHeightConstraint.constant = 150.0
                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = true
                        cell.inputTextField.isHidden = false
                        cell.checkBoxView.isHidden = true
                        cell.uploadPhotoView.isHidden =  true
                        cell.updateHint(hint: vehicleObj.yFields?.hint ?? "")
                        if(vehicleObj.remarks == nil || vehicleObj.remarks == ""){
                            cell.inputTextField.inputTextfield.placeholder = vehicleObj.yFields?.hint
                        }
                        else{
                            cell.inputTextField.inputTextfield.text = vehicleObj.remarks
                        }
                    
                    default:
                        cell.bottomCollectionViewHeightConstraint.constant = 0.0
                        cell.collectionViewContainer.isHidden = true
                    }
                }
            }
            else if(vehicleObj.status == StatusEnum.Green.rawValue)
            {
                cell.redButton.setImage(UIImage(named: "redunchecked"), for: .normal)
                cell.yellowButton.setImage(UIImage(named: "yellowunchecked"), for: .normal)
                cell.greenButton.setImage(UIImage(named: "greencheckedicon"), for: .normal)
                cell.bottomCollectionViewHeightConstraint.constant = 0.0
                cell.collectionViewContainer.isHidden = true
            }
            return cell
        }
        return UITableViewCell()
    }
    
    // MARK: - Upload photos action
    @objc func uploadPhotosTapped(_ sender:UIGestureRecognizer){
        guard let getTag = sender.view?.tag else { return }
        
        vehicleDelegate?.updatePhotos(input: getTag)
    }
    
    // MARK: - update values in model
    func updateValuesinModel(inputVal: [SubSection]){
        vehicleDelegate?.updateVehicleModel(inputModel: inputVal)
    }
    
    // MARK: - Text field did change delegate method
    @objc func textFieldDidChange(textField: UITextField){
        let tag = textField.tag
        data[tag].remarks = textField.text
        data[tag].isValidated = true
    }
    // MARK: - Red button Action
    @objc func redButtonClicked(sender: UIButton) {
        let indexPath = IndexPath(row: sender.tag, section: 0)
        if let cell = tableView.cellForRow(at: indexPath)as? VehicleSubsectionsTableViewCell
        {
            if(sender.isSelected == false)
            {
                let vehicleObj = data[indexPath.row]
                let typeval = vehicleObj.rType
                
                data[sender.tag].isValidated = (typeval == StringConstants.vehicleHealthCheckType4 && data[sender.tag].rFields?.radios?.count ?? 0 == 0)

                selectedIndex = sender.tag
                sender.isSelected = false
               
                
                data[sender.tag].status = StatusEnum.Red.rawValue
                self.updateValuesinModel(inputVal: data)
                
                cell.redButton.setImage(UIImage(named: "redcheckedicon"), for: .normal)
                cell.yellowButton.setImage(UIImage(named: "yellowunchecked"), for: .normal)
                cell.greenButton.setImage(UIImage(named: "greenunchecked"), for: .normal)
                
                if(typeval == ""){
                    cell.bottomCollectionViewHeightConstraint.constant = 0.0
                    cell.collectionViewContainer.isHidden = true
                }
                else{
                    switch typeval {
                    case StringConstants.vehicleHealthCheckType1:
                        if let rFields =  data[sender.tag].rFields,
                           let yFields = data[sender.tag].yFields,
                           let radiosCount = data[sender.tag].rFields?.radios?.count
                        {
                            var count:  Int = 1
                            if(radiosCount == 1){
                                count += 2
                            }
                            else{
                                count = radiosCount
                            }
                            cell.updateCollectionView(rFields: rFields, yFields: yFields, status: StringConstants.vehicleHealthRedStatus, value: data[sender.tag].value ?? "", isMulti: false)
                            cell.bottomCollectionViewHeightConstraint.constant = CGFloat(count * 40)
                        }
                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = false
                        cell.inputTextField.isHidden = true
                        cell.checkBoxView.isHidden = false
                        cell.uploadPhotoView.isHidden =  true
                        
                    case StringConstants.vehicleHealthCheckType2:
                        if let rFields =  data[sender.tag].rFields,
                           let yFields = data[sender.tag].yFields,
                           let radiosCount = data[sender.tag].rFields?.radios?.count
                        {
                            var count:  Int = 1
                            if(radiosCount == 1){
                                count += 2
                            }
                            else{
                                count = radiosCount
                            }
                            cell.updateCollectionView(rFields: rFields, yFields: yFields, status: StringConstants.vehicleHealthRedStatus, value: data[sender.tag].value ?? "", isMulti: false)
                            cell.bottomCollectionViewHeightConstraint.constant = CGFloat(count * 40)
                        }
                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = false
                        cell.inputTextField.isHidden = true
                        cell.checkBoxView.isHidden = true
                        cell.uploadPhotoView.isHidden =  false
                    
                    case StringConstants.vehicleHealthCheckType3:
                        
                        cell.bottomCollectionViewHeightConstraint.constant = 150.0
                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = true
                        cell.inputTextField.isHidden = false
                        cell.updateHint(hint: data[sender.tag].rFields?.hint ?? "")
                        if(data[sender.tag].remarks == nil || data[sender.tag].remarks == ""){
                            cell.inputTextField.inputTextfield.placeholder = data[sender.tag].rFields?.hint
                        }
                        else{
                            cell.inputTextField.inputTextfield.text = data[sender.tag].remarks
                        }
                    
                        cell.checkBoxView.isHidden = true
                        cell.uploadPhotoView.isHidden =  false
                        
                        cell.inputTextField.inputTextfield.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
                        cell.inputTextField.inputTextfield.tag = indexPath.row
                        
                    case StringConstants.vehicleHealthCheckType4:
                        if let rFields =  data[sender.tag].rFields,
                           let yFields = data[sender.tag].yFields,
                           let radiosCount = data[sender.tag].rFields?.radios?.count
                        {
                            var count:  Int = 1
                            if(radiosCount == 1){
                                count += 2
                            }
                            else{
                                count = radiosCount
                            }
                            cell.updateCollectionView(rFields: rFields, yFields: yFields, status: StringConstants.vehicleHealthRedStatus, value: data[sender.tag].value ?? "", isMulti: false)
                            cell.bottomCollectionViewHeightConstraint.constant = CGFloat(count * 40)
                        }
                        
                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = false
                        cell.inputTextField.isHidden = true
                        cell.checkBoxView.isHidden = true
                        cell.uploadPhotoView.isHidden =  true
                        cell.bottomView.isHidden = true
                        cell.bottomViewHeightConstraint.constant = 2.0
                        
                    case StringConstants.vehicleHealthCheckType5:
                        
                        if let radiosCount = data[sender.tag].rFields?.radios?.count,
                           let rFields =  data[sender.tag].rFields,
                           let yFields = data[sender.tag].yFields{
                            var count:  Int = 1
                            if(radiosCount == 1){
                                count += 2
                            }
                            else{
                                count = radiosCount
                            }
                            cell.updateCollectionView(rFields: rFields, yFields: yFields, status: StringConstants.vehicleHealthRedStatus, value: vehicleObj.value ?? "", isMulti: true)
                            cell.bottomCollectionViewHeightConstraint.constant = CGFloat(count * 40)
                        }
                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = false
                        cell.inputTextField.isHidden = true
                        cell.checkBoxView.isHidden = true
                        cell.uploadPhotoView.isHidden =  true
                        cell.bottomView.isHidden = true
                        cell.bottomViewHeightConstraint.constant = 2.0
                    case StringConstants.vehicleHealthCheckType6:
                        
                        if let radiosCount = data[sender.tag].rFields?.radios?.count,
                           let rFields =  data[sender.tag].rFields,
                           let yFields = data[sender.tag].yFields{
                            var count:  Int = 1
                            if(radiosCount == 1){
                                count += 1
                            }
                            else{
                                count = radiosCount
                            }
                            cell.updateCollectionView(rFields: rFields, yFields: yFields, status: StringConstants.vehicleHealthRedStatus, value: vehicleObj.value ?? "", isMulti: false)
                            cell.bottomCollectionViewHeightConstraint.constant = CGFloat(count * 40)
                        }
                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = false
                        cell.inputTextField.isHidden = true
                        cell.checkBoxView.isHidden = true
                        cell.uploadPhotoView.isHidden =  false
                        cell.bottomView.isHidden = true
                        cell.bottomViewHeightConstraint.constant = 2.0
                    case StringConstants.vehicleHealthCheckType7:
                        
                        cell.bottomCollectionViewHeightConstraint.constant = 150.0
                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = true
                        cell.inputTextField.isHidden = false
                        cell.checkBoxView.isHidden = true
                        cell.uploadPhotoView.isHidden =  true
                        cell.updateHint(hint: data[sender.tag].rFields?.hint ?? "")
                        if(data[sender.tag].remarks == nil || data[sender.tag].remarks == ""){
                            cell.inputTextField.inputTextfield.placeholder = data[sender.tag].rFields?.hint
                        }
                        else{
                            cell.inputTextField.inputTextfield.text = data[sender.tag].remarks
                        }
                       
                        
                    default:
                        cell.bottomCollectionViewHeightConstraint.constant = 0.0
                        cell.collectionViewContainer.isHidden = true
                    }
                }
            }
            tableView.beginUpdates()
            tableView.endUpdates()
        }
    }
    
    // MARK: - Yellow button action
    @objc func yellowButtonClicked(sender: UIButton) {
        let indexPath = IndexPath(row: sender.tag, section: 0)
        if let cell = tableView.cellForRow(at: indexPath)as? VehicleSubsectionsTableViewCell
        {
            if(sender.isSelected == false)
            {
                let vehicleObj = data[indexPath.row]
                let typeval = vehicleObj.yType
                
                data[sender.tag].isValidated = (typeval == StringConstants.vehicleHealthCheckType4 && data[sender.tag].yFields?.radios?.count ?? 0 == 0)
                
                selectedIndex = sender.tag
                sender.isSelected = false
              
                
                data[sender.tag].status = StatusEnum.Yellow.rawValue
                self.updateValuesinModel(inputVal: data)
                
                cell.yellowButton.setImage(UIImage(named: "yellowcheckedicon"), for: .normal)
                cell.redButton.setImage(UIImage(named: "redunchecked"), for: .normal)
                cell.greenButton.setImage(UIImage(named: "greenunchecked"), for: .normal)
                
             
                if(typeval == ""){
                    cell.bottomCollectionViewHeightConstraint.constant = 0.0
                    cell.collectionViewContainer.isHidden = true
                }
                else{
                    switch typeval {
                    case StringConstants.vehicleHealthCheckType1:
                        if let rFields =  data[sender.tag].rFields,
                           let yFields = data[sender.tag].yFields,
                           let radiosCount = data[sender.tag].yFields?.radios?.count
                        {
                            var count:  Int = 1
                            if(radiosCount == 1){
                                count += 2
                            }
                            else{
                                count = radiosCount
                            }
                            cell.updateCollectionView(rFields: rFields, yFields: yFields, status: StringConstants.vehicleHealthYellowStatus, value: data[sender.tag].value ?? "", isMulti: false)
                            cell.bottomCollectionViewHeightConstraint.constant = CGFloat(count * 40)
                        }

                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = false
                        cell.inputTextField.isHidden = true
                        cell.checkBoxView.isHidden = false
                        cell.uploadPhotoView.isHidden =  true
                        
                    case StringConstants.vehicleHealthCheckType2:
                        if let rFields =  data[sender.tag].rFields,
                           let yFields = data[sender.tag].yFields,
                           let radiosCount = data[sender.tag].yFields?.radios?.count
                        {
                            var count:  Int = 1
                            if(radiosCount == 1){
                                count += 2
                            }
                            else{
                                count = radiosCount
                            }
                            
                            cell.updateCollectionView(rFields: rFields, yFields: yFields, status: StringConstants.vehicleHealthYellowStatus, value: data[sender.tag].value ?? "", isMulti: false)
                            cell.bottomCollectionViewHeightConstraint.constant = CGFloat(count * 40)
                        }

                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = false
                        cell.inputTextField.isHidden = true
                        cell.checkBoxView.isHidden = true
                        cell.uploadPhotoView.isHidden =  false
                    
                    case StringConstants.vehicleHealthCheckType3:
                        
                        cell.bottomCollectionViewHeightConstraint.constant = 150.0
                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = true
                        cell.inputTextField.isHidden = false
                        cell.updateHint(hint: data[sender.tag].yFields?.hint ?? "")
                        if(data[sender.tag].remarks == nil || data[sender.tag].remarks == ""){
                            cell.inputTextField.inputTextfield.placeholder = data[sender.tag].yFields?.hint
                        }
                        else{
                            cell.inputTextField.inputTextfield.text = data[sender.tag].remarks
                        }
                        cell.checkBoxView.isHidden = true
                        cell.uploadPhotoView.isHidden =  false
                        
                    case StringConstants.vehicleHealthCheckType4:
                        if let rFields =  data[sender.tag].rFields,
                           let yFields = data[sender.tag].yFields,
                           let radiosCount = data[sender.tag].yFields?.radios?.count
                        {
                            var count:  Int = 1
                            if(radiosCount == 1){
                                count += 2
                            }
                            else{
                                count = radiosCount
                            }
                            
                            cell.updateCollectionView(rFields: rFields, yFields: yFields, status: StringConstants.vehicleHealthYellowStatus, value: data[sender.tag].value ?? "", isMulti: false)
                            cell.bottomCollectionViewHeightConstraint.constant = CGFloat(count * 40)
                        }

                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = false
                        cell.inputTextField.isHidden = true
                        cell.checkBoxView.isHidden = true
                        cell.uploadPhotoView.isHidden =  true
                        cell.bottomView.isHidden = true
                        cell.bottomViewHeightConstraint.constant = 2.0
                        
                    case StringConstants.vehicleHealthCheckType5:
                        
                        if let radiosCount = data[sender.tag].yFields?.radios?.count,
                           let rFields =  data[sender.tag].rFields,
                           let yFields = data[sender.tag].yFields{
                            var count:  Int = 1
                            if(radiosCount == 1){
                                count += 1
                            }
                            else{
                                count = radiosCount
                            }
                            cell.updateCollectionView(rFields: rFields, yFields: yFields, status: StringConstants.vehicleHealthYellowStatus, value: vehicleObj.value ?? "", isMulti: true)
                            cell.bottomCollectionViewHeightConstraint.constant = CGFloat(count * 40)
                        }
                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = false
                        cell.inputTextField.isHidden = true
                        cell.checkBoxView.isHidden = true
                        cell.uploadPhotoView.isHidden =  true
                        cell.bottomView.isHidden = true
                        cell.bottomViewHeightConstraint.constant = 2.0
                    case StringConstants.vehicleHealthCheckType6:
                        
                        if let radiosCount = data[sender.tag].yFields?.radios?.count,
                           let rFields =  data[sender.tag].rFields,
                           let yFields = data[sender.tag].yFields{
                            var count:  Int = 1
                            if(radiosCount == 1){
                                count += 1
                            }
                            else{
                                count = radiosCount
                            }
                            cell.updateCollectionView(rFields: rFields, yFields: yFields, status: StringConstants.vehicleHealthYellowStatus, value: vehicleObj.value ?? "", isMulti: false)
                            cell.bottomCollectionViewHeightConstraint.constant = CGFloat(count * 40)
                        }
                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = false
                        cell.inputTextField.isHidden = true
                        cell.checkBoxView.isHidden = true
                        cell.uploadPhotoView.isHidden =  false
                        cell.bottomView.isHidden = true
                        cell.bottomViewHeightConstraint.constant = 2.0
                    case StringConstants.vehicleHealthCheckType7:
                        
                        cell.bottomCollectionViewHeightConstraint.constant = 150.0
                        cell.collectionViewContainer.isHidden = false
                        cell.radiosCollectionView.isHidden = true
                        cell.inputTextField.isHidden = false
                        cell.checkBoxView.isHidden = true
                        cell.uploadPhotoView.isHidden =  true
                        cell.updateHint(hint: data[sender.tag].yFields?.hint ?? "")
                        if(vehicleObj.remarks == nil || vehicleObj.remarks == ""){
                            cell.inputTextField.inputTextfield.placeholder = vehicleObj.yFields?.hint
                        }
                        else{
                            cell.inputTextField.inputTextfield.text = vehicleObj.remarks
                        }
                    
                    default:
                        cell.bottomCollectionViewHeightConstraint.constant = 0.0
                        cell.collectionViewContainer.isHidden = true
                    }
                }
            }
            
            tableView.beginUpdates()
            tableView.endUpdates()
        }
    }
    
    // MARK: - Green button Action
    @objc func greenButtonClicked(sender: UIButton) {
        let indexPath = IndexPath(row: sender.tag, section: 0)
       if let cell = tableView.cellForRow(at: indexPath)as? VehicleSubsectionsTableViewCell
       {
        if(sender.isSelected == false)
        {
            selectedIndex = sender.tag
            sender.isSelected = false
            
            data[sender.tag].status = StatusEnum.Green.rawValue
            data[sender.tag].isValidated = true
            self.updateValuesinModel(inputVal: data)
            
            cell.greenButton.setImage(UIImage(named: "greencheckedicon"), for: .normal)
            cell.redButton.setImage(UIImage(named: "redunchecked"), for: .normal)
            cell.yellowButton.setImage(UIImage(named: "yellowunchecked"), for: .normal)
            cell.bottomCollectionViewHeightConstraint.constant = 0.0
            cell.collectionViewContainer.isHidden = true
        }
        tableView.beginUpdates()
        tableView.endUpdates()
       }
    }
    
    @objc func updateCheckBoxValues(sender: UIButton){
        sender.isSelected = !sender.isSelected
        if(sender.isSelected){
            data[sender.tag].remarks = "true"
        }
        else{
            data[sender.tag].remarks = ""
        }
        self.updateValuesinModel(inputVal: data)
    }
}
