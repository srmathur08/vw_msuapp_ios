//
//  VehicleHealthCheckTableViewContract.swift
//  MSU
//
//  Created by vectorform on 29/07/21.
//

import Foundation

protocol IVehicleHealthCheckTableViewController: IBaseViewController {

}


protocol IVehicleHealthCheckTableViewPresenter: IBasePresenter {
    
    ///This function called to update the VehicleHealthCheckModel in Vehicle health router
    ///
    ///     Warning input should not be empty
    ///  - Parameter input: must be a VehicleHealthCheckModel model
    func updateModelInRouter(input: VehicleHealthCheckModel)
    
    ///This function called to pop view controller
    ///
    func didSelectBack()
}
