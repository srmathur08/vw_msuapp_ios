//
//  VehicleHealthCheckTableViewController.swift
//  MSU
//
//  Created by vectorform on 29/07/21.
//

import UIKit
import Photos

class VehicleHealthCheckTableViewController: BaseViewController, IVehicleHealthCheckTableViewController, UpdateVehicleModelDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
   
    // MARK: - View Contoller Outlets
    @IBOutlet weak var sectionTitleLabel: UILabel!
    @IBOutlet weak var sectionsTableView: UITableView!
    
    let picker = UIImagePickerController()
    
    var vehicleModel: VehicleHealthCheckModel?
    
    var seletedIndex: Int = 0
    var receivedSRId: Int = 0
    private let presenter: IVehicleHealthCheckTableViewPresenter = VehicleHealthCheckTableViewPresenter()
    private lazy var tableViewAdapter: VehicleHealthCheckTableViewAdaptor  = {
        VehicleHealthCheckTableViewAdaptor(tableView: self.sectionsTableView)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.connectToView(view: self)
        // Do any additional setup after loading the view.
        picker.delegate = self
        picker.isEditing = true
        applyStylings()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    // MARK: - Apply Stylings to Objects
    private func applyStylings(){
        sectionTitleLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: false, size: 28.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        
        bindVehicleCheckList()
    }
    
    // MARK: - Bind Vehicle check list model
    ///This function called to bind the vehicle health check list
    ///
    private func bindVehicleCheckList() {
        sectionTitleLabel.text = vehicleModel?.sectionName ?? ""
        tableViewAdapter.vehicleDelegate = self
        tableViewAdapter.updateData(data: (vehicleModel?.subSection)!)
    }
    
    // MARK: - Update Vehicle model in router
    func updateVehicleModel(inputModel: [SubSection]) {
        vehicleModel?.subSection = inputModel
        presenter.updateModelInRouter(input: vehicleModel!)
    }
    
    // MARK: - Upload Photos
    func updatePhotos(input: Int) {
        print(input)
        seletedIndex = input
        camera()
    }
    
    // MARK: - Open Camera
    func camera()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
                picker.sourceType = .camera
                picker.cameraDevice = .rear
            
            self.present(picker, animated: true, completion: nil)
        }
        else{
            self.displayDefaultAlertForError(errorTitle: ErrorStringConstants.errorTitle, errorDescription: "No Camera")
        }
    }

    // MARK: - Image Picker Delegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
            if let chosenImage = info[.originalImage] as? UIImage{
                let imagesSection = ImagesDirectory()
                var imageName: String = ""
                let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let currentDateStr = formatter.string(from: Date())
                let currentDate = formatter.date(from: currentDateStr)
                    formatter.dateFormat = "dd-MM-yyyy-HH:mm:ss"
                let myStringafd = formatter.string(from: currentDate!)
                
                imageName = "Image\(myStringafd).png"
                let srID = String(describing: FacadeLayer.sharedInstance.vehicelCheckSrId).removeOptionalFromString()
                var subSection = vehicleModel?.subSection?[seletedIndex].name
                subSection = subSection?.replacingOccurrences(of: " ", with: "")
                let imagePath = imagesSection.writeImageToPath(imageName, image: chosenImage, srId: srID, subSection: subSection!)
              
                vehicleModel?.subSection?[seletedIndex].assetPaths.append(imagePath)
                presenter.updateModelInRouter(input: vehicleModel!)
                tableViewAdapter.updateData(data: (vehicleModel?.subSection)!)
            }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
           self.dismiss(animated: true, completion: nil)
       }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
