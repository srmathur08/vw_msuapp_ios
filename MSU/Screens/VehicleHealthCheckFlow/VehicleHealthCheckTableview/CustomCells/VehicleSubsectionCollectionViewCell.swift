//
//  VehicleSubsectionCollectionViewCell.swift
//  MSU
//
//  Created by vectorform on 05/08/21.
//

import UIKit

class VehicleSubsectionCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var radiobutton: UIButton!
    @IBOutlet weak var radioLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        addViews()
    }
 
    // MARK: - Add Views
    func addViews() {
        radioLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: false, size: 14.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
    }

}
