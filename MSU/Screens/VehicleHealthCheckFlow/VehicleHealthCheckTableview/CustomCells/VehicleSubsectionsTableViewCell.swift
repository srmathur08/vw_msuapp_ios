//
//  VehicleSubsectionsTableViewCell.swift
//  MSU
//
//  Created by vectorform on 05/08/21.
//

import UIKit
import Kingfisher

protocol SubSectionRadioButtonDelegate: AnyObject {
    ///This function called pass value from tableview cell to table view adaptor
    ///
    ///     Warning inputText should not be empty
    ///   - Parameter inputText: must be a string value
    func getSeletedValue(inputText: String)
    
    ///This function called pass value from tableview cell to table view adaptor
    ///
    ///     Warning input should not be empty
    ///   - Parameter input: must be a string value
    func cancelAssetClicked(input: Int)
}
class VehicleSubsectionsTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var subSectionlabel: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var redButton: UIButton!
    @IBOutlet weak var yellowButton: UIButton!
    @IBOutlet weak var greenButton: UIButton!
    
    @IBOutlet weak var collectionViewContainer: UIView!
    @IBOutlet weak var radiosCollectionView: UICollectionView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bottomCollectionViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var photosView: UIView!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var inputTextField: MSUInputTextFields!
    
    @IBOutlet weak var checkBoxView: UIView!
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var checkBoxLabel: UILabel!
    
    @IBOutlet weak var uploadPhotoView: UIView!
    @IBOutlet weak var uploadPhotoLabel: UILabel!
    @IBOutlet weak var photosCollectionView: UICollectionView!
    
    var galleryArray = [String]()
    let picker = UIImagePickerController()
    var radioButtonDelegate: SubSectionRadioButtonDelegate?
    var rDataFields: RFields?
    var yDataFields: YFields?
    var statusReceived: String?
    var valueReceived: String = ""
    var selectedIndexPath: IndexPath?
    var selectedRadios: [Int] = [Int]()
    var multiReceived: Bool = false
    private let spacing:CGFloat = 1.0
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.radiosCollectionView.register(UINib(nibName: "VehicleSubsectionCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "VehicleSubsectionCollectionViewCell")
        
        self.photosCollectionView.register(UINib(nibName: "VehicleHealthImagesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "VehicleHealthImagesCollectionViewCell")
       
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing)
        layout.minimumLineSpacing = spacing
        layout.minimumInteritemSpacing = spacing
        self.radiosCollectionView?.collectionViewLayout = layout
        

        
        checkBoxLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: false, size: 16.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        uploadPhotoLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: false, size: 16.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        checkBoxView.isHidden = true
        inputTextField.isHidden = true
        uploadPhotoView.isHidden = true
        radiosCollectionView.isHidden = true
        addViews()
    }

    // MARK: - Update Hint
    func updateHint(hint: String){
        inputTextField.textInputView( hintText: hint, isSecureTextEntry: false, returnKeyType: UIReturnKeyType.done, keyboardtype: UIKeyboardType.default, textColor: ColorConstants.appPrimaryColor, tintColor: ColorConstants.appPrimaryColor, lineColor: ColorConstants.underlineColor)
    }
    // MARK: - Add views
    func addViews() {
        subSectionlabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: false, size: 16.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    }

    // MARK: - Update collection view
    ///This function called to update the collection view on radio button check
    ///
    ///     Warning rFields, yFields, status and value should not be empty
    ///  - Parameter rFields: must be a RFields model
    ///  - Parameter yFields: must be a YFields model
    ///  - Parameter status: must be a string value
    ///  - Parameter value: must be a string value
    public func updateCollectionView(rFields: RFields, yFields: YFields, status: String, value: String, isMulti: Bool){
        if(status == StringConstants.vehicleHealthRedStatus){
            self.rDataFields = rFields
        }
        else{
            self.yDataFields = yFields
        }
        if(isMulti){
            radiosCollectionView.allowsMultipleSelection = true
        }
        else{
            radiosCollectionView.allowsMultipleSelection = false
        }
        statusReceived = status
        valueReceived = value
        multiReceived = isMulti
        radiosCollectionView.dataSource = self
        radiosCollectionView.delegate = self
        radiosCollectionView.setContentOffset(radiosCollectionView.contentOffset, animated:false)
        DispatchQueue.main.async {
            self.radiosCollectionView.reloadData()
        }

//        if((self.yDataFields?.radios!.count)! > 0 && statusReceived == StringConstants.vehicleHealthYellowStatus){
//            radiosCollectionView.reloadData()
//        }
//        else if((self.rDataFields?.radios!.count)! > 0 && statusReceived == StringConstants.vehicleHealthRedStatus){
//            radiosCollectionView.reloadData()
//        }
       
    }
    
    // MARK: - Update collection view
    ///This function called to update the collection view
    ///
    ///      Warning input should not be empty
    ///  - Parameter input: list of string URLs
    public func updatePhotosCollectionView(input: [String]){
        self.galleryArray = input
        self.photosCollectionView.reloadData()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
//    override func prepareForReuse() {
//        super.prepareForReuse()
//
//        self.redButton.setImage(UIImage(named: "redunchecked"), for: .normal)
//        self.yellowButton.setImage(UIImage(named: "yellowunchecked"), for: .normal)
//        self.greenButton.setImage(UIImage(named: "greenunchecked"), for: .normal)
//    }
    
}

// MARK: - Collection view delegate methods
extension VehicleSubsectionsTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == radiosCollectionView){
            if(statusReceived == StringConstants.vehicleHealthRedStatus){
                return (rDataFields?.radios!.count)!
            }
            else if(statusReceived == StringConstants.vehicleHealthYellowStatus){
                return (yDataFields?.radios?.count)!
            }
            else{
              return 0
            }
        }
        else{
            return self.galleryArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == radiosCollectionView){
            if let cell: VehicleSubsectionCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "VehicleSubsectionCollectionViewCell", for: indexPath) as? VehicleSubsectionCollectionViewCell{
                if(statusReceived == StringConstants.vehicleHealthRedStatus){
                    let radioText = rDataFields?.radios?[indexPath.item]
                    
                    if(radioText?.caseInsensitiveCompare(valueReceived) == .orderedSame){
                        cell.radiobutton.setImage(UIImage(named: "bluechecked"), for: .normal)
                    }
                   
                    cell.radioLabel.text = radioText
                    var count:  Int = 1
                    if(rDataFields?.radios!.count == 1){
                        count += 1
                    }
                    else{
                        count = (rDataFields?.radios!.count) ?? 0
                    }
                    cell.frame = CGRect(x: cell.frame.origin.x, y: cell.frame.origin.y, width: cell.frame.size.width, height: CGFloat(count * 25))
                }
                else if(statusReceived == StringConstants.vehicleHealthYellowStatus){
                    let radioText = yDataFields?.radios?[indexPath.item]
                    
                    if(radioText?.caseInsensitiveCompare(valueReceived) == .orderedSame){
                        cell.radiobutton.setImage(UIImage(named: "bluechecked"), for: .normal)
                    }
                    cell.radioLabel.text = radioText
                    var count:  Int = 1
                    if(yDataFields?.radios!.count == 1){
                        count += 1
                    }
                    else{
                        count = (yDataFields?.radios!.count) ?? 0
                    }
                    cell.frame = CGRect(x: cell.frame.origin.x, y: cell.frame.origin.y, width: cell.frame.size.width, height: CGFloat(count * 25))
                }
                if(multiReceived){
                    if self.selectedRadios.contains(indexPath.item) {
                        cell.radiobutton.setImage(UIImage(named: "bluechecked"), for: .normal)
                    }
                    else{
                        cell.radiobutton.setImage(UIImage(named: "blueunchecked"), for: .normal)
                    }
                }
                else{
                    if indexPath == self.selectedIndexPath{
                        cell.radiobutton.setImage(UIImage(named: "bluechecked"), for: .normal)
                    }
                    else{
                        cell.radiobutton.setImage(UIImage(named: "blueunchecked"), for: .normal)
                    }
                }
               
                cell.radiobutton.tag = indexPath.item
    //            cell.radiobutton.addTarget(self, action: #selector(radioButtonAction(sender:)), for: .touchUpInside)
                cell.layoutSubviews()
                cell.layoutIfNeeded()
             
                return cell
            }
        }
        else{
            if let cell: VehicleHealthImagesCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "VehicleHealthImagesCollectionViewCell", for: indexPath) as? VehicleHealthImagesCollectionViewCell{
                
                let anObj = galleryArray[indexPath.item]

                let imagesDir = ImagesDirectory()
                let assetIamge = imagesDir.getFileAt(filepath: anObj)
                cell.assetImageView.image = assetIamge
                cell.assetImageView.layer.cornerRadius = 5.0
                cell.assetImageView.layer.masksToBounds = true
                
                cell.cancelButton.tag = indexPath.row
                cell.cancelButton.addTarget(self, action: #selector(cancelButtonClick(sender:)), for: .touchUpInside)
                return cell
            }
        }
        return UICollectionViewCell()
    }
  
    // MARK: - cancel button clicked
    @objc func cancelButtonClick(sender: UIButton){
        radioButtonDelegate?.cancelAssetClicked(input: sender.tag)
        if(galleryArray.count > 0){
            galleryArray.remove(at: sender.tag)
            let indexPath = IndexPath(item: sender.tag, section: 0)
            self.photosCollectionView.performBatchUpdates({
                self.photosCollectionView.deleteItems(at: [indexPath])
            }) { (finished) in
                self.photosCollectionView.reloadItems(at: self.photosCollectionView.indexPathsForVisibleItems)
            }
        }
    }
    // MARK: - Radio button action and passing the value in delegate
//    @objc func radioButtonAction(sender: UIButton){
//        let indexPath = IndexPath(item: sender.tag, section: 0)
//        if let cell = radiosCollectionView.cellForItem(at: indexPath) as? VehicleSubsectionCollectionViewCell{
//            print(cell.radioLabel.text)
//            if(sender.isSelected == false)
//            {
//                sender.isSelected = true
//                cell.radiobutton.setImage(UIImage(named: "checked"), for: .normal)
//            }
//            else
//            {
//                sender.isSelected = false
//            }
//
//            radioButtonDelegate?.getSeletedValue(inputText: cell.radioLabel.text ?? "")
//        }
//
//    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.radiosCollectionView{
            if let selectedCell: VehicleSubsectionCollectionViewCell = collectionView.cellForItem(at: indexPath) as? VehicleSubsectionCollectionViewCell{

//                selectedCell.radiobutton.setImage(UIImage(named: "checked"), for: .normal)
                radioButtonDelegate?.getSeletedValue(inputText: selectedCell.radioLabel.text ?? "")
                self.selectedIndexPath = indexPath
                if(!self.selectedRadios.contains(indexPath.item)){
                    self.selectedRadios.append(indexPath.item)
                }
            }
            collectionView.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(collectionView == radiosCollectionView){
            let numberOfItemsPerRow:CGFloat = 2
            let spacingBetweenCells:CGFloat = 1
            let totalSpacing = (3 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells)
            let width = (radiosCollectionView.bounds.width - totalSpacing)/numberOfItemsPerRow
            return CGSize(width: width, height: 50.0)
        }
        else{
            return CGSize(width: 80.0, height: 80.0)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
}
