//
//  VehicleHealthImagesCollectionViewCell.swift
//  MSU
//
//  Created by vectorform on 10/08/21.
//

import UIKit

class VehicleHealthImagesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var assetImageView: UIImageView!
    @IBOutlet weak var cancelButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cancelButton.layer.cornerRadius = cancelButton.frame.size.width / 2
    }

}
