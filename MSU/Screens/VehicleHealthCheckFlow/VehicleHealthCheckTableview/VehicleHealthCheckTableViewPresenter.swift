//
//  VehicleHealthCheckTableViewPresenter.swift
//  MSU
//
//  Created by vectorform on 29/07/21.
//

import Foundation

class VehicleHealthCheckTableViewPresenter: BasePresenter, IVehicleHealthCheckTableViewPresenter {
   
    
    
    private weak var myView: VehicleHealthCheckTableViewController? {
        return self.view as? VehicleHealthCheckTableViewController
    }
    
    private var vehicleHealthCheckRouter: VehicleHealthCheckRouter? {
        return navController as? VehicleHealthCheckRouter
    }
    
    // MARK: - Update model in router
    func updateModelInRouter(input: VehicleHealthCheckModel) {
        self.vehicleHealthCheckRouter?.updateVehicleHealthModel(inputModel: input)
    }
    
    // MARK: - did select back
    func didSelectBack() {
        vehicleHealthCheckRouter?.didSelectBack()
    }
}
