//
//  AcknowledgePresenter.swift
//  MSU
//
//  Created by vectorform on 11/08/21.
//

import Foundation
import UIKit

class AcknowledgePresenter: BasePresenter, IAcknowledgeViewPresenter {
        
    private weak var myView: AcknowledgeViewController? {
        return self.view as? AcknowledgeViewController
    }
    
    private var vehicleHealthCheckRouter: VehicleHealthCheckRouter? {
        return navController as? VehicleHealthCheckRouter
    }
    
    // MARK: - Present signature view
    func presentSignatureView(delegate: ISignatureView, imageView: UIImage) {
        vehicleHealthCheckRouter?.presentSignatureViewController(animated: true, delegate: delegate, imageView: imageView)
    }
    
    // MARK: - Update vehicle model
    func updateVehicleModel(model: VehicleHealthCheckModel)
    {
        vehicleHealthCheckRouter?.updateVehicleHealthModel(inputModel: model)
    }
    
}
