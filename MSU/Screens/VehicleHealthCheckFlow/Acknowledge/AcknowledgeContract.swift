//
//  AcknowledgeContract.swift
//  MSU
//
//  Created by vectorform on 11/08/21.
//

import Foundation
import UIKit

protocol IAcknowledgeViewController: IBaseViewController {

}


protocol IAcknowledgeViewPresenter: IBasePresenter {
    ///This function called to present the signature view in landscape
    ///
    ///  - Parameter delegate: ISignatureView
    ///  - Parameter imageView: UIImage
    func presentSignatureView(delegate: ISignatureView, imageView: UIImage)
    
    ///This function called to update values in model
    ///
    ///     Warning model must not be empty
    ///  - Parameter model: must be a VehicleHealthCheckModel model
    func updateVehicleModel(model: VehicleHealthCheckModel)
}
