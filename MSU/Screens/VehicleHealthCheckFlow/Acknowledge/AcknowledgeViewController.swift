//
//  AcknowledgeViewController.swift
//  MSU
//
//  Created by vectorform on 11/08/21.
//

import UIKit

enum kSignedBy : String {
    case customer = "Customer"
    case advisor = "Advisor"
}
class AcknowledgeViewController: BaseViewController, IAcknowledgeViewController, ISignatureView {
    
  

    // MARK: - View Contoller Outlets
    @IBOutlet weak var sectionTitleLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var advisorSignView: UIView!
    @IBOutlet weak var serviceAdvisorLabel: UILabel!
    @IBOutlet weak var serviceAdvisorImageView: UIImageView!
    @IBOutlet weak var customerSignView: UIView!
    @IBOutlet weak var customerSignLabel: UILabel!
    @IBOutlet weak var customerSignImageView: UIImageView!
    
    
    private let presenter: IAcknowledgeViewPresenter = AcknowledgePresenter()
    
    var vehicleModel: VehicleHealthCheckModel?
    
    var selectedSign: String = ""
    var seletedImage: UIImage = UIImage()
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.connectToView(view: self)
        
        // Do any additional setup after loading the view.
        applyStylings()
    }
    
    // MARK: - Apply Stylings to Objects
   private func applyStylings(){
    
    sectionTitleLabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: false, size: 28.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
    
    serviceAdvisorLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: false, size: 18.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    
    serviceAdvisorImageView.imageViewBorder()
    
    customerSignLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: false, size: 18.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
    
    customerSignImageView.imageViewBorder()
    
    scrollViewheightConstraint.constant = 500.0
    
    let advisorImageTap = UITapGestureRecognizer(target: self, action: #selector(self.imageViewTapped(_:)))
    serviceAdvisorImageView.addGestureRecognizer(advisorImageTap)
    serviceAdvisorImageView.tag = 1
    serviceAdvisorImageView.isUserInteractionEnabled = true
    
    let customerImageTap = UITapGestureRecognizer(target: self, action: #selector(self.imageViewTapped(_:)))
    customerSignImageView.addGestureRecognizer(customerImageTap)
    customerSignImageView.tag = 2
    customerSignImageView.isUserInteractionEnabled = true
    
    bindVehicleCheckList()
    
   }
    
    
    // MARK: - Bind Vehicle Model
    private func bindVehicleCheckList(){
        sectionTitleLabel.text = vehicleModel?.sectionName ?? ""
    }

    
    // MARK: - User Interactions
    
    @objc func imageViewTapped(_ sender:UIGestureRecognizer){
        guard let getTag = sender.view?.tag else { return }
        if(getTag == 1){
            selectedSign = kSignedBy.advisor.rawValue
        }
        else{
            selectedSign = kSignedBy.customer.rawValue
        }
        presenter.presentSignatureView(delegate: self, imageView: seletedImage)
    }
    
    // MARK: - Signature received
    func signatureAccpted(image: UIImage) {
        var imageName: String = ""
        let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let currentDateStr = formatter.string(from: Date())
        let currentDate = formatter.date(from: currentDateStr)
            formatter.dateFormat = "dd-MM-yyyy-HH:mm:ss"
        let myStringafd = formatter.string(from: currentDate!)
        let imagesDirectory = ImagesDirectory()
        imageName = "Image\(myStringafd).png"
       let imagepath = imagesDirectory.writeSignatureImagesToFile(imageName, image: image, name: selectedSign)
        
        if(selectedSign == kSignedBy.advisor.rawValue){
            serviceAdvisorImageView.image = image
            self.vehicleModel?.serviceAdvisorSign = imagepath
        }
        else{
            customerSignImageView.image = image
            self.vehicleModel?.customerSign = imagepath
        }
      
    
        print(imagepath)
        presenter.updateVehicleModel(model: self.vehicleModel!)
        seletedImage = image
    }
    
    // MARK: - Clear Signature
    func clearSignature() {
        if(selectedSign == kSignedBy.advisor.rawValue){
            serviceAdvisorImageView.image = nil
        }
        else{
            customerSignImageView.image = nil
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
