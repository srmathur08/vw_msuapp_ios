//
//  SignaturePresenter.swift
//  MSU
//
//  Created by vectorform on 11/08/21.
//

import Foundation

class SignaturePresenter: BasePresenter, ISignatureViewPresenter {
    
    
    private weak var myView: SignatureViewController? {
        return self.view as? SignatureViewController
    }
    
    // MARK: - Dismiss view
    func dismissView() {
        myView?.dismiss(animated: true, completion: nil)
    }
    
}
