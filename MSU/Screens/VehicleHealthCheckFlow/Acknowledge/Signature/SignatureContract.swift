//
//  SignatureContractContract.swift
//  MSU
//
//  Created by vectorform on 11/08/21.
//

import Foundation

protocol ISignatureController: IBaseViewController {

}


protocol ISignatureViewPresenter: IBasePresenter {
    ///This function dismiss the view
    ///
    func dismissView()
}
