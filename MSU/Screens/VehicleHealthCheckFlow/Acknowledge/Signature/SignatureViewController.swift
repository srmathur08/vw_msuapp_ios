//
//  SignatureViewController.swift
//  MSU
//
//  Created by vectorform on 11/08/21.
//

import UIKit

protocol ISignatureView: AnyObject {
    func signatureAccpted(image: UIImage)
    func clearSignature()
}
class SignatureViewController: BaseViewController, ISignatureController {


    // MARK: - View Contoller Outlets
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var signatureView: MSUSignatureView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var acceptButton: MSUButton!
    @IBOutlet weak var clearButton: MSUButton!
    
    private let presenter: ISignatureViewPresenter = SignaturePresenter()
    var signatureImage : UIImage?
    var receivedSignName: String = ""
    var delegate: ISignatureView?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter.connectToView(view: self)
        AppUtility.lockOrientation(.landscapeRight)
        self.signatureView.delegate = self
        signatureView.isUserInteractionEnabled = true
        acceptButton.isHidden = true
        applyStylings()
    }
    
    // MARK: - Apply Stylings to Objects
   private func applyStylings(){
    
    acceptButton.appButtonStyling(titleColor: ColorConstants.solidWhite, backgroundColor: ColorConstants.appSecondaryBlueColor)
    
    acceptButton.titleLabel?.font = Font.vwheadfont.with(size: CGFloat(16.0), weight: .vwheadregular)
    
    clearButton.appButtonStyling(titleColor: ColorConstants.solidWhite, backgroundColor: ColorConstants.appSecondaryBlueColor)
    
    clearButton.titleLabel?.font = Font.vwheadfont.with(size: CGFloat(16.0), weight: .vwheadregular)
    
    if let signImage = self.signatureImage{
    }
   }

    // MARK: - Accept Button action
    @IBAction func acceptButtonAction(_ sender: MSUButton) {
        signatureImage = self.signatureView.getSignature()
       
        delegate?.signatureAccpted(image: signatureImage ?? UIImage())
        AppUtility.lockOrientation(.portrait)
        presenter.dismissView()
    }
    
    // MARK: - Clear button action
    @IBAction func clearButtonAction(_ sender: MSUButton) {
        acceptButton.isHidden = true
        delegate?.clearSignature()
        self.signatureView.clear()
    }
    
    // MARK: - Close Button action
    @IBAction func closeButtonAction(_ sender: UIButton) {
        AppUtility.lockOrientation(.portrait)
        presenter.dismissView()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SignatureViewController: MSUSignatureDelegate{
    func startedDrawing() {
        acceptButton.isHidden = false
    }
    
    func finishedDrawing() {
        
    }
    
    
}
