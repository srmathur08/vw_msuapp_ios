//
//  VehicleHealthCheckRouter.swift
//  MSU
//
//  Created by vectorform on 29/07/21.
//

import Foundation
import UIKit
import SnapKit
import SwiftyJSON
import ObjectMapper

class VehicleHealthCheckRouter: BaseNavigationController, IVehicleHealthCheckRouter, UINavigationControllerDelegate {

    

    private let sectionStackView: UIStackView = UIStackView()
    private var currentSection: Int = 1
    private var chunkNumber: Int = 0
    private var totalSections: Int = 0 //TODO: Replace with dynamic count from section array
    var vehicleCheckModel: [VehicleHealthCheckModel]?

    var topView = UIView()
    var bottomView = UIView()
    var style = ToastStyle()
    var vehicleCheckUpId: Int = 0
    
    var pdfArray: [String] = [String]()
    init() {
        super.init(nibName: nil, bundle: nil)
        addTopBottomViews()
        getVehicleData()
       
    }
    // MARK: - Geting vehicle health checkup ID
    ///This function called to get the vehicleCheckUpId
    ///
    ///  - Returns vehicleCheckUpId: integer value
    func getVehicleCheckUpId() -> Int {
        return vehicleCheckUpId
    }
    
   
    // MARK: - Geting vehicle health check data from JSON and mapped in Model
    ///This function called to get the JSON data and convert to model
    ///
    private func getVehicleData(){
        if let path = Bundle.main.path(forResource: "VehicleHealthcheckJSON", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                let jsonObj = try JSON(data: data)
                self.vehicleCheckModel = Mapper<VehicleHealthCheckModel>().mapArray(JSONObject: jsonObj.arrayObject)
                self.setSectionImages()
                self.set360ImagesModel()
                self.totalSections = self.vehicleCheckModel?.count ?? 8
                pushVehicleHealthCheckTableViewController(animated: false, inputModel: (self.vehicleCheckModel?[0])!)
            } catch let error {
                print("parse error: \(error.localizedDescription)")
            }
        } else {
            print("Invalid filename/path.")
        }
    }
    
    // MARK: - Setting the section images
    ///This function called to set the section images
    ///
    private func setSectionImages(){
        for i in 0..<self.vehicleCheckModel!.count{
            
            let sectionName = self.vehicleCheckModel?[i].sectionName
            
            switch sectionName {
            case "Inspection":
                self.vehicleCheckModel?[i].icon = "inspection"
            case "Interior":
                self.vehicleCheckModel?[i].icon = "interior"
            case "Exterior":
                self.vehicleCheckModel?[i].icon = "exterior"
            case "Engine Compartment":
                self.vehicleCheckModel?[i].icon = "engineCompartment"
            case "Boot":
                self.vehicleCheckModel?[i].icon = "boot"
            case "Exterior Check":
                self.vehicleCheckModel?[i].icon = "exteriorlift"
            case "Scratch/Dent/Damage":
                self.vehicleCheckModel?[i].icon = "vehicleImages"
            default:
                self.vehicleCheckModel?[i].icon  = ""
            }
        }
    }
    // MARK: - Add images in model
    ///This function called to set the 360 model images
    ///
    private func set360ImagesModel(){
        var imageModel: [Images360Model] = [Images360Model]()
        imageModel.append(Images360Model(imagePath: "carRightAngle", angle: "Right", markers: [])!)
        imageModel.append(Images360Model(imagePath: "carFrontAngle", angle: "Front", markers: [])!)
        imageModel.append(Images360Model(imagePath: "carLeftAngle", angle: "Left", markers: [])!)
        imageModel.append(Images360Model(imagePath: "carBackAngle", angle: "Back", markers: [])!)
      
        let filterArray = self.vehicleCheckModel?.filter({
            $0.sectionName == StringConstants.scratch
        })
        if(filterArray!.count > 0){
            self.vehicleCheckModel?[(filterArray?[0].index)! - 1].images360Array = imageModel
        }
    }
    
    // MARK: - Update router vehicleCheckModel
    ///This function called to update the vehicleCheckModel
    ///
    public func updateVehicleHealthModel(inputModel: VehicleHealthCheckModel){
        self.vehicleCheckModel?[currentSection - 1] = inputModel
    }
    
    // MARK: - Add top bottom views
    ///This function called to add the addTopBottomViews of the navigation bar
    ///
    private func addTopBottomViews(){
        
        self.topView.isHidden = false
        self.bottomView.isHidden = false
        topView.backgroundColor = ColorConstants.clear

        let headerlabel = MSULabel()
        headerlabel.text = "Vehicle Healthcheck"
        headerlabel.applyLabelStyling(textColor: ColorConstants.solidBlack, isBold: true, size: 18.0, alignment: .center, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        
        for i in 1 ... 4 {
            let vehicleHealthCheckHeaderview: VehicleHealthCheckHeaderview = VehicleHealthCheckHeaderview(withIndex: i)
            sectionStackView.addArrangedSubview(vehicleHealthCheckHeaderview)
        }
        
        self.updateSectionProgress()
        
        sectionStackView.axis = .horizontal
        sectionStackView.distribution = .fillEqually
        sectionStackView.alignment = .center
        sectionStackView.spacing = 0.0
        sectionStackView.translatesAutoresizingMaskIntoConstraints = false

    
        bottomView.backgroundColor = ColorConstants.solidWhite
        bottomView.frame = CGRect(x: 0, y: self.view.frame.size.height - 120, width: self.view.frame.size.width, height: 120.0)
        
        let forwardButton = MSUButton()
        forwardButton.setImage(UIImage(named: "ForwordIcon"), for: .normal)
        forwardButton.appButtonStyling(titleColor: ColorConstants.clear, backgroundColor: ColorConstants.appSecondaryBlueColor)
        forwardButton.addTarget(self, action: #selector(frwdButtonAction), for: .touchUpInside)
        
        let backButton = MSUButton()
        backButton.setImage(UIImage(named: "backicon"), for: .normal)
        backButton.appButtonStyling(titleColor: ColorConstants.clear, backgroundColor: ColorConstants.clear)
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        
        topView.addSubviews(views: [headerlabel,sectionStackView])
        bottomView.addSubviews(views: [forwardButton])
        bottomView.addSubviews(views: [backButton])
        
        self.view.addSubviews(views: [topView,bottomView])
        
        let topPadding: CGFloat = 40.0
        let topviewHeight: CGFloat = 100.0
        topView.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(view.safeAreaLayoutGuide).offset(topPadding)
            make.top.left.right.equalTo(view.safeAreaLayoutGuide)
            make.height.equalTo(topviewHeight)
        }
        
        let headerHeight: CGFloat = 25.0
        headerlabel.snp.makeConstraints {(make: ConstraintMaker) in
            make.top.equalTo(20.0)
            make.centerX.equalToSuperview()
            make.height.equalTo(headerHeight)
        }
        
        let stackViewHeight: CGFloat = 50.0
        sectionStackView.snp.makeConstraints {(make: ConstraintMaker) in
            make.bottom.equalToSuperview()
            make.left.equalTo(10)
            make.right.equalToSuperview()
            make.height.equalTo(stackViewHeight)
        }
        
        let bottomViewHeight: CGFloat = 100.0
        bottomView.snp.makeConstraints { (make: ConstraintMaker) in
            make.bottom.left.right.equalTo(view.safeAreaLayoutGuide)
            make.height.equalTo(bottomViewHeight)
        }
        
        let buttonWidthHeight: CGFloat = 50.0
        let buttonLeftRightPadding: CGFloat = 20.0
        let buttonCenterOffset: CGFloat = 10.0

        backButton.snp.makeConstraints {(make: ConstraintMaker) in
            make.left.equalToSuperview().offset(buttonLeftRightPadding)
            make.centerY.equalToSuperview().offset(buttonCenterOffset)
            make.width.height.equalTo(buttonWidthHeight)
        }
        
        forwardButton.snp.makeConstraints {(make: ConstraintMaker) in
            make.right.equalToSuperview().offset(-buttonLeftRightPadding)
            make.centerY.equalToSuperview().offset(buttonCenterOffset)
            make.width.height.equalTo(buttonWidthHeight)
        }
        
    }

    // MARK: - Forward button action
    ///This function called on fowrad button action
    ///
    @objc func frwdButtonAction(){
        if(currentSection % 4 == 0) {
            chunkNumber += 1
        }
      
        if currentSection < totalSections{
            calculatePercentage(model: (self.vehicleCheckModel?[currentSection - 1])!)
            let sectionName = self.vehicleCheckModel?[currentSection].sectionName
            if(sectionName?.caseInsensitiveCompare(StringConstants.scratch) == .orderedSame){
                self.pushVehicleHealthCheck360ViewController(animated: false, inputModel: (self.vehicleCheckModel?[currentSection])!)
                self.currentSection += 1
                self.updateSectionProgress()
            }
            else if(sectionName?.caseInsensitiveCompare(StringConstants.acknowledge) == .orderedSame){
                self.pushVehicleHealthCheckAcknowledgeViewController(animated: false, inputModel: (self.vehicleCheckModel?[self.currentSection])!)
                self.currentSection += 1
                self.updateSectionProgress()
            }
            else{
              
                let subMenu = self.vehicleCheckModel?[currentSection - 1].subSection
                
                let subMenuFilter = subMenu?.filter({
                    $0.isValidated == true
                })
               
                
                let subSections = self.vehicleCheckModel?[currentSection - 1].subSection

                let statusFilter = subSections?.filter({
                    $0.status > 0
                })
                if(statusFilter?.count != subSections?.count){
                    self.view.makeToast(ErrorStringConstants.vcSubSectionError, duration: 3.0, position: .bottom, style: style)
                    return
                }
                else if(subMenuFilter!.count != statusFilter?.count ){
                    self.view.makeToast(ErrorStringConstants.vcSubMenuError, duration: 3.0, position: .bottom, style: style)
                    return
                }
                else{
           
                    self.pushVehicleHealthCheckTableViewController(animated: false, inputModel: (self.vehicleCheckModel?[self.currentSection])!)
                    self.currentSection += 1
                    self.updateSectionProgress()
                   
                    
                }
            }
        }
        else if(currentSection == totalSections){
            if let model = self.vehicleCheckModel{
                let customerSign = model[currentSection - 1].customerSign
                let advisorSign = model[currentSection - 1].serviceAdvisorSign
                if(customerSign == "" || advisorSign == ""){
                    self.view.makeToast(ErrorStringConstants.vcSignatureError, duration: 3.0, position: .bottom, style: style)
                }
                else{
                    updateFinalDetailsTOAPI()
                    pushPreviewViewController(animated: true, inputModel: model)
                }
            }
        }
        
    }
    
    // MARK: - Send all sections data
    ///This function called to save all the sections data to API
    ///
    func updateFinalDetailsTOAPI()
    {
        for i in 0..<totalSections{
            let uploadSectionName = self.vehicleCheckModel?[i].sectionName
            if(uploadSectionName == StringConstants.scratch){
                Dispatch.background {
                    self.uploadMarkersDetailsToAPI(model: (self.vehicleCheckModel?[i])!, index: i)
                }
            }
            else if(uploadSectionName?.caseInsensitiveCompare(StringConstants.acknowledge) == .orderedSame){
                Dispatch.background {
                self.uploadSignatureDetailsToAPI(model: (self.vehicleCheckModel?[i])!, index: i)
                }
            }
            else{
                Dispatch.background {
                self.uploadModelDetailsToAPI(model: (self.vehicleCheckModel?[i])!, index: i)
                }
            }
        }
    }
    // MARK: - Back button action
    ///This function called on backword button action
    ///
    @objc func backButtonAction(){
        if currentSection > 1 { // Need to reduce before calculation
            currentSection -= 1
            self.didSelectBack()
        } else {
            self.dismiss(animated: true, completion: nil)
        }
        if(currentSection % 4 == 0) {
            chunkNumber -= 1
        }
        updateSectionProgress()
        
    }
    // MARK: - Calculate the percentage
    
    ///This function called to calculate the percentages of the selected sub section values
    ///
    ///     Warning model must not be empty
    ///   - Parameter model: VehicleHealthCheckModel
    func calculatePercentage(model: VehicleHealthCheckModel){
        if let subsections = model.subSection{
        let subSectionCount = (subsections.count) * 2
        let selectedRedArray = subsections.filter({
            $0.status == StatusEnum.Red.rawValue
        })
        let selectedYellowArray = subsections.filter({
            $0.status == StatusEnum.Yellow.rawValue
        })
        let selectedGreenArray = subsections.filter({
            $0.status == StatusEnum.Green.rawValue
        })
        
        let redCount = selectedRedArray.count * IntConstants.redValue
        let yellowCount = selectedYellowArray.count * IntConstants.yellowValue
        let greenCount = selectedGreenArray.count * IntConstants.greenValue
        
        let statusCount = redCount + yellowCount + greenCount
        let statusCountDivide = CGFloat(statusCount) / CGFloat(subSectionCount)
            var finalScore: Int = 0
        if(statusCount != 0){
            var finalvalDouble = Double(statusCountDivide * 100)
            finalvalDouble = finalvalDouble.rounded(toPlaces: 0)
            finalScore = Int(finalvalDouble)
            self.vehicleCheckModel?[currentSection - 1].score = String(describing: finalScore).removeOptionalFromString()
        }
            
        if(finalScore < 80){
            self.vehicleCheckModel?[currentSection - 1].scoreColor = StringConstants.vehicleHealthRedStatus
          }
        else if(80...90 ~= finalScore){
            self.vehicleCheckModel?[currentSection - 1].scoreColor = StringConstants.vehicleHealthYellowStatus
          }
        else{
            self.vehicleCheckModel?[currentSection - 1].scoreColor = StringConstants.vehicleHealthGreenStatus
          }
        }
    }
    // MARK: - Upload details to API
    ///This function called to Upload the section details to API
    ///
    ///     Warning model and index must not be empty
    ///   - Parameter model: VehicleHealthCheckModel
    func uploadModelDetailsToAPI(model: VehicleHealthCheckModel, index: Int){
        let vehicleCheckUpId = self.getVehicleCheckUpId()
        print(vehicleCheckUpId)
        var checkUpData: [[String: AnyObject]] =  [[String: AnyObject]]()
        let subSections = model.subSection
            for i in 0..<subSections!.count{
                let name = subSections?[i].name ?? ""
                let value = subSections?[i].value ?? ""
                let remarks = subSections?[i].remarks ?? ""
                let staus = subSections?[i].status ?? 0
                let sectionModel :[String: AnyObject] = [
                    "SectionName": model.sectionName as Any,
                    "Name": name as Any,
                    "Value": value as Any,
                    "Remarks": remarks as Any,
                    "Status" : staus as Any,
                    "VehicleCheckupId": vehicleCheckUpId as Any] as [String:AnyObject]
                checkUpData.append(sectionModel)
            }
            
            let postData: [String: AnyObject] = ["VehicleCheckupId": vehicleCheckUpId as Any,
            "CheckupData": checkUpData as Any] as [String: AnyObject]
            print(postData)
            FacadeLayer.sharedInstance.webserviceManager.genericAPICalls(requestParams: postData, apiPath: VFApiCalls.createVehicleCheckupArray) { [weak self] (status, response) in
                guard let strongSelf = self else{ return }
                if(status){
                    let responseVal = JSON(response as Any)
                    if let responseModel = Mapper<VehicleArrayResponseModel>().map(JSONObject:responseVal.dictionaryObject){
                        
                        strongSelf.updateVehicleDataId(arrayModel: responseModel.checkupData!, model: subSections!, sectionIndex: index)
                    }
                }
            }
    }
    // MARK: - To Update values in model
    ///This function called to update the vehicleCheckupDataId in vehicleCheckModel
    ///
    ///     Warning arrayModel, model and sectionIndex must not be empty
    ///   - Parameter arrayModel: List of CheckupData model
    ///   - Parameter model: List of SubSection model
    ///   - Parameter sectionIndex: must be a integer
    func updateVehicleDataId(arrayModel: [CheckupData], model: [SubSection], sectionIndex: Int){
        print("updateVehicleDataId")
        if(arrayModel.count == model.count){
            for i in 0..<model.count{
                let name = model[i].name ?? ""
                let responseName = arrayModel[i].name ?? ""
                if(name.caseInsensitiveCompare(responseName) == .orderedSame){
                    self.vehicleCheckModel?[sectionIndex].subSection![i].vehicleCheckUpDataId = arrayModel[i].vehicleCheckupDataId!
                }
            }
            self.getSubSectionAssets(model:(self.vehicleCheckModel?[sectionIndex].subSection)!)
        }
    }
  
    // MARK: - Get subsection assets
    ///This function called to get the assets from sub sections
    ///
    ///     Warning model must not be empty
    ///   - Parameter model: List of SubSection model
    func getSubSectionAssets(model: [SubSection]){
      print("called assets")
        for i in 0..<model.count{
            let vcCheckDataId = model[i].vehicleCheckUpDataId
            let assetsArray = model[i].assetPaths
            for j in 0..<assetsArray.count{
                let galleryImg = assetsArray[j]
                let assetName = galleryImg.lastPathComponent
                let imagesDir = ImagesDirectory()
                if let assetImage = imagesDir.getFileAt(filepath: galleryImg){
                    let assetBase64String = self.convertImageToBase64String(img: assetImage)
                    self.uploadSectionsAssetsTOAPI(checkDataId: vcCheckDataId, assetName: assetName, assetbasae64: assetBase64String)
                }
            }
        }
    }
    
    // MARK: - Upload Signature details to API
    ///This function called to upload the signature details to API
    ///
    ///     Warning model and index must not be empty
    ///   - Parameter model: VehicleHealthCheckModel model
    ///   - Parameter index: must be a integer value
    func uploadSignatureDetailsToAPI(model: VehicleHealthCheckModel, index: Int){
        let vehicleCheckUpId = self.getVehicleCheckUpId()
        let signatures = [StringConstants.saSignature, StringConstants.ccSignature]
        var checkUpData: [[String: AnyObject]] =  [[String: AnyObject]]()
        for i in 0..<signatures.count{
            let sectionModel :[String: AnyObject] = [
                "SectionName": model.sectionName as Any,
                "Name": signatures[i] as Any,
                "Value": "" as Any,
                "Remarks": "" as Any,
                "Status" : 0 as Any,
                "VehicleCheckupId": vehicleCheckUpId as Any] as [String:AnyObject]
            checkUpData.append(sectionModel)
            }
        let postData: [String: AnyObject] = ["VehicleCheckupId": vehicleCheckUpId as Any,
        "CheckupData": checkUpData as Any] as [String: AnyObject]
        print(postData)
        FacadeLayer.sharedInstance.webserviceManager.genericAPICalls(requestParams: postData, apiPath: VFApiCalls.createVehicleCheckupArray) { [weak self] (status, response) in
            guard let strongSelf = self else{ return }
            if(status){
                let responseVal = JSON(response as Any)
                if let responseModel = Mapper<VehicleArrayResponseModel>().map(JSONObject:responseVal.dictionaryObject){
                    if let checkUpModel = responseModel.checkupData{
                        strongSelf.updateIDForSignatures(modelVal: checkUpModel, vehicleModel: model)
                    }
                }
            }
        }
    }
    
    // MARK: - Update Signatures vehicle check Ids
    ///This function called to update the signature vehicleCheckupDataId in model
    ///
    ///     Warning modelVal and vehicleModel not be empty
    ///   - Parameter modelVal: list of CheckupData model
    ///   - Parameter vehicleModel:  VehicleHealthCheckModel model
    func updateIDForSignatures(modelVal: [CheckupData], vehicleModel: VehicleHealthCheckModel){
        var customerSignId: Int = 0
        var advisorSignId: Int = 0
        for i in 0..<modelVal.count{
            let name = modelVal[i].name
            let imagesDir = ImagesDirectory()
            if(name == StringConstants.ccSignature){
                customerSignId = modelVal[i].vehicleCheckupDataId ?? 0
                let assetName = vehicleModel.customerSign.lastPathComponent
                if let assetImage = imagesDir.getFileAt(filepath: vehicleModel.customerSign){
                    let assetBase64String = self.convertImageToBase64String(img: assetImage)
                    self.uploadSectionsAssetsTOAPI(checkDataId: customerSignId, assetName: assetName, assetbasae64: assetBase64String)
                }
            }
            else{
                advisorSignId = modelVal[i].vehicleCheckupDataId ?? 0
                let assetName = vehicleModel.serviceAdvisorSign.lastPathComponent
                if let assetImage = imagesDir.getFileAt(filepath: vehicleModel.serviceAdvisorSign){
                    let assetBase64String = self.convertImageToBase64String(img: assetImage)
                    self.uploadSectionsAssetsTOAPI(checkDataId: advisorSignId, assetName: assetName, assetbasae64: assetBase64String)
                }
            }
        }
//        self.markVehicleHealthCheckCompleted()
    }
    
    // MARK: - mark vehicle health check completed
    ///This function called to mark the vehicle health check completed
    ///
    ///     Warning vehicleCheckUpId not be empty
    func markVehicleHealthCheckCompleted(){
        let vehicleCheckUpId = self.getVehicleCheckUpId()
        let postData :[String: AnyObject] = ["Id": vehicleCheckUpId as Any]  as [String: AnyObject]
        
        FacadeLayer.sharedInstance.webserviceManager.genericAPICalls(requestParams: postData, apiPath: VFApiCalls.markVehicleHealthCheckComplete) { [weak self] (status, response) in
            guard let strongSelf = self else{ return }
            if(status){
            }
        }
        
    }
    // MARK: - Upload Marker Details to API
    ///This function called to update the marker details to API
    ///
    ///     Warning model and index should not be empty
    ///   - Parameter index: must be a integer
    ///   - Parameter model:  VehicleHealthCheckModel model
    func uploadMarkersDetailsToAPI(model: VehicleHealthCheckModel, index: Int){
        let vehicleCheckUpId = self.getVehicleCheckUpId()
        let imagesModel = model.images360Array
        var checkUpData: [[String: AnyObject]] =  [[String: AnyObject]]()
        
        for i in 0..<imagesModel.count{
            let markersJson = (imagesModel[i].markers)!
            let markersJsonString = self.convertModelToString(model: markersJson)
            let sectionModel :[String: AnyObject] = [
                "SectionName": model.sectionName as Any,
                "Name": imagesModel[i].angle as Any,
                "Value": markersJsonString as Any,
                "Remarks": "" as Any,
                "Status" : 0 as Any,
                "VehicleCheckupId": vehicleCheckUpId as Any] as [String:AnyObject]
            checkUpData.append(sectionModel)
            }
       
        let remarks  = [StringConstants.cleaningRemark, StringConstants.damageRemark, StringConstants.otherNotes]
        let values = [model.cleaningRemark, model.damageRemark, model.otherNotes]
       
        for j in 0..<remarks.count{
            let sectionModel :[String: AnyObject] = [
                "SectionName": model.sectionName as Any,
                "Name": remarks[j] as Any,
                "Value": values[j] as Any,
                "Remarks": "" as Any,
                "Status" : 0 as Any,
                "VehicleCheckupId": vehicleCheckUpId as Any] as [String:AnyObject]
            checkUpData.append(sectionModel)
        }
       
        let postData: [String: AnyObject] = ["VehicleCheckupId": vehicleCheckUpId as Any,
        "CheckupData": checkUpData as Any] as [String: AnyObject]
        print(postData)
        FacadeLayer.sharedInstance.webserviceManager.genericAPICalls(requestParams: postData, apiPath: VFApiCalls.createVehicleCheckupArray) { [weak self] (status, response) in
            guard let strongSelf = self else{ return }
            if(status){
                
            }
        }
        
    }
    // MARK: - Upload section assets to API
    ///This function called to upload the section assets to API
    ///
    ///     Warning checkDataId, assetName and assetbasae64 should not be empty
    ///   - Parameter checkDataId: must be a integer value
    ///   - Parameter assetName: must be a string value
    ///   - Parameter assetbasae64: must be a string value
    func uploadSectionsAssetsTOAPI(checkDataId: Int, assetName: String, assetbasae64:String){
        
        let postData: [String: AnyObject] = ["VehicleCheckupDataId": checkDataId as Any,
                                             "Asset": assetName as Any,
                                             "AssetBase64String": assetbasae64 as Any,
                                             "IsLastChunk": false as Any] as [String: AnyObject]
        print(assetName)
        FacadeLayer.sharedInstance.webserviceManager.genericAPICalls(requestParams: postData, apiPath: VFApiCalls.createVCDataAsset) { [weak self] (status, response) in
            guard let strongSelf = self else{ return }
            if(status){
                
            }
        }
    }

    
    // MARK: - Update section progress
    ///This function called to update the top section progress
    ///
    private func updateSectionProgress(){
        var indexCount = 1
        if chunkNumber != 0 {
            indexCount = (chunkNumber * 4) + 1
        }
        for subview in sectionStackView.subviews {
            if let vehicleHealthCheckHeaderview = subview as? VehicleHealthCheckHeaderview {
                if (indexCount <= totalSections) {
                    vehicleHealthCheckHeaderview.updateIndex(index: "\(indexCount)")
                } else {
                    vehicleHealthCheckHeaderview.updateIndex(index: "")
                }
                if indexCount < currentSection {
                    vehicleHealthCheckHeaderview.setViewColor(color: ColorConstants.appPrimaryColor)
                }
                else if indexCount > currentSection {
                    vehicleHealthCheckHeaderview.setViewColor(color: ColorConstants.underlineColor)
                }
                else if indexCount == currentSection {
                    vehicleHealthCheckHeaderview.setViewColor(color: ColorConstants.appSecondaryBlueColor)
                }
            }
            indexCount += 1
        }
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        
    }
    
    // MARK: - Vehicle health check table view controller
    func pushVehicleHealthCheckTableViewController(animated: Bool, inputModel: VehicleHealthCheckModel) {
        let controller = VehicleHealthCheckTableViewController.instantiate(fromAppStoryboard: .VehicleHealthCheckStoryboard)
        controller.modalPresentationStyle = .fullScreen
        controller.vehicleModel = inputModel
        pushViewController(controller, animated: animated)
    }
    
    // MARK: - Vehicle health check 360 view
    func pushVehicleHealthCheck360ViewController(animated: Bool, inputModel: VehicleHealthCheckModel) {
        let controller = VehicleHealth360ViewController.instantiate(fromAppStoryboard: .VehicleHealthCheckStoryboard)
        controller.modalPresentationStyle = .fullScreen
        controller.vehicleModel = inputModel
        pushViewController(controller, animated: animated)
        
    }
    
    // MARK: - Vehicle health check acknowlegde view
    
    func pushVehicleHealthCheckAcknowledgeViewController(animated: Bool, inputModel: VehicleHealthCheckModel) {
        let controller = AcknowledgeViewController.instantiate(fromAppStoryboard: .VehicleHealthCheckStoryboard)
        controller.modalPresentationStyle = .fullScreen
        controller.vehicleModel = inputModel
        pushViewController(controller, animated: animated)
    }
    
    // MARK: - Present Signature View controller
    func presentSignatureViewController(animated: Bool, delegate: ISignatureView, imageView: UIImage) {
        
        let controller = SignatureViewController.instantiate(fromAppStoryboard: .VehicleHealthCheckStoryboard)
        controller.modalPresentationStyle = .fullScreen
        controller.delegate = delegate
        present(controller, animated: true, completion: nil)
        
    }
    
    // MARK: - Push preview controller
    func pushPreviewViewController(animated: Bool, inputModel: [VehicleHealthCheckModel]) {
        FacadeLayer.sharedInstance.isFromHealthCheck = true
        FacadeLayer.sharedInstance.setHealthCheckModel(input: inputModel)
        self.dismiss(animated: true, completion: nil)
    }
    // MARK: - Present 360 landscape  View controller
    func presentLandScape360ViewController(animated: Bool, index: Int, inputModel: VehicleHealthCheckModel, delegate: Landscape360ViewDelagate, heightVal: CGFloat, widthVal: CGFloat){
        let controller = Landscape360ViewController.instantiate(fromAppStoryboard: .VehicleHealthCheckStoryboard)
        controller.modalPresentationStyle = .fullScreen
        controller.delegate = delegate
        controller.selectedIndex = index
        controller.vehicleModel = inputModel
        controller.imageHeight = heightVal
        controller.imageWidth = widthVal
        present(controller, animated: true, completion: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Convert Model to JSON String
    ///This function called to convert the model to JSON string
    ///
    ///     Warning model should not be empty
    ///   - Returns string value
    ///   - Parameter model: list of ImageMarkerModel
   
    func convertModelToString(model: [ImageMarkerModel]) -> String{

        if let jsonData = try? JSONEncoder().encode(model),
          let jsonString = String(data: jsonData, encoding: .utf8) {
            let convertedString = String(data: jsonData, encoding: .utf8)
          return convertedString ?? ""
        }
        return ""
}
    
}
