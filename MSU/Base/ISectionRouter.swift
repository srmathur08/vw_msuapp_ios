//
//  ISectionRouter.swift
//  MSU
//
//  Created by vectorform on 08/06/21.
//

import Foundation
import UIKit

protocol ISectionRouter: AnyObject {
    ///This function called start login navigation flow
    ///
    ///   - Parameter animated: true/false
    func navigateToLoginFlow(animate: Bool)
    
    ///This function called start home navigation flow
    ///
    ///   - Parameter animated: true/false
    func navigateToHomeFlow(animate: Bool)
    
    ///This function called start Vehicle health check navigation flow
    ///
    ///   - Parameter animated: true/false
    func navigateToVehicleHealthCheckFlow(animate: Bool, checkUpId: Int)
}
