//
//  BasePresenter.swift
//  MSU
//
//  Created by vectorform on 08/06/21.
//

import Foundation
import UIKit

class BasePresenter: IBasePresenter {
    var view: UIViewController?
    
    func connectToView(view: UIViewController) {
        self.view = view
    }
    
    var navController: UINavigationController? {
        return view?.navigationController
    }

    var sectionRouter: ISectionRouter? {
        return navController?.parent as? ISectionRouter
    }

}

