//
//  BaseNavigationController.swift
//  MSU
//
//  Created by vectorform on 08/06/21.
//

import Foundation
import UIKit

class BaseNavigationController: UINavigationController {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        isNavigationBarHidden = true

        // set nav title attributes

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

    }

    func setNavigationBar(color: UIColor) {
        navigationBar.barTintColor = color
    }

    func showNavigationBar(animated: Bool = true) {
        setNavigationBarHidden(false, animated: animated)
    }

    func hideNavigationBar(animated: Bool = true) {
        setNavigationBarHidden(true, animated: animated)
    }

    func setNavigationTitle(title: String) {
        self.navigationItem.title = title
    }

    func push(viewController: UIViewController, animated: Bool, replace: Bool = false) {
        if replace {
            CATransaction.begin()
            CATransaction.setCompletionBlock {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    viewController.navigationController?.viewControllers = [viewController]
                }
            }
            pushViewController(viewController, animated: animated)
            CATransaction.commit()
            return
        }

        pushViewController(viewController, animated: animated)
    }

    @objc func didSelectBack(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }

    @objc func didSelectClose(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }

    @objc func didSelectBack() {
        self.popViewController(animated: true)
    }

    @objc func didSelectClose() {
        dismiss(animated: true, completion: nil)
    }
    @objc func didSelectBackToRoot() {
        self.popToRootViewController(animated: true)
    }
}
