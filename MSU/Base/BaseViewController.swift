//
//  BaseViewController.swift
//  MSU
//
//  Created by vectorform on 14/06/21.
//

import Foundation
import UIKit

class BaseViewController: UIViewController, IBaseViewController{
   
    func displayAlert(title: String?, message: String?, actions: [UIAlertAction]) {
        let alertView: UIAlertController = UIAlertController(title: title,
                                                             message: message,
                                                             preferredStyle: .alert)
        for action: UIAlertAction in actions {
            alertView.addAction(action)
        }
        present(alertView, animated: true, completion: nil)
    }
    func displayDefaultAlertForError(errorTitle: String, errorDescription: String) {
        let okAction: UIAlertAction = UIAlertAction(title: "Ok", style: .default) { (_)
            in { [weak self] in self?.dismiss(animated: true, completion: nil)
            }()
        }
        displayAlert(title: errorTitle, message: errorDescription, actions: [okAction])
    }
    
    //MARK: - For showing loader
    func showLoader() {
        let blurLoader = LoaderView(frame: self.view.frame)
        self.view.addSubview(blurLoader)
        self.view.bringSubviewToFront(blurLoader)
    }

    //MARK: - For removing loader
     func removeLoader() {
        if let blurLoader = self.view.subviews.first(where: { $0 is LoaderView }) {
            blurLoader.removeFromSuperview()
        }
    }
    


}

