//
//  SectionRouter.swift
//  MSU
//
//  Created by vectorform on 08/06/21.
//

import Foundation
import UIKit

class SectionRouter: UIViewController, ISectionRouter {

    var currentController: UIViewController?
    // MARK: - Login Router
    func navigateToLoginFlow(animate: Bool = true) {
        let navController = LoginRouter()
        switchTo(navController: navController)
    }
    // MARK: - Home Router
    func navigateToHomeFlow(animate: Bool) {
        let navController = HomeRouter()
        switchTo(navController: navController)
    }
    
    // MARK: - Vehicle Health Check Router
    func navigateToVehicleHealthCheckFlow(animate: Bool, checkUpId: Int) {
        let navController = VehicleHealthCheckRouter()
        navController.modalPresentationStyle = .fullScreen
        navController.vehicleCheckUpId = checkUpId
        self.present(navController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigateToLoginFlow(animate: false)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    private func switchTo(navController: UIViewController, data: [String: Any]? = nil) {
        navController.willMove(toParent: self)

        self.addChild(navController)
        self.view.insertSubview(navController.view, at: 0)

        navController.view.frame = self.view.frame
        navController.didMove(toParent: self)

        if let currentNavController = self.currentController {
            currentNavController.willMove(toParent: nil)
            UIView.transition(from: currentNavController.view,
                              to: navController.view,
                              duration: 0.15,
                              options: [.transitionCrossDissolve, .allowAnimatedContent],
                              completion: { (_) in

                                currentNavController.view.removeFromSuperview()
                                currentNavController.removeFromParent()
            })
        }

        self.currentController = navController
    }
    
}
