//
//  IBasePresenter.swift
//  MSU
//
//  Created by vectorform on 08/06/21.
//

import Foundation
import UIKit

public protocol IBasePresenter: AnyObject {

    var view: UIViewController? { get }

    func connectToView(view: UIViewController)
}

