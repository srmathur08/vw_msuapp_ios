//
//  IBaseViewController.swift
//  MSU
//
//  Created by vectorform on 14/06/21.
//

import Foundation
import UIKit

protocol IBaseViewController: UIViewController {
    
    func displayAlert(title: String?, message: String?, actions: [UIAlertAction])
    func displayDefaultAlertForError(errorTitle: String, errorDescription: String)
    func showLoader()
    func removeLoader()
}
