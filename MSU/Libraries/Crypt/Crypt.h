//
//  Crypt.h
//  Skoda
//
//  Created by Vectorform on 4/3/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Crypt: NSObject {
    
}

void *base64Decode(const char *inputBuffer, size_t length, size_t *outputLength);
char *base64Encode(const void *inputBuffer, size_t length, bool separateLines, size_t *outputLength);

NSData *dataFromBase64String(NSString *aString);
NSString *base64EncodedString(NSData* data);

+ (NSString*)encrypt:(NSString*)plainText withKey:(NSString*)key;
+ (NSString*)decrypt:(NSString*)cipherText withKey:(NSString*)key;

@end
