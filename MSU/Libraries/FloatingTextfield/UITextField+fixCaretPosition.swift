//
//  UITextField+fixCaretPosition.swift
//  MSU
//
//  Created by vectorform on 09/06/21.
//

// MARK: - UITextField extension

import UIKit

extension UITextField {
    func fixCaretPosition() {
        // Moving the caret to the correct position by removing the trailing whitespace

        let beginning = beginningOfDocument
        selectedTextRange = textRange(from: beginning, to: beginning)
        let end = endOfDocument
        selectedTextRange = textRange(from: end, to: end)
    }
}
