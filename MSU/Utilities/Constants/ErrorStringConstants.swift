//
//  ErrorStringConstants.swift
//  MSU
//
//  Created by vectorform on 08/06/21.
//

import Foundation

struct ErrorStringConstants{
    static let noInternet = "No Internet"
    static let errorTitle = "Failure"
    static let successTitle = "Success"
    static let mobileNumberError = "Please enter valid mobile number"
    static let passwordError = "Please enter password"
    static let otpError = "Please enter valid OTP"
    static let vcSubSectionError = "All fields are mandatory"
    static let vcSubMenuError = "Please mark the checkpoints in subsections"
    static let vcSignatureError = "Signatures are mandatory"
    static let inProgressError = "No in-progress service"
    static let defaultTitle = "Alert"
    static let skipForVHCMessage = "Are you sure want to skip?"
    static let authorizeError = "You dont have permission for login. Please contact admin."
    static let versionUpdate = "A new version of app is available.Please update to continue."
    static let defaultError = "An error occurred, please try again"
    static let sendSHR = "Service History sent successfully"
    static let sendVHC = "Vehicle Health Check sent successfully"
    static let mileageError = "Mileage cannot be less than"
    static let subSectionError = "Please mark the checkpoints in subsections"
}

