//
//  ColorConstants.swift
//  MSU
//
//  Created by vectorform on 08/06/21.
//

import Foundation
import UIKit

struct ColorConstants{
    static let clear = UIColor.clear
    static let solidBlack = UIColor.black
    static let solidWhite = UIColor.white
    
    static let appPrimaryColor = UIColor(red: 0.0/255.0, green: 30.0/255.0, blue: 80.0/255.0, alpha: 1.0)
    static let appSecondaryBlueColor = UIColor(red: 0.0/255.0, green: 176.0/255.0, blue: 240.0/255.0, alpha: 1.0)
    static let underlineColor = UIColor(red: 223.0/255.0, green: 228.0/255.0, blue: 232.0/255.0, alpha: 1.0)
    static let appGreyColor =  UIColor(red: 194.0/255.0, green: 202.0/255.0, blue: 207.0/255.0, alpha: 1.0)
    static let appLabelsTextColor =  UIColor(red: 106.0/255.0, green: 118.0/255.0, blue: 125.0/255.0, alpha: 1.0)
    static let appButtonDisableColor =  UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.25)
    static let appYellowColor =  UIColor(red: 255.0/255.0, green: 209.0/255.0, blue: 48.0/255.0, alpha: 1.0)
    static let appPrimaryShadowColor = UIColor(red: 0.0/255.0, green: 30.0/255.0, blue: 80.0/255.0, alpha: 0.15)
    static let appGradient1 = UIColor(red: 55.0/255.0, green: 79.0/255.0, blue: 116.0/255.0, alpha: 1.0)
    static let appGradient2 = UIColor(red: 0.0/255.0, green: 30.0/255.0, blue: 80.0/255.0, alpha: 1.0)
    static let appPrimaryColorWithAlpha = UIColor(red: 242.0/255.0, green: 242.0/255.0, blue: 246.0/255.0, alpha: 1.0)
    struct Gradient {
        static let appGradientColor: [CGColor] = [appGradient1.cgColor, appGradient2.cgColor]
    }
    
    static let yellowMarkColor = UIColor(red: 255.0/255.0, green: 209.0/255.0, blue: 48.0/255.0, alpha: 1.0)
    
    static let redMarkColor = UIColor(red: 255.0/255.0, green: 51.0/255.0, blue: 92.0/255.0, alpha: 1.0)
    static let greenMarkColor = UIColor(red: 0.0/255.0, green: 201.0/255.0, blue: 113.0/255.0, alpha: 1.0)
    
    static let greenMarkColorWithOpacity = UIColor(red: 0.0/255.0, green: 201.0/255.0, blue: 113.0/255.0, alpha: 0.05)
    static let redMarkColorWithOpacity = UIColor(red: 255.0/255.0, green: 51.0/255.0, blue: 92.0/255.0, alpha: 0.05)
    static let yellowMarkColorWithOpacity = UIColor(red: 255.0/255.0, green: 209.0/255.0, blue: 48.0/255.0, alpha: 0.05)
}
