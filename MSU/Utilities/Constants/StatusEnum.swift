//
//  StatusEnum.swift
//  MSU
//
//  Created by vectorform on 23/08/21.
//

import Foundation
import UIKit

enum StatusEnum:Int {
    case Red = 1
    case Yellow = 2
    case Green = 3
    
}
