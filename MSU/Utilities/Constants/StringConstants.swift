//
//  StringConstants.swift
//  MSU
//
//  Created by vectorform on 08/06/21.
//

import Foundation

struct StringConstants{
    static let fontHead = "HEAD"
    static let fontText = "TEXT"
    static let vehicleHealthRedStatus = "Red"
    static let vehicleHealthYellowStatus = "Yellow"
    static let vehicleHealthGreenStatus = "Green"
    static let vehicleHealthCheckType1 = "Type1"
    static let vehicleHealthCheckType2 = "Type2"
    static let vehicleHealthCheckType3 = "Type3"
    static let vehicleHealthCheckType4 = "Type4"
    static let vehicleHealthCheckType5 = "Type5"
    static let vehicleHealthCheckType6 = "Type6"
    static let vehicleHealthCheckType7 = "Type7"
    static let damageMarker = "Dent"
    static let repairMarker = "Scratch"
    static let scratch = "Scratch/Dent/Damage"
    static let acknowledge = "Acknowledge"
    static let saSignature = "SA Signature"
    static let ccSignature = "Customer Signature"
    static let cleaningRemark = "Cleaning Remark"
    static let damageRemark = "Damage Remark"
    static let otherNotes = "Other Offers & Notes"
    static let vehicleImagesText = "Vehicle Images"
    
    static let getOTP = "Get OTP"
    static let login = "Login"
    static let noDataText = "No Data"
    static let iOSVersion = "IOsVersion"
    static let iTunesLink = "ITunesLink"
    static let assigned = "Assigned"
    static let completed = "Completed"
    static let missed = "Missed"
    static let amount = "Amount"
    static let today = "TODAY"
    static let mtd = "MTD"
    
    static let JWTSecretkey = "ERMN05OPLoDvbTTa/QkqLNMI7cPLguaRyHzyg7n5qNBVjQmtBhz4SzBh4NBVCXi3KJHlSXKP+oi2+bXr6CUBTR=="
    static let JWTIssuerKey = "B2lzB29zcGFyazovL3VzL09SR0FOSVpBVElPTi85BmM0Zjk1Ni1mBzliLTQwNjQtBjI1Ny05OWMwNjhlB2ZiNjB"
    static let encryptionKey = "gMO1ExV3aJz5t6dA0jFpIsJhZ5DiwK48"
//    static let GoogleAPIKEY = "AIzaSyD0TaLpYEY-EMYimGg6wkkhsv3cEuu5Yy4"
//    static let GoogleAPIKEY = "AIzaSyDq0XAhYrYbou0qpOubckWv3mSIzq1CPbw" //JEEP
//    static let GoogleAPIKEY = "AIzaSyCLl40SjIzoO4WGVAGT78G7n2egz0Z5Slg" // MSU
    static let GoogleAPIKEY = "AIzaSyD0TaLpYEY-EMYimGg6wkkhsv3cEuu5Yy4"  // Ravi provided
    
}


struct VFApiCalls{
    
    static let login = "api/User/Login" // used for login
    static let getServiceRequests = "api/SR/SRsByDate" // used for getting SR with input date
    static let createVehicleCheckup = "api/SR/CreateVC" // used for creation of vehicle checkup
    static let createVehicleCheckupAsset = "api/SR/CreateVCAsset" // used for uploading assets in vehicle checkup
    static let createVehicleCheckupData = "api/SR/CreateVehicleCheckupData" // used for creating vehicle checkupdata
    static let appendAsset = "api/SR/AppendAsset" // used to upload asset in chunks for vehicle checkup
    static let getVehicleCheckupData = "api/SR/GetVCData"  // used to get the vehicle checkup data with input SR id
    static let createVehicleCheckupArray = "api/SR/CreateVCDataArray"  // used for creating vehicle checkupdata with Array
    static let createVCDataAsset = "api/SR/CreateVCDataAsset"  // used to upload assets sub section wise
    static let startVehicleHealthCheck = "api/SR/StartSR" // used to start the vehicle health check
    static let completeVehicleHealthCheck = "api/SR/CompleteSR" // used to complete the vehicle health check
    static let markVehicleHealthCheckComplete = "api/SR/CompleteVC" // used to mark vehicle health check complete
    static let updateLocation = "api/user/updatelocation" // updating current location
    
    static let updateSRStatus = "api/sr/UpdateSRStatus" // Update SR status
    
    static let updateServiceDetailSR = "api/sr/UpdateServiceDetailSR" // Update Service Details
    
    static let updateUserProfilePic = "api/user/UpdateProfilePic"  // update profile pic
    
    static let getSettings = "api/user/APISetting" // get settings data
    
    static let getNotifications = "api/user/Notifications" // get notifications list
    
    static let updateNotifications = "api/user/NotificationRead" // update notification
    
    static let getDashboardCount = "api/sr/ServiceRequestCount" // Get Dashboard Count
    
    static let getOTP = "api/user/GetOtp" // To ge the OTP
    
    static let sendServiceHistory = "api/user/SendSRHistorySMS" // To send Send service History
    
    static let sendVehicleHistory = "api/user/SendVHCSMS"  // To send VHC
}

struct SFDCCalls {
    static let grant_type = "password"
    static let client_id = "3MVG910YPh8zrcR1yVy5MQ5lXnYSpTr26OIVaCqz8qxqdo4CxvWGM2j5li8TWncRJQVHWcYjp8jJcrtKB0hAj"
    static let client_secret = "4000850827053520041"
    static let username = "integrationuser@nscindia.com.vwuat"
    static let password = "July@2019"
    
    static let baseRequest = "https://test.salesforce.com/services/oauth2"
    static let token = "/token"
    static let getServiceHistory = "/services/apexrest/vindata/VINNUMBER"
}
struct IntConstants {
    static let redValue = 0
    static let yellowValue = 1
    static let greenValue = 2
}


struct UpdateStatus{
    static let isServiceEngOutforRepair = 2
    static let isStarted = 3
    static let isCompleted = 4
    static let isVehicleHandedOver = 5
    
}


struct ServiceComplaints{
    static let oilChange = "Oil and Filter Replacement"
    static let airFilter = "Air Filter Replacement"
    static let brakes = "Brake Pad Replacement"
    static let brakeDisc = "Brake Disk Replacement"
    static let suspensionStrut = "Suspension Strut Replacement"
    static let shockAbsorber = "Shock Absorber Replacement"
    static let controlAlarm = "Control Arm Replacement"
    static let batteryReplace = "Battery Replacement"
    static let blubReplace = "Bulb / Fuse Replacement"
    static let hornReplace = "Horn Replacement"
    static let tireCheck = "Tire Repair"
    static let tireReplace = "Tire Replacement"
    
}

struct ServiceTypes{
    static let maintenance = "Maintenance"
    static let inspectionService = "Inspection Service"
}


struct Roles{
    static let serviceTechnician = "Service Technicians"
    static let serviceAdvisor = "Service Advisors"
}
