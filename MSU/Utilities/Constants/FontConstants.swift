//
//  FontConstants.swift
//  VFViperFramework
//
//  Created by vectorform on 08/06/21.
//

import Foundation
import Texty

enum FontWeight {
    case vwheadbold
    case vwheadregular
    case vwtextbold
    case vwtextregular
    
    var weight: UIFont.Weight {
        switch self {
        case .vwheadbold: return .bold
        case .vwheadregular: return .regular
        case .vwtextbold: return .bold
        case .vwtextregular: return .regular
        }
    }

    var identifier: String {
        switch self {
        case .vwheadbold: return "-Bold"
        case .vwheadregular: return ""
        case .vwtextbold: return "-Bold"
        case .vwtextregular: return ""
        }
    }
}

enum Font {
    case vwheadfont
    case vwtextfont

    var kern: CGFloat {
        switch self {
        case .vwheadfont: return 0.0
        case .vwtextfont: return 0.0
        }
    }

    func name(with weight: FontWeight) -> String {
        
        switch self {
        case .vwheadfont:
            return "VWHead" + weight.identifier
        case .vwtextfont:
            return "VWText" + weight.identifier
        }
    }

    func with(size: CGFloat, weight: FontWeight) -> UIFont {
        return UIFont(name: name(with: weight), size: size) ?? UIFont.systemFont(ofSize: size, weight: weight.weight)
    }

    func textStyle(withSize size: CGFloat, weight: FontWeight, color: UIColor, kern: CGFloat? = nil) -> TextStyle {
        let style = TextStyle(attributes: [.font: self.with(size: size, weight: weight), .foregroundColor: color, .kern: kern ?? self.kern])
        return style
    }
}
