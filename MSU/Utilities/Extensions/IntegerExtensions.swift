//
//  IntegerExtensions.swift
//  MSU
//
//  Created by vectorform on 08/06/21.
//

import Foundation
import UIKit

extension Int{
    
    //MARK: - Convert Int to days and hours
    func convertIntToDaysHours(intVal: Int) -> String
    {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.day, .hour]
        formatter.unitsStyle = .full

        let formattedString = formatter.string(from: TimeInterval(intVal))!
        print(formattedString)
        return formattedString
    }
    
    //MARK: - Convert Int to hours and minutes
    func convertIntToHoursMinutes(intVal: Int) -> String
    {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute]
        formatter.unitsStyle = .full

        let formattedString = formatter.string(from: TimeInterval(intVal))!
        print(formattedString)
        return formattedString
    }
    
    //MARK: - Currency formatter
    func currenyFormatter(inputVal: Int) -> String
    {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "en_IN")
        formatter.numberStyle = .currency
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        if inputVal != 0{
            let stringVal = formatter.string(from: NSNumber(value: inputVal))!
            var finalStr = String(describing: stringVal).removeOptionalFromString()
            finalStr = finalStr.replacingOccurrences(of: "\"", with: "")
                return finalStr
        }
        else{
            return "0"
        }
    }
    
    //MARK: - Double extensiom
    func formatPoints(num: Double) ->String{
        let thousandNum = num/1000
        let millionNum = num/1000000
        if num >= 1000 && num < 1000000{
            return("\(thousandNum.rounded(toPlaces: 2)) K")
        }
        if num > 1000000{
            return ("\(millionNum.rounded(toPlaces:2)) M")
        }
        else{
            if(floor(num) == num){
                return ("\(Int(num))")
            }
            return ("\(num)")
        }

    }
}

//MARK: - Double extensiom

extension Double {
    //MARK: - Rounded values
    func rounded(toPlaces places:Int) -> Double
    {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    

}

