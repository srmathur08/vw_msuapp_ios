//
//  StringExtensions.swift
//  MSU
//
//  Created by vectorform on 08/06/21.
//

import Foundation
import UIKit


extension String
{
    //MARK: - Filtering special characters
    var filterSpecialChar: String
    {

         let okayChars = Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890")
         return self.filter {okayChars.contains($0) }

    }
    
    //MARK: - Removing optional from string
    func removeOptionalFromString() -> String
    {
        var actualString = self.replacingOccurrences(of: "Optional(", with:  "", options: .literal, range: nil)
        actualString = actualString.replacingOccurrences(of: ")", with:  "", options: .literal, range: nil)
        return actualString
    }
    
    //MARK: - Generate Image from URL
    mutating func generateImageForURL() -> UIImage
    {
        self = self.replacingOccurrences(of: "\"", with:  "", options: .literal, range: nil)
        let url = URL(string: self)!
        let data = try? Data(contentsOf: url)
        var image = UIImage()
        
        if let imageData = data {
            image = UIImage(data: imageData) ?? image
        }
        return image
    }
    
    //MARK: - Get String from date
    func getFormattedStringFromDate(oldFormat: String , newFormat: String , Date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = oldFormat
        let olddate = dateFormatter.date(from: self)
        
        if let olddate = olddate {
            let newdateFormatter = DateFormatter()
            newdateFormatter.dateFormat = newFormat
            let mydate = dateFormatter.string(from: olddate)
            return mydate
        }
        let date = Date.split(separator: "T", maxSplits: 1, omittingEmptySubsequences: true)
        let onlydate : String = String(date[0])
        if (onlydate != nil ) {
            let newdateFormatter = DateFormatter()
            newdateFormatter.dateFormat = "yyyy-MM-dd"
            let olddate = newdateFormatter.date(from: onlydate)
            if let old = olddate {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = newFormat
                var updatedDate = dateFormatter.string(from: old)
                
                return updatedDate
            }
        }
        return "";
    }
    
    //MARK: - Get formatted date with time
    func getFormatDateWithTime(oldFormat: String , newFormat: String , Date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = oldFormat
        let olddate = dateFormatter.date(from: self)
        
        if let olddate = olddate {
            let newdateFormatter = DateFormatter()
            newdateFormatter.dateFormat = newFormat
            let mydate = dateFormatter.string(from: olddate)
            return mydate
        }
        
        let date = Date.split(separator: "T", maxSplits: 1, omittingEmptySubsequences: true)
        
        let onlydate : String = String(date[0])
        var onlyTime : String = String(date[1])
        let onlyTimeVal = onlyTime.split(separator: ".", maxSplits: 1, omittingEmptySubsequences: true)
        onlyTime = String(onlyTimeVal[0])
        if (onlydate != nil ) {
            let newdateFormatter = DateFormatter()
            newdateFormatter.dateFormat = "yyyy-MM-dd"
            let olddate = newdateFormatter.date(from: onlydate)
            if let old = olddate {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = newFormat
                var updatedDate = dateFormatter.string(from: old)
                updatedDate = updatedDate + " " + onlyTime
                return updatedDate
            }
        }
        return "";
    }
    
    //MARK: - Convert String to Date
    func convertStringToDate() -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        let date = dateFormatter.date(from: self)
        return date ?? Date()
    }
    
    //MARK: - Convert String to Date time
    func convertStringToDateTime() -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        let date = dateFormatter.date(from: self)
        return date ?? Date()
    }

    func convertDateStringToFormats(input: String) -> String{
        var dateStr: String = ""
         let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        if let date = dateFormatterGet.date(from: input) {
            dateStr =  dateFormatterGet.string(from: date )
        }
        let dateFormatterShow = DateFormatter()
            dateFormatterShow.dateStyle = .medium
            dateFormatterShow.timeStyle = .none
            dateFormatterShow.dateFormat = "dd MMM yyyy | hh:mm a"
        var returnDate: String = ""
        if let date = dateFormatterGet.date(from: dateStr) {
                returnDate = dateFormatterShow.string(from: date)
            }
        return returnDate
    }
    
    func convertDateStringToFormatsNotifications(input: String) -> String{
        var dateStr: String = ""
         let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd"
        if let date = dateFormatterGet.date(from: input) {
            dateStr =  dateFormatterGet.string(from: date )
        }
        let dateFormatterShow = DateFormatter()
            dateFormatterShow.dateStyle = .medium
            dateFormatterShow.timeStyle = .none
            dateFormatterShow.dateFormat = "dd-MMM-yyyy"
        var returnDate: String = ""
        if let date = dateFormatterGet.date(from: dateStr) {
                returnDate = dateFormatterShow.string(from: date)
            }
        return returnDate
    }
    func convertDateOnGoingServiceDetails(input: String) -> String{
        var dateStr: String = ""
         let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd"
        if let date = dateFormatterGet.date(from: input) {
            dateStr =  dateFormatterGet.string(from: date )
        }
        
        let dateFormatterShow = DateFormatter()
            dateFormatterShow.dateStyle = .medium
            dateFormatterShow.timeStyle = .none
            dateFormatterShow.dateFormat = "d MMMM yyyy"
        var returnDate: String = ""
        if let date = dateFormatterGet.date(from: dateStr) {
                returnDate = dateFormatterShow.string(from: date)
            }
        return returnDate
    }
    func convert24hoursTimeToTwelveHoursServiceDetails() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"

        let date = dateFormatter.date(from: self) ?? Date()
        dateFormatter.dateFormat = "h:mm a"
        let Date12 = dateFormatter.string(from: date)
        return Date12
    }
    
    
    //MARK: - delete string last component
    var stringByDeletingLastPathComponent: String
    {
        get {
            return (self as NSString).deletingLastPathComponent
        }
    }
    
    //MARK: - Get File extension
    func fileExtension() -> String
    {
        if let fileExtension = NSURL(fileURLWithPath: self).pathExtension {
            return fileExtension
        } else {
            return ""
        }
    }
    //MARK: - Parse JSON String
    func parseJSONString() -> NSDictionary
    {
        let blankDict : NSDictionary = [:]
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            } catch {
            }
        }
        return blankDict
    }
    
    //MARK: - Get date and month
   func convertDateStringToFormatDate(input: String) -> String
   {
        var dateStr: String = ""
         let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "dd-MM-yyyy HH:mm"
        if let date = dateFormatterGet.date(from: input) {
            dateStr =  dateFormatterGet.string(from: date )
        }
        let dateFormatterShow = DateFormatter()
            dateFormatterShow.dateStyle = .medium
            dateFormatterShow.timeStyle = .none
            dateFormatterShow.dateFormat = "d MMMM"
        var returnDate: String = ""
        if let date = dateFormatterGet.date(from: dateStr) {
                returnDate = dateFormatterShow.string(from: date)
            }
        return returnDate
    }
    
    //MARK: - Convert date with input string and format
    func convertDateStringwith(inputString: String , inputformat: String, expectedformat: String) -> String
    {
         var dateStr: String = ""
          let dateFormatterGet = DateFormatter()
             dateFormatterGet.dateFormat = inputformat
         if let date = dateFormatterGet.date(from: inputString) {
             dateStr =  dateFormatterGet.string(from: date )
         }
         let dateFormatterShow = DateFormatter()
             dateFormatterShow.dateStyle = .medium
             dateFormatterShow.timeStyle = .none
             dateFormatterShow.dateFormat = expectedformat
         var returnDate: String = ""
         if let date = dateFormatterGet.date(from: dateStr) {
                 returnDate = dateFormatterShow.string(from: date)
             }
         return returnDate
     }
    
    //MARK: - Convert date string to date
    func convertDateStringToFormatDateWithoutTime(input: String) -> String
    {
         var dateStr: String = ""
          let dateFormatterGet = DateFormatter()
             dateFormatterGet.dateFormat = "dd/MM/yyyy"
         if let date = dateFormatterGet.date(from: input) {
             dateStr =  dateFormatterGet.string(from: date )
         }
         let dateFormatterShow = DateFormatter()
             dateFormatterShow.dateStyle = .medium
             dateFormatterShow.timeStyle = .none
             dateFormatterShow.dateFormat = "d MMM yyyy"
         var returnDate: String = ""
         if let date = dateFormatterGet.date(from: dateStr) {
                 returnDate = dateFormatterShow.string(from: date)
             }
         return returnDate
     }
    
    //MARK: - Convet date to date and time
    func convertDateStringToFormatDateWithTime(input: String) -> String
    {
         var dateStr: String = ""
          let dateFormatterGet = DateFormatter()
             dateFormatterGet.dateFormat = "dd/MM/yyyy HH:mm"
         if let date = dateFormatterGet.date(from: input) {
             dateStr =  dateFormatterGet.string(from: date )
         }
         let dateFormatterShow = DateFormatter()
             dateFormatterShow.dateStyle = .medium
             dateFormatterShow.timeStyle = .none
             dateFormatterShow.dateFormat = "d MMM yyyy"
         var returnDate: String = ""
         if let date = dateFormatterGet.date(from: dateStr) {
                 returnDate = dateFormatterShow.string(from: date)
             }
         return returnDate
     }
    
    //MARK: - Convert datet to date and time
    func convertDateStringToFormatDateTime(input: String) -> String
    {
        var dateStr: String = ""
         let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "dd-MM-yyyy HH:mm"
        if let date = dateFormatterGet.date(from: input) {
            dateStr =  dateFormatterGet.string(from: date )
        }
        
               
        let dateFormatterShow = DateFormatter()
            dateFormatterShow.dateStyle = .medium
            dateFormatterShow.timeStyle = .none
            dateFormatterShow.dateFormat = "d MMMM yyyy | hh:mm a"
        var returnDate: String = ""
        if let date = dateFormatterGet.date(from: dateStr) {
                returnDate = dateFormatterShow.string(from: date)
            }
        return returnDate
    }
    
    //MARK: - Convert 24 hrs format to 12 hrs
    func convert24hoursTimeToTwelveHours() -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"

        let date = dateFormatter.date(from: self) ?? Date()
        dateFormatter.dateFormat = "h:mm a"
        let Date12 = dateFormatter.string(from: date)
        return Date12
    }
    
    //MARK: - Get date from milli seconds
    func dateFromMilliseconds(millisec: Int64 , format:String) -> String
    {
        let date : NSDate! = NSDate(timeIntervalSince1970:TimeInterval(millisec) / 1000.0)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone.current
        let timeStamp = dateFormatter.string(from: date as Date)

        return timeStamp
    }
    
    //MARK: - make first letter capital
    func capitalizingFirstLetter() -> String
    {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    //MARK: - Make first letter small
    func lowercaseFirstLetter() -> String {
      return prefix(1).lowercased() + self.lowercased().dropFirst()
    }

    mutating func lowercaseFirstLetter() {
      self = self.lowercaseFirstLetter()
    }
       
    
    var fileURL: URL {
        return URL(fileURLWithPath: self)
    }
    var pathExtension: String {
        return fileURL.pathExtension
    }
    var lastPathComponent: String {
        return fileURL.lastPathComponent
    }
    
}
