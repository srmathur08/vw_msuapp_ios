//
//  UIViewControllerExtensions.swift
//  MSU
//
//  Created by vectorform on 08/06/21.
//

import Foundation
import UIKit

extension UIViewController{
    //MARK: - Show default alert
    func showAlert(WithTitle title:String, message:String)
    {
        DispatchQueue.main.async
            {
                let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default) { (action) -> Void in
                    alert.dismiss(animated: true, completion: nil)
                }
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: - Password Validations
    func isValidPassword(_ password: String) -> Bool
    {
        
      let passwordreg =  ("(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z])(?=.*[@#$%^&*!]).{6,}")
      let passwordtesting = NSPredicate(format: "SELF MATCHES %@", passwordreg)
        return passwordtesting.evaluate(with: password)

    }
    
    //MARK: - Convert String to dictionary
    func convertStringToDictionary(text: String) -> [String:AnyObject]?
    {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    func convertImageToBase64String (img: UIImage) -> String {
        return img.jpegData(compressionQuality: 0.8)?.base64EncodedString() ?? ""
    }
    var topbarHeight: CGFloat {
        return 170.0
        }

    struct SF {
 
        ///  Returns screen width
        public static var screenWidth: CGFloat {
            return UIScreen.main.bounds.size.width

        }
        
        ///  Returns screen height
        public static var screenHeight: CGFloat {
            return UIScreen.main.bounds.size.height - 450
        }
    }
}


extension UISlider {

    func setThumbValueWithLabel() -> CGPoint {
    
        let sliderTrack : CGRect = self.trackRect(forBounds: self.bounds)
        let sliderFrm : CGRect = self.thumbRect(forBounds: self.bounds, trackRect: sliderTrack, value: self.value)
        return CGPoint(x: sliderFrm.origin.x + self.frame.origin.x + 40, y: self.frame.origin.y - 25)
    }
}

@IBDesignable
class FuelSlider: UISlider {

 var sliderTrackHeight : CGFloat = 100

    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        let originalRect = super.trackRect(forBounds: bounds)
        return CGRect(origin: CGPoint(x: originalRect.origin.x, y: originalRect.origin.y - (sliderTrackHeight / 2)), size: CGSize(width: bounds.width, height: sliderTrackHeight))
    }
}
