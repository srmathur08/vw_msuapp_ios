//
//  UIViewExtension.swift
//  MSU
//
//  Created by vectorform on 08/06/21.
//

import Foundation
import UIKit

extension UIView {
    
    //MARK: - View rounded corners
    func roundCorners(corners: UIRectCorner, radius: CGFloat)
    {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    func addSubviews(views: [UIView], parent: UIView? = nil) {
        let viewToAddTo: UIView = parent ?? self
        for subView: UIView in views {
            viewToAddTo.addSubview(subView)
        }
    }
    
    //MARK: - Add border to image view
    func imageViewBorder()
    {
        self.layer.borderColor = ColorConstants.underlineColor.cgColor
        self.backgroundColor = ColorConstants.solidWhite
        self.layer.borderWidth = 1.0
    }
    

    // Export pdf from Save pdf in drectory and return pdf file path
    func exportAsPdfFromView() -> String
    {
        let pdfPageFrame = self.bounds
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageFrame, nil)
        UIGraphicsBeginPDFPageWithInfo(pdfPageFrame, nil)
        guard let pdfContext = UIGraphicsGetCurrentContext() else { return "" }
        self.layer.render(in: pdfContext)
        UIGraphicsEndPDFContext()
        return self.saveViewPdf(data: pdfData)

    }

    // Save pdf file in document directory
    func saveViewPdf(data: NSMutableData) -> String {
      let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
      let docDirectoryPath = paths[0]
      let pdfPath = docDirectoryPath.appendingPathComponent("VehicleHealthCheck.pdf")
      if data.write(to: pdfPath, atomically: true) {
          return pdfPath.path
      } else {
          return ""
      }
    }
    
    // sourcePdfFiles is array of source file full paths, destPdfFile is dest file full path
    func mergePdfFiles(sourcePdfFiles:[String], destPdfFile:String) {
        guard UIGraphicsBeginPDFContextToFile(destPdfFile, CGRect.zero, nil) else {
            return
        }
        guard let destContext = UIGraphicsGetCurrentContext() else {
            return
        }

        for index in 0 ..< sourcePdfFiles.count {
            let pdfFile = sourcePdfFiles[index]
            let pdfUrl = NSURL(fileURLWithPath: pdfFile)
            guard let pdfRef = CGPDFDocument(pdfUrl) else {
                continue
            }

            for i in 1 ... pdfRef.numberOfPages {
                if let page = pdfRef.page(at: i) {
                    var mediaBox = page.getBoxRect(.mediaBox)
                    destContext.beginPage(mediaBox: &mediaBox)
                    destContext.drawPDFPage(page)
                    destContext.endPage()
                }
            }
        }

        destContext.closePDF()
        UIGraphicsEndPDFContext()
    }
}


struct AppUtility {
    
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = orientation
        }
    }
    
    /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
        self.lockOrientation(orientation)
        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
    }
    
}


extension UIImageView {
  func setImageColor(color: UIColor) {
    let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
    self.image = templateImage
    self.tintColor = color
  }
}

extension UIPanGestureRecognizer {

    enum GestureDirection {
        case Up
        case Down
        case Left
        case Right
    }

    /// Get current vertical direction
    ///
    /// - Parameter target: view target
    /// - Returns: current direction
    func verticalDirection(target: UIView) -> GestureDirection {
        return self.velocity(in: target).y > 0 ? .Down : .Up
    }

    /// Get current horizontal direction
    ///
    /// - Parameter target: view target
    /// - Returns: current direction
    func horizontalDirection(target: UIView) -> GestureDirection {
        return self.velocity(in: target).x > 0 ? .Right : .Left
    }

    /// Get a tuple for current horizontal/vertical direction
    ///
    /// - Parameter target: view target
    /// - Returns: current direction
    func versus(target: UIView) -> (horizontal: GestureDirection, vertical: GestureDirection) {
        return (self.horizontalDirection(target: target), self.verticalDirection(target: target))
    }

}
