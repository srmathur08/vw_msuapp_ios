//
//  DateExtensions.swift
//  MSU
//
//  Created by vectorform on 08/06/21.
//

import Foundation
import UIKit

extension Date {
    
    //MARK: - Get number of daya
    func daysBetweenDate(toDate: Date) -> Int
    {
        let components = Calendar.current.dateComponents([.day], from: self, to: toDate)
        return components.day ?? 0
    }
    
    //MARK: - Get start of week
    var startOfWeekVal: Date
    {
        let date = Calendar.current.date(from: Calendar.current.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))!
        return date.nextDate(difference:2)!
    }
    
    //MARK: - Get start of month
    func startOfMonth() -> Date
    {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    //MARK: - Get end of month
    func endOfMonth() -> Date
    {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
    
    //MARK: - Get month name
    func getMonthName() -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM"
        let nameOfMonth = dateFormatter.string(from: self)
        return nameOfMonth
    }
    
    //MARK: - Get short month name
    func getShortMonthName() -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM"
        let nameOfMonth = dateFormatter.string(from: self)
        return nameOfMonth
    }
    
    //MARK: -Get interval between dates
    func interval(ofComponent comp: Calendar.Component, fromDate fdate: Date, toDate tdate: Date) -> Int
    {
        
        let currentCalendar = Calendar.current
        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: fdate) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: tdate) else { return 0 }
        return end - start
    }
    
    //MARK: - Date comparision
    func dateForComparison() -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        let str = dateFormatter.string(from: self)
        let date = dateFormatter.date(from: str)
        return date!
    }
    
    func getFormattedDate(input: Date) -> String{
        let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let currentDateStr = formatter.string(from: Date())
        let currentDate = formatter.date(from: currentDateStr)
            formatter.dateFormat = "dd/MM/yyyy"
        let dateString = formatter.string(from: currentDate!)
        
        return dateString
    }
    
    //MARK: - Get only date
    func getOnlyDate(format: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let date = dateFormatter.string(from: self)
        return date
    }
    
    //MARK: -Get Date
    func getDate() -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd"
        let date = dateFormatter.string(from: self)
        return date
    }
    
    //MARK: - Get Time
    func getTime() -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        let time = dateFormatter.string(from: self)
        return time
    }
    
    //MARK: - Get Time format
    func getTimeFormat() -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "a"
        let timeFormat = dateFormatter.string(from: self)
        return timeFormat
    }
    
    //MARK: - Get Next date
    func nextDate() -> Date?
    {
        let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: self)
        return tomorrow
    }
    
    //MARK: - Get next date with int
    func nextDate(difference: Int) -> Date?
    {
        let comingDate = Calendar.current.date(byAdding: .day, value: difference, to: self)
        return comingDate
    }
    
    //MARK: - Get milliseconds
    func millisecondsSince1970() -> String
    {
        let milliseconds = Int((self.timeIntervalSince1970 * 1000.0).rounded())
        return String(milliseconds)
    }
    
    //MARK: - date suffix
    func daySuffix() -> String
    {
        let calendar = Calendar.current
        let dayOfMonth = calendar.component(.day, from: self)
        switch dayOfMonth {
        case 1, 21, 31: return "st "
        case 2, 22: return "nd "
        case 3, 23: return "rd "
        default: return "th "
        }
    }
    
    //MARK: - Get difference in days
    func dateDifferenceInDays(toDate: Date) -> Int
    {
        let mycalendar = Calendar.current
        let dateDiff = mycalendar.dateComponents([.day], from: self, to: toDate)
        var numberOfDays = 1
        if dateDiff.day! < 0
        {
            numberOfDays = dateDiff.day! - 1
        }
        else if dateDiff.day! > 0{
            numberOfDays = dateDiff.day! + 1
        }
        return numberOfDays
    }
    
    //MARK: - Start of week
    var startOfWeek: Date?
    {
        let gregorian = Calendar.current
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
           return gregorian.date(byAdding: .day, value: 0, to: sunday)
    }
    
    //MARK: - End of week
    var endOfWeek: Date?
    {
        let gregorian = Calendar.current
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
           return gregorian.date(byAdding: .day, value: 6, to: sunday)
    }
    
    //MARK: - Get event day name
    func getEventDayName(todate: Date?) -> String!
    {
        var day = ""
        if let _ = todate{
            let calendar = Calendar.current
            let _ = Date()
            let startOfThisWeek: Date = self.startOfWeekVal
            let startofNextWeek: Date = calendar.date(byAdding: .weekOfMonth, value: 1, to: startOfThisWeek)!
            let _: Date = calendar.date(byAdding: .weekOfMonth, value: 1, to: startofNextWeek)!
            
            if calendar.isDateInToday(self)
            {
                day =  "Today"
            }
            else if calendar.isDateInTomorrow(self)
            {
                day =  "Tomorrow"
            }
                
            else if self.dateDifferenceInDays(toDate: startOfThisWeek) <= 0
            {
                day =  "This Week"
            }
            else if self.dateDifferenceInDays(toDate: startofNextWeek) <= 0
            {
                day =  "Next Week"
            }
            else if self.getShortMonthName() != Date().getShortMonthName()
            {
                if self.dateForComparison().compare(Date().dateForComparison()) == .orderedAscending {
                    day = "Previous Month"
                }
                else{
                    day =  "Next Month"
                }
            }
            else{
                day =  "This Month"
            }
        }
        else
        {
            day =  ""
        }
        return day
    }
 
    //MARK: - Is greater than date
    func isGreater(than date: Date) -> Bool
    {
        return self >= date
    }
    
    //MARK: - Is smaller than date
    func isSmaller(than date: Date) -> Bool
    {
        return self < date
    }
    
    //MARK: - Is Equal to date
    func isEqual(to date: Date) -> Bool {
        return self == date
    }
    
    //MARK: - Adding minutes
    func adding(minutes: Int) -> Date
    {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
    
    func getFormattedDayDate(input: Date) -> String{
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateStr =  dateFormatterGet.string(from: input)
        
        let dateFormatterShow = DateFormatter()
        dateFormatterShow.dateStyle = .medium
        dateFormatterShow.timeStyle = .none
        dateFormatterShow.dateFormat = "EEEE, MMM dd"
        
        if let date = dateFormatterGet.date(from: dateStr) {
            return dateFormatterShow.string(from: date)
        } else {
            return ""
        }
    }
}
