//
//  GradientView.swift
//  SmokeTree
//
//  Created by Cory Bechtel on 4/22/21.
//  Copyright © 2021 Cory Bechtel. All rights reserved.
//

import UIKit

struct GradientItem {
    var color: UIColor
    var location: NSNumber
}

// swiftlint:disable force_cast

class GradientView: UIView {

    var gradientLayer: CAGradientLayer {
        return self.layer as! CAGradientLayer
    }

    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }

    struct Direction: OptionSet, Hashable {

        var hashValue: Int {
            return self.rawValue
        }

        let rawValue: Int

        static let    up = Direction(rawValue: 1 << 0)
        static let  left = Direction(rawValue: 1 << 1)
        static let right = Direction(rawValue: 1 << 2)
        static let  down = Direction(rawValue: 1 << 3)
    }

    static let pointsForDirections: [Direction:(start: CGPoint, end: CGPoint)] = [
        .up: (start: CGPoint(x: 0.5, y: 1.0), end: CGPoint(x: 0.5, y: 0.0)),
        .left: (start: CGPoint(x: 1.0, y: 0.5), end: CGPoint(x: 0.0, y: 0.5)),
        .right: (start: CGPoint(x: 0.0, y: 0.5), end: CGPoint(x: 1.0, y: 0.5)),
        .down: (start: CGPoint(x: 0.5, y: 0.0), end: CGPoint(x: 0.5, y: 1.0)),
        [.up, .right]: (start: CGPoint(x: 0.0, y: 1.0), end: CGPoint(x: 1.0, y: 0.0)),
        [.up, .left]: (start: CGPoint(x: 1.0, y: 1.0), end: CGPoint(x: 0.0, y: 0.0)),
        [.down, .right]: (start: CGPoint(x: 0.0, y: 0.0), end: CGPoint(x: 1.0, y: 1.0)),
        [.down, .left]: (start: CGPoint(x: 1.0, y: 0.0), end: CGPoint(x: 0.0, y: 1.0))]

    init(direction: GradientView.Direction, colors: [CGColor], locations: [NSNumber] = [0, 1]) {
        super.init(frame: .zero)

        guard let gradientLayer = self.layer as? CAGradientLayer else {
            return
        }
        gradientLayer.colors = colors
        gradientLayer.locations = locations

        let points = GradientView.pointsForDirections[direction]
        if let start = points?.start, let end = points?.end {
            gradientLayer.startPoint = start
            gradientLayer.endPoint = end
        }
    }

    public required init?(coder aDecoder: NSCoder) {
        fatalError("not supported")
    }

    func setColors(colors: [CGColor]) {
        guard let gradientLayer = self.layer as? CAGradientLayer else {
            return
        }
        gradientLayer.colors = colors
    }
}
