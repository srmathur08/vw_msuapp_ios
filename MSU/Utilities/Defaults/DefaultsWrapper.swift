//
//  DefaultsWrapper.swift
//  MSU
//
//  Created by vectorform on 08/06/21.
//

import Foundation

struct DefaultsWrapper {
    private static let deviceIdKey = "deviceId"
    private static let loginUserIDkey = "loginUserId"
    private static let dealerShipIDKey = "dealershipId"
    private static let usernameKey = "username"
    private static let isLoginKey = "isLoggedin"
    private static let loggedMobilekey = "loggedMobile"
    private static let roleKey = "loggedRole"
    private static let dealerShipNameKey = "dealershipName"
    private static let emailKey = "loggedEmail"
    private static let profilePicKey = "userProfilePic"
    //MARK: - Device ID
    static var deviceId: String
    {
        get {
            return UserDefaults.standard.string(forKey: deviceIdKey) ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: deviceIdKey)
        }
    }
    
    //MARK: - Login user id
    static var loginUserId: Int
    {
        get {
            return UserDefaults.standard.integer(forKey: loginUserIDkey)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: loginUserIDkey)
        }
    }

    //MARK: - Dealership id
    static var dealershipId: Int
    {
        get {
            return UserDefaults.standard.integer(forKey: dealerShipIDKey)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: dealerShipIDKey)
        }
    }
    
    //MARK: - Username
    static var username: String
    {
        get {
            return UserDefaults.standard.string(forKey: usernameKey) ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: usernameKey)
        }
    }
    
    //MARK: - is logged in
    
    static var isLoggedIn: Bool
    {
        get{
            return UserDefaults.standard.bool(forKey: isLoginKey)
        }
        set{
            UserDefaults.standard.setValue(newValue, forKey: isLoginKey)
        }
    }
    
    static func setSRCompleted(key: String, value: Bool){            UserDefaults.standard.setValue(value, forKey: key)
    }
    
    static func getSRCompleted(key: String) -> Bool{
        return   UserDefaults.standard.bool(forKey: key)
    }
    
    //MARK: - Mobile Number
    static var mobileNumber: String
    {
        get {
            return UserDefaults.standard.string(forKey: loggedMobilekey) ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: loggedMobilekey)
        }
    }
    
    //MARK: - Role
    static var loggedInRole: String
    {
        get {
            return UserDefaults.standard.string(forKey: roleKey) ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: roleKey)
        }
    }
    
    //MARK: - Dealership
    static var dealership: String
    {
        get {
            return UserDefaults.standard.string(forKey: dealerShipNameKey) ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: dealerShipNameKey)
        }
    }
    
    //MARK: - email
    static var loggedEmail: String
    {
        get {
            return UserDefaults.standard.string(forKey: emailKey) ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: emailKey)
        }
    }
    
    //MARK: - Profile Pic
    static var userProfilePic: String
    {
        get {
            return UserDefaults.standard.string(forKey: profilePicKey) ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: profilePicKey)
        }
    }
}
