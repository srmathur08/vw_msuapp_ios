//
//  ImagesDirectory.swift
//  MSU
//
//  Created by vectorform on 10/08/21.
//

import Foundation
import UIKit

class ImagesDirectory {
    func writeImageToPath(_ path:String, image:UIImage, srId: String, subSection: String) -> String {

        let fileManager = FileManager.default

        do {
            let documentDirectoryURL = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let imagesFolderURL = documentDirectoryURL.appendingPathComponent("VehicleHealthCheck/\(srId)/\(subSection)")
            let imageURL = imagesFolderURL.appendingPathComponent(path)

            let image = image// UIImage(named: "apple.jpg")

            if !fileManager.fileExists(atPath: imagesFolderURL.path){
                try fileManager.createDirectory(at: imagesFolderURL, withIntermediateDirectories: true, attributes: nil)
            } else {
                print("Already dictionary created.")
            }
            let imageData = image.jpegData(compressionQuality: 0.5)
            try imageData?.write(to: imageURL)

            print("imagePath =", imageURL.path)
            
            return imageURL.path
        } catch {
            print(error)
            return ""
        }
}
    func getFileAt(filepath: String) -> UIImage? {

        if FileManager.default.fileExists(atPath: filepath) {
            print("fileURL.path \(filepath)")

            do{
                let image = UIImage(contentsOfFile: filepath)
                return image
            }catch{
                print("error getting image")
            }
        } else {
            print("No image in directory")
        }

        return nil
    }
    
    func writeImageVehicleImages(_ path:String, image:UIImage, srId: String) -> String {

        let fileManager = FileManager.default

        do {
            let documentDirectoryURL = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let imagesFolderURL = documentDirectoryURL.appendingPathComponent("VehicleImages/\(srId)")
            let imageURL = imagesFolderURL.appendingPathComponent(path)

            let image = image// UIImage(named: "apple.jpg")

            if !fileManager.fileExists(atPath: imagesFolderURL.path){
                try fileManager.createDirectory(at: imagesFolderURL, withIntermediateDirectories: true, attributes: nil)
            } else {
                print("Already dictionary created.")
            }
            let imageData = image.jpegData(compressionQuality: 0.5)
            try imageData?.write(to: imageURL)

            print("imagePath =", imageURL.path)
            
            return imageURL.path
        } catch {
            print(error)
            return ""
        }
}
    func writeSignatureImagesToFile(_ path:String, image:UIImage, name: String) -> String {

        let fileManager = FileManager.default

        do {
            let documentDirectoryURL = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let imagesFolderURL = documentDirectoryURL.appendingPathComponent("Signatures/\(name)")
            let imageURL = imagesFolderURL.appendingPathComponent(path)

            let image = image// UIImage(named: "apple.jpg")

            if !fileManager.fileExists(atPath: imagesFolderURL.path){
                try fileManager.createDirectory(at: imagesFolderURL, withIntermediateDirectories: true, attributes: nil)
            } else {
                print("Already dictionary created.")
            }
            let imageData = image.jpegData(compressionQuality: 0.5)
            try imageData?.write(to: imageURL)

            print("imagePath =", imageURL.path)
            
            return imageURL.path
        } catch {
            print(error)
            return ""
        }
}
}
