//
//  MSUCardView.swift
//  MSU
//
//  Created by vectorform on 14/06/21.
//

import UIKit

class MSUCardView: UIView {

    
    @IBOutlet weak var cardView: CardView!
    @IBOutlet weak var vehicleImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var regNoLabel: UILabel!
    @IBOutlet weak var kmsLabel: UILabel!
    @IBOutlet weak var typeofServiceLabel: UILabel!
    @IBOutlet weak var seperatorLine: UIView!
    @IBOutlet weak var updateStatusButton: MSUButton!
    @IBOutlet weak var cardViewHeightConstraint: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
         commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
         commonInit()
//        fatalError("init(coder:) has not been implemented")
    }
    private func commonInit(){
        let viewFromXib =  Bundle.main.loadNibNamed("MSUCardView", owner: self, options: nil)![0] as! UIView
           viewFromXib.frame = self.bounds
        usernameLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: false, size: 18.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontHead)
        regNoLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        kmsLabel.applyLabelStyling(textColor: ColorConstants.appPrimaryColor, isBold: false, size: 12.0, alignment: .center, bgColor: ColorConstants.appYellowColor, fontType: StringConstants.fontText)
        kmsLabel.layer.cornerRadius = 5
        kmsLabel.layer.masksToBounds = true
        typeofServiceLabel.applyLabelStyling(textColor: ColorConstants.appLabelsTextColor, isBold: false, size: 12.0, alignment: .left, bgColor: ColorConstants.clear, fontType: StringConstants.fontText)
        seperatorLine.backgroundColor = ColorConstants.underlineColor
        updateStatusButton.appButtonStyling(titleColor: ColorConstants.appSecondaryBlueColor, backgroundColor: ColorConstants.clear)
        updateStatusButton.titleLabel?.font = Font.vwheadfont.with(size: CGFloat(14.0), weight: .vwheadregular)
        addSubview(viewFromXib)
    }

    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
