//
//  MSULabel.swift
//  MSU
//
//  Created by vectorform on 15/06/21.
//

import UIKit
import Texty

class MSULabel: TextyLabel {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    func applyLabelStylings() {
        self.backgroundColor = ColorConstants.clear
        self.textAlignment = .left
        self.numberOfLines = 0
        self.lineBreakMode = .byCharWrapping
    }
}


extension UILabel {
    func applyLabelStyling(textColor: UIColor, isBold: Bool, size: Double, alignment: NSTextAlignment, bgColor: UIColor, fontType: String) {
        self.backgroundColor = bgColor
        self.textAlignment = alignment
        self.numberOfLines = 0
//        self.lineBreakMode = .word
        self.textColor = textColor
        if(fontType == StringConstants.fontHead){
            if(isBold){
                self.font = Font.vwheadfont.with(size: CGFloat(size), weight: .vwheadbold)
            }
            else{
                self.font = Font.vwheadfont.with(size: CGFloat(size), weight: .vwheadregular)
            }
        }
        else{
            if(isBold){
                self.font = Font.vwtextfont.with(size: CGFloat(size), weight: .vwtextbold)
            }
            else{
                self.font = Font.vwtextfont.with(size: CGFloat(size), weight: .vwtextregular)
            }
               
        }
        
    }
}
