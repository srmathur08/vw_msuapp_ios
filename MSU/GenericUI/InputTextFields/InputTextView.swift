//
//  InputTextView.swift
//  MSU
//
//  Created by vectorform on 09/06/21.
//

import UIKit

class InputTextView: UIView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var inputTextfield: SkyFloatingLabelTextField!
    @IBOutlet weak var tickImageView: UIImageView!
    @IBOutlet weak var errorLabel: UILabel!
    
    @IBOutlet weak var viewWidthConstraint: NSLayoutConstraint!
    //MARK: - Mapping text field
    public func textInputView(hintText: String, isSecureTextEntry: Bool, returnKeyType: UIReturnKeyType, keyboardtype: UIKeyboardType, textColor: UIColor, tintColor: UIColor, lineColor: UIColor) {
        inputTextfield.isSecureTextEntry = isSecureTextEntry
        inputTextfield.returnKeyType = returnKeyType
        inputTextfield.autocorrectionType = .no
        inputTextfield.keyboardType = keyboardtype
        inputTextfield.title = hintText
        inputTextfield.placeholder = hintText
        inputTextfield.lineColor = lineColor
        inputTextfield.tintColor = tintColor
        inputTextfield.textColor = textColor
        inputTextfield.font = Font.vwheadfont.with(size: 16.0, weight: .vwheadregular)
    }
    //MARK: - Show Error
    public func showErrorView(message: String? = nil, errorMessage: String? = nil) {
        inputTextfield.errorMessage = message
        inputTextfield.errorColor = .red
        inputTextfield.lineErrorColor = .red
        inputTextfield.placeholderColor = .red
        tickImageView.isHidden = true
        errorLabel.isHidden = false
        errorLabel.text = errorMessage
    }

    //MARK: - Update Width
    public func updateWidthConstraint(widthVal: CGFloat){
        viewWidthConstraint.constant = widthVal
        self.updateConstraints()
    }
    //MARK: - Hide Error
    public func hideErrorView() {
        inputTextfield.errorMessage = ""
        inputTextfield.errorColor = .clear
        inputTextfield.lineErrorColor = .clear
        tickImageView.isHidden = true
        errorLabel.isHidden = true
    }
    
    //MARK: - show tick image
    public func showTickImage(){
        tickImageView.isHidden = false
    }
    
    //MARK: - hide tick image
    public func hideTickImage(){
        tickImageView.isHidden = true
    }
    
    //MARK: - show and hide text values
    public func showSecureEntry(input: Bool){
            inputTextfield.isSecureTextEntry = input
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
         commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
         commonInit()
//        fatalError("init(coder:) has not been implemented")
    }
    private func commonInit(){
        Bundle.main.loadNibNamed("InputTextView", owner: self, options: nil)
        addSubview(containerView)
        containerView.frame = self.bounds
        containerView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
    }

 
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
