//
//  LoaderView.swift
//  MSU
//
//  Created by vectorform on 25/08/21.
//

import Foundation
import UIKit

class LoaderView: UIView {

    var blurEffectView: UIVisualEffectView?

        override init(frame: CGRect) {
            let blurEffect = UIBlurEffect(style: .light)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = frame
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.blurEffectView = blurEffectView
            super.init(frame: frame)
            addSubview(blurEffectView)
            addLoader()
        }

        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }

        private func addLoader() {
            guard let blurEffectView = blurEffectView else { return }
             DispatchQueue.main.async {
                let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
                activityIndicator.color = ColorConstants.appPrimaryColor
                activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
                blurEffectView.contentView.addSubview(activityIndicator)
                activityIndicator.center = blurEffectView.contentView.center
                activityIndicator.startAnimating()
            }
        }

}
