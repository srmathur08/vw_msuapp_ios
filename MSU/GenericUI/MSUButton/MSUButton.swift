//
//  MSUButton.swift
//  MSU
//
//  Created by vectorform on 11/06/21.
//

import Foundation
import UIKit

class MSUButton: UIButton {

    override var isEnabled: Bool {
        didSet {
            if !isEnabled {
                backgroundColor = ColorConstants.appButtonDisableColor
                
            } else {
                backgroundColor = ColorConstants.appSecondaryBlueColor
            }
        }
    }

    func appButtonStyling(titleColor: UIColor, backgroundColor: UIColor) {
        self.backgroundColor = backgroundColor
        self.setTitleColor(titleColor, for: .normal)
        self.layer.cornerRadius = 25
        self.titleLabel?.font = UIFont.systemFont(ofSize: 16.0)
    }

    func circularButtonStyling(titleColor: UIColor, backgroundColor: UIColor) {
        self.backgroundColor = backgroundColor
        self.setTitleColor(titleColor, for: .normal)
        self.layer.cornerRadius = self.frame.height/2
        self.clipsToBounds = true
    }


    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
}

extension UIButton{
    func checkboxAnimation(closure: @escaping () -> Void){
        guard let image = self.imageView else {return}
        self.adjustsImageWhenHighlighted = false
        self.isHighlighted = false
        
        UIView.animate(withDuration: 0.1, delay: 0.1, options: .curveLinear, animations: {
            image.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            
        }) { (success) in
            UIView.animate(withDuration: 0.1, delay: 0, options: .curveLinear, animations: {
                self.isSelected = !self.isSelected
                //to-do
                closure()
                image.transform = .identity
            }, completion: nil)
        }
        
    }
}
