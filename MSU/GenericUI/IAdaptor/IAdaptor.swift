//
//  IAdaptor.swift
//  MSU
//
//  Created by vectorform on 16/06/21.
//

import UIKit

public protocol IAdapter {
    associatedtype DataType
    var data: [DataType] { get }
    func updateData(data: [DataType])
}

public protocol ITableViewAdapter: IAdapter, UITableViewDataSource {
    init(tableView: UITableView)
    var tableView: UITableView { get }
}

public protocol ICollectionViewAdapter: IAdapter, UICollectionViewDataSource {
    init(collectionView: UICollectionView)
    var collectionView: UICollectionView { get }
}

