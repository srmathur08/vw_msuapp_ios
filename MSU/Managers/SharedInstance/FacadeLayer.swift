//
//  FacadeLayer.swift
//  MSU
//
//  Created by vectorform on 09/06/21.
//

import Foundation

class FacadeLayer{
    
    var webserviceManager: VFServiceManager!
    var sfdcServiceManeger: SFDCServiceManager!
    private var loginDetails:LoginModel?
    private var healthCheckModel: [VehicleHealthCheckModel]?
    var isFromHealthCheck: Bool = false

    var sfdcAccessToken: String = ""
    var sfdcInstanceUrl: String = ""
    
    var vehicleCheckUpId: Int = 0
    var vehicelCheckSrId: Int = 0
    
    var fcmTokem: String = ""
    
    static let sharedInstance: FacadeLayer = {
        let instance = FacadeLayer()
        instance.webserviceManager = VFServiceManager()
        instance.sfdcServiceManeger = SFDCServiceManager()
        return instance
    }()
    
    func setLoginDetails(input:LoginModel) {
        self.loginDetails = input
    }
    func getLoginDetails() -> LoginModel {
        return self.loginDetails!
    }
    func setHealthCheckModel(input:[VehicleHealthCheckModel]) {
        self.healthCheckModel = input
    }
    func getHealthCheckModel() -> [VehicleHealthCheckModel]? {
        return self.healthCheckModel
    }
    
}
