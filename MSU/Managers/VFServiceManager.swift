//
//  ServiceManager.swift
//  MSU
//
//  Created by vectorform on 08/06/21.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import SwiftyJSON
import Reachability
import JWT

class VFServiceManager : BaseRequest{
    
    var reachability = try! Reachability()
    
    var networkAvailable: Bool = true
   
    // MARK: -  For checking Network connection
    override init() {

        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do{
          try reachability.startNotifier()
        }catch{
          print("could not start reachability notifier")
        }
    }
    
    @objc func reachabilityChanged(note: Notification) {

       let reachability = note.object as! Reachability

        switch reachability.connection {
         case .wifi:
             print("Reachable via WiFi")
            self.networkAvailable = true
         case .cellular:
             print("Reachable via Cellular")
            self.networkAvailable = true
         case .none:
             print("Network not reachable")
            self.networkAvailable = false
        case .unavailable:
            self.networkAvailable = false
            print("Network unavailable")
        }
        if(reachability.connection == .wifi || reachability.connection == .cellular){
            NotificationCenter.default.post(name: Notification.Name("NetworkPresent"), object: nil)
        }
    }
    // MARK: -  GenerateToken
  func generateToken() -> String
  {
     var claims = ClaimSet()
         claims.issuer = StringConstants.JWTIssuerKey
         claims.issuedAt = Date()
         claims.expiration = Date().adding(minutes: 2)
      // claims["custom"] = "Hi"
     let generateToken =  JWT.encode(claims: claims, algorithm: .hs256(Data(base64Encoded: StringConstants.JWTSecretkey)!))
        return  generateToken
    }
    
    
    // MARK: -  Create Post Request
   func createPOSTRequest(requestParams params: [String : AnyObject],url:String, completion:@escaping (_ finished: Bool, _ responseVal: AnyObject?) ->Void) {
       if(!self.networkAvailable){
        completion(false, ErrorStringConstants.noInternet as AnyObject)
           return
       }
    
        let request = baseUrlString + "\(url)"
        let requestUrl = URL(string:request)!
        let jwtToken = self.generateToken()
        let headers: HTTPHeaders = [
               "Authorization": "Bearer \(jwtToken)",
               "Content-Type": "application/json",
           ]
           #if DEBUG
              print(requestUrl)
           #endif
           var encrypStr = String()
           do {
               let data1 =  try JSONSerialization.data(withJSONObject: params, options:[])
               let convertedString = String(data: data1, encoding: .utf8)
               let finalConvertedString = convertedString?.replacingOccurrences(of: "’", with: "'").replacingOccurrences(of: "`", with: "'").replacingOccurrences(of: "‘", with: "'")
                   encrypStr = Crypt.encrypt(finalConvertedString, withKey: StringConstants.encryptionKey)
                   #if DEBUG
//                       print(encrypStr as Any)
                   #endif
               } catch let myJSONError {
                   #if DEBUG
                       print(myJSONError)
                    #endif
               }
           let postString: [String : String]  = ["Data":encrypStr]
           Alamofire.request(requestUrl, method: .post, parameters: postString, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<300).responseJSON{ response in
             switch response.result {
               case .success :
                   if let resVal = response.result.value {
                       let resultVal = JSON(resVal)
                       let dataVal = resultVal["data"].rawString()
                       let decreptVal = Crypt.decrypt(dataVal, withKey: StringConstants.encryptionKey)
                       #if DEBUG
   //                        print(resultVal)
                        #endif
                            completion(true, decreptVal as AnyObject?)
                       
                   } else {
                       completion(false, response.result.error as AnyObject?)
                   }
               case .failure :
                   completion(false, response.result.error as AnyObject?)
               }
           }
           
       }
    // MARK: -  Group Management API Calls
  func genericAPICalls(requestParams params: [String : AnyObject], apiPath: String, completion:@escaping (_ finished: Bool, _ responseVal: AnyObject?) ->Void){
           self.createPOSTRequest(requestParams: params, url: apiPath) { (status, response) in
           if(status){
               let resultJSON = JSON.init(parseJSON:response as? String ?? "")
               let resultType = resultJSON["Result"].rawString()
               let reasonString = resultJSON["Reason"].rawString()
               if(resultType?.caseInsensitiveCompare("success") == .orderedSame){
                    completion(true, resultJSON as AnyObject)
                 }
                else{
                    if(reasonString?.caseInsensitiveCompare("UNAUTHORIZED") == .orderedSame){
//                        self.clearSessionAndLogout()
                    }
                    else{
                        completion(false, resultJSON as AnyObject)
                    }
                }
              }
               else{
                   completion(false, response)
               }
             }
         }
    
    // MARK: -  Create Post Req Without Params
     func createPOSTRequestWithoutParam(url:String, completion:@escaping (_ finished: Bool,_ responseVal: AnyObject?) ->Void) {
        if(!self.networkAvailable){
            completion(false, "No Internet" as AnyObject)
            return
        }
            let request = baseUrlString + "\(url)"
           let requestUrl = URL(string:request)!
           let jwtToken = self.generateToken()
           let headers: HTTPHeaders = [
               "Authorization": "Bearer \(jwtToken)",
               "Content-Type": "application/json"
           ]
               #if DEBUG
                  print(requestUrl)
               #endif
            let encrypStr = Crypt.encrypt("", withKey: StringConstants.encryptionKey)
            let postString: [String : String]  = ["Data":encrypStr!]
           Alamofire.request(requestUrl, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<300).responseJSON{ response in
               
             switch response.result {
               case .success :
                   if let resVal = response.result.value {
                       let resultVal = JSON(resVal)
                       let dataVal = resultVal["data"].rawString()
                       let decreptVal = Crypt.decrypt(dataVal, withKey: StringConstants.encryptionKey)
                       #if DEBUG
    //                      //  print(resultVal)
                        #endif
                            completion(true, decreptVal as AnyObject?)
                       
                   } else {
                       completion(false, response.result.error as AnyObject?)
                   }
               case .failure :
                   completion(false, response.result.error as AnyObject?)
               }
           }
           
        }
    // MARK: -  API calls withoutParams
  func genericAPICallsWithoutParam(requestParams params: [String : AnyObject], apiPath: String, completion:@escaping (_ finished: Bool, _ responseVal: AnyObject?) ->Void){
          self.createPOSTRequestWithoutParam(url: apiPath) { (status, response) in
          if(status){
              let resultJSON = JSON.init(parseJSON:response as? String ?? "")
              let resultType = resultJSON["Result"].rawString()
              if(resultType?.caseInsensitiveCompare("success") == .orderedSame){
                  completion(true, resultJSON as AnyObject)
                  }
                  else{
                      completion(false, resultJSON as AnyObject)
                  }
              }
              else{
                    completion(false, response)
                  }
          }
  }
}
