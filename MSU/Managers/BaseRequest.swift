//
//  BaseRequest.swift
//  MSU
//
//  Created by vectorform on 08/06/21.
//

import Foundation


class BaseRequest: NSObject{

//    let baseUrlString = "http://vwfs-qa.vectorform.com/vwassistapp/" //  QA
//    let baseUrlString = "http://pos.skodalive.in/vwmsu-dev/" // new QA
//    let baseUrlString = "http://pos.skodalive.in/vwmsu-pilot/" // Pilot
//    let baseUrlString = "http://20.101.69.125/" // Ip Prod
    let baseUrlString =  "https://vwassistance.com/" // Production - Live
     var apiRequest:NSMutableURLRequest = NSMutableURLRequest()
    
    override init() {
      super.init()
        let baseUrl = URL(string:baseUrlString)!
            self.apiRequest = NSMutableURLRequest(url:baseUrl)
    }
}
