//
//  SFDCServiceManager.swift
//  MSU
//
//  Created by vectorform on 21/08/21.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import SwiftyJSON
import Reachability
import JWT
import FirebaseAnalytics

class SFDCServiceManager:NSObject{
    
    var reachability = try! Reachability()
    
    var networkAvailable: Bool = true
    // MARK: -  For checking Network connection
    override init() {

        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do{
          try reachability.startNotifier()
        }catch{
          print("could not start reachability notifier")
        }
    }
    
    @objc func reachabilityChanged(note: Notification) {

       let reachability = note.object as! Reachability

        switch reachability.connection {
         case .wifi:
             print("Reachable via WiFi")
            self.networkAvailable = true
         case .cellular:
             print("Reachable via Cellular")
            self.networkAvailable = true
         case .none:
             print("Network not reachable")
            self.networkAvailable = false
        case .unavailable:
            self.networkAvailable = false
            print("Network unavailable")
        }
        if(reachability.connection == .wifi || reachability.connection == .cellular){
            NotificationCenter.default.post(name: Notification.Name("NetworkPresent"), object: nil)
        }
    }
    
    // MARK: - Create Sales Force Token
  func createSalesForceTokenForPostCalls(url:String, completion:@escaping (_ finished: Bool,_ responseVal: AnyObject?) ->Void) {
     if(!self.networkAvailable){
            completion(false, "No Internet" as AnyObject)
            return
        }
        let requestUrl = URL(string:url)!
        let headers: HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        #if DEBUG
            print(requestUrl)
        #endif
        let postString: [String : String]  = ["grant_type":SFDCCalls.grant_type,
                                          "client_id":SFDCCalls.client_id,
                                          "client_secret":SFDCCalls.client_secret,
                                          "username":SFDCCalls.username,
                                          "password":SFDCCalls.password]

        Alamofire.request(requestUrl, method: .post, parameters: postString, encoding:URLEncoding.httpBody , headers: headers).validate(statusCode: 200..<300).responseJSON{ response in
             switch response.result {
               case .success :
                   if let resVal = response.result.value {
                       let resultVal = JSON(resVal)
                        completion(true, resultVal as AnyObject?)
                   } else {
                       completion(false, response.result.error as AnyObject?)
                   }
               case .failure :
                   completion(false, response.result.error as AnyObject?)
               }
           }
        }
    
    // MARK: - Get SalesForce Token
    func createSalesForceToken( url: String,completion:@escaping (_ finished: Bool, _ responseVal: AnyObject?) ->Void){
       self.createSalesForceTokenForPostCalls(url: url) { (status, response) in
          if(status){
            if(response != nil){
                completion(true, response as AnyObject)
              }
             else{
                completion(false, response as AnyObject)
                }
             }
             else{
                completion(false, response)
            }
        }
    }
    // MARK: -  Create Get Request for SFDC calls
  func createGETRequestForMoparSalesForce(url:String,accesstoken:String, completion:@escaping (_ finished: Bool,_ responseVal: AnyObject?) ->Void) {
        if(!self.networkAvailable){
            completion(false, "No Internet" as AnyObject)
            return
        }

        let requestUrl = URL(string:url)!
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(accesstoken)",
            "Content-Type": "application/json"
        ]
           #if DEBUG
              print(requestUrl)
           #endif

            Alamofire.request(requestUrl, method: .get, headers: headers).validate(statusCode: 200..<300).responseJSON{ response in
                   
                switch response.result {
                 case .success :
                   if let resVal = response.result.value {
                      let resultVal = JSON(resVal)
                        #if DEBUG
                            print(resultVal)
                        #endif
                        completion(true, resultVal as AnyObject?)
                     } else {
                         completion(false, response.result.error as AnyObject?)
                     }
                 case .failure :
                     completion(false, response.result.error as AnyObject?)
                 }
               }
        }
    
    // MARK: - Get Calls for SFDC
    func getSFDCGenericAPICalls( url: String,accesstoken: String, completion:@escaping (_ finished: Bool, _ responseVal: AnyObject?) ->Void){
        self.createGETRequestForMoparSalesForce(url: url, accesstoken: accesstoken, completion: { (status, response) in
          if(status){
            if(response != nil){
                completion(true, response as AnyObject)
              }
             else{
                  completion(false, response as AnyObject)
              }
            }
            else{
                completion(false, response)
            }
        })
    }
}
